# Downloads #

### Design Assistant Plugin for Eclipse ###

* [Download v1.0.1](https://bitbucket.org/design-assistant/tesis-projects/downloads/tesis-design-assistant-plugin-1.0.1.jar)

##### Requirements #####

* Eclipse IDE ([download](https://eclipse.org/downloads/))
* AmaterasUML plugin ([download](http://sourceforge.jp/projects/amateras/downloads/56447/AmaterasUML_1.3.4.zip/))

##### Installation #####

* Just put jar files into ECLIPSE_HOME/plugins

##### How to use #####

* Go to menu *Window* > *Show View* > *Other*, expand folder *Design Assistant* and choose *Design Assistant*
* Set your assistant server URL in settings panel
* Open an AmaterasUML class diagram in editor and then click on *Get Assistance* button

### Design Assistant Server ###

* [Download v1.0.1](https://bitbucket.org/design-assistant/tesis-projects/downloads/tesis-design-assistant-webapp-1.0.1.war)

##### Requirements #####

* Apache Tomcat 7 ([download](http://tomcat.apache.org/download-70.cgi))
* MySQL database ([download](http://dev.mysql.com/downloads/mysql/))

##### Installation #####

* Setup DB connection configuration at
    - */WEB-INF/classes/assistant.properties*
* Create a DB in MySQL, the schema will be automatically built on the first run of the application

# Assistant Server running at Openshift #

Set the following URL in your Assistant Plugin settings:

* http://designassistant-exaunicen.rhcloud.com

# Development #

##### Requirements #####

* Java JDK 1.7 (or greater)
* A Java IDE
* Maven integration for your IDE
* MySQL database

Eclipse IDE was used to build the Eclipse Plugin for AmaterasUML assistance. In this case you'll also need:

* AmaterasUML plugin ([download](http://sourceforge.jp/projects/amateras/downloads/56447/AmaterasUML_1.3.4.zip/))

##### MySQL configuration #####

* All you need is a DB with the name *design_assistant_db*, the application will create the schema on the first run

##### Import Maven projects into Eclipse #####

* Go to menu *File* > *Import...*, expand folder *Maven* and choose *Existing Maven Projects*
* Select **tesis-projects** directory to import all projects together
* Install suggested plugins (if prompted)
* After restarting Eclipse select all projects and press *Alt + F5* to update Maven configuration

##### Debug server with Maven goal #####

* Setup DB connection configuration at
    - *tesis-design-assistant-webapp/profiles/dev/assistant.properties*
* Right click on **tesis-design-assistant-webapp** project, go to *Debug As* and choose *Maven build...*
* Type **tomcat7:run** in *Goals* field and then start debugging
* The server will be available at *http://localhost:8080/tesis-design-assistant-webapp/*

##### Debug Eclipse plugin #####

* Right click on **tesis-design-assistant-plugin** project, go to *Debug As* and choose *Eclipse Application*