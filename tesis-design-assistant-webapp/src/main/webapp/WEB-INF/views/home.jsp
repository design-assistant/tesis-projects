<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE HTML>
<html>

<head>
<meta charset="UTF-8" />
<title>Design Assistant</title>
<script type="text/javascript" src="js/jquery-2.0.3.min.js"></script>
<script type="text/javascript" src="js/tesis.js"></script>
</head>

<body>
	<h1>Design Assistant Home</h1>
	<hr />
	<section>
		<h3>Amateras Assistance</h3>
		<table>
		<tr>
  		<td>
		    <input type="radio" name="matching" value="0" id="all" /><label for="all"> All</label><br />
		    <input type="radio" name="matching" value="1" id="any" checked="checked" /><label for="any"> Any</label><br />
		</td>
  		<td>
		    <input type="checkbox" name="qualityAttributes[]" value="flexibilidad" id="flexibility" /><label for="flexibility"> Flexibility</label><br />
		    <input type="checkbox" name="qualityAttributes[]" value="adaptabilidad" id="adaptability" /><label for="adaptability"> Adaptability</label><br />
		    <input type="checkbox" name="qualityAttributes[]" value="performance" id="performance" /><label for="performance"> Performance</label><br />
		    <input type="checkbox" name="qualityAttributes[]" value="usabilidad" id="usability" /><label for="usability"> Usability</label><br />
		    <input type="checkbox" name="qualityAttributes[]" value="seguridad" id="security" /><label for="security"> Security</label><br />
		    <input type="checkbox" name="qualityAttributes[]" value="confiabilidad" id="reliability" /><label for="reliability"> Reliability</label><br />
		    <input type="checkbox" name="qualityAttributes[]" value="modificabilidad" id="modifiability" /><label for="modifiability"> Modifiability</label><br />
		</td>
		<td>
		<textarea cols="60" name="userDesign" rows="8" id="amateras-design">&lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot;?&gt;
&lt;net.java.amateras.uml.model.RootModel&gt;
  &lt;backgroundColor&gt;
    &lt;red&gt;255&lt;/red&gt;
    &lt;green&gt;255&lt;/green&gt;
    &lt;blue&gt;206&lt;/blue&gt;
  &lt;/backgroundColor&gt;
  &lt;foregroundColor&gt;
    &lt;red&gt;0&lt;/red&gt;
    &lt;green&gt;0&lt;/green&gt;
    &lt;blue&gt;0&lt;/blue&gt;
  &lt;/foregroundColor&gt;
  &lt;showIcon&gt;true&lt;/showIcon&gt;
  &lt;listeners serialization=&quot;custom&quot;&gt;
    &lt;java.beans.PropertyChangeSupport&gt;
      &lt;default&gt;
        &lt;source class=&quot;net.java.amateras.uml.model.RootModel&quot; reference=&quot;../../../..&quot;/&gt;
        &lt;propertyChangeSupportSerializedDataVersion&gt;2&lt;/propertyChangeSupportSerializedDataVersion&gt;
      &lt;/default&gt;
      &lt;null/&gt;
    &lt;/java.beans.PropertyChangeSupport&gt;
  &lt;/listeners&gt;
  &lt;sourceConnections/&gt;
  &lt;targetConnections/&gt;
  &lt;children&gt;
    &lt;net.java.amateras.uml.classdiagram.model.ClassModel&gt;
      &lt;backgroundColor&gt;
        &lt;red&gt;255&lt;/red&gt;
        &lt;green&gt;255&lt;/green&gt;
        &lt;blue&gt;206&lt;/blue&gt;
      &lt;/backgroundColor&gt;
      &lt;foregroundColor&gt;
        &lt;red&gt;0&lt;/red&gt;
        &lt;green&gt;0&lt;/green&gt;
        &lt;blue&gt;0&lt;/blue&gt;
      &lt;/foregroundColor&gt;
      &lt;showIcon&gt;true&lt;/showIcon&gt;
      &lt;parent class=&quot;net.java.amateras.uml.model.RootModel&quot;&gt;
        &lt;backgroundColor reference=&quot;../../backgroundColor&quot;/&gt;
        &lt;foregroundColor reference=&quot;../../foregroundColor&quot;/&gt;
        &lt;showIcon&gt;true&lt;/showIcon&gt;
        &lt;listeners serialization=&quot;custom&quot;&gt;
          &lt;java.beans.PropertyChangeSupport&gt;
            &lt;default&gt;
              &lt;source class=&quot;net.java.amateras.uml.model.RootModel&quot; reference=&quot;../../../..&quot;/&gt;
              &lt;propertyChangeSupportSerializedDataVersion&gt;2&lt;/propertyChangeSupportSerializedDataVersion&gt;
            &lt;/default&gt;
            &lt;null/&gt;
          &lt;/java.beans.PropertyChangeSupport&gt;
        &lt;/listeners&gt;
        &lt;sourceConnections/&gt;
        &lt;targetConnections/&gt;
        &lt;children&gt;
          &lt;net.java.amateras.uml.classdiagram.model.ClassModel reference=&quot;../../..&quot;/&gt;
          &lt;net.java.amateras.uml.classdiagram.model.ClassModel&gt;
            &lt;backgroundColor reference=&quot;../../../../backgroundColor&quot;/&gt;
            &lt;foregroundColor reference=&quot;../../../../foregroundColor&quot;/&gt;
            &lt;showIcon&gt;true&lt;/showIcon&gt;
            &lt;parent class=&quot;net.java.amateras.uml.model.RootModel&quot; reference=&quot;../../..&quot;/&gt;
            &lt;listeners serialization=&quot;custom&quot;&gt;
              &lt;java.beans.PropertyChangeSupport&gt;
                &lt;default&gt;
                  &lt;source class=&quot;net.java.amateras.uml.classdiagram.model.ClassModel&quot; reference=&quot;../../../..&quot;/&gt;
                  &lt;propertyChangeSupportSerializedDataVersion&gt;2&lt;/propertyChangeSupportSerializedDataVersion&gt;
                &lt;/default&gt;
                &lt;null/&gt;
              &lt;/java.beans.PropertyChangeSupport&gt;
            &lt;/listeners&gt;
            &lt;constraint&gt;
              &lt;height&gt;-1&lt;/height&gt;
              &lt;width&gt;-1&lt;/width&gt;
              &lt;x&gt;341&lt;/x&gt;
              &lt;y&gt;247&lt;/y&gt;
            &lt;/constraint&gt;
            &lt;sourceConnections&gt;
              &lt;net.java.amateras.uml.classdiagram.model.DependencyModel&gt;
                &lt;showIcon&gt;true&lt;/showIcon&gt;
                &lt;listeners serialization=&quot;custom&quot;&gt;
                  &lt;java.beans.PropertyChangeSupport&gt;
                    &lt;default&gt;
                      &lt;source class=&quot;net.java.amateras.uml.classdiagram.model.DependencyModel&quot; reference=&quot;../../../..&quot;/&gt;
                      &lt;propertyChangeSupportSerializedDataVersion&gt;2&lt;/propertyChangeSupportSerializedDataVersion&gt;
                    &lt;/default&gt;
                    &lt;null/&gt;
                  &lt;/java.beans.PropertyChangeSupport&gt;
                &lt;/listeners&gt;
                &lt;source class=&quot;net.java.amateras.uml.classdiagram.model.ClassModel&quot; reference=&quot;../../..&quot;/&gt;
                &lt;target class=&quot;net.java.amateras.uml.classdiagram.model.ClassModel&quot; reference=&quot;../../../../../..&quot;/&gt;
                &lt;bendpoints/&gt;
                &lt;stereoType&gt;&lt;/stereoType&gt;
              &lt;/net.java.amateras.uml.classdiagram.model.DependencyModel&gt;
            &lt;/sourceConnections&gt;
            &lt;targetConnections/&gt;
            &lt;children/&gt;
            &lt;filterProperty/&gt;
            &lt;propertyDescriptors&gt;
              &lt;org.eclipse.ui.views.properties.TextPropertyDescriptor&gt;
                &lt;id class=&quot;string&quot;&gt;_stereo_type&lt;/id&gt;
                &lt;display&gt;Stereo Type&lt;/display&gt;
                &lt;incompatible&gt;false&lt;/incompatible&gt;
              &lt;/org.eclipse.ui.views.properties.TextPropertyDescriptor&gt;
              &lt;org.eclipse.ui.views.properties.PropertyDescriptor&gt;
                &lt;id class=&quot;string&quot;&gt;_simpleEntityName&lt;/id&gt;
                &lt;display&gt;Simple Name&lt;/display&gt;
                &lt;incompatible&gt;false&lt;/incompatible&gt;
              &lt;/org.eclipse.ui.views.properties.PropertyDescriptor&gt;
              &lt;org.eclipse.ui.views.properties.TextPropertyDescriptor&gt;
                &lt;id class=&quot;string&quot;&gt;_entityName&lt;/id&gt;
                &lt;display&gt;Name&lt;/display&gt;
                &lt;incompatible&gt;false&lt;/incompatible&gt;
              &lt;/org.eclipse.ui.views.properties.TextPropertyDescriptor&gt;
              &lt;org.eclipse.ui.views.properties.ColorPropertyDescriptor&gt;
                &lt;id class=&quot;string&quot;&gt;_background&lt;/id&gt;
                &lt;display&gt;Background Color&lt;/display&gt;
                &lt;incompatible&gt;false&lt;/incompatible&gt;
              &lt;/org.eclipse.ui.views.properties.ColorPropertyDescriptor&gt;
              &lt;org.eclipse.ui.views.properties.PropertyDescriptor&gt;
                &lt;id class=&quot;string&quot;&gt;_attrs&lt;/id&gt;
                &lt;display&gt;Attributes...&lt;/display&gt;
                &lt;incompatible&gt;false&lt;/incompatible&gt;
              &lt;/org.eclipse.ui.views.properties.PropertyDescriptor&gt;
              &lt;org.eclipse.ui.views.properties.PropertyDescriptor&gt;
                &lt;id class=&quot;string&quot;&gt;_operations&lt;/id&gt;
                &lt;display&gt;Operations...&lt;/display&gt;
                &lt;incompatible&gt;false&lt;/incompatible&gt;
              &lt;/org.eclipse.ui.views.properties.PropertyDescriptor&gt;
              &lt;net.java.amateras.uml.properties.BooleanPropertyDescriptor&gt;
                &lt;id class=&quot;string&quot;&gt;_abstract&lt;/id&gt;
                &lt;display&gt;abstract&lt;/display&gt;
                &lt;incompatible&gt;false&lt;/incompatible&gt;
              &lt;/net.java.amateras.uml.properties.BooleanPropertyDescriptor&gt;
            &lt;/propertyDescriptors&gt;
            &lt;stereoType&gt;&lt;/stereoType&gt;
            &lt;name&gt;SmallerA&lt;/name&gt;
            &lt;isAbstract&gt;false&lt;/isAbstract&gt;
          &lt;/net.java.amateras.uml.classdiagram.model.ClassModel&gt;
          &lt;net.java.amateras.uml.classdiagram.model.ClassModel&gt;
            &lt;backgroundColor reference=&quot;../../../../backgroundColor&quot;/&gt;
            &lt;foregroundColor reference=&quot;../../../../foregroundColor&quot;/&gt;
            &lt;showIcon&gt;true&lt;/showIcon&gt;
            &lt;parent class=&quot;net.java.amateras.uml.model.RootModel&quot; reference=&quot;../../..&quot;/&gt;
            &lt;listeners serialization=&quot;custom&quot;&gt;
              &lt;java.beans.PropertyChangeSupport&gt;
                &lt;default&gt;
                  &lt;source class=&quot;net.java.amateras.uml.classdiagram.model.ClassModel&quot; reference=&quot;../../../..&quot;/&gt;
                  &lt;propertyChangeSupportSerializedDataVersion&gt;2&lt;/propertyChangeSupportSerializedDataVersion&gt;
                &lt;/default&gt;
                &lt;null/&gt;
              &lt;/java.beans.PropertyChangeSupport&gt;
            &lt;/listeners&gt;
            &lt;constraint&gt;
              &lt;height&gt;-1&lt;/height&gt;
              &lt;width&gt;-1&lt;/width&gt;
              &lt;x&gt;555&lt;/x&gt;
              &lt;y&gt;116&lt;/y&gt;
            &lt;/constraint&gt;
            &lt;sourceConnections&gt;
              &lt;net.java.amateras.uml.classdiagram.model.AssociationModel&gt;
                &lt;showIcon&gt;true&lt;/showIcon&gt;
                &lt;listeners serialization=&quot;custom&quot;&gt;
                  &lt;java.beans.PropertyChangeSupport&gt;
                    &lt;default&gt;
                      &lt;source class=&quot;net.java.amateras.uml.classdiagram.model.AssociationModel&quot; reference=&quot;../../../..&quot;/&gt;
                      &lt;propertyChangeSupportSerializedDataVersion&gt;2&lt;/propertyChangeSupportSerializedDataVersion&gt;
                    &lt;/default&gt;
                    &lt;null/&gt;
                  &lt;/java.beans.PropertyChangeSupport&gt;
                &lt;/listeners&gt;
                &lt;source class=&quot;net.java.amateras.uml.classdiagram.model.ClassModel&quot; reference=&quot;../../..&quot;/&gt;
                &lt;target class=&quot;net.java.amateras.uml.classdiagram.model.ClassModel&quot; reference=&quot;../../../../../..&quot;/&gt;
                &lt;bendpoints/&gt;
                &lt;stereoType&gt;&lt;/stereoType&gt;
                &lt;fromMultiplicity&gt;&lt;/fromMultiplicity&gt;
                &lt;toMultiplicity&gt;&lt;/toMultiplicity&gt;
              &lt;/net.java.amateras.uml.classdiagram.model.AssociationModel&gt;
            &lt;/sourceConnections&gt;
            &lt;targetConnections/&gt;
            &lt;children/&gt;
            &lt;filterProperty/&gt;
            &lt;propertyDescriptors&gt;
              &lt;org.eclipse.ui.views.properties.TextPropertyDescriptor&gt;
                &lt;id class=&quot;string&quot;&gt;_stereo_type&lt;/id&gt;
                &lt;display&gt;Stereo Type&lt;/display&gt;
                &lt;incompatible&gt;false&lt;/incompatible&gt;
              &lt;/org.eclipse.ui.views.properties.TextPropertyDescriptor&gt;
              &lt;org.eclipse.ui.views.properties.PropertyDescriptor&gt;
                &lt;id class=&quot;string&quot;&gt;_simpleEntityName&lt;/id&gt;
                &lt;display&gt;Simple Name&lt;/display&gt;
                &lt;incompatible&gt;false&lt;/incompatible&gt;
              &lt;/org.eclipse.ui.views.properties.PropertyDescriptor&gt;
              &lt;org.eclipse.ui.views.properties.TextPropertyDescriptor&gt;
                &lt;id class=&quot;string&quot;&gt;_entityName&lt;/id&gt;
                &lt;display&gt;Name&lt;/display&gt;
                &lt;incompatible&gt;false&lt;/incompatible&gt;
              &lt;/org.eclipse.ui.views.properties.TextPropertyDescriptor&gt;
              &lt;org.eclipse.ui.views.properties.ColorPropertyDescriptor&gt;
                &lt;id class=&quot;string&quot;&gt;_background&lt;/id&gt;
                &lt;display&gt;Background Color&lt;/display&gt;
                &lt;incompatible&gt;false&lt;/incompatible&gt;
              &lt;/org.eclipse.ui.views.properties.ColorPropertyDescriptor&gt;
              &lt;org.eclipse.ui.views.properties.PropertyDescriptor&gt;
                &lt;id class=&quot;string&quot;&gt;_attrs&lt;/id&gt;
                &lt;display&gt;Attributes...&lt;/display&gt;
                &lt;incompatible&gt;false&lt;/incompatible&gt;
              &lt;/org.eclipse.ui.views.properties.PropertyDescriptor&gt;
              &lt;org.eclipse.ui.views.properties.PropertyDescriptor&gt;
                &lt;id class=&quot;string&quot;&gt;_operations&lt;/id&gt;
                &lt;display&gt;Operations...&lt;/display&gt;
                &lt;incompatible&gt;false&lt;/incompatible&gt;
              &lt;/org.eclipse.ui.views.properties.PropertyDescriptor&gt;
              &lt;net.java.amateras.uml.properties.BooleanPropertyDescriptor&gt;
                &lt;id class=&quot;string&quot;&gt;_abstract&lt;/id&gt;
                &lt;display&gt;abstract&lt;/display&gt;
                &lt;incompatible&gt;false&lt;/incompatible&gt;
              &lt;/net.java.amateras.uml.properties.BooleanPropertyDescriptor&gt;
            &lt;/propertyDescriptors&gt;
            &lt;stereoType&gt;&lt;/stereoType&gt;
            &lt;name&gt;SmallerB&lt;/name&gt;
            &lt;isAbstract&gt;false&lt;/isAbstract&gt;
          &lt;/net.java.amateras.uml.classdiagram.model.ClassModel&gt;
          &lt;net.java.amateras.uml.classdiagram.model.ClassModel&gt;
            &lt;backgroundColor&gt;
              &lt;red&gt;255&lt;/red&gt;
              &lt;green&gt;255&lt;/green&gt;
              &lt;blue&gt;206&lt;/blue&gt;
            &lt;/backgroundColor&gt;
            &lt;foregroundColor&gt;
              &lt;red&gt;0&lt;/red&gt;
              &lt;green&gt;0&lt;/green&gt;
              &lt;blue&gt;0&lt;/blue&gt;
            &lt;/foregroundColor&gt;
            &lt;showIcon&gt;true&lt;/showIcon&gt;
            &lt;parent class=&quot;net.java.amateras.uml.model.RootModel&quot; reference=&quot;../../..&quot;/&gt;
            &lt;listeners serialization=&quot;custom&quot;&gt;
              &lt;java.beans.PropertyChangeSupport&gt;
                &lt;default&gt;
                  &lt;source class=&quot;net.java.amateras.uml.classdiagram.model.ClassModel&quot; reference=&quot;../../../..&quot;/&gt;
                  &lt;propertyChangeSupportSerializedDataVersion&gt;2&lt;/propertyChangeSupportSerializedDataVersion&gt;
                &lt;/default&gt;
                &lt;null/&gt;
              &lt;/java.beans.PropertyChangeSupport&gt;
            &lt;/listeners&gt;
            &lt;constraint&gt;
              &lt;height&gt;-1&lt;/height&gt;
              &lt;width&gt;-1&lt;/width&gt;
              &lt;x&gt;46&lt;/x&gt;
              &lt;y&gt;264&lt;/y&gt;
            &lt;/constraint&gt;
            &lt;sourceConnections&gt;
              &lt;net.java.amateras.uml.classdiagram.model.AssociationModel&gt;
                &lt;showIcon&gt;true&lt;/showIcon&gt;
                &lt;listeners serialization=&quot;custom&quot;&gt;
                  &lt;java.beans.PropertyChangeSupport&gt;
                    &lt;default&gt;
                      &lt;source class=&quot;net.java.amateras.uml.classdiagram.model.AssociationModel&quot; reference=&quot;../../../..&quot;/&gt;
                      &lt;propertyChangeSupportSerializedDataVersion&gt;2&lt;/propertyChangeSupportSerializedDataVersion&gt;
                    &lt;/default&gt;
                    &lt;null/&gt;
                  &lt;/java.beans.PropertyChangeSupport&gt;
                &lt;/listeners&gt;
                &lt;source class=&quot;net.java.amateras.uml.classdiagram.model.ClassModel&quot; reference=&quot;../../..&quot;/&gt;
                &lt;target class=&quot;net.java.amateras.uml.classdiagram.model.ClassModel&quot; reference=&quot;../../../../../..&quot;/&gt;
                &lt;bendpoints/&gt;
                &lt;stereoType&gt;&lt;/stereoType&gt;
                &lt;fromMultiplicity&gt;&lt;/fromMultiplicity&gt;
                &lt;toMultiplicity&gt;&lt;/toMultiplicity&gt;
              &lt;/net.java.amateras.uml.classdiagram.model.AssociationModel&gt;
            &lt;/sourceConnections&gt;
            &lt;targetConnections/&gt;
            &lt;children/&gt;
            &lt;filterProperty/&gt;
            &lt;propertyDescriptors&gt;
              &lt;org.eclipse.ui.views.properties.TextPropertyDescriptor&gt;
                &lt;id class=&quot;string&quot;&gt;_stereo_type&lt;/id&gt;
                &lt;display&gt;Stereo Type&lt;/display&gt;
                &lt;incompatible&gt;false&lt;/incompatible&gt;
              &lt;/org.eclipse.ui.views.properties.TextPropertyDescriptor&gt;
              &lt;org.eclipse.ui.views.properties.PropertyDescriptor&gt;
                &lt;id class=&quot;string&quot;&gt;_simpleEntityName&lt;/id&gt;
                &lt;display&gt;Simple Name&lt;/display&gt;
                &lt;incompatible&gt;false&lt;/incompatible&gt;
              &lt;/org.eclipse.ui.views.properties.PropertyDescriptor&gt;
              &lt;org.eclipse.ui.views.properties.TextPropertyDescriptor&gt;
                &lt;id class=&quot;string&quot;&gt;_entityName&lt;/id&gt;
                &lt;display&gt;Name&lt;/display&gt;
                &lt;incompatible&gt;false&lt;/incompatible&gt;
              &lt;/org.eclipse.ui.views.properties.TextPropertyDescriptor&gt;
              &lt;org.eclipse.ui.views.properties.ColorPropertyDescriptor&gt;
                &lt;id class=&quot;string&quot;&gt;_background&lt;/id&gt;
                &lt;display&gt;Background Color&lt;/display&gt;
                &lt;incompatible&gt;false&lt;/incompatible&gt;
              &lt;/org.eclipse.ui.views.properties.ColorPropertyDescriptor&gt;
              &lt;org.eclipse.ui.views.properties.PropertyDescriptor&gt;
                &lt;id class=&quot;string&quot;&gt;_attrs&lt;/id&gt;
                &lt;display&gt;Attributes...&lt;/display&gt;
                &lt;incompatible&gt;false&lt;/incompatible&gt;
              &lt;/org.eclipse.ui.views.properties.PropertyDescriptor&gt;
              &lt;org.eclipse.ui.views.properties.PropertyDescriptor&gt;
                &lt;id class=&quot;string&quot;&gt;_operations&lt;/id&gt;
                &lt;display&gt;Operations...&lt;/display&gt;
                &lt;incompatible&gt;false&lt;/incompatible&gt;
              &lt;/org.eclipse.ui.views.properties.PropertyDescriptor&gt;
              &lt;net.java.amateras.uml.properties.BooleanPropertyDescriptor&gt;
                &lt;id class=&quot;string&quot;&gt;_abstract&lt;/id&gt;
                &lt;display&gt;abstract&lt;/display&gt;
                &lt;incompatible&gt;false&lt;/incompatible&gt;
              &lt;/net.java.amateras.uml.properties.BooleanPropertyDescriptor&gt;
            &lt;/propertyDescriptors&gt;
            &lt;stereoType&gt;&lt;/stereoType&gt;
            &lt;name&gt;SmallerD&lt;/name&gt;
            &lt;isAbstract&gt;false&lt;/isAbstract&gt;
          &lt;/net.java.amateras.uml.classdiagram.model.ClassModel&gt;
          &lt;net.java.amateras.uml.classdiagram.model.ClassModel&gt;
            &lt;backgroundColor&gt;
              &lt;red&gt;255&lt;/red&gt;
              &lt;green&gt;255&lt;/green&gt;
              &lt;blue&gt;206&lt;/blue&gt;
            &lt;/backgroundColor&gt;
            &lt;foregroundColor&gt;
              &lt;red&gt;0&lt;/red&gt;
              &lt;green&gt;0&lt;/green&gt;
              &lt;blue&gt;0&lt;/blue&gt;
            &lt;/foregroundColor&gt;
            &lt;showIcon&gt;true&lt;/showIcon&gt;
            &lt;parent class=&quot;net.java.amateras.uml.model.RootModel&quot; reference=&quot;../../..&quot;/&gt;
            &lt;listeners serialization=&quot;custom&quot;&gt;
              &lt;java.beans.PropertyChangeSupport&gt;
                &lt;default&gt;
                  &lt;source class=&quot;net.java.amateras.uml.classdiagram.model.ClassModel&quot; reference=&quot;../../../..&quot;/&gt;
                  &lt;propertyChangeSupportSerializedDataVersion&gt;2&lt;/propertyChangeSupportSerializedDataVersion&gt;
                &lt;/default&gt;
                &lt;null/&gt;
              &lt;/java.beans.PropertyChangeSupport&gt;
            &lt;/listeners&gt;
            &lt;constraint&gt;
              &lt;height&gt;-1&lt;/height&gt;
              &lt;width&gt;-1&lt;/width&gt;
              &lt;x&gt;20&lt;/x&gt;
              &lt;y&gt;144&lt;/y&gt;
            &lt;/constraint&gt;
            &lt;sourceConnections&gt;
              &lt;net.java.amateras.uml.classdiagram.model.DependencyModel&gt;
                &lt;showIcon&gt;true&lt;/showIcon&gt;
                &lt;listeners serialization=&quot;custom&quot;&gt;
                  &lt;java.beans.PropertyChangeSupport&gt;
                    &lt;default&gt;
                      &lt;source class=&quot;net.java.amateras.uml.classdiagram.model.DependencyModel&quot; reference=&quot;../../../..&quot;/&gt;
                      &lt;propertyChangeSupportSerializedDataVersion&gt;2&lt;/propertyChangeSupportSerializedDataVersion&gt;
                    &lt;/default&gt;
                    &lt;null/&gt;
                  &lt;/java.beans.PropertyChangeSupport&gt;
                &lt;/listeners&gt;
                &lt;source class=&quot;net.java.amateras.uml.classdiagram.model.ClassModel&quot; reference=&quot;../../..&quot;/&gt;
                &lt;target class=&quot;net.java.amateras.uml.classdiagram.model.ClassModel&quot; reference=&quot;../../../../../..&quot;/&gt;
                &lt;bendpoints/&gt;
                &lt;stereoType&gt;&lt;/stereoType&gt;
              &lt;/net.java.amateras.uml.classdiagram.model.DependencyModel&gt;
            &lt;/sourceConnections&gt;
            &lt;targetConnections/&gt;
            &lt;children/&gt;
            &lt;filterProperty/&gt;
            &lt;propertyDescriptors&gt;
              &lt;org.eclipse.ui.views.properties.TextPropertyDescriptor&gt;
                &lt;id class=&quot;string&quot;&gt;_stereo_type&lt;/id&gt;
                &lt;display&gt;Stereo Type&lt;/display&gt;
                &lt;incompatible&gt;false&lt;/incompatible&gt;
              &lt;/org.eclipse.ui.views.properties.TextPropertyDescriptor&gt;
              &lt;org.eclipse.ui.views.properties.PropertyDescriptor&gt;
                &lt;id class=&quot;string&quot;&gt;_simpleEntityName&lt;/id&gt;
                &lt;display&gt;Simple Name&lt;/display&gt;
                &lt;incompatible&gt;false&lt;/incompatible&gt;
              &lt;/org.eclipse.ui.views.properties.PropertyDescriptor&gt;
              &lt;org.eclipse.ui.views.properties.TextPropertyDescriptor&gt;
                &lt;id class=&quot;string&quot;&gt;_entityName&lt;/id&gt;
                &lt;display&gt;Name&lt;/display&gt;
                &lt;incompatible&gt;false&lt;/incompatible&gt;
              &lt;/org.eclipse.ui.views.properties.TextPropertyDescriptor&gt;
              &lt;org.eclipse.ui.views.properties.ColorPropertyDescriptor&gt;
                &lt;id class=&quot;string&quot;&gt;_background&lt;/id&gt;
                &lt;display&gt;Background Color&lt;/display&gt;
                &lt;incompatible&gt;false&lt;/incompatible&gt;
              &lt;/org.eclipse.ui.views.properties.ColorPropertyDescriptor&gt;
              &lt;org.eclipse.ui.views.properties.PropertyDescriptor&gt;
                &lt;id class=&quot;string&quot;&gt;_attrs&lt;/id&gt;
                &lt;display&gt;Attributes...&lt;/display&gt;
                &lt;incompatible&gt;false&lt;/incompatible&gt;
              &lt;/org.eclipse.ui.views.properties.PropertyDescriptor&gt;
              &lt;org.eclipse.ui.views.properties.PropertyDescriptor&gt;
                &lt;id class=&quot;string&quot;&gt;_operations&lt;/id&gt;
                &lt;display&gt;Operations...&lt;/display&gt;
                &lt;incompatible&gt;false&lt;/incompatible&gt;
              &lt;/org.eclipse.ui.views.properties.PropertyDescriptor&gt;
              &lt;net.java.amateras.uml.properties.BooleanPropertyDescriptor&gt;
                &lt;id class=&quot;string&quot;&gt;_abstract&lt;/id&gt;
                &lt;display&gt;abstract&lt;/display&gt;
                &lt;incompatible&gt;false&lt;/incompatible&gt;
              &lt;/net.java.amateras.uml.properties.BooleanPropertyDescriptor&gt;
            &lt;/propertyDescriptors&gt;
            &lt;stereoType&gt;&lt;/stereoType&gt;
            &lt;name&gt;SmallerC&lt;/name&gt;
            &lt;isAbstract&gt;false&lt;/isAbstract&gt;
          &lt;/net.java.amateras.uml.classdiagram.model.ClassModel&gt;
          &lt;net.java.amateras.uml.classdiagram.model.ClassModel&gt;
            &lt;backgroundColor reference=&quot;../../../../backgroundColor&quot;/&gt;
            &lt;foregroundColor reference=&quot;../../../../foregroundColor&quot;/&gt;
            &lt;showIcon&gt;true&lt;/showIcon&gt;
            &lt;parent class=&quot;net.java.amateras.uml.model.RootModel&quot; reference=&quot;../../..&quot;/&gt;
            &lt;listeners serialization=&quot;custom&quot;&gt;
              &lt;java.beans.PropertyChangeSupport&gt;
                &lt;default&gt;
                  &lt;source class=&quot;net.java.amateras.uml.classdiagram.model.ClassModel&quot; reference=&quot;../../../..&quot;/&gt;
                  &lt;propertyChangeSupportSerializedDataVersion&gt;2&lt;/propertyChangeSupportSerializedDataVersion&gt;
                &lt;/default&gt;
                &lt;null/&gt;
              &lt;/java.beans.PropertyChangeSupport&gt;
            &lt;/listeners&gt;
            &lt;constraint&gt;
              &lt;height&gt;-1&lt;/height&gt;
              &lt;width&gt;-1&lt;/width&gt;
              &lt;x&gt;194&lt;/x&gt;
              &lt;y&gt;292&lt;/y&gt;
            &lt;/constraint&gt;
            &lt;sourceConnections&gt;
              &lt;net.java.amateras.uml.classdiagram.model.AssociationModel&gt;
                &lt;showIcon&gt;true&lt;/showIcon&gt;
                &lt;listeners serialization=&quot;custom&quot;&gt;
                  &lt;java.beans.PropertyChangeSupport&gt;
                    &lt;default&gt;
                      &lt;source class=&quot;net.java.amateras.uml.classdiagram.model.AssociationModel&quot; reference=&quot;../../../..&quot;/&gt;
                      &lt;propertyChangeSupportSerializedDataVersion&gt;2&lt;/propertyChangeSupportSerializedDataVersion&gt;
                    &lt;/default&gt;
                    &lt;null/&gt;
                  &lt;/java.beans.PropertyChangeSupport&gt;
                &lt;/listeners&gt;
                &lt;source class=&quot;net.java.amateras.uml.classdiagram.model.ClassModel&quot; reference=&quot;../../..&quot;/&gt;
                &lt;target class=&quot;net.java.amateras.uml.classdiagram.model.ClassModel&quot; reference=&quot;../../../../../..&quot;/&gt;
                &lt;bendpoints/&gt;
                &lt;stereoType&gt;&lt;/stereoType&gt;
                &lt;fromMultiplicity&gt;&lt;/fromMultiplicity&gt;
                &lt;toMultiplicity&gt;&lt;/toMultiplicity&gt;
              &lt;/net.java.amateras.uml.classdiagram.model.AssociationModel&gt;
            &lt;/sourceConnections&gt;
            &lt;targetConnections/&gt;
            &lt;children/&gt;
            &lt;filterProperty/&gt;
            &lt;propertyDescriptors&gt;
              &lt;org.eclipse.ui.views.properties.TextPropertyDescriptor&gt;
                &lt;id class=&quot;string&quot;&gt;_stereo_type&lt;/id&gt;
                &lt;display&gt;Stereo Type&lt;/display&gt;
                &lt;incompatible&gt;false&lt;/incompatible&gt;
              &lt;/org.eclipse.ui.views.properties.TextPropertyDescriptor&gt;
              &lt;org.eclipse.ui.views.properties.PropertyDescriptor&gt;
                &lt;id class=&quot;string&quot;&gt;_simpleEntityName&lt;/id&gt;
                &lt;display&gt;Simple Name&lt;/display&gt;
                &lt;incompatible&gt;false&lt;/incompatible&gt;
              &lt;/org.eclipse.ui.views.properties.PropertyDescriptor&gt;
              &lt;org.eclipse.ui.views.properties.TextPropertyDescriptor&gt;
                &lt;id class=&quot;string&quot;&gt;_entityName&lt;/id&gt;
                &lt;display&gt;Name&lt;/display&gt;
                &lt;incompatible&gt;false&lt;/incompatible&gt;
              &lt;/org.eclipse.ui.views.properties.TextPropertyDescriptor&gt;
              &lt;org.eclipse.ui.views.properties.ColorPropertyDescriptor&gt;
                &lt;id class=&quot;string&quot;&gt;_background&lt;/id&gt;
                &lt;display&gt;Background Color&lt;/display&gt;
                &lt;incompatible&gt;false&lt;/incompatible&gt;
              &lt;/org.eclipse.ui.views.properties.ColorPropertyDescriptor&gt;
              &lt;org.eclipse.ui.views.properties.PropertyDescriptor&gt;
                &lt;id class=&quot;string&quot;&gt;_attrs&lt;/id&gt;
                &lt;display&gt;Attributes...&lt;/display&gt;
                &lt;incompatible&gt;false&lt;/incompatible&gt;
              &lt;/org.eclipse.ui.views.properties.PropertyDescriptor&gt;
              &lt;org.eclipse.ui.views.properties.PropertyDescriptor&gt;
                &lt;id class=&quot;string&quot;&gt;_operations&lt;/id&gt;
                &lt;display&gt;Operations...&lt;/display&gt;
                &lt;incompatible&gt;false&lt;/incompatible&gt;
              &lt;/org.eclipse.ui.views.properties.PropertyDescriptor&gt;
              &lt;net.java.amateras.uml.properties.BooleanPropertyDescriptor&gt;
                &lt;id class=&quot;string&quot;&gt;_abstract&lt;/id&gt;
                &lt;display&gt;abstract&lt;/display&gt;
                &lt;incompatible&gt;false&lt;/incompatible&gt;
              &lt;/net.java.amateras.uml.properties.BooleanPropertyDescriptor&gt;
            &lt;/propertyDescriptors&gt;
            &lt;stereoType&gt;&lt;/stereoType&gt;
            &lt;name&gt;SmallerE&lt;/name&gt;
            &lt;isAbstract&gt;false&lt;/isAbstract&gt;
          &lt;/net.java.amateras.uml.classdiagram.model.ClassModel&gt;
        &lt;/children&gt;
        &lt;filterProperty/&gt;
      &lt;/parent&gt;
      &lt;listeners serialization=&quot;custom&quot;&gt;
        &lt;java.beans.PropertyChangeSupport&gt;
          &lt;default&gt;
            &lt;source class=&quot;net.java.amateras.uml.classdiagram.model.ClassModel&quot; reference=&quot;../../../..&quot;/&gt;
            &lt;propertyChangeSupportSerializedDataVersion&gt;2&lt;/propertyChangeSupportSerializedDataVersion&gt;
          &lt;/default&gt;
          &lt;null/&gt;
        &lt;/java.beans.PropertyChangeSupport&gt;
      &lt;/listeners&gt;
      &lt;constraint&gt;
        &lt;height&gt;-1&lt;/height&gt;
        &lt;width&gt;-1&lt;/width&gt;
        &lt;x&gt;168&lt;/x&gt;
        &lt;y&gt;107&lt;/y&gt;
      &lt;/constraint&gt;
      &lt;sourceConnections/&gt;
      &lt;targetConnections&gt;
        &lt;net.java.amateras.uml.classdiagram.model.DependencyModel reference=&quot;../../parent/children/net.java.amateras.uml.classdiagram.model.ClassModel[5]/sourceConnections/net.java.amateras.uml.classdiagram.model.DependencyModel&quot;/&gt;
        &lt;net.java.amateras.uml.classdiagram.model.DependencyModel reference=&quot;../../parent/children/net.java.amateras.uml.classdiagram.model.ClassModel[2]/sourceConnections/net.java.amateras.uml.classdiagram.model.DependencyModel&quot;/&gt;
        &lt;net.java.amateras.uml.classdiagram.model.AssociationModel reference=&quot;../../parent/children/net.java.amateras.uml.classdiagram.model.ClassModel[4]/sourceConnections/net.java.amateras.uml.classdiagram.model.AssociationModel&quot;/&gt;
        &lt;net.java.amateras.uml.classdiagram.model.AssociationModel reference=&quot;../../parent/children/net.java.amateras.uml.classdiagram.model.ClassModel[6]/sourceConnections/net.java.amateras.uml.classdiagram.model.AssociationModel&quot;/&gt;
        &lt;net.java.amateras.uml.classdiagram.model.AssociationModel reference=&quot;../../parent/children/net.java.amateras.uml.classdiagram.model.ClassModel[3]/sourceConnections/net.java.amateras.uml.classdiagram.model.AssociationModel&quot;/&gt;
      &lt;/targetConnections&gt;
      &lt;children&gt;
        &lt;net.java.amateras.uml.classdiagram.model.OperationModel&gt;
          &lt;backgroundColor reference=&quot;../../../backgroundColor&quot;/&gt;
          &lt;foregroundColor reference=&quot;../../../foregroundColor&quot;/&gt;
          &lt;showIcon&gt;true&lt;/showIcon&gt;
          &lt;parent class=&quot;net.java.amateras.uml.classdiagram.model.ClassModel&quot; reference=&quot;../../..&quot;/&gt;
          &lt;listeners serialization=&quot;custom&quot;&gt;
            &lt;java.beans.PropertyChangeSupport&gt;
              &lt;default&gt;
                &lt;source class=&quot;net.java.amateras.uml.classdiagram.model.OperationModel&quot; reference=&quot;../../../..&quot;/&gt;
                &lt;propertyChangeSupportSerializedDataVersion&gt;2&lt;/propertyChangeSupportSerializedDataVersion&gt;
              &lt;/default&gt;
              &lt;null/&gt;
            &lt;/java.beans.PropertyChangeSupport&gt;
          &lt;/listeners&gt;
          &lt;visibility class=&quot;net.java.amateras.uml.classdiagram.model.Visibility$1&quot;/&gt;
          &lt;name&gt;operation1&lt;/name&gt;
          &lt;type&gt;void&lt;/type&gt;
          &lt;params&gt;
            &lt;net.java.amateras.uml.classdiagram.model.Argument&gt;
              &lt;name&gt;String a&lt;/name&gt;
              &lt;type&gt;int&lt;/type&gt;
            &lt;/net.java.amateras.uml.classdiagram.model.Argument&gt;
            &lt;net.java.amateras.uml.classdiagram.model.Argument&gt;
              &lt;name&gt;String b&lt;/name&gt;
              &lt;type&gt;int&lt;/type&gt;
            &lt;/net.java.amateras.uml.classdiagram.model.Argument&gt;
            &lt;net.java.amateras.uml.classdiagram.model.Argument&gt;
              &lt;name&gt;int c&lt;/name&gt;
              &lt;type&gt;int&lt;/type&gt;
            &lt;/net.java.amateras.uml.classdiagram.model.Argument&gt;
          &lt;/params&gt;
          &lt;isAbstract&gt;false&lt;/isAbstract&gt;
          &lt;isStatic&gt;false&lt;/isStatic&gt;
        &lt;/net.java.amateras.uml.classdiagram.model.OperationModel&gt;
      &lt;/children&gt;
      &lt;filterProperty/&gt;
      &lt;propertyDescriptors&gt;
        &lt;org.eclipse.ui.views.properties.TextPropertyDescriptor&gt;
          &lt;id class=&quot;string&quot;&gt;_stereo_type&lt;/id&gt;
          &lt;display&gt;Stereo Type&lt;/display&gt;
          &lt;incompatible&gt;false&lt;/incompatible&gt;
        &lt;/org.eclipse.ui.views.properties.TextPropertyDescriptor&gt;
        &lt;org.eclipse.ui.views.properties.PropertyDescriptor&gt;
          &lt;id class=&quot;string&quot;&gt;_simpleEntityName&lt;/id&gt;
          &lt;display&gt;Simple Name&lt;/display&gt;
          &lt;incompatible&gt;false&lt;/incompatible&gt;
        &lt;/org.eclipse.ui.views.properties.PropertyDescriptor&gt;
        &lt;org.eclipse.ui.views.properties.TextPropertyDescriptor&gt;
          &lt;id class=&quot;string&quot;&gt;_entityName&lt;/id&gt;
          &lt;display&gt;Name&lt;/display&gt;
          &lt;incompatible&gt;false&lt;/incompatible&gt;
        &lt;/org.eclipse.ui.views.properties.TextPropertyDescriptor&gt;
        &lt;org.eclipse.ui.views.properties.ColorPropertyDescriptor&gt;
          &lt;id class=&quot;string&quot;&gt;_background&lt;/id&gt;
          &lt;display&gt;Background Color&lt;/display&gt;
          &lt;incompatible&gt;false&lt;/incompatible&gt;
        &lt;/org.eclipse.ui.views.properties.ColorPropertyDescriptor&gt;
        &lt;org.eclipse.ui.views.properties.PropertyDescriptor&gt;
          &lt;id class=&quot;string&quot;&gt;_attrs&lt;/id&gt;
          &lt;display&gt;Attributes...&lt;/display&gt;
          &lt;incompatible&gt;false&lt;/incompatible&gt;
        &lt;/org.eclipse.ui.views.properties.PropertyDescriptor&gt;
        &lt;org.eclipse.ui.views.properties.PropertyDescriptor&gt;
          &lt;id class=&quot;string&quot;&gt;_operations&lt;/id&gt;
          &lt;display&gt;Operations...&lt;/display&gt;
          &lt;incompatible&gt;false&lt;/incompatible&gt;
        &lt;/org.eclipse.ui.views.properties.PropertyDescriptor&gt;
        &lt;net.java.amateras.uml.properties.BooleanPropertyDescriptor&gt;
          &lt;id class=&quot;string&quot;&gt;_abstract&lt;/id&gt;
          &lt;display&gt;abstract&lt;/display&gt;
          &lt;incompatible&gt;false&lt;/incompatible&gt;
        &lt;/net.java.amateras.uml.properties.BooleanPropertyDescriptor&gt;
      &lt;/propertyDescriptors&gt;
      &lt;stereoType&gt;&lt;/stereoType&gt;
      &lt;name&gt;Biggest&lt;/name&gt;
      &lt;isAbstract&gt;false&lt;/isAbstract&gt;
    &lt;/net.java.amateras.uml.classdiagram.model.ClassModel&gt;
    &lt;net.java.amateras.uml.classdiagram.model.ClassModel reference=&quot;../net.java.amateras.uml.classdiagram.model.ClassModel/parent/children/net.java.amateras.uml.classdiagram.model.ClassModel[2]&quot;/&gt;
    &lt;net.java.amateras.uml.classdiagram.model.ClassModel reference=&quot;../net.java.amateras.uml.classdiagram.model.ClassModel/parent/children/net.java.amateras.uml.classdiagram.model.ClassModel[3]&quot;/&gt;
    &lt;net.java.amateras.uml.classdiagram.model.ClassModel reference=&quot;../net.java.amateras.uml.classdiagram.model.ClassModel/parent/children/net.java.amateras.uml.classdiagram.model.ClassModel[4]&quot;/&gt;
    &lt;net.java.amateras.uml.classdiagram.model.ClassModel reference=&quot;../net.java.amateras.uml.classdiagram.model.ClassModel/parent/children/net.java.amateras.uml.classdiagram.model.ClassModel[5]&quot;/&gt;
    &lt;net.java.amateras.uml.classdiagram.model.ClassModel reference=&quot;../net.java.amateras.uml.classdiagram.model.ClassModel/parent/children/net.java.amateras.uml.classdiagram.model.ClassModel[6]&quot;/&gt;
  &lt;/children&gt;
  &lt;filterProperty/&gt;
&lt;/net.java.amateras.uml.model.RootModel&gt;</textarea>
		</td>
		</tr>
		<tr>
			<td colspan="3">
				<button id="amateras-assist">Get assistance</button>
			</td>
		</tr>
		</table>
	</section>
	<hr />
	<section>
		<h3>Response:</h3>
		<div id="amateras-response"></div>
	</section>
	<hr />
	<section>
		<h3>Load initial Amateras diagrams</h3>
		<table>
			<tr>
				<td>User: </td>
				<td><input type="text" id="db-user" value="" /></td>
			</tr>
			<tr>
				<td>Pass: </td>
				<td><input type="password" id="db-pass" value="" /></td>
			</tr>
			<tr>
				<td colspan="2"><button id="load-amateras-db">Load diagrams</button></td>
			</tr>
		</table>
	</section>
</body>

</html>
