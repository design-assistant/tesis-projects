String.prototype.htmlEntities = function() {
	return this.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
};

var amaterasAssist = function() {

	var data = {
		design : $("#amateras-design").val(),
		qualityAttributes : $("input:checkbox:checked").map(function() {
			return $(this).val();
		}).get(),
		matching : parseInt($("input:radio:checked").val())
	};

	$.ajax({
		url : "assist/amateras",
		type : "POST",
		data : JSON.stringify(data),
		contentType : "application/json"
	}).done(function(data) {
		var result = "";
		$.each(data.assistResult, function(index, value) {
			result += value.name + " (" + value.success + "), ";
		});
		$("#amateras-response").html(result.substr(0, (result.length > 0) ? result.length - 2 : 0));
	}).fail(function(jqXHR, textStatus) {
		$("#amateras-response").html("Request failed: " + textStatus);
	});

};

var loadAmaterasDb = function() {

	$.ajax({
		url : "loaddb/amateras",
		type : "POST",
		contentType : "application/json",
		beforeSend : function(xhr) {
			var user = $("#db-user").val();
			var pass = $("#db-pass").val();
			xhr.setRequestHeader("Authorization", "Basic " + window.btoa(user + ":" + pass));
			$("#db-user").val("");
			$("#db-pass").val("");
		}
	}).done(function(data) {
		$("#amateras-response").html("DB successfully loaded.");
	}).fail(function(jqXHR, textStatus) {
		$("#amateras-response").html("Request failed: " + textStatus);
	});

};

$(document).ready(function() {
	$("#amateras-assist").click(amaterasAssist);
	$("#load-amateras-db").click(loadAmaterasDb);
});
