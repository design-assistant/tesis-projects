package tesis.assistant.webapp.msg;

import java.util.List;

public class AssistRequest {

	private String design;
	private List<String> qualityAttributes;
	private int matching;

	public AssistRequest() {
	}

	public String getDesign() {
		return design;
	}

	public void setDesign(String design) {
		this.design = design;
	}

	public List<String> getQualityAttributes() {
		return qualityAttributes;
	}

	public void setQualityAttributes(List<String> qualityAttributes) {
		this.qualityAttributes = qualityAttributes;
	}

	public int getMatching() {
		return matching;
	}

	public void setMatching(int matching) {
		this.matching = matching;
	}

	@Override
	public String toString() {
		return "AssistRequest [qualityAttributes=" + qualityAttributes + ", matching=" + matching + "]";
	}

}
