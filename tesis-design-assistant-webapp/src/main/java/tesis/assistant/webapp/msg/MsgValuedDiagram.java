package tesis.assistant.webapp.msg;

import java.util.Map;

public class MsgValuedDiagram extends MsgDiagram {

	private double success;

	public MsgValuedDiagram() {
	}

	public MsgValuedDiagram(String name, String design, String description, Map<String, Integer> qualityAttributes, double success) {
		super(name, design, description, qualityAttributes);
		this.success = success;
	}

	public double getSuccess() {
		return success;
	}

	public void setSuccess(double success) {
		this.success = success;
	}

}
