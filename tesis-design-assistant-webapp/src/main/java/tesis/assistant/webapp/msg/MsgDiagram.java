package tesis.assistant.webapp.msg;

import java.util.Map;

public class MsgDiagram {

	private String name;
	private String design;
	private String description;
	private Map<String, Integer> qualityAttributes;

	public MsgDiagram() {
	}

	public MsgDiagram(String name, String design, String description, Map<String, Integer> qualityAttributes) {
		this.name = name;
		this.design = design;
		this.description = description;
		this.qualityAttributes = qualityAttributes;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDesign() {
		return design;
	}

	public void setDesign(String design) {
		this.design = design;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Map<String, Integer> getQualityAttributes() {
		return qualityAttributes;
	}

	public void setQualityAttributes(Map<String, Integer> qualityAttributes) {
		this.qualityAttributes = qualityAttributes;
	}

}
