package tesis.assistant.webapp.init;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import tesis.assistant.contants.AssistantConstants;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		if (!AssistantConstants.WS_LOADDB_USER.isEmpty()) {
			auth.inMemoryAuthentication().withUser(AssistantConstants.WS_LOADDB_USER).password(AssistantConstants.WS_LOADDB_PASS).roles("SUPERADMIN");
		}

		if (!AssistantConstants.WS_ADD_USER.isEmpty()) {
			auth.inMemoryAuthentication().withUser(AssistantConstants.WS_ADD_USER).password(AssistantConstants.WS_ADD_PASS).roles("ADMIN");
		}

		if (!AssistantConstants.WS_ASSIST_USER.isEmpty()) {
			auth.inMemoryAuthentication().withUser(AssistantConstants.WS_ASSIST_USER).password(AssistantConstants.WS_ASSIST_PASS).roles("USER");
		}
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		// disable cross-site protection
		http.csrf().disable();

		if (!AssistantConstants.WS_LOADDB_USER.isEmpty()) {
			http.authorizeRequests().antMatchers("/loaddb/**").access("hasRole('ROLE_SUPERADMIN')").and().httpBasic();
		}

		if (!AssistantConstants.WS_ADD_USER.isEmpty()) {
			http.authorizeRequests().antMatchers("/add/**").access("hasRole('ROLE_ADMIN')").and().httpBasic();
		}

		if (!AssistantConstants.WS_ASSIST_USER.isEmpty()) {
			http.authorizeRequests().antMatchers("/assist/**").access("hasRole('ROLE_USER')").and().httpBasic();
		}
	}

}