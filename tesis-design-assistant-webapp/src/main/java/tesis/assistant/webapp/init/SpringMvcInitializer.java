package tesis.assistant.webapp.init;

import java.io.File;
import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.apache.commons.io.FileUtils;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import tesis.assistant.contants.AssistantConstants;

public class SpringMvcInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

	@Override
	protected Class<?>[] getRootConfigClasses() {
		return new Class[] { WebAppConfig.class };
	}

	@Override
	protected Class<?>[] getServletConfigClasses() {
		return null;
	}

	@Override
	protected String[] getServletMappings() {
		return new String[] { "/" };
	}

	@Override
	public void onStartup(ServletContext servletContext) throws ServletException {
		super.onStartup(servletContext);

		// clean temporal directory
		try {
			FileUtils.forceMkdir(new File(AssistantConstants.TEMP_DIR_PATH));
			FileUtils.cleanDirectory(new File(AssistantConstants.TEMP_DIR_PATH));
		} catch (IOException e) {
			throw new ServletException("There was a problem when creating/cleaning the temporal directory: "
					+ AssistantConstants.TEMP_DIR_PATH, e);
		}
	}

}