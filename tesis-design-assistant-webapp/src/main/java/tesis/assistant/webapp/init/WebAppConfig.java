package tesis.assistant.webapp.init;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.List;
import java.util.zip.GZIPInputStream;
import java.util.zip.Inflater;
import java.util.zip.InflaterInputStream;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import tesis.assistant.util.AppConfig;

@Configuration
@ComponentScan({ "tesis.assistant.webapp" })
@Import({ SecurityConfig.class, AppConfig.class })
public class WebAppConfig extends WebMvcConfigurationSupport {

	@Bean
	public InternalResourceViewResolver setupViewResolver() {
		InternalResourceViewResolver resolver = new InternalResourceViewResolver();
		resolver.setPrefix("/WEB-INF/views/");
		resolver.setSuffix(".jsp");
		resolver.setViewClass(JstlView.class);
		return resolver;
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/css/**").addResourceLocations("/css/").setCachePeriod(31556926);
		registry.addResourceHandler("/img/**").addResourceLocations("/img/").setCachePeriod(31556926);
		registry.addResourceHandler("/js/**").addResourceLocations("/js/").setCachePeriod(31556926);
	}

	@Override
	public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
		converters.add(jacksonCompressConverter());
		addDefaultHttpMessageConverters(converters);
	}

	@Bean
	MappingJackson2HttpMessageConverter jacksonCompressConverter() { // extends Jackson converter to read compressed messages
		MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter() {

			@Override
			protected Object readInternal(Class<?> clazz, HttpInputMessage inputMessage) throws IOException,
					HttpMessageNotReadableException {
				return super.readInternal(clazz, decompress(inputMessage));
			}

			@Override
			public Object read(Type type, Class<?> contextClass, HttpInputMessage inputMessage) throws IOException,
					HttpMessageNotReadableException {
				return super.read(type, contextClass, decompress(inputMessage));
			}
		};

		return converter;
	}

	protected HttpInputMessage decompress(final HttpInputMessage inputMessage) {
		return new HttpInputMessage() {
			private static final int DECOMPRESS_BUFFER_SIZE = 4096; // 4 KiB

			@Override
			public HttpHeaders getHeaders() {
				return inputMessage.getHeaders();
			}

			@Override
			public InputStream getBody() throws IOException {
				InputStream body = inputMessage.getBody();
				if (!(body instanceof InflaterInputStream)) {
					String contentEncoding = inputMessage.getHeaders().getFirst("Content-Encoding");
					if ("deflate".equalsIgnoreCase(contentEncoding)) {
						body = new InflaterInputStream(body, new Inflater(true), DECOMPRESS_BUFFER_SIZE);
					} else if ("gzip".equalsIgnoreCase(contentEncoding)) {
						body = new GZIPInputStream(body, DECOMPRESS_BUFFER_SIZE);
					}
				}
				return body;
			}
		};
	}

}
