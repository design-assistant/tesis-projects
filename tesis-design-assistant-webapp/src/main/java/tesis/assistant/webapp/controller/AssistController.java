package tesis.assistant.webapp.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.TransactionException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import tesis.assistant.db.DesignAssistantDT;
import tesis.assistant.service.DesignAssistantService;
import tesis.assistant.service.DesignAssistantServiceManager;
import tesis.assistant.util.DesignAssistanceException;
import tesis.assistant.webapp.msg.AddResponse;
import tesis.assistant.webapp.msg.AssistRequest;
import tesis.assistant.webapp.msg.AssistResponse;
import tesis.assistant.webapp.util.ResponseUtil;

@RestController
@RequestMapping("/assist")
public class AssistController {

	private static final Logger LOGGER = Logger.getLogger(AssistController.class);

	/**
	 * 
	 * @param serviceType
	 * @param assistRequest
	 * @return
	 * @throws DesignAssistanceException
	 */
	@RequestMapping(value = "/{serviceType}", method = RequestMethod.POST)
	public AssistResponse assist(
			@PathVariable String serviceType,
			@RequestBody AssistRequest assistRequest) throws DesignAssistanceException {

		long start = System.currentTimeMillis();
		LOGGER.info("start " + serviceType + " service");

		try {
			DesignAssistantService assistantService = DesignAssistantServiceManager.getService(serviceType);
			List<DesignAssistantDT> diagramTypes = assistantService.assist(assistRequest.getDesign(),
					assistRequest.getQualityAttributes(), assistRequest.getMatching());
			return ResponseUtil.getAssistResponseFromDTs(null, diagramTypes);

		} finally {
			LOGGER.info("end " + serviceType + " service in " + (System.currentTimeMillis() - start) + " ms");
		}
	}

	/**
	 * 
	 */
	@ExceptionHandler(DesignAssistanceException.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public AssistResponse handleException(DesignAssistanceException tr) {
		LOGGER.error("", tr);
		return new AssistResponse(tr.getMessage());
	}

	/**
	 * 
	 */
	@ExceptionHandler(TransactionException.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public AddResponse handleException(TransactionException tr) {
		LOGGER.error("", tr);
		return new AddResponse("There was an error in the transaction.");
	}

}
