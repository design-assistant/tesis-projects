package tesis.assistant.webapp.controller;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.TransactionException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import tesis.assistant.db.DesignAssistantDT;
import tesis.assistant.service.DesignAssistantService;
import tesis.assistant.service.DesignAssistantServiceManager;
import tesis.assistant.util.DesignAssistanceException;
import tesis.assistant.webapp.msg.AddRequest;
import tesis.assistant.webapp.msg.AddResponse;
import tesis.assistant.webapp.util.ResponseUtil;

@RestController
@RequestMapping("/add")
public class AddController {

	private static final Logger LOGGER = Logger.getLogger(AddController.class);

	/**
	 * 
	 * @param serviceType
	 * @param addRequest
	 * @return
	 * @throws DesignAssistanceException
	 */
	@RequestMapping(value = "/{serviceType}", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.CREATED)
	public AddResponse add(
			@PathVariable String serviceType,
			@RequestBody AddRequest addRequest) throws DesignAssistanceException {

		long start = System.currentTimeMillis();
		LOGGER.info("start add " + serviceType + " service");

		try {
			DesignAssistantService assistantService = DesignAssistantServiceManager.getService(serviceType);
			DesignAssistantDT diagramType = assistantService.add(addRequest.getDesign(), addRequest.getName(),
					addRequest.getDescription(), addRequest.getQualityAttributes());
			return ResponseUtil.getAddResponseFromDT(null, diagramType);

		} finally {
			LOGGER.info("end add " + serviceType + " service in " + (System.currentTimeMillis() - start) + " ms");
		}
	}

	/**
	 * 
	 */
	@ExceptionHandler(DesignAssistanceException.class)
	@ResponseStatus(HttpStatus.CONFLICT)
	public AddResponse handleException(DesignAssistanceException tr) {
		LOGGER.error("", tr);
		return new AddResponse(tr.getMessage());
	}

	/**
	 * 
	 */
	@ExceptionHandler(TransactionException.class)
	@ResponseStatus(HttpStatus.CONFLICT)
	public AddResponse handleException(TransactionException tr) {
		LOGGER.error("", tr);
		return new AddResponse("There was an error in the transaction.");
	}

}
