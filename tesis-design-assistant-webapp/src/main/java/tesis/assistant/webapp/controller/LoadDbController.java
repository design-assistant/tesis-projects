package tesis.assistant.webapp.controller;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.TransactionException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import tesis.assistant.service.DesignAssistantService;
import tesis.assistant.service.DesignAssistantServiceManager;
import tesis.assistant.util.DesignAssistanceException;
import tesis.assistant.webapp.msg.AddResponse;

@RestController
@RequestMapping("/loaddb")
public class LoadDbController {

	private static final Logger LOGGER = Logger.getLogger(LoadDbController.class);

	/**
	 * 
	 * @param serviceType
	 * @throws DesignAssistanceException
	 */
	@RequestMapping(value = "/{serviceType}", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.CREATED)
	public void loadDb(@PathVariable String serviceType) throws DesignAssistanceException {

		long start = System.currentTimeMillis();
		LOGGER.info("start loadDb service");

		try {
			DesignAssistantService assistantService = DesignAssistantServiceManager.getService(serviceType);
			assistantService.loadDb();

		} finally {
			LOGGER.info("end loadDb service in " + (System.currentTimeMillis() - start) + " ms");
		}
	}

	/**
	 * 
	 */
	@ExceptionHandler(DesignAssistanceException.class)
	@ResponseStatus(HttpStatus.CONFLICT)
	public @ResponseBody
	AddResponse handleException(DesignAssistanceException tr) {
		LOGGER.error("", tr);
		return new AddResponse(tr.getMessage());
	}

	/**
	 * 
	 */
	@ExceptionHandler(TransactionException.class)
	@ResponseStatus(HttpStatus.CONFLICT)
	public AddResponse handleException(TransactionException tr) {
		LOGGER.error("", tr);
		return new AddResponse("There was an error in the transaction.");
	}

}
