package tesis.assistant.webapp.util;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import tesis.assistant.db.DesignAssistantDT;
import tesis.assistant.db.DesignAssistantDiagram;
import tesis.assistant.db.DesignAssistantQA;
import tesis.assistant.webapp.msg.AddResponse;
import tesis.assistant.webapp.msg.AssistResponse;
import tesis.assistant.webapp.msg.MsgDiagram;
import tesis.assistant.webapp.msg.MsgValuedDiagram;

public class ResponseUtil {

	public static AddResponse getAddResponseFromDT(String errorMsg, DesignAssistantDT diagramType) {
		AddResponse response = new AddResponse(errorMsg);
		response.setAddResult(getAddResponseItemFromDT(diagramType));
		return response;
	}

	public static AssistResponse getAssistResponseFromDTs(String errorMsg, List<DesignAssistantDT> diagramTypes) {
		AssistResponse response = new AssistResponse(errorMsg);
		for (DesignAssistantDT diagramType : diagramTypes) {
			response.addAssistResponseItem(getAssistResponseItemFromDT(diagramType));
		}
		return response;
	}

	/**
	 * 
	 * @param diagramType
	 * @return
	 */
	private static MsgDiagram getAddResponseItemFromDT(DesignAssistantDT diagramType) {
		DesignAssistantDiagram diagram = diagramType.getDiagram();
		Map<String, Integer> qualityAttrs = getQualityTrributesMap(diagram.getQualityAttributes());
		return new MsgDiagram(diagram.getName(), diagramType.getDesign(), diagram.getDescription(), qualityAttrs);
	}

	/**
	 * 
	 * @param diagramType
	 * @return
	 */
	private static MsgValuedDiagram getAssistResponseItemFromDT(DesignAssistantDT diagramType) {
		DesignAssistantDiagram diagram = diagramType.getDiagram();
		Map<String, Integer> qualityAttrs = getQualityTrributesMap(diagram.getQualityAttributes());
		return new MsgValuedDiagram(diagram.getName(), diagramType.getDesign(), diagram.getDescription(), qualityAttrs,
				diagram.getSuccess());
	}

	/**
	 * This method loads the quality attributes into a map (maintains the order of the attributes)
	 * 
	 * @param qualityAttributes
	 * @return
	 */
	private static Map<String, Integer> getQualityTrributesMap(Set<DesignAssistantQA> qualityAttributes) {
		Map<String, Integer> qualityAttributesMap = new LinkedHashMap<>();
		for (DesignAssistantQA qualityAttribute : qualityAttributes) {
			qualityAttributesMap.put(qualityAttribute.getName(), qualityAttribute.getValue());
		}
		return qualityAttributesMap;
	}

}
