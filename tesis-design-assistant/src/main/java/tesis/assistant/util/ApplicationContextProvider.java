package tesis.assistant.util;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import tesis.assistant.modelparser.ModelParserManager;
import tesis.assistant.service.DesignAssistantServiceManager;
import context.ImplementationFactory;

public class ApplicationContextProvider implements ApplicationContextAware {

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {

		// set application context to tesis-design-comparer project
		ImplementationFactory.setApplicationContext(applicationContext);

		// set application context to bean managers
		ModelParserManager.setApplicationContext(applicationContext);
		DesignAssistantServiceManager.setApplicationContext(applicationContext);
	}

}
