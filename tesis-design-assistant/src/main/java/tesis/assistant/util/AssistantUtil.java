package tesis.assistant.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

public class AssistantUtil {

	private static final Logger LOGGER = Logger.getLogger(AssistantUtil.class);

	/**
	 * 
	 * @param directory
	 * @param data
	 * @param extension
	 * @return
	 * @throws IOException
	 */
	public static Path writeStringToFile(String directory, String data, String extension) throws IOException {
		long threadId = Thread.currentThread().getId();
		long relativeNanos = System.nanoTime();

		String fileName = "design_" + threadId + "_" + relativeNanos + "." + extension;
		Path path = Paths.get(directory, fileName);
		Files.write(path, data.getBytes("UTF-8"));

		return path;
	}

	/**
	 * 
	 * @param map
	 * @param key1
	 * @param key2
	 * @param value
	 */
	public static <K, L, V> void addToMapMap(Map<K, Map<L, V>> map, K key1, L key2, V value) {
		if (!map.containsKey(key1)) {
			map.put(key1, new HashMap<L, V>());
		}
		map.get(key1).put(key2, value);
	}

	/**
	 * 
	 * @param real
	 * @return
	 */
	public static double round(double real) {
		return Math.round(real * 1000.0) / 1000.0;
	}

	/**
	 * 
	 * @param value
	 * @param minInValue
	 * @param maxInValue
	 * @param minOutValue
	 * @param maxOutValue
	 * @param inverse
	 * @return
	 */
	public static double normalize(double value, double minInValue, double maxInValue, double minOutValue, double maxOutValue, boolean inverse) {
		double truncatedValue = Math.max(Math.min(value, maxInValue), minInValue);
		double normalizedValue = (truncatedValue - minInValue) / (maxInValue - minInValue);
		if (inverse) {
			normalizedValue = 1 - normalizedValue;
		}
		double outputValue = normalizedValue * (maxOutValue - minOutValue) + minOutValue;

		LOGGER.debug("      value: " + round(value) + " - normalized: " + round(outputValue));

		return outputValue;
	}

}
