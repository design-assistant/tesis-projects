package tesis.assistant.util;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import tesis.assistant.comparer.MinSuccessComparer;
import tesis.assistant.comparer.PlainDesignComparer;
import tesis.assistant.comparer.QualityAttributesComparer;
import tesis.assistant.comparer.TopologicalComparer;
import tesis.assistant.contants.AssistantConstants;
import tesis.assistant.service.DesignAssistantService;
import tesis.assistant.service.DesignAssistantServiceImpl;

@Configuration
@EnableTransactionManagement
@ComponentScan(basePackages = { "tesis.assistant" })
@ImportResource({ "classpath*:/applicationContext-core-strategies.xml" })
@PropertySource({ "classpath:assistant.properties" })
public class AppConfig {

	/*
	 * PROPERTIES
	 */

	@Value("${db.name}")
	private String dbName;

	@Value("${db.host}")
	private String dbHost;

	@Value("${db.port}")
	private String dbPort;

	@Value("${db.user}")
	private String dbUser;

	@Value("${db.pass}")
	private String dbPass;

	@Bean
	public static PropertySourcesPlaceholderConfigurer properties() {
		return new PropertySourcesPlaceholderConfigurer();
	}

	/*
	 * APPLICATION CONTEXT AWARE
	 */

	@Bean
	public ApplicationContextProvider applicationContextProvider() {
		return new ApplicationContextProvider();
	}

	/*
	 * DESIGN ASISTANT SERVICE INITIALIZATION
	 * 
	 * To add a new service just create a new bean :)
	 * Don't forget to create the parsers for each comparing algorithm
	 */

	@Bean
	public DesignAssistantService designAssistantServiceAmateras() {
		return new DesignAssistantServiceImpl("amateras");
	}

	@Bean
	public DesignAssistantService designAssistantServiceUmlet() {
		return new DesignAssistantServiceImpl("umlet");
	}

	/*
	 * COMPARERS INITIALIZATION
	 */

	@Bean
	public MinSuccessComparer minSuccessComparer() {
		MinSuccessComparer comparer = new MinSuccessComparer(AssistantConstants.COMPARER_MIN_SUCCESS, AssistantConstants.COMPARER_MAX_RESULTS);
		comparer.addDesignComparer(plainDesignComparer(), AssistantConstants.COMPARER_PLAIN_COEFF);
		comparer.addDesignComparer(topologicalComparer(), AssistantConstants.COMPARER_TOPOLOGICAL_COEFF);
		comparer.addDesignComparer(qualityAttributesComparer(), AssistantConstants.COMPARER_QUALITYATTRIB_COEFF);
		return comparer;
	}

	@Bean
	public PlainDesignComparer plainDesignComparer() {
		return new PlainDesignComparer();
	}

	@Bean
	public TopologicalComparer topologicalComparer() {
		return new TopologicalComparer();
	}

	@Bean
	public QualityAttributesComparer qualityAttributesComparer() {
		return new QualityAttributesComparer();
	}

	/*
	 * ENTITY MANAGER CONFIGURATION
	 */

	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactoryBean() {
		LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
		em.setDataSource(dataSource());
		em.setPackagesToScan("tesis.assistant.db");
		em.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
		em.setJpaProperties(additionalProperties());
		return em;
	}

	@Bean
	public DataSource dataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		dataSource.setUrl("jdbc:mysql://" + dbHost + ":" + dbPort + "/" + dbName);
		dataSource.setUsername(dbUser);
		dataSource.setPassword(dbPass);
		return dataSource;
	}

	@Bean
	public PlatformTransactionManager transactionManager(EntityManagerFactory emf) {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(emf);
		return transactionManager;
	}

	@Bean
	public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
		return new PersistenceExceptionTranslationPostProcessor();
	}

	private Properties additionalProperties() {
		return new Properties() {
			private static final long serialVersionUID = 1L;
			{
				setProperty("hibernate.dialect", "org.hibernate.dialect.MySQL5Dialect");
				setProperty("hibernate.ejb.naming_strategy", "org.hibernate.cfg.ImprovedNamingStrategy");
				setProperty("hibernate.hbm2ddl.auto", "update");
				setProperty("hibernate.show_sql", "false");
				setProperty("hibernate.format_sql", "true");
				setProperty("hibernate.connection.autoreconnect", "true");
			}
		};
	}

}
