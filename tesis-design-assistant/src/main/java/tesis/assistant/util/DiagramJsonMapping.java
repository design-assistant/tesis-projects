package tesis.assistant.util;

import java.util.Map;

public class DiagramJsonMapping {

	private String name;
	private String design;
	private String description;
	private Map<String, Integer> qualityAttributes;

	public DiagramJsonMapping() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDesign() {
		return design;
	}

	public void setDesign(String design) {
		this.design = design;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Map<String, Integer> getQualityAttributes() {
		return qualityAttributes;
	}

	public void setQualityAttributes(Map<String, Integer> qualityAttributes) {
		this.qualityAttributes = qualityAttributes;
	}

}