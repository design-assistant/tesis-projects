package tesis.assistant.util;

public class DesignAssistanceException extends Exception {

	private static final long serialVersionUID = 1L;

	public DesignAssistanceException() {
		super();
	}

	public DesignAssistanceException(String message, Throwable cause) {
		super(message, cause);
	}

	public DesignAssistanceException(String message) {
		super(message);
	}

	public DesignAssistanceException(Throwable cause) {
		super(cause);
	}

}
