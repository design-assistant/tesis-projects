package tesis.assistant.comparer;

import java.util.ArrayList;
import java.util.List;

import tesis.assistant.contants.AssistantConstants;
import tesis.assistant.db.DesignAssistantDT;
import tesis.assistant.db.DesignAssistantDiagram;
import tesis.assistant.db.DesignAssistantQA;
import tesis.assistant.util.AssistantUtil;
import tesis.assistant.util.DesignAssistanceException;

public class QualityAttributesComparer implements DesignComparer {

	public static final double MIN_ATTRIB_VALUE = -10.0;
	public static final double MAX_ATTRIB_VALUE = 10.0;

	@Override
	public List<DesignAssistantDT> compare(List<DesignAssistantDT> diagramTypes, String userDiagram,
			List<String> attributes, String serviceType) throws DesignAssistanceException {

		if (diagramTypes.isEmpty()) {
			return diagramTypes;
		}

		try {

			for (DesignAssistantDT diagramType : diagramTypes) {
				DesignAssistantDiagram diagram = diagramType.getDiagram();

				int sumAttributes = 0;

				for (String attribute : attributes) {
					for (DesignAssistantQA qualityAttribute : diagram.getQualityAttributes()) {
						if (qualityAttribute.getName().equalsIgnoreCase(attribute)) {
							sumAttributes += qualityAttribute.getValue();
						}
					}
				}

				double avgAttrValue = (double) sumAttributes / (double) attributes.size();
				double success = AssistantUtil.normalize(avgAttrValue, MIN_ATTRIB_VALUE, MAX_ATTRIB_VALUE,
						AssistantConstants.MIN_ASSIST_SUCCESS, AssistantConstants.MAX_ASSIST_SUCCESS, false);

				diagram.setSuccess(success);
			}

		} catch (Exception e) {
			throw new DesignAssistanceException("Error comparing attributes", e);
		}

		return new ArrayList<>(diagramTypes);
	}

}