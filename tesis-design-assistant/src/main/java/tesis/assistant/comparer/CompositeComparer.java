package tesis.assistant.comparer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import tesis.assistant.db.DesignAssistantDT;
import tesis.assistant.db.DesignAssistantDiagram;
import tesis.assistant.util.AssistantUtil;
import tesis.assistant.util.DesignAssistanceException;

public abstract class CompositeComparer implements DesignComparer {

	private static final Logger LOGGER = Logger.getLogger(CompositeComparer.class);

	protected Map<DesignComparer, Double> designComparers = new LinkedHashMap<>();

	public Double addDesignComparer(DesignComparer designComparer, double coefficient) {
		return this.designComparers.put(designComparer, coefficient);
	}

	public Double removeDesignComparer(DesignComparer designComparer) {
		return this.designComparers.remove(designComparer);
	}

	public boolean containsDesignComparer(DesignComparer designComparer) {
		return this.designComparers.containsKey(designComparer);
	}

	@Override
	public List<DesignAssistantDT> compare(List<DesignAssistantDT> diagramTypes, String userDiagram,
			List<String> attributes, String serviceType) throws DesignAssistanceException {

		if (diagramTypes.isEmpty()) {
			return diagramTypes;
		}

		// the following structure is a matrix that assigns a success value to the intersection of a design and a comparer
		Map<DesignAssistantDiagram, Map<DesignComparer, Double>> successMap = new HashMap<>();
		double acummCoeffs = 0.0;

		for (DesignComparer designComparer : this.designComparers.keySet()) {
			// fill with 0.0 the matrix
			for (DesignAssistantDT diagramType : diagramTypes) {
				AssistantUtil.addToMapMap(successMap, diagramType.getDiagram(), designComparer, 0.0);
			}

			// add every comparer coefficient to normalize the final value
			acummCoeffs += this.designComparers.get(designComparer);
		}

		for (DesignComparer designComparer : this.designComparers.keySet()) {
			// get all results
			List<DesignAssistantDT> compareResults = designComparer.compare(diagramTypes, userDiagram, attributes, serviceType);

			// load success values in the matrix
			for (DesignAssistantDT diagramType : compareResults) {
				DesignAssistantDiagram diagram = diagramType.getDiagram();
				AssistantUtil.addToMapMap(successMap, diagram, designComparer, diagram.getSuccess());

				LOGGER.info("    " + designComparer.getClass().getSimpleName() + " - " + diagram.getName() + " - "
						+ AssistantUtil.round(diagram.getSuccess()));
			}
		}

		for (DesignAssistantDT diagramType : diagramTypes) {
			// calculate average success based on preset coefficients for each comparer
			Map<DesignComparer, Double> comparersSuccess = successMap.get(diagramType.getDiagram());
			double average = getAverageSuccessForDiagram(comparersSuccess, acummCoeffs);
			diagramType.getDiagram().setSuccess(average);
		}

		return new ArrayList<>(diagramTypes);
	}

	/**
	 * 
	 * @param comparersSuccess
	 * @param accumCoeffs
	 * @return
	 */
	private double getAverageSuccessForDiagram(Map<DesignComparer, Double> comparersSuccess, double accumCoeffs) {
		double average = 0.0;
		for (Entry<DesignComparer, Double> entry : comparersSuccess.entrySet()) {
			double success = entry.getValue();
			double designComparerCoeff = this.designComparers.get(entry.getKey());
			average += success * designComparerCoeff;
		}
		return AssistantUtil.round(average / accumCoeffs); // divided by acummCoeffs to normalize it
	}

}
