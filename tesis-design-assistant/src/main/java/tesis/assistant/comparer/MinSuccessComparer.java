package tesis.assistant.comparer;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import tesis.assistant.db.DesignAssistantDT;
import tesis.assistant.filter.MinSuccessFilter;
import tesis.assistant.util.DesignAssistanceException;

public class MinSuccessComparer extends CompositeComparer {

	protected MinSuccessFilter minSuccessFilter;
	protected int maxResults;

	public MinSuccessComparer(double minSuccess, int maxResults) {
		this.minSuccessFilter = new MinSuccessFilter(minSuccess);
		this.maxResults = maxResults;
	}

	@Override
	public List<DesignAssistantDT> compare(List<DesignAssistantDT> diagramTypes, String userDiagram,
			List<String> attributes, String serviceType) throws DesignAssistanceException {

		List<DesignAssistantDT> compareResults = super.compare(diagramTypes, userDiagram, attributes, serviceType);

		// filter results
		compareResults = this.minSuccessFilter.filter(compareResults);

		// order valid results by success descendant
		Collections.sort(compareResults, SUCCESS_COMPARATOR);

		// take only 'maxResults' from the beginning
		int toIndex = Math.min(this.maxResults, compareResults.size());
		return compareResults.subList(0, toIndex);
	}

	protected static final Comparator<DesignAssistantDT> SUCCESS_COMPARATOR = new Comparator<DesignAssistantDT>() {

		// compare success value, descendant order
		@Override
		public int compare(DesignAssistantDT d1, DesignAssistantDT d2) {
			if (d1.getDiagram().getSuccess() < d2.getDiagram().getSuccess()) {
				return 1;
			}
			if (d1.getDiagram().getSuccess() > d2.getDiagram().getSuccess()) {
				return -1;
			}
			return d1.getDiagram().getName().compareTo(d2.getDiagram().getName());
		}
	};

}
