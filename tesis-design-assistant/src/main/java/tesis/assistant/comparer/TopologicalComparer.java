package tesis.assistant.comparer;

import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FilenameUtils;

import tesis.assistant.contants.AssistantConstants;
import tesis.assistant.db.DesignAssistantDT;
import tesis.assistant.modelparser.ModelParser;
import tesis.assistant.modelparser.ModelParserManager;
import tesis.assistant.util.AssistantUtil;
import tesis.assistant.util.DesignAssistanceException;
import algorithms.GraphMatching;

public class TopologicalComparer implements DesignComparer {

	public static final double MIN_DISTANCE_VALUE = 0.0;
	public static final double MAX_DISTANCE_VALUE = 20.0;

	@Override
	public List<DesignAssistantDT> compare(List<DesignAssistantDT> diagramTypes, String userDiagram,
			List<String> attributes, String serviceType) throws DesignAssistanceException {

		if (diagramTypes.isEmpty()) {
			return diagramTypes;
		}

		try {
			// temporally save user diagram on disk
			Path userGmtPath = saveGmtDesign(serviceType, userDiagram);

			for (DesignAssistantDT diagramType : diagramTypes) {

				// temporally save pattern diagram on disk
				Path gmtPath = saveGmtDesign(serviceType, diagramType.getDesign());
				// temporally save GMT configuration file on disk
				Path propertiesPath = saveGmtProperties(userGmtPath, gmtPath);

				// perform topological comparison
				GraphMatching graphMatching = new GraphMatching(propertiesPath.toString());

				// access private field distanceMatrix
				Field field = GraphMatching.class.getDeclaredField("distanceMatrix");
				field.setAccessible(true);
				double[][] d = (double[][]) field.get(graphMatching);

				double success = AssistantUtil.normalize(d[0][0], MIN_DISTANCE_VALUE, MAX_DISTANCE_VALUE,
						AssistantConstants.MIN_ASSIST_SUCCESS, AssistantConstants.MAX_ASSIST_SUCCESS, true);

				diagramType.getDiagram().setSuccess(success);
			}

		} catch (Exception e) {
			throw new DesignAssistanceException("Error comparing diagrams", e);
		}

		return new ArrayList<>(diagramTypes);
	}

	/**
	 * 
	 * @param serviceType
	 * @param diagram
	 * @return
	 * @throws IOException
	 */
	private static Path saveGmtDesign(String serviceType, String diagram) throws IOException {
		ModelParser<String> modelParser = ModelParserManager.getModelParser(serviceType, "Gmt");
		String gmtModel = modelParser.parse(diagram);
		Path designPath = AssistantUtil.writeStringToFile(AssistantConstants.TEMP_DIR_PATH, gmtModel, "xml");
		String gmtInputFile = "<?xml version=\"1.0\"?><GraphCollection><graphs><print file=\"" + designPath.getFileName()
				+ "\"/></graphs></GraphCollection>";
		return AssistantUtil.writeStringToFile(AssistantConstants.TEMP_DIR_PATH, gmtInputFile, "xml");
	}

	/**
	 * 
	 * @param gmtPath1
	 * @param gmtPath2
	 * @return
	 * @throws IOException
	 */
	private static Path saveGmtProperties(Path gmtPath1, Path gmtPath2) throws IOException {
		Path propertiesTemplatePath = Paths.get(AssistantConstants.COMPARER_GMT_PROPERTIES_TEMPLATE);
		String propertiesTemplate = new String(Files.readAllBytes(propertiesTemplatePath), "UTF-8");
		propertiesTemplate = propertiesTemplate.replaceAll("\\$\\{source_path\\}", FilenameUtils.separatorsToUnix(gmtPath1.toString()));
		propertiesTemplate = propertiesTemplate.replaceAll("\\$\\{target_path\\}", FilenameUtils.separatorsToUnix(gmtPath2.toString()));
		propertiesTemplate = propertiesTemplate.replaceAll("\\$\\{temp_path\\}", FilenameUtils.separatorsToUnix(AssistantConstants.TEMP_DIR_PATH) + "/");
		return AssistantUtil.writeStringToFile(AssistantConstants.TEMP_DIR_PATH, propertiesTemplate, "prop");
	}

}
