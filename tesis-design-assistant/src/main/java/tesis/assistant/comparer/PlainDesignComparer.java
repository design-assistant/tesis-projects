package tesis.assistant.comparer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import model.data.generalStructure.Design;
import model.data.generalStructure.classDiagram.ClassDiagram;
import model.group.PlainDesignBox;

import org.springframework.beans.factory.annotation.Autowired;

import presenter.SimpleUserPresenter;
import presenter.comparition.PlainDesignBoxCompared;
import presenter.logic.local.LocalComparer;
import tesis.assistant.contants.AssistantConstants;
import tesis.assistant.db.DesignAssistantDT;
import tesis.assistant.db.DesignAssistantDiagram;
import tesis.assistant.modelparser.ModelParser;
import tesis.assistant.modelparser.ModelParserManager;
import tesis.assistant.util.AssistantUtil;
import tesis.assistant.util.DesignAssistanceException;

public class PlainDesignComparer implements DesignComparer {

	public static final double MIN_COMPR_VALUE = 0.0;
	public static final double MAX_COMPR_VALUE = 160.0;
	public static final double MIN_TRANS_VALUE = 0.0;
	public static final double MAX_TRANS_VALUE = 2000.0;

	public static final double PLAIN_COMPR_WEIGHT = 0.35;
	public static final double PLAIN_TRANS_WEIGHT = 0.65;

	private static final Object LOCK = new Object();

	@Autowired
	private LocalComparer localComparer;
	@Autowired
	private SimpleUserPresenter presenter;

	@Override
	public List<DesignAssistantDT> compare(List<DesignAssistantDT> diagramTypes, String userDiagram,
			List<String> attributes, String serviceType) throws DesignAssistanceException {

		if (diagramTypes.isEmpty()) {
			return diagramTypes;
		}

		// it is static-synchronized because of the use of the Repository class in design-comparer project
		synchronized (LOCK) {

			try {
				// get the ClassDiagram from the raw diagram
				Map<String, ClassDiagram> classDiagrams = calculateClassDiagrams(diagramTypes, serviceType);
				ClassDiagram userClassDiagram = calculateClassDiagram(userDiagram, null, serviceType);

				// get the plain designs from ClassDiagrams
				List<PlainDesignBox> plainDesignBoxes = getPlainBoxDesigns(classDiagrams);
				PlainDesignBox plainUserDesginBox = getPlainBoxDesign(userClassDiagram);

				// compare designs
				List<PlainDesignBoxCompared> comparedPlains = localComparer.obtainFinalResult(plainDesignBoxes, plainUserDesginBox, attributes);

				// set the success to every result diagram
				for (PlainDesignBoxCompared comparedPlain : comparedPlains) {

					double normalizedComprValue = AssistantUtil.normalize(comparedPlain.getComparationValue(), MIN_COMPR_VALUE,
							MAX_COMPR_VALUE, AssistantConstants.MIN_ASSIST_SUCCESS, AssistantConstants.MAX_ASSIST_SUCCESS, true);

					double normalizedTransValue = AssistantUtil.normalize(comparedPlain.getTransformationValue(), MIN_TRANS_VALUE,
							MAX_TRANS_VALUE, AssistantConstants.MIN_ASSIST_SUCCESS, AssistantConstants.MAX_ASSIST_SUCCESS, true);

					double success = PLAIN_COMPR_WEIGHT * normalizedComprValue + PLAIN_TRANS_WEIGHT * normalizedTransValue;

					DesignAssistantDT diagramType = getDiagramByName(diagramTypes, comparedPlain.getPlain().getDesign().getName());
					diagramType.getDiagram().setSuccess(success);
				}

			} catch (Exception e) {
				throw new DesignAssistanceException("Error comparing diagrams", e);
			}

			return new ArrayList<>(diagramTypes);

		} // end lock
	}

	/**
	 * 
	 * @param classDiagrams
	 * @return
	 * @throws IOException
	 */
	private List<PlainDesignBox> getPlainBoxDesigns(Map<String, ClassDiagram> classDiagrams) throws IOException {
		List<PlainDesignBox> plainDesignBoxes = new ArrayList<>();
		for (Entry<String, ClassDiagram> entry : classDiagrams.entrySet()) {
			plainDesignBoxes.add(getPlainBoxDesign(entry.getValue()));
		}
		return plainDesignBoxes;
	}

	/**
	 * 
	 * @param classDiagram
	 * @return
	 * @throws IOException
	 */
	private PlainDesignBox getPlainBoxDesign(ClassDiagram classDiagram) throws IOException {
		Design design = new Design();
		design.getDiagrams().add(classDiagram);
		design.setName(classDiagram.getDiagName());
		presenter.loadDesign(design);
		return presenter.getUserDesignBox();
	}

	/**
	 * 
	 * @param diagramTypes
	 * @param serviceType
	 * @return
	 * @throws IOException
	 */
	private Map<String, ClassDiagram> calculateClassDiagrams(List<DesignAssistantDT> diagramTypes, String serviceType) throws IOException {
		Map<String, ClassDiagram> classDiagrams = new LinkedHashMap<>();

		for (DesignAssistantDT diagramType : diagramTypes) {
			DesignAssistantDiagram diagram = diagramType.getDiagram();
			ClassDiagram classDiagram = calculateClassDiagram(diagramType.getDesign(), diagram.getName(), serviceType);
			classDiagrams.put(diagram.getName(), classDiagram);
		}

		return classDiagrams;
	}

	/**
	 * 
	 * @param diagram
	 * @param name
	 * @param serviceType
	 * @return
	 * @throws IOException
	 */
	private ClassDiagram calculateClassDiagram(String diagram, String name, String serviceType) throws IOException {
		ModelParser<ClassDiagram> modelParser = ModelParserManager.getModelParser(serviceType, "DesignComparer");
		ClassDiagram classDiagram = modelParser.parse(diagram);
		classDiagram.setDiagName(name);
		return classDiagram;
	}

	/**
	 * 
	 * @param diagramTypes
	 * @param name
	 * @return
	 */
	private static DesignAssistantDT getDiagramByName(List<DesignAssistantDT> diagramTypes, String name) {
		for (DesignAssistantDT diagramType : diagramTypes) {
			if (name.equals(diagramType.getDiagram().getName())) {
				return diagramType;
			}
		}
		return null;
	}

}
