package tesis.assistant.comparer;

import java.util.List;

import tesis.assistant.db.DesignAssistantDT;
import tesis.assistant.util.DesignAssistanceException;

public interface DesignComparer {

	/**
	 * 
	 * @param diagramTypes
	 * @param userDiagram
	 * @param attributes
	 * @param serviceType
	 * @return
	 * @throws DesignAssistanceException
	 */
	public List<DesignAssistantDT> compare(List<DesignAssistantDT> diagramTypes, String userDiagram,
			List<String> attributes, String serviceType) throws DesignAssistanceException;

}
