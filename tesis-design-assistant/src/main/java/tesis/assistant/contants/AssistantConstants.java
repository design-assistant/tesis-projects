package tesis.assistant.contants;

import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.Locale;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

public class AssistantConstants {

	private static final ResourceBundle ASSISTANT_PROPS = ResourceBundle.getBundle("assistant", Locale.ENGLISH);
	private static final Logger LOGGER = Logger.getLogger(AssistantConstants.class);

	public static final String CLASSPATH = getClasspath();
	public static final String TEMP_DIR_PATH = Paths.get(CLASSPATH, ASSISTANT_PROPS.getString("app.temp.dir")).toString();

	// topological comparer settings
	public static final String COMPARER_GMT_PROPERTIES_TEMPLATE = Paths.get(CLASSPATH,
			ASSISTANT_PROPS.getString("comparer.gmt.properties.template")).toString();

	// min success comparer settings
	public static final double COMPARER_MIN_SUCCESS = Double.parseDouble(ASSISTANT_PROPS.getString("comparer.min.success"));
	public static final int COMPARER_MAX_RESULTS = Integer.parseInt(ASSISTANT_PROPS.getString("comparer.max.results"));
	public static final double COMPARER_PLAIN_COEFF = Double.parseDouble(ASSISTANT_PROPS.getString("comparer.plain.coeff"));
	public static final double COMPARER_TOPOLOGICAL_COEFF = Double.parseDouble(ASSISTANT_PROPS.getString("comparer.topological.coeff"));
	public static final double COMPARER_QUALITYATTRIB_COEFF = Double.parseDouble(ASSISTANT_PROPS.getString("comparer.qualityattrib.coeff"));

	// web services credentials
	public static final String WS_LOADDB_USER = ASSISTANT_PROPS.getString("ws.loaddb.user");
	public static final String WS_LOADDB_PASS = ASSISTANT_PROPS.getString("ws.loaddb.pass");
	public static final String WS_ADD_USER = ASSISTANT_PROPS.getString("ws.add.user");
	public static final String WS_ADD_PASS = ASSISTANT_PROPS.getString("ws.add.pass");
	public static final String WS_ASSIST_USER = ASSISTANT_PROPS.getString("ws.assist.user");
	public static final String WS_ASSIST_PASS = ASSISTANT_PROPS.getString("ws.assist.pass");

	public static final double MIN_ASSIST_SUCCESS = 0.0;
	public static final double MAX_ASSIST_SUCCESS = 10.0;

	private static String getClasspath() {
		try {
			return Paths.get(AssistantConstants.class.getResource("/").toURI()).toString();
		} catch (URISyntaxException e) {
			LOGGER.error("", e);
			return null;
		}
	}

}
