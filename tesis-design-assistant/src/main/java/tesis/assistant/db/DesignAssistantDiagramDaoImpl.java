package tesis.assistant.db;

import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

@Repository
public class DesignAssistantDiagramDaoImpl extends GenericDaoImpl<Long, DesignAssistantDiagram> implements DesignAssistantDiagramDao {

	@Override
	public List<DesignAssistantDiagram> getDiagramsByAllAttributes(List<String> qualityAttributes) {
		String hqlQuery = "select d from DesignAssistantDiagram d where :expectedAmount > 0 and ("
				+ "select count(qa) from DesignAssistantQA qa where qa.diagram = d and qa.name in (:qualityAttributes)"
				+ ") = :expectedAmount";
		TypedQuery<DesignAssistantDiagram> query = em.createQuery(hqlQuery, DesignAssistantDiagram.class);
		query.setParameter("qualityAttributes", qualityAttributes);
		query.setParameter("expectedAmount", qualityAttributes == null ? 0 : qualityAttributes.size());
		return query.getResultList();
	}

	@Override
	public List<DesignAssistantDiagram> getDiagramsByAnyAttribute(List<String> qualityAttributes) {
		String hqlQuery = "select d from DesignAssistantDiagram d where :expectedAmount > 0 and ("
				+ "select count(qa) from DesignAssistantQA qa where qa.diagram = d and qa.name in (:qualityAttributes)"
				+ ") > 0";
		TypedQuery<DesignAssistantDiagram> query = em.createQuery(hqlQuery, DesignAssistantDiagram.class);
		query.setParameter("qualityAttributes", qualityAttributes);
		query.setParameter("expectedAmount", qualityAttributes == null ? 0 : qualityAttributes.size());
		return query.getResultList();
	}

	@Override
	public DesignAssistantDiagram getDiagramByName(String name) {
		String hqlQuery = "select d from DesignAssistantDiagram d where d.name = :name";
		TypedQuery<DesignAssistantDiagram> query = em.createQuery(hqlQuery, DesignAssistantDiagram.class);
		query.setParameter("name", name);
		return getSingleResult(query);
	}

}
