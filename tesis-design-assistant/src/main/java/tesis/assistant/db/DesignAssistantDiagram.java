package tesis.assistant.db;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "DIAGRAM")
public class DesignAssistantDiagram extends GenericEntity<Long> {

	@Column(name = "NAME", unique = true, nullable = false)
	private String name;

	@Column(name = "DESCRIPTION", columnDefinition = "MEDIUMTEXT")
	private String description;

	@OneToMany(mappedBy = "diagram", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
	@OrderBy("value desc")
	private Set<DesignAssistantQA> qualityAttributes;

	@OneToMany(mappedBy = "diagram", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
	@OrderBy("type asc")
	private Set<DesignAssistantDT> diagramTypes;

	@Transient
	private double success;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Set<DesignAssistantQA> getQualityAttributes() {
		return qualityAttributes;
	}

	public void setQualityAttributes(Set<DesignAssistantQA> qualityAttributes) {
		this.qualityAttributes = qualityAttributes;
	}

	public Set<DesignAssistantDT> getDiagramTypes() {
		return diagramTypes;
	}

	public void setDiagramTypes(Set<DesignAssistantDT> diagramTypes) {
		this.diagramTypes = diagramTypes;
	}

	public double getSuccess() {
		return success;
	}

	public void setSuccess(double success) {
		this.success = success;
	}

	@Override
	public String toString() {
		return "{" + name + ", " + qualityAttributes + ", " + diagramTypes + ", success=" + success + "}";
	}

}
