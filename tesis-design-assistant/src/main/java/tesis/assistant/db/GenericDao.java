package tesis.assistant.db;

import java.io.Serializable;
import java.util.List;

import javax.persistence.LockModeType;

/**
 * 
 * @param <K>
 *            primary-key class
 * @param <E>
 *            entity class
 */
public interface GenericDao<K extends Serializable, E extends GenericEntity<K>> {

	/*
	 * Basic functionalities
	 */

	/**
	 * Find by primary key. If the entity instance is contained in the persistence context, it is returned from there.
	 * 
	 * @param primaryKey
	 * @return the found entity instance or null if the entity does not exist
	 */
	public E find(K primaryKey);

	/**
	 * Get an instance, whose state may be lazily fetched. Useful when a reference to an entity object is required but not its content, such
	 * as when a reference to it has to be set from another entity object.
	 * 
	 * @param primaryKey
	 * @return the found entity instance
	 */
	public E getReference(K primaryKey);

	/**
	 * Make an instance managed and persistent.
	 * 
	 * @param entity
	 */
	public void persist(E entity);

	/**
	 * Make the instances managed and persistent.
	 * 
	 * @param entities
	 */
	public void persist(List<E> entities);

	/**
	 * Merge the state of the given entity into the current persistence context.
	 * 
	 * @param entity
	 * @return the managed instance that the state was merged to
	 */
	public E merge(E entity);

	/**
	 * Merge the state of the given entities into the current persistence context.
	 * 
	 * @param entities
	 * @return the managed instances that the states were merged to
	 */
	public List<E> merge(List<E> entities);

	/**
	 * Remove the entity instance.
	 * 
	 * @param entity
	 */
	public void remove(E entity);

	/**
	 * Remove the entities instances.
	 * 
	 * @param entities
	 */
	public void remove(List<E> entities);

	/**
	 * Refresh the state of the instance from the database, overwriting changes made to the entity, if any.
	 * 
	 * @param entity
	 */
	public void refresh(E entity);

	/**
	 * Refresh the state of the instances from the database, overwriting changes made to the entity, if any.
	 * 
	 * @param entities
	 */
	public void refresh(List<E> entities);

	/**
	 * Remove the given entity from the persistence context.
	 * 
	 * @param entity
	 */
	public void detach(E entity);

	/**
	 * Remove the given entities from the persistence context.
	 * 
	 * @param entities
	 */
	public void detach(List<E> entities);

	/**
	 * Lock an entity instance that is contained in the persistence context with the specified lock mode type.
	 * 
	 * @param entity
	 * @param lockModeType
	 */
	public void lock(E entity, LockModeType lockModeType);

	/**
	 * Lock the entity instances that are contained in the persistence context with the specified lock mode type.
	 * 
	 * @param entities
	 * @param lockModeType
	 */
	public void lock(List<E> entities, LockModeType lockModeType);

	/**
	 * Synchronize the persistence context to the underlying database.
	 * 
	 */
	public void flush();

	/*
	 * Extended functionalities
	 */

	/**
	 * The name of the entity managed by the concrete DAO
	 * 
	 * @return
	 */
	public String getEntityName();

	/**
	 * The class of the entity managed by the concrete DAO
	 * 
	 * @return
	 */
	public Class<E> getEntityClass();

	/**
	 * 
	 * @param orderBy
	 * @return
	 */
	public List<E> findAll(String... orderBy);

	/**
	 * 
	 * @param page
	 * @param pageSize
	 * @param orderBy
	 * @return
	 */
	public List<E> findAll(int page, int pageSize, String... orderBy);

	/**
	 * 
	 * @return
	 */
	public int count();

}
