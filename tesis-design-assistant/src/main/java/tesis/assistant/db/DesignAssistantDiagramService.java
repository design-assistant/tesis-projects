package tesis.assistant.db;

import java.util.List;

public interface DesignAssistantDiagramService extends GenericService<Long, DesignAssistantDiagram> {

	/**
	 * 
	 * @param qualityAttributes
	 * @return
	 */
	public List<DesignAssistantDiagram> getDiagramsByAllAttributes(List<String> qualityAttributes);

	/**
	 * 
	 * @param qualityAttributes
	 * @return
	 */
	public List<DesignAssistantDiagram> getDiagramsByAnyAttribute(List<String> qualityAttributes);

	/**
	 * 
	 * @param name
	 * @return
	 */
	public DesignAssistantDiagram getDiagramByName(String name);

}
