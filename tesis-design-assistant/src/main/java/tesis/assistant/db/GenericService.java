package tesis.assistant.db;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * @param <K>
 *            primary-key class
 * @param <E>
 *            entity class
 */
public interface GenericService<K extends Serializable, E extends GenericEntity<K>> {

	/*
	 * Basic functionalities
	 */

	/**
	 * Find by primary key. If the entity instance is contained in the persistence context, it is returned from there.
	 * 
	 * @param primaryKey
	 * @return the found entity instance or null if the entity does not exist
	 */
	public E find(K primaryKey);

	/**
	 * Get an instance, whose state may be lazily fetched. Useful when a reference to an entity object is required but not its content, such
	 * as when a reference to it has to be set from another entity object.
	 * 
	 * @param primaryKey
	 * @return the found entity instance
	 */
	public E getReference(K primaryKey);

	/**
	 * Make an instance managed and persistent. Transactional.
	 * 
	 * @param entity
	 */
	public void persist(E entity);

	/**
	 * Make the instances managed and persistent. Transactional.
	 * 
	 * @param entities
	 */
	public void persist(List<E> entities);

	/**
	 * Merge the state of the given entity into the current persistence context. Transactional.
	 * 
	 * @param entity
	 * @return the managed instance that the state was merged to
	 */
	public E merge(E entity);

	/**
	 * Merge the state of the given entities into the current persistence context. Transactional.
	 * 
	 * @param entities
	 * @return the managed instances that the states were merged to
	 */
	public List<E> merge(List<E> entities);

	/**
	 * Remove the entity instance. Transactional.
	 * 
	 * @param entity
	 */
	public void remove(E entity);

	/**
	 * Remove the entities instances. Transactional.
	 * 
	 * @param entities
	 */
	public void remove(List<E> entities);

	/*
	 * Extended functionalities
	 */

	/**
	 * 
	 * @param orderBy
	 * @return
	 */
	public List<E> findAll(String... orderBy);

	/**
	 * 
	 * @param page
	 * @param pageSize
	 * @param orderBy
	 * @return
	 */
	public List<E> findAll(int page, int pageSize, String... orderBy);

	/**
	 * 
	 * @return
	 */
	public int count();

	/**
	 * 
	 * @param pageSize
	 * @return
	 */
	public int getPages(int pageSize);

}
