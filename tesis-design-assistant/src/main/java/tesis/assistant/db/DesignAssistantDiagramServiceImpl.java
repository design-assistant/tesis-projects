package tesis.assistant.db;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class DesignAssistantDiagramServiceImpl extends GenericServiceImpl<Long, DesignAssistantDiagram, DesignAssistantDiagramDao>
		implements DesignAssistantDiagramService {

	@Override
	public List<DesignAssistantDiagram> getDiagramsByAllAttributes(List<String> qualityAttributes) {
		return dao.getDiagramsByAllAttributes(qualityAttributes);
	}

	@Override
	public List<DesignAssistantDiagram> getDiagramsByAnyAttribute(List<String> qualityAttributes) {
		return dao.getDiagramsByAnyAttribute(qualityAttributes);
	}

	@Override
	public DesignAssistantDiagram getDiagramByName(String name) {
		return dao.getDiagramByName(name);
	}

}
