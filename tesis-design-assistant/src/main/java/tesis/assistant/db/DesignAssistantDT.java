package tesis.assistant.db;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Index;

@Entity
@Table(name = "DIAGRAM_TYPE", uniqueConstraints = @UniqueConstraint(columnNames = { "ID_DIAGRAM", "TYPE" }))
public class DesignAssistantDT extends GenericEntity<Long> {

	@ManyToOne
	@JoinColumn(name = "ID_DIAGRAM", nullable = false)
	private DesignAssistantDiagram diagram;

	@Index(name = "DIAGRAM_TYPE_TYPE")
	@Column(name = "TYPE", nullable = false)
	private String type;

	@Column(name = "DESIGN", columnDefinition = "MEDIUMTEXT")
	private String design;

	public DesignAssistantDiagram getDiagram() {
		return diagram;
	}

	public void setDiagram(DesignAssistantDiagram diagram) {
		this.diagram = diagram;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDesign() {
		return design;
	}

	public void setDesign(String design) {
		this.design = design;
	}

	@Override
	public String toString() {
		return type;
	}

}
