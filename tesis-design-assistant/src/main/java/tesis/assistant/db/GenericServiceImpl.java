package tesis.assistant.db;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 * 
 * @param <K>
 *            primary-key class
 * @param <E>
 *            entity class
 * @param <D>
 *            dao class
 */
@Transactional(readOnly = true)
public abstract class GenericServiceImpl<K extends Serializable, E extends GenericEntity<K>, D extends GenericDao<K, E>>
		implements GenericService<K, E> {

	@Autowired
	protected D dao;

	/*
	 * Basic functionalities
	 */

	@Override
	public E find(K primaryKey) {
		return dao.find(primaryKey);
	}

	@Override
	public E getReference(K primaryKey) {
		return dao.getReference(primaryKey);
	}

	@Override
	@Transactional
	public void persist(E entity) {
		dao.persist(entity);
		dao.flush();
		dao.refresh(entity);
	}

	@Override
	@Transactional
	public void persist(List<E> entities) {
		dao.persist(entities);
		dao.flush();
		dao.refresh(entities);
	}

	@Override
	@Transactional
	public E merge(E entity) {
		return dao.merge(entity);
	}

	@Override
	@Transactional
	public List<E> merge(List<E> entities) {
		return dao.merge(entities);
	}

	@Override
	@Transactional
	public void remove(E entity) {
		dao.remove(entity);
	}

	@Override
	@Transactional
	public void remove(List<E> entities) {
		dao.remove(entities);
	}

	/*
	 * Extended functionalities
	 */

	@Override
	public List<E> findAll(String... orderBy) {
		return dao.findAll();
	}

	@Override
	public List<E> findAll(int page, int pageSize, String... orderBy) {
		return dao.findAll(page, pageSize, orderBy);
	}

	@Override
	public int count() {
		return dao.count();
	}

	@Override
	public int getPages(int pageSize) {
		return (int) Math.ceil((double) this.count() / (double) pageSize);
	}

}
