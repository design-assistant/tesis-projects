package tesis.assistant.db;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "QUALITY_ATTRIBUTE", uniqueConstraints = @UniqueConstraint(columnNames = { "ID_DIAGRAM", "NAME" }))
public class DesignAssistantQA extends GenericEntity<Long> {

	@ManyToOne
	@JoinColumn(name = "ID_DIAGRAM", nullable = false)
	private DesignAssistantDiagram diagram;

	@Column(name = "NAME", nullable = false)
	private String name;

	@Column(name = "VALUE")
	private int value;

	public DesignAssistantDiagram getDiagram() {
		return diagram;
	}

	public void setDiagram(DesignAssistantDiagram diagram) {
		this.diagram = diagram;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return name + ":" + value;
	}

}
