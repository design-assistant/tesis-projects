package tesis.assistant.db;

import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class DesignAssistantDTServiceImpl extends GenericServiceImpl<Long, DesignAssistantDT, DesignAssistantDTDao>
		implements DesignAssistantDTService {

	@Override
	public int countByType(String type) {
		return dao.countByType(type);
	}

	@Override
	public List<DesignAssistantDT> getDiagramTypesByAllAttributes(String type, List<String> qualityAttributes) {
		return dao.getDiagramTypesByAllAttributes(type, qualityAttributes);
	}

	@Override
	public List<DesignAssistantDT> getDiagramTypesByAnyAttribute(String type, List<String> qualityAttributes) {
		return dao.getDiagramTypesByAnyAttribute(type, qualityAttributes);
	}

	@Override
	public DesignAssistantDT getDiagramTypeByName(String type, String name) {
		return dao.getDiagramTypeByName(type, name);
	}

	@Override
	public DesignAssistantDT getFromDiagram(String type, DesignAssistantDiagram diagram) {
		return CollectionUtils.select(diagram.getDiagramTypes(), new DesignAssistantDTPredicate(type)).iterator().next();
	}

	@Override
	public boolean removeFromDiagram(String type, DesignAssistantDiagram diagram) {
		return CollectionUtils.filterInverse(diagram.getDiagramTypes(), new DesignAssistantDTPredicate(type));
	}

	private static class DesignAssistantDTPredicate implements Predicate<DesignAssistantDT> {

		private String type;

		public DesignAssistantDTPredicate(String type) {
			this.type = type;
		}

		@Override
		public boolean evaluate(DesignAssistantDT diagramType) {
			return type.equals(diagramType.getType());
		}

	}

}
