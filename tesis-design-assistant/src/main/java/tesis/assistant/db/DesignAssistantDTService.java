package tesis.assistant.db;

import java.util.List;

public interface DesignAssistantDTService extends GenericService<Long, DesignAssistantDT> {

	/**
	 * 
	 * @param type
	 * @return
	 */
	public int countByType(String type);

	/**
	 * 
	 * @param type
	 * @param qualityAttributes
	 * @return
	 */
	public List<DesignAssistantDT> getDiagramTypesByAllAttributes(String type, List<String> qualityAttributes);

	/**
	 * 
	 * @param type
	 * @param qualityAttributes
	 * @return
	 */
	public List<DesignAssistantDT> getDiagramTypesByAnyAttribute(String type, List<String> qualityAttributes);

	/**
	 * 
	 * @param type
	 * @param name
	 * @return
	 */
	public DesignAssistantDT getDiagramTypeByName(String type, String name);

	/**
	 * 
	 * @param type
	 * @param diagram
	 * @return
	 */
	public DesignAssistantDT getFromDiagram(String type, DesignAssistantDiagram diagram);

	/**
	 * 
	 * @param type
	 * @param diagram
	 * @return
	 */
	public boolean removeFromDiagram(String type, DesignAssistantDiagram diagram);

}
