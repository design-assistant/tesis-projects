package tesis.assistant.db;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class DesignAssistantQAServiceImpl extends GenericServiceImpl<Long, DesignAssistantQA, DesignAssistantQADao>
		implements DesignAssistantQAService {

}
