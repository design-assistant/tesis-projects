package tesis.assistant.db;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

import org.apache.commons.lang3.StringUtils;

/**
 * 
 * @param <K>
 *            primary-key class
 * @param <E>
 *            entity class
 */
public abstract class GenericDaoImpl<K extends Serializable, E extends GenericEntity<K>> implements GenericDao<K, E> {

	@PersistenceContext
	protected EntityManager em;

	/**
	 * The class of the entities managed by the concrete DAO
	 */
	protected Class<E> entityClass;

	/**
	 * 
	 */
	@SuppressWarnings("unchecked")
	public GenericDaoImpl() {
		this.entityClass = (Class<E>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[1];
	}

	/*
	 * Basic functionalities
	 */

	@Override
	public E find(K primaryKey) {
		return em.find(entityClass, primaryKey);
	}

	@Override
	public E getReference(K primaryKey) {
		return em.getReference(entityClass, primaryKey);
	}

	@Override
	public void persist(E entity) {
		em.persist(entity);
	}

	@Override
	public void persist(List<E> entities) {
		for (E entity : entities) {
			this.persist(entity);
		}
	}

	@Override
	public E merge(E entity) {
		return em.merge(entity);
	}

	@Override
	public List<E> merge(List<E> entities) {
		List<E> mergedEntities = new ArrayList<E>();
		for (E entity : entities) {
			mergedEntities.add(this.merge(entity));
		}
		return mergedEntities;
	}

	@Override
	public void remove(E entity) {
		entity = em.getReference(entityClass, entity.getId());
		em.remove(entity);
	}

	@Override
	public void remove(List<E> entities) {
		for (E entity : entities) {
			this.remove(entity);
		}
	}

	@Override
	public void refresh(E entity) {
		em.refresh(entity);
	}

	@Override
	public void refresh(List<E> entities) {
		for (E entity : entities) {
			this.refresh(entity);
		}
	}

	@Override
	public void detach(E entity) {
		em.detach(entity);
	}

	@Override
	public void detach(List<E> entities) {
		for (E entity : entities) {
			this.detach(entity);
		}
	}

	@Override
	public void lock(E entity, LockModeType lockModeType) {
		em.lock(entity, lockModeType);
	}

	@Override
	public void lock(List<E> entities, LockModeType lockModeType) {
		for (E entity : entities) {
			this.lock(entity, lockModeType);
		}
	}

	@Override
	public void flush() {
		em.flush();
	}

	/*
	 * Extended functionalities
	 */

	@Override
	public String getEntityName() {
		return this.entityClass.getSimpleName();
	}

	@Override
	public Class<E> getEntityClass() {
		return this.entityClass;
	}

	@Override
	public List<E> findAll(String... orderBy) {
		return getFindAllQuery(orderBy).getResultList();
	}

	@Override
	public List<E> findAll(int page, int pageSize, String... orderBy) {
		TypedQuery<E> query = getFindAllQuery(orderBy);
		query.setFirstResult(page * pageSize);
		query.setMaxResults(pageSize);
		return query.getResultList();
	}

	@Override
	public int count() {
		return em.createQuery("SELECT COUNT(e) FROM " + entityClass.getSimpleName() + " e", Number.class).getSingleResult().intValue();
	}

	/**
	 * 
	 * @param query
	 * @return
	 */
	protected E getSingleResult(TypedQuery<E> query) {
		try {
			return query.getSingleResult();
		} catch (PersistenceException e) {
			return null;
		}
	}

	/**
	 * 
	 * @param orderBy
	 * @return
	 */
	private TypedQuery<E> getFindAllQuery(String... orderBy) {
		String order = orderBy.length > 0 ? " order by e." + StringUtils.join(orderBy, ", e.") : "";
		return em.createQuery("SELECT e FROM " + entityClass.getSimpleName() + " e" + order, entityClass);
	}

}
