package tesis.assistant.db;

import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

@Repository
public class DesignAssistantDTDaoImpl extends GenericDaoImpl<Long, DesignAssistantDT> implements DesignAssistantDTDao {

	public int countByType(String type) {
		String hqlQuery = "select count(t) from DesignAssistantDT t where t.type = :type";
		TypedQuery<Number> query = em.createQuery(hqlQuery, Number.class);
		query.setParameter("type", type);
		return query.getSingleResult().intValue();
	}

	@Override
	public List<DesignAssistantDT> getDiagramTypesByAllAttributes(String type, List<String> qualityAttributes) {
		String hqlQuery = "select t from DesignAssistantDT t, DesignAssistantDiagram d "
				+ "where t.diagram = d and t.type = :type and :expectedAmount > 0 and ("
				+ "select count(qa) from DesignAssistantQA qa where qa.diagram = d and qa.name in (:qualityAttributes)"
				+ ") = :expectedAmount";
		TypedQuery<DesignAssistantDT> query = em.createQuery(hqlQuery, DesignAssistantDT.class);
		query.setParameter("type", type);
		query.setParameter("qualityAttributes", qualityAttributes);
		query.setParameter("expectedAmount", qualityAttributes == null ? 0 : qualityAttributes.size());
		return query.getResultList();
	}

	@Override
	public List<DesignAssistantDT> getDiagramTypesByAnyAttribute(String type, List<String> qualityAttributes) {
		String hqlQuery = "select t from DesignAssistantDT t, DesignAssistantDiagram d "
				+ "where t.diagram = d and t.type = :type and :expectedAmount > 0 and ("
				+ "select count(qa) from DesignAssistantQA qa where qa.diagram = d and qa.name in (:qualityAttributes)"
				+ ") > 0";
		TypedQuery<DesignAssistantDT> query = em.createQuery(hqlQuery, DesignAssistantDT.class);
		query.setParameter("type", type);
		query.setParameter("qualityAttributes", qualityAttributes);
		query.setParameter("expectedAmount", qualityAttributes == null ? 0 : qualityAttributes.size());
		return query.getResultList();
	}

	@Override
	public DesignAssistantDT getDiagramTypeByName(String type, String name) {
		String hqlQuery = "select t from DesignAssistantDT t, DesignAssistantDiagram d "
				+ "where t.diagram = d and t.type = :type and d.name = :name";
		TypedQuery<DesignAssistantDT> query = em.createQuery(hqlQuery, DesignAssistantDT.class);
		query.setParameter("type", type);
		query.setParameter("name", name);
		return getSingleResult(query);
	}

}
