package tesis.assistant.db;

import java.io.Serializable;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * Base class to derive entity classes from.
 * 
 * @param <K>
 *            primary-key class
 */
@MappedSuperclass
public abstract class GenericEntity<K extends Serializable> {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	protected K id;

	/**
	 * Returns the identifier of the entity.
	 * 
	 * @return the id
	 */
	public K getId() {
		return id;
	}

	/**
	 * Sets the identifier of the entity.
	 * 
	 * @param id
	 *            the identifier
	 */
	public void setId(K id) {
		this.id = id;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (this.id == null || obj == null || !(this.getClass().equals(obj.getClass()))) {
			return false;
		}
		GenericEntity<?> that = (GenericEntity<?>) obj;
		return this.id.equals(that.id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return id == null ? 0 : id.hashCode();
	}

}
