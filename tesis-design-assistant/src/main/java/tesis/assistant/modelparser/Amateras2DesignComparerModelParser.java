package tesis.assistant.modelparser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import model.data.generalStructure.Design;
import model.data.generalStructure.Diagram;
import model.data.generalStructure.classDiagram.Agregation;
import model.data.generalStructure.classDiagram.Asociation;
import model.data.generalStructure.classDiagram.Attribute;
import model.data.generalStructure.classDiagram.BoxComponent;
import model.data.generalStructure.classDiagram.Class;
import model.data.generalStructure.classDiagram.ClassDiagram;
import model.data.generalStructure.classDiagram.ClassDiagramComponent;
import model.data.generalStructure.classDiagram.Composition;
import model.data.generalStructure.classDiagram.Dependence;
import model.data.generalStructure.classDiagram.Implementation;
import model.data.generalStructure.classDiagram.Inheritance;
import model.data.generalStructure.classDiagram.Interface;
import model.data.generalStructure.classDiagram.Method;
import model.data.generalStructure.classDiagram.Relation;
import net.java.amateras.uml.classdiagram.model.AggregationModel;
import net.java.amateras.uml.classdiagram.model.Argument;
import net.java.amateras.uml.classdiagram.model.AssociationModel;
import net.java.amateras.uml.classdiagram.model.AttributeModel;
import net.java.amateras.uml.classdiagram.model.ClassModel;
import net.java.amateras.uml.classdiagram.model.CommonEntityModel;
import net.java.amateras.uml.classdiagram.model.CompositeModel;
import net.java.amateras.uml.classdiagram.model.DependencyModel;
import net.java.amateras.uml.classdiagram.model.GeneralizationModel;
import net.java.amateras.uml.classdiagram.model.InterfaceModel;
import net.java.amateras.uml.classdiagram.model.OperationModel;
import net.java.amateras.uml.classdiagram.model.RealizationModel;
import net.java.amateras.uml.model.AbstractUMLConnectionModel;
import net.java.amateras.uml.model.AbstractUMLEntityModel;
import net.java.amateras.uml.model.AbstractUMLModel;
import net.java.amateras.uml.model.RootModel;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import com.thoughtworks.xstream.XStream;

@Component
public class Amateras2DesignComparerModelParser implements ModelParser<ClassDiagram> {

	private static final XStream XSTREAM = new XStream();
	static {
		XSTREAM.setClassLoader(Amateras2DesignComparerModelParser.class.getClassLoader());
	}

	@Override
	public ClassDiagram parse(String amaterasDiagram) throws IOException {
		RootModel rootModel = getRootModel(amaterasDiagram);

		ClassDiagram classDiagram = new ClassDiagram();
		Design design = transformModel(classDiagram, rootModel);
		classDiagram.separateRelationComponents(design);

		return classDiagram;
	}

	private static RootModel getRootModel(String externalModel) throws IOException {
		return (RootModel) XSTREAM.fromXML(externalModel);
	}

	private Design transformModel(ClassDiagram classDiagram, AbstractUMLEntityModel parent) {
		Map<AbstractUMLEntityModel, ClassDiagramComponent> components = new HashMap<>();

		for (AbstractUMLModel element : parent.getChildren()) {
			obtainComponent(classDiagram, components, (AbstractUMLEntityModel) element);
		}

		List<Diagram> diagrams = new ArrayList<Diagram>();
		diagrams.add(classDiagram);

		Design design = new Design();
		design.setDiagrams(diagrams);

		return design;
	}

	private ClassDiagramComponent obtainComponent(ClassDiagram classDiagram, Map<AbstractUMLEntityModel, ClassDiagramComponent> components,
			AbstractUMLEntityModel element) {

		if (components.containsKey(element)) {
			return components.get(element);
		}

		ClassDiagramComponent classDiagramComponent = null;
		List<Attribute> attributes = new ArrayList<>();
		List<Method> methods = new ArrayList<>();

		obtainAttMet(element, methods, attributes);

		if (element instanceof ClassModel) {
			// avoid repeated components' names
			remaneComponent(element, components);

			classDiagramComponent = new Class(((ClassModel) element).getName(), methods, attributes, ((ClassModel) element).isAbstract());

			components.put(element, classDiagramComponent);

			List<AbstractUMLConnectionModel> targets = element.getModelTargetConnections();
			for (AbstractUMLConnectionModel abstractUMLConnectionModel : targets) {
				if (element != abstractUMLConnectionModel.getSource()) {
					obtainRelation(classDiagram, components, classDiagramComponent, abstractUMLConnectionModel);
				}
			}

		}

		if (element instanceof InterfaceModel) {
			// avoid repeated components' names
			remaneComponent(element, components);

			classDiagramComponent = new Interface(((InterfaceModel) element).getName(), methods);

			components.put(element, classDiagramComponent);

			List<AbstractUMLConnectionModel> targets = element.getModelTargetConnections();
			for (AbstractUMLConnectionModel abstractUMLConnectionModel : targets) {
				if (element != abstractUMLConnectionModel.getSource()) {
					obtainRelation(classDiagram, components, classDiagramComponent, abstractUMLConnectionModel);
				}
			}
		}

		if (classDiagramComponent != null) {
			components.put(element, classDiagramComponent);
			classDiagram.addComponent(classDiagramComponent);
		}

		return classDiagramComponent;
	}

	private Relation obtainRelation(ClassDiagram classDiagram, Map<AbstractUMLEntityModel, ClassDiagramComponent> components,
			ClassDiagramComponent classDiagramComponent, AbstractUMLConnectionModel abstractUMLConnectionModel) {

		Relation relation = null;

		ClassDiagramComponent target = obtainComponent(classDiagram, components, abstractUMLConnectionModel.getSource());

		if (abstractUMLConnectionModel instanceof CompositeModel) {
			relation = new Composition(String.valueOf(System.nanoTime()), (BoxComponent) classDiagramComponent, (BoxComponent) target);
		} else if (abstractUMLConnectionModel instanceof AggregationModel) {
			relation = new Agregation(String.valueOf(System.nanoTime()), (BoxComponent) classDiagramComponent, (BoxComponent) target);
		} else if (abstractUMLConnectionModel instanceof AssociationModel) {
			relation = new Asociation(String.valueOf(System.nanoTime()), (BoxComponent) classDiagramComponent, (BoxComponent) target);
		} else if (abstractUMLConnectionModel instanceof DependencyModel) {
			relation = new Dependence(String.valueOf(System.nanoTime()), (BoxComponent) classDiagramComponent, (BoxComponent) target);
		} else if (abstractUMLConnectionModel instanceof RealizationModel) {
			relation = new Implementation(String.valueOf(System.nanoTime()), (BoxComponent) classDiagramComponent, (BoxComponent) target);
		} else if (abstractUMLConnectionModel instanceof GeneralizationModel) {
			relation = new Inheritance(String.valueOf(System.nanoTime()), (BoxComponent) classDiagramComponent, (BoxComponent) target);
		}

		if (relation != null) {
			classDiagram.addComponent(relation);
		}

		return relation;
	}

	private void obtainAttMet(AbstractUMLEntityModel element, List<Method> methods, List<Attribute> attributes) {

		for (AbstractUMLModel child : element.getChildren()) {

			if (child instanceof AttributeModel) {
				Attribute attribute = new Attribute(((AttributeModel) child).getName(), "", ((AttributeModel) child).getType());
				attributes.add(attribute);
			}

			if (child instanceof OperationModel) {
				List<String> parameters = new ArrayList<>();
				for (Argument argument : ((OperationModel) child).getParams()) {
					parameters.add(argument.getName());
				}
				Method method = new Method(((OperationModel) child).getName(), "", StringUtils.join(parameters, ", "),
						((OperationModel) child).getType(), ((OperationModel) child).isAbstract());
				methods.add(method);
			}
		}
	}

	private static void remaneComponent(AbstractUMLEntityModel element, Map<AbstractUMLEntityModel, ClassDiagramComponent> components) {
		if (!(element instanceof CommonEntityModel)) {
			return;
		}

		String name = ((CommonEntityModel) element).getName();
		String newName = name;
		int counter = 1;
		while (CollectionUtils.exists(components.values(), new RepeatedComponentPredicate(newName))) {
			newName = name + "_" + (counter++);
		}
		((CommonEntityModel) element).setName(newName);
	}

	private static final class RepeatedComponentPredicate implements Predicate<ClassDiagramComponent> {
		private String name;

		public RepeatedComponentPredicate(String name) {
			this.name = name;
		}

		@Override
		public boolean evaluate(ClassDiagramComponent component) {
			return name.equalsIgnoreCase(component.getCompName());
		}
	}

}
