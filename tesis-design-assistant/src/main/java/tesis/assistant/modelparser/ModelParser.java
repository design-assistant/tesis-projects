package tesis.assistant.modelparser;

import java.io.IOException;

/**
 * 
 * @param <O>
 *            Output model
 */
public interface ModelParser<O> {

	public O parse(String diagram) throws IOException;

}
