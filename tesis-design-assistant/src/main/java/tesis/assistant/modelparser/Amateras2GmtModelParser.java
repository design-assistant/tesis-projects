package tesis.assistant.modelparser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.java.amateras.uml.classdiagram.model.CommonEntityModel;
import net.java.amateras.uml.model.AbstractUMLConnectionModel;
import net.java.amateras.uml.model.AbstractUMLEntityModel;
import net.java.amateras.uml.model.AbstractUMLModel;
import net.java.amateras.uml.model.RootModel;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import com.thoughtworks.xstream.XStream;

@Component
public class Amateras2GmtModelParser implements ModelParser<String> {

	private static final XStream XSTREAM = new XStream();
	static {
		XSTREAM.setClassLoader(Amateras2GmtModelParser.class.getClassLoader());
	}

	@Override
	public String parse(String amaterasDiagram) throws IOException {
		RootModel rootModel = getRootModel(amaterasDiagram);
		return transformModel(rootModel);
	}

	private static RootModel getRootModel(String externalModel) throws IOException {
		return (RootModel) XSTREAM.fromXML(externalModel);
	}

	private String transformModel(AbstractUMLEntityModel parent) {

		String gmtGraph = "<?xml version=\"1.0\"?><!DOCTYPE gxl SYSTEM \"Tesis Design Assistant\"><gxl>"
				+ "<graph id=\"graphId\" edgeids=\"false\" edgemode=\"undirected\">\n";

		List<String> nodes = new ArrayList<>();
		List<String> edges = new ArrayList<>();

		for (AbstractUMLModel element : parent.getChildren()) {
			Map<String, List<String>> components = obtainComponent((AbstractUMLEntityModel) element, edges);
			nodes.addAll(components.get("nodes"));
			edges.addAll(components.get("edges"));
		}

		gmtGraph += StringUtils.join(nodes, "\n");
		gmtGraph += "\n";
		gmtGraph += StringUtils.join(edges, "\n");

		gmtGraph += "</graph></gxl>";
		return gmtGraph;
	}

	private Map<String, List<String>> obtainComponent(AbstractUMLEntityModel element, List<String> edges) {

		Map<String, List<String>> nodesEdges = new HashMap<>();
		nodesEdges.put("nodes", new ArrayList<String>());
		nodesEdges.put("edges", new ArrayList<String>());

		if (!(element instanceof CommonEntityModel)) {
			return nodesEdges;
		}

		int elementId = System.identityHashCode(element);
		String node = "<node id=\"_" + elementId + "\" />";

		nodesEdges.get("nodes").add(node);

		for (AbstractUMLConnectionModel abstractUMLConnectionModel : element.getModelTargetConnections()) {
			AbstractUMLEntityModel source = abstractUMLConnectionModel.getSource();
			if (source instanceof CommonEntityModel) {
				int sourceId = System.identityHashCode(source);
				String edge1 = "<edge from=\"_" + sourceId + "\" to=\"_" + elementId + "\" />";
				String edge2 = "<edge from=\"_" + elementId + "\" to=\"_" + sourceId + "\" />";

				if (!edges.contains(edge1) && !edges.contains(edge2) ) {
					nodesEdges.get("edges").add(edge1);
				}
			}
		}

		return nodesEdges;
	}

}
