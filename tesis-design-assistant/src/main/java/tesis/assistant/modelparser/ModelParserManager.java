package tesis.assistant.modelparser;

import org.springframework.context.ApplicationContext;

public class ModelParserManager {

	private static ApplicationContext applicationContext;

	/**
	 * 
	 * @param serviceType
	 * @param format
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <O> ModelParser<O> getModelParser(String serviceType, String format) {
		String name = serviceType + "2" + format + "ModelParser";

		if (!applicationContext.containsBean(name)) {
			throw new RuntimeException("Model parser for " + serviceType + " was not found.");
		}

		return applicationContext.getBean(name, ModelParser.class);
	}

	/**
	 * 
	 * @param applicationContext
	 */
	public static void setApplicationContext(ApplicationContext applicationContext) {
		ModelParserManager.applicationContext = applicationContext;
	}

}
