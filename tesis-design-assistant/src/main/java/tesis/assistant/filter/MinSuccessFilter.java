package tesis.assistant.filter;

import java.util.ArrayList;
import java.util.List;

import tesis.assistant.db.DesignAssistantDT;

public class MinSuccessFilter {

	private double minSuccess;

	public MinSuccessFilter(double minSuccess) {
		this.minSuccess = minSuccess;
	}

	public double getMinSuccess() {
		return minSuccess;
	}

	public void setMinSuccess(double minSuccess) {
		this.minSuccess = minSuccess;
	}

	public List<DesignAssistantDT> filter(List<DesignAssistantDT> diagramTypes) {
		List<DesignAssistantDT> results = new ArrayList<>();

		for (DesignAssistantDT diagramType : diagramTypes) {
			if (diagramType.getDiagram().getSuccess() >= this.minSuccess) {
				results.add(diagramType);
			}
		}

		return results;
	}

}
