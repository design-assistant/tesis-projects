package tesis.assistant.service;

import java.util.List;
import java.util.Map;

import tesis.assistant.db.DesignAssistantDT;
import tesis.assistant.util.DesignAssistanceException;

public interface DesignAssistantService {

	/**
	 * 
	 * @param diagram
	 * @param qualityAttributes
	 * @param matchingType
	 * @return
	 * @throws DesignAssistanceException
	 */
	public List<DesignAssistantDT> assist(String diagram, List<String> qualityAttributes, int matchingType)
			throws DesignAssistanceException;

	/**
	 * 
	 * @param diagram
	 * @param name
	 * @param description
	 * @param qualityAttributes
	 * @return
	 * @throws DesignAssistanceException
	 */
	public DesignAssistantDT add(String diagram, String name, String description, Map<String, Integer> qualityAttributes)
			throws DesignAssistanceException;

	/**
	 * 
	 * @param name
	 * @throws DesignAssistanceException
	 */
	public void remove(String name) throws DesignAssistanceException;

	/**
	 * 
	 * @param name
	 * @return
	 * @throws DesignAssistanceException
	 */
	public boolean contains(String name) throws DesignAssistanceException;

	/**
	 * 
	 * @throws DesignAssistanceException
	 */
	public void loadDb() throws DesignAssistanceException;

}
