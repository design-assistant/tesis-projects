package tesis.assistant.service;

import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.collections4.Transformer;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tesis.assistant.comparer.MinSuccessComparer;
import tesis.assistant.comparer.QualityAttributesComparer;
import tesis.assistant.contants.AssistantConstants;
import tesis.assistant.contants.MatchingType;
import tesis.assistant.db.DesignAssistantDT;
import tesis.assistant.db.DesignAssistantDTService;
import tesis.assistant.db.DesignAssistantDiagram;
import tesis.assistant.db.DesignAssistantDiagramService;
import tesis.assistant.db.DesignAssistantQA;
import tesis.assistant.util.DesignAssistanceException;
import tesis.assistant.util.DiagramJsonMapping;

import com.fasterxml.jackson.databind.ObjectMapper;

@Transactional(readOnly = true)
public class DesignAssistantServiceImpl implements DesignAssistantService {

	private static final Logger LOGGER = Logger.getLogger(DesignAssistantServiceImpl.class);
	private static final ObjectMapper MAPPER = new ObjectMapper();

	@Autowired
	private MinSuccessComparer designComparer;
	@Autowired
	private DesignAssistantDiagramService diagramService;
	@Autowired
	private DesignAssistantDTService diagramTypeService;

	private String serviceType;

	public DesignAssistantServiceImpl(String serviceType) {
		this.serviceType = serviceType;
	}

	@Override
	public List<DesignAssistantDT> assist(String diagram, List<String> qualityAttributes, int matchingType)
			throws DesignAssistanceException {

		/*
		 * Parameters check
		 */

		if (StringUtils.isBlank(diagram)) {
			throw new DesignAssistanceException("The diagram is empty!");
		}
		if (CollectionUtils.isEmpty(qualityAttributes)) {
			throw new DesignAssistanceException("Quality attributes list is empty!");
		}
		if (diagramTypeService.countByType(serviceType) == 0) {
			throw new DesignAssistanceException("Diagrams table is empty!");
		}

		try {

			// transform all quality attributes to lower case
			CollectionUtils.transform(qualityAttributes, TO_LOWERCASE_TRANSFORMER);

			/*
			 * Retrieve candidate diagrams
			 */

			List<DesignAssistantDT> diagramTypes = null;

			switch (matchingType) {
			case MatchingType.ALL:
				diagramTypes = diagramTypeService.getDiagramTypesByAllAttributes(serviceType, qualityAttributes);
				break;
			case MatchingType.ANY:
				diagramTypes = diagramTypeService.getDiagramTypesByAnyAttribute(serviceType, qualityAttributes);
				break;
			default:
				throw new DesignAssistanceException("The matching type is not valid!");
			}

			/*
			 * Perform comparisons
			 */
			return designComparer.compare(diagramTypes, diagram, qualityAttributes, serviceType);

		} catch (Exception e) {
			throw new DesignAssistanceException("There was a problem in the assistance.", e);
		}
	}

	@Override
	@Transactional
	public DesignAssistantDT add(String diagram, String name, String description, Map<String, Integer> qualityAttributes)
			throws DesignAssistanceException {

		/*
		 * Parameters check
		 */

		if (StringUtils.isBlank(diagram)) {
			throw new DesignAssistanceException("The diagram is empty!");
		}
		if (StringUtils.isBlank(name)) {
			throw new DesignAssistanceException("The name of the diagram is empty!");
		}
		if (diagramTypeService.getDiagramTypeByName(serviceType, name) != null) {
			throw new DesignAssistanceException("The name '" + name + "' is already in use!");
		}
		if (diagramService.getDiagramByName(name) == null) {
			if (qualityAttributes == null || qualityAttributes.isEmpty()) {
				throw new DesignAssistanceException("Quality attributes list is empty!");
			}
			if (CollectionUtils.exists(qualityAttributes.keySet(), INVALID_ATTRIBUTES_PREDICATE)) {
				throw new DesignAssistanceException("At least one quality attribute is invalid!");
			}
			if (CollectionUtils.exists(qualityAttributes.values(), INVALID_ATTRVALUES_PREDICATE)) {
				throw new DesignAssistanceException("At least one quality attribute value is invalid!");
			}
		}

		try {

			/*
			 * Prepare and persist entities
			 */

			DesignAssistantDiagram designAssistantDiagram = diagramService.getDiagramByName(name);

			// create diagram
			if (designAssistantDiagram == null) {
				designAssistantDiagram = new DesignAssistantDiagram();

				// create quality attributes
				Set<DesignAssistantQA> designAssistantQAs = new LinkedHashSet<>(qualityAttributes.size());
				for (Entry<String, Integer> entry : qualityAttributes.entrySet()) {
					DesignAssistantQA designAssistantQA = new DesignAssistantQA();
					designAssistantQA.setDiagram(designAssistantDiagram);
					designAssistantQA.setName(entry.getKey().toLowerCase(Locale.ENGLISH));
					designAssistantQA.setValue(entry.getValue());
					designAssistantQAs.add(designAssistantQA);
				}

				designAssistantDiagram.setName(name);
				designAssistantDiagram.setDescription(description);
				designAssistantDiagram.setQualityAttributes(designAssistantQAs);

				// persist diagram
				diagramService.persist(designAssistantDiagram);

			// edit diagram
			} else {

				// set description if it doesn't exist
				if (!StringUtils.isBlank(description) && StringUtils.isBlank(designAssistantDiagram.getDescription())) {
					designAssistantDiagram.setDescription(description);
				}

			}

			// create diagram-type
			DesignAssistantDT designAssistantDT = new DesignAssistantDT();
			designAssistantDT.setDiagram(designAssistantDiagram);
			designAssistantDT.setType(serviceType);
			designAssistantDT.setDesign(diagram);

			// merge diagram (diagram-type is created in cascade)
			designAssistantDiagram.getDiagramTypes().add(designAssistantDT);
			designAssistantDiagram = diagramService.merge(designAssistantDiagram);

			return diagramTypeService.getFromDiagram(serviceType, designAssistantDiagram);

		} catch (Exception e) {
			throw new DesignAssistanceException("There was a problem saving the diagram.", e);
		}

	}

	@Override
	@Transactional
	public void remove(String diagramName) throws DesignAssistanceException {

		/*
		 * Parameters check
		 */

		if (StringUtils.isEmpty(diagramName)) {
			throw new DesignAssistanceException("The name of the diagram is empty!");
		}

		try {

			/*
			 * Retrieve and delete entities
			 */

			DesignAssistantDiagram diagram = diagramService.getDiagramByName(diagramName);

									// removes the diagram-type entity from the diagram
			if (diagram != null && diagramTypeService.removeFromDiagram(serviceType, diagram)) {

				// merge changes (the diagram-type is removed thanks to orphanRemoval)
				diagram = diagramService.merge(diagram);

				// if it was the only one diagram-type, removes the diagram
				if (diagram.getDiagramTypes().isEmpty()) {
					diagramService.remove(diagram);
				}
			}

		} catch (Exception e) {
			throw new DesignAssistanceException("There was a problem deleting the diagram.", e);
		}

	}

	@Override
	public boolean contains(String diagramName) throws DesignAssistanceException {

		/*
		 * Parameters check
		 */

		if (StringUtils.isEmpty(diagramName)) {
			throw new DesignAssistanceException("The name of the diagram is empty!");
		}

		try {

			/*
			 * Check if entity exists
			 */

			return diagramTypeService.getDiagramTypeByName(serviceType, diagramName) != null;

		} catch (Exception e) {
			throw new DesignAssistanceException("There was a problem while getting diagram.", e);
		}

	}

	@Override
	@Transactional
	public void loadDb() throws DesignAssistanceException {
		boolean exception = false;

		if (diagramTypeService.countByType(serviceType) > 0) {
			throw new DesignAssistanceException("DB is not empty!");
		}

		try {

			/*
			 * Iterate over JSON files and parse them
			 */

			List<DiagramJsonMapping> diagrams = new ArrayList<>();
			try (DirectoryStream<Path> ds = Files.newDirectoryStream(Paths.get(AssistantConstants.CLASSPATH, "diagrams", serviceType),
					"*.{json}")) {
				for (Path jsonPath : ds) {

					// parse diagram
					DiagramJsonMapping diagram = MAPPER.readValue(jsonPath.toFile(), DiagramJsonMapping.class);
					diagrams.add(diagram);
				}
			}

			/*
			 * Store diagrams into DB
			 */

			for (DiagramJsonMapping diagram : diagrams) {

				// tries to save as many diagrams as possible
				try {

					// add diagram
					this.add(diagram.getDesign(), diagram.getName(), diagram.getDescription(), diagram.getQualityAttributes());

				} catch (DesignAssistanceException e) {
					exception = true;
					LOGGER.error("Error saving diagram: " + diagram.getName(), e);
				}
			}

		} catch (Exception e) {
			throw new DesignAssistanceException("There was a problem loading the DB.", e);
		}

		if (exception) {
			throw new DesignAssistanceException("Some diagram(s) couldn't be loaded.");
		}

	}

	private static final Predicate<String> INVALID_ATTRIBUTES_PREDICATE = new Predicate<String>() {
		public boolean evaluate(String qualityAttribute) {
			return StringUtils.isBlank(qualityAttribute);
		}
	};

	private static final Predicate<Integer> INVALID_ATTRVALUES_PREDICATE = new Predicate<Integer>() {
		public boolean evaluate(Integer value) {
			return value == null
					|| value < QualityAttributesComparer.MIN_ATTRIB_VALUE
					|| value > QualityAttributesComparer.MAX_ATTRIB_VALUE;
		}
	};

	private static final Transformer<String, String> TO_LOWERCASE_TRANSFORMER = new Transformer<String, String>() {
		@Override
		public String transform(String input) {
			return input.toLowerCase(Locale.ENGLISH);
		}
	};

}
