package tesis.assistant.service;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.ApplicationContext;

public class DesignAssistantServiceManager {

	private static ApplicationContext applicationContext;

	/**
	 * 
	 * @param serviceType
	 * @return
	 */
	public static DesignAssistantService getService(String serviceType) {
		String name = "designAssistantService" + StringUtils.capitalize(StringUtils.lowerCase(serviceType));

		if (!applicationContext.containsBean(name)) {
			throw new RuntimeException("Service not found: " + serviceType);
		}

		return applicationContext.getBean(name, DesignAssistantService.class);
	}

	/**
	 * 
	 * @param applicationContext
	 */
	public static void setApplicationContext(ApplicationContext applicationContext) {
		DesignAssistantServiceManager.applicationContext = applicationContext;
	}

}
