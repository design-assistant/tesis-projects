package tesis.assistant.util;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import tesis.assistant.service.DesignAssistantService;
import tesis.assistant.service.DesignAssistantServiceImpl;

@Configuration
@Import({ AppConfig.class })
public class TestAppConfig {

	/*
	 * DESIGN ASISTANT SERVICE INITIALIZATION
	 */

	@Bean // test
	public DesignAssistantService designAssistantServiceTest() {
		return new DesignAssistantServiceImpl("test");
	}

}
