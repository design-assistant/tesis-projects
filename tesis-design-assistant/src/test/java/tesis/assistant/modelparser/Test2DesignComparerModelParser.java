package tesis.assistant.modelparser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import model.data.generalStructure.Design;
import model.data.generalStructure.Diagram;
import model.data.generalStructure.classDiagram.Agregation;
import model.data.generalStructure.classDiagram.Asociation;
import model.data.generalStructure.classDiagram.Attribute;
import model.data.generalStructure.classDiagram.BoxComponent;
import model.data.generalStructure.classDiagram.Class;
import model.data.generalStructure.classDiagram.ClassDiagram;
import model.data.generalStructure.classDiagram.ClassDiagramComponent;
import model.data.generalStructure.classDiagram.Composition;
import model.data.generalStructure.classDiagram.Dependence;
import model.data.generalStructure.classDiagram.Implementation;
import model.data.generalStructure.classDiagram.Inheritance;
import model.data.generalStructure.classDiagram.Interface;
import model.data.generalStructure.classDiagram.Method;
import model.data.generalStructure.classDiagram.Relation;
import net.java.amateras.uml.classdiagram.model.AggregationModel;
import net.java.amateras.uml.classdiagram.model.Argument;
import net.java.amateras.uml.classdiagram.model.AssociationModel;
import net.java.amateras.uml.classdiagram.model.AttributeModel;
import net.java.amateras.uml.classdiagram.model.ClassModel;
import net.java.amateras.uml.classdiagram.model.CompositeModel;
import net.java.amateras.uml.classdiagram.model.DependencyModel;
import net.java.amateras.uml.classdiagram.model.GeneralizationModel;
import net.java.amateras.uml.classdiagram.model.InterfaceModel;
import net.java.amateras.uml.classdiagram.model.OperationModel;
import net.java.amateras.uml.classdiagram.model.RealizationModel;
import net.java.amateras.uml.model.AbstractUMLConnectionModel;
import net.java.amateras.uml.model.AbstractUMLEntityModel;
import net.java.amateras.uml.model.AbstractUMLModel;
import net.java.amateras.uml.model.RootModel;

import com.thoughtworks.xstream.XStream;

@Component
public class Test2DesignComparerModelParser implements ModelParser<ClassDiagram> {

	private static final XStream XSTREAM = new XStream();
	static {
		XSTREAM.setClassLoader(Amateras2DesignComparerModelParser.class.getClassLoader());
	}

	public ClassDiagram parse(String amaterasDiagram) throws IOException {
		RootModel rootModel = getRootModel(amaterasDiagram);

		ClassDiagram classDiagram = new ClassDiagram();
		Design design = transformModel(classDiagram, rootModel);
		classDiagram.separateRelationComponents(design);

		return classDiagram;
	}

	private static RootModel getRootModel(String externalModel) throws IOException {
		return (RootModel) XSTREAM.fromXML(externalModel);
	}

	private Design transformModel(ClassDiagram classDiagram, AbstractUMLEntityModel parent) {
		Map<AbstractUMLEntityModel, ClassDiagramComponent> components = new HashMap<>();

		List<AbstractUMLModel> children = parent.getChildren();
		for (Iterator<AbstractUMLModel> iter = children.iterator(); iter.hasNext();) {
			AbstractUMLModel element = iter.next();
			obtainComponent(classDiagram, components, (AbstractUMLEntityModel) element);
		}

		List<Diagram> diagrams = new ArrayList<Diagram>();
		diagrams.add(classDiagram);

		Design design = new Design();
		design.setDiagrams(diagrams);

		return design;
	}

	private ClassDiagramComponent obtainComponent(ClassDiagram classDiagram, Map<AbstractUMLEntityModel, ClassDiagramComponent> components, AbstractUMLEntityModel element) {

		if (components.containsKey(element)) {
			return components.get(element);
		}

		ClassDiagramComponent classDiagramComponent = null;
		List<Attribute> attributes = new ArrayList<>();
		List<Method> methods = new ArrayList<>();

		obtainAttMet(element, methods, attributes);

		if (element instanceof ClassModel) {

//			System.out.println(element + " - " + ((ClassModel) element).getName());
			classDiagramComponent = new Class(((ClassModel) element).getName(), methods, attributes, ((ClassModel) element).isAbstract());

			components.put(element, classDiagramComponent);

			List<AbstractUMLConnectionModel> targets = ((ClassModel) element).getModelTargetConnections();
			for (AbstractUMLConnectionModel abstractUMLConnectionModel : targets) {
//				System.out.println(element + " - " + abstractUMLConnectionModel.getSource());
				if (element != abstractUMLConnectionModel.getSource()) {
					obtainRelation(classDiagram, components, classDiagramComponent, abstractUMLConnectionModel);
				}
			}

		}

		if (element instanceof InterfaceModel) {
//			System.out.println(element + " - " + ((InterfaceModel) element).getName());
			classDiagramComponent = new Interface(((InterfaceModel) element).getName(), methods);

			components.put(element, classDiagramComponent);

			List<AbstractUMLConnectionModel> targets = ((InterfaceModel) element).getModelTargetConnections();
			for (AbstractUMLConnectionModel abstractUMLConnectionModel : targets) {
//				System.out.println(element + " - " + abstractUMLConnectionModel.getSource());
				if (element != abstractUMLConnectionModel.getSource()) {
					obtainRelation(classDiagram, components, classDiagramComponent, abstractUMLConnectionModel);
				}
			}
		}

		if (classDiagramComponent != null) {
			components.put(element, classDiagramComponent);
			classDiagram.addComponent(classDiagramComponent);
		}

		return classDiagramComponent;
	}

	private Relation obtainRelation(ClassDiagram classDiagram, Map<AbstractUMLEntityModel, ClassDiagramComponent> components, ClassDiagramComponent classDiagramComponent, AbstractUMLConnectionModel abstractUMLConnectionModel) {

		Relation relation = null;

		ClassDiagramComponent target = obtainComponent(classDiagram, components, abstractUMLConnectionModel.getSource());

		if (abstractUMLConnectionModel instanceof CompositeModel) {
//			System.out.println(classDiagramComponent.getCompName() + "   --> Composicion" + target.getCompName());
			relation = new Composition(String.valueOf(System.nanoTime()), (BoxComponent) classDiagramComponent,
					(BoxComponent) target);
		} else if (abstractUMLConnectionModel instanceof AggregationModel) {
//			System.out.println(classDiagramComponent.getCompName() + "   --> Agregation" + target.getCompName());
			relation = new Agregation(String.valueOf(System.nanoTime()), (BoxComponent) classDiagramComponent,
					(BoxComponent) target);
		} else if (abstractUMLConnectionModel instanceof AssociationModel) {
//			System.out.println(classDiagramComponent.getCompName() + "   --> Asociacion" + target.getCompName());
			relation = new Asociation(String.valueOf(System.nanoTime()), (BoxComponent) classDiagramComponent,
					(BoxComponent) target);
		} else if (abstractUMLConnectionModel instanceof DependencyModel) {
//			System.out.println(classDiagramComponent.getCompName() + "   --> Dependencia" + target.getCompName());
			relation = new Dependence(String.valueOf(System.nanoTime()), (BoxComponent) classDiagramComponent,
					(BoxComponent) target);
		} else if (abstractUMLConnectionModel instanceof RealizationModel) {
//			System.out.println(classDiagramComponent.getCompName() + "   --> Implementacion" + target.getCompName());
			relation = new Implementation(String.valueOf(System.nanoTime()), (BoxComponent) classDiagramComponent,
					(BoxComponent) target);
		} else if (abstractUMLConnectionModel instanceof GeneralizationModel) {
//			System.out.println(classDiagramComponent.getCompName() + "   --> Generalizacion" + target.getCompName());
			relation = new Inheritance(String.valueOf(System.nanoTime()), (BoxComponent) classDiagramComponent,
					(BoxComponent) target);
		}

		if (relation != null) {
			classDiagram.addComponent(relation);
		}

		return relation;
	}

	private void obtainAttMet(AbstractUMLEntityModel element, List<Method> methods, List<Attribute> attributes) {

		List<AbstractUMLModel> children = element.getChildren();
		for (Iterator<AbstractUMLModel> iter = children.iterator(); iter.hasNext();) {
			AbstractUMLModel child = iter.next();

			if (child instanceof AttributeModel) {
				Attribute attribute = new Attribute(((AttributeModel) child).getName(), "", ((AttributeModel) child).getType());
				attributes.add(attribute);
			}
			if (child instanceof OperationModel) {
				String parameters = "";
				for (Argument argument : ((OperationModel) child).getParams()) {
					parameters += argument.getName() + ", ";
				}
				if (parameters.length() > 1 && parameters.substring(parameters.length() - 2).equals(", "))
					parameters = parameters.substring(0, parameters.length() - 2);
				Method method = new Method(((OperationModel) child).getName(), "", parameters, ((OperationModel) child).getType(),
						((OperationModel) child).isAbstract());
				methods.add(method);
			}
		}
	}

}
