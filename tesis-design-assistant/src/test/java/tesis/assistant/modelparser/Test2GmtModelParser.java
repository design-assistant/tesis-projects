package tesis.assistant.modelparser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.java.amateras.uml.classdiagram.model.ClassModel;
import net.java.amateras.uml.classdiagram.model.InterfaceModel;
import net.java.amateras.uml.model.AbstractUMLConnectionModel;
import net.java.amateras.uml.model.AbstractUMLEntityModel;
import net.java.amateras.uml.model.AbstractUMLModel;
import net.java.amateras.uml.model.RootModel;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import com.thoughtworks.xstream.XStream;

@Component
public class Test2GmtModelParser implements ModelParser<String> {

	@Override
	public String parse(String amaterasDiagram) throws IOException {
		RootModel rootModel = getRootModel(amaterasDiagram);
		return transformModel(rootModel);
	}

	private static RootModel getRootModel(String externalModel) throws IOException {
		XStream xstream = new XStream();
		xstream.setClassLoader(Amateras2DesignComparerModelParser.class.getClassLoader());
		RootModel rootModel = (RootModel) xstream.fromXML(externalModel);
		return rootModel;
	}

	private String transformModel(AbstractUMLEntityModel parent) {

		String gmtGraph = "<?xml version=\"1.0\"?><!DOCTYPE gxl SYSTEM \"Tesis Design Assistant\"><gxl>"
				+ "<graph id=\"graphId\" edgeids=\"false\" edgemode=\"undirected\">";

		List<String> nodes = new ArrayList<>();
		List<String> edges = new ArrayList<>();

		List<AbstractUMLModel> children = parent.getChildren();
		for (AbstractUMLModel element : children) {
			Map<String, List<String>> components = obtainComponent((AbstractUMLEntityModel) element);
			nodes.addAll(components.get("nodes"));
			edges.addAll(components.get("edges"));
		}

		gmtGraph += StringUtils.join(nodes, "");
		gmtGraph += StringUtils.join(edges, "");

		gmtGraph += "</graph></gxl>";
		return gmtGraph;
	}

	private Map<String, List<String>> obtainComponent(AbstractUMLEntityModel element) {
		Map<String, List<String>> nodesEdges = new HashMap<>();
		nodesEdges.put("nodes", new ArrayList<String>());
		nodesEdges.put("edges", new ArrayList<String>());

		List<AbstractUMLConnectionModel> targets;
		
		if (element instanceof ClassModel) 
			targets = ((ClassModel) element).getModelTargetConnections();
		else
			targets = ((InterfaceModel) element).getModelTargetConnections();
		
		int elementId = System.identityHashCode(element);
		String node = "<node id=\"_" + elementId + "\">";
		node += "<attr name=\"x\"><float>0.0</float></attr>";
		node += "<attr name=\"y\"><float>0.0</float></attr>";
		node += "</node>";

		nodesEdges.get("nodes").add(node);

		for (AbstractUMLConnectionModel abstractUMLConnectionModel : targets) {
			if (element != abstractUMLConnectionModel.getSource()) {
				int sourceId = System.identityHashCode(abstractUMLConnectionModel.getSource());
				String edge = "<edge from=\"_" + sourceId + "\" to=\"_" + elementId +"\">";
				edge += "<attr name=\"orient\"><float>0.0</float></attr>";
				edge += "<attr name=\"angle\"><float>0.0</float></attr>";
				edge += "</edge>";

				nodesEdges.get("edges").add(edge);
			}
		}

		return nodesEdges;
	}

}
