package tesis.assistant;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import tesis.assistant.contants.AssistantConstants;
import tesis.assistant.contants.MatchingType;
import tesis.assistant.db.DesignAssistantDT;
import tesis.assistant.db.DesignAssistantDTService;
import tesis.assistant.db.DesignAssistantDiagram;
import tesis.assistant.db.DesignAssistantDiagramService;
import tesis.assistant.service.DesignAssistantService;
import tesis.assistant.service.DesignAssistantServiceManager;
import tesis.assistant.util.DesignAssistanceException;
import tesis.assistant.util.TestAppConfig;

@ContextConfiguration(classes = TestAppConfig.class)
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class DesignAssistantServiceTest {

	private static final Logger LOGGER = Logger.getLogger(DesignAssistantServiceTest.class);

	private DesignAssistantService designAssistantServiceAmateras;
	private DesignAssistantService designAssistantServiceTest;

	@Autowired
	private DesignAssistantDiagramService designAssistantDiagramService;
	@Autowired
	private DesignAssistantDTService designAssistantDTService;

	private String amaterasDiagram;
	private String testDiagram;
	private String name;
	private String description;
	private Map<String, Integer> qualityAttributes;
	private List<String> qualityAttributesList;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		// clean temporal directory
		FileUtils.forceMkdir(new File(AssistantConstants.TEMP_DIR_PATH));
		FileUtils.cleanDirectory(new File(AssistantConstants.TEMP_DIR_PATH));
	}

	@SuppressWarnings("serial")
	@Before
	public void setUp() throws Exception {
		designAssistantServiceAmateras = DesignAssistantServiceManager.getService("amateras");
		designAssistantServiceTest = DesignAssistantServiceManager.getService("test");

		amaterasDiagram = new String(Files.readAllBytes(Paths.get("./src/test/resources/diagrams/amateras/abstract_factory.cld")), "UTF-8");
		testDiagram = new String(Files.readAllBytes(Paths.get("./src/test/resources/diagrams/test/singleton.ext")), "UTF-8");
		name = "diagram-" + (int) (Math.random() * 1000.0);
		description = "This is a description.";
		qualityAttributes = new HashMap<String, Integer>() {{
			put("flexibilidad", -10 + (int) (Math.random() * 21));
			put("adaptabilidad", -10 + (int) (Math.random() * 21));
			put("escalabilidad", -10 + (int) (Math.random() * 21));
			put("usabilidad", -10 + (int) (Math.random() * 21));
		}};
		qualityAttributesList = new ArrayList<String>(qualityAttributes.keySet());
	}

	@Test
	public void testAssistant() throws Exception {

		LOGGER.info("Init test");

		try {
			// try to populate the DB
			designAssistantServiceAmateras.loadDb();
		} catch (DesignAssistanceException e) {
			// nothing
		}

		List<DesignAssistantDT> assistResult = designAssistantServiceAmateras.assist(amaterasDiagram, qualityAttributesList,
				MatchingType.ANY);

		List<DesignAssistantDiagram> assistantDiagrams = new ArrayList<>();
		for (DesignAssistantDT designAssistantDT : assistResult) {
			assistantDiagrams.add(designAssistantDT.getDiagram());
		}
		LOGGER.info("Result: " + assistantDiagrams);

		assertEquals(8.211, getSuccess(assistResult, "abstract factory"), 0.001);
		assertEquals(-1.0, getSuccess(assistResult, "singleton"), 0.001);
		assertEquals(-1.0, getSuccess(assistResult, "big object"), 0.001);
	}

	@Test
	public void testAdd() throws Exception {

		LOGGER.info("Init test");
		DesignAssistantDT storedDiagram1 = designAssistantServiceAmateras.add(amaterasDiagram, name, description, qualityAttributes);
		DesignAssistantDT storedDiagram2 = designAssistantServiceTest.add(testDiagram, name, description, qualityAttributes);
		LOGGER.info("Result 1: " + storedDiagram1.getDiagram());
		LOGGER.info("Result 2: " + storedDiagram2.getDiagram());

		designAssistantServiceAmateras.remove(name);
		assertNull(designAssistantDTService.getDiagramTypeByName("amateras", name));
		assertNotNull(designAssistantDTService.getDiagramTypeByName("test", name));
		assertNotNull(designAssistantDiagramService.getDiagramByName(name));

		designAssistantServiceTest.remove(name);
		assertNull(designAssistantDTService.getDiagramTypeByName("amateras", name));
		assertNull(designAssistantDTService.getDiagramTypeByName("test", name));
		assertNull(designAssistantDiagramService.getDiagramByName(name));

		assertEquals(qualityAttributes.size(), storedDiagram1.getDiagram().getQualityAttributes().size());
	}

	@Test
	public void testLoadDb() throws Exception {

		LOGGER.info("Init test");
		designAssistantServiceAmateras.loadDb();
	}

	@Test
	public void testSimultaneousRequests() throws Exception {

		LOGGER.info("Init test");
		ExecutorService es = Executors.newFixedThreadPool(5);
		List<Future<Object>> result = es.invokeAll(Arrays.asList(new TestSimultaneousRequests(), new TestSimultaneousRequests(), new TestSimultaneousRequests(), new TestSimultaneousRequests(), new TestSimultaneousRequests()), 30, TimeUnit.SECONDS);

		int done = 0;
		for (Future<Object> future : result) {
			try {
				future.get();
				done++;
			} catch (Exception e) {
				LOGGER.error(e.getLocalizedMessage());
			}
		}

		assertEquals(1, done);
	}

	private static double getSuccess(List<DesignAssistantDT> diagramTypes, String name) {
		for (DesignAssistantDT diagramType : diagramTypes) {
			DesignAssistantDiagram diagram = diagramType.getDiagram();
			if (name.equals(diagram.getName())) {
				return diagram.getSuccess();
			}
		}
		return -1.0;
	}

	private class TestSimultaneousRequests implements Callable<Object> {
		@Override
		public Object call() throws Exception {
			LOGGER.info("Init test");
			DesignAssistantDT storedDiagram1 = designAssistantServiceAmateras.add(amaterasDiagram, name, description, qualityAttributes);
			DesignAssistantDT storedDiagram2 = designAssistantServiceTest.add(testDiagram, name, description, qualityAttributes);
			LOGGER.info("Result 1: " + storedDiagram1.getDiagram());
			LOGGER.info("Result 2: " + storedDiagram2.getDiagram());

			designAssistantServiceAmateras.remove(name);
			assertNull(designAssistantDTService.getDiagramTypeByName("amateras", name));
			assertNotNull(designAssistantDTService.getDiagramTypeByName("test", name));
			assertNotNull(designAssistantDiagramService.getDiagramByName(name));

			designAssistantServiceTest.remove(name);
			assertNull(designAssistantDTService.getDiagramTypeByName("amateras", name));
			assertNull(designAssistantDTService.getDiagramTypeByName("test", name));
			assertNull(designAssistantDiagramService.getDiagramByName(name));

			return null;
		}
	}

}
