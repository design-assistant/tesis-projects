package tesis.comparer;

import java.util.ArrayList;
import java.util.List;

import model.data.generalStructure.Design;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import presenter.SimpleUserPresenter;
import presenter.logic.AbstractDesignComparer;
import tesis.assistant.util.AppConfig;
import context.ImplementationFactory;

public class Comparer {

	enum KindOfMatching {
		FULL, LESS, MORE, LIMIT
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		String designPath = "./src/test/resources/comparer/design/big_object.mtc";

		List<String> qAttribs = new ArrayList<String>();
		qAttribs.add("flexibility");

		int qualityAttribCount = -1;
		int kindOfMatching = KindOfMatching.LIMIT.ordinal() + 1;

		try (AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(AppConfig.class)) {
			SimpleUserPresenter presenter = (SimpleUserPresenter) ImplementationFactory.getBean("userPresenter");
			AbstractDesignComparer localComparer = (AbstractDesignComparer) ImplementationFactory.getBean("localComparer");

			presenter.setComparer(localComparer);
			presenter.setQualityAttributes(qAttribs, qualityAttribCount, kindOfMatching);
			presenter.loadDesign(designPath);
			presenter.compare();
			List<Design> result = presenter.getFinalResult();

			System.out.println("Result:");
			for (Design design : result) {
				System.out.println("    " + design.getName());
			}

		}

	}

}
