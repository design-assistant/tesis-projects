package tesis.amateras;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Iterator;
import java.util.List;

import net.java.amateras.uml.classdiagram.model.AttributeModel;
import net.java.amateras.uml.classdiagram.model.ClassModel;
import net.java.amateras.uml.classdiagram.model.InterfaceModel;
import net.java.amateras.uml.classdiagram.model.OperationModel;
import net.java.amateras.uml.model.AbstractUMLConnectionModel;
import net.java.amateras.uml.model.AbstractUMLEntityModel;
import net.java.amateras.uml.model.AbstractUMLModel;
import net.java.amateras.uml.model.RootModel;

import com.thoughtworks.xstream.XStream;

public class Amateras {

	public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException {

		String filePath = "./src/test/resources/amateras/diagram/big_object.cld";
		String charsetName = "UTF-8";

		try (InputStream in = new FileInputStream(filePath);
				BufferedReader reader = new BufferedReader(new InputStreamReader(in, charsetName))) {

			XStream xstream = new XStream();
			xstream.setClassLoader(Amateras.class.getClassLoader());
			RootModel rootModel = (RootModel) xstream.fromXML(reader);

			validateModel(rootModel);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private static void validateModel(AbstractUMLEntityModel parent) {
		List<AbstractUMLModel> children = parent.getChildren();
		for (Iterator<AbstractUMLModel> iter = children.iterator(); iter.hasNext();) {
			AbstractUMLModel element = iter.next();
			if (element.getParent() == null) {
				element.setParent(parent);
			}
			if (element instanceof AttributeModel) {
				System.out.println("    " + ((AttributeModel) element).getEditableValue());
			}
			if (element instanceof OperationModel) {
				System.out.println("    " + ((OperationModel) element).getEditableValue());
			}
			if (element instanceof ClassModel) {
				System.out.println(((ClassModel) element).getName());
				List<AbstractUMLConnectionModel> sources = ((ClassModel) element).getModelSourceConnections();
				List<AbstractUMLConnectionModel> targets = ((ClassModel) element).getModelTargetConnections();
				for (AbstractUMLConnectionModel abstractUMLConnectionModel : sources) {
					if (abstractUMLConnectionModel.getTarget() instanceof ClassModel) {
						System.out.println("    --> source: " + ((ClassModel) abstractUMLConnectionModel.getTarget()).getName());
					}
					if (abstractUMLConnectionModel.getTarget() instanceof InterfaceModel) {
						System.out.println("    --> source: " + ((InterfaceModel) abstractUMLConnectionModel.getTarget()).getName());
					}
				}
				for (AbstractUMLConnectionModel abstractUMLConnectionModel : targets) {
					if (abstractUMLConnectionModel.getSource() instanceof ClassModel) {
						System.out.println("    --> target: " + ((ClassModel) abstractUMLConnectionModel.getSource()).getName());
					}
					if (abstractUMLConnectionModel.getSource() instanceof InterfaceModel) {
						System.out.println("    --> target: " + ((InterfaceModel) abstractUMLConnectionModel.getSource()).getName());
					}
				}
			}
			if (element instanceof InterfaceModel) {
				System.out.println(((InterfaceModel) element).getName());
				List<AbstractUMLConnectionModel> sources = ((InterfaceModel) element).getModelSourceConnections();
				List<AbstractUMLConnectionModel> targets = ((InterfaceModel) element).getModelTargetConnections();
				for (AbstractUMLConnectionModel abstractUMLConnectionModel : sources) {
					if (abstractUMLConnectionModel.getTarget() instanceof ClassModel) {
						System.out.println("    --> source: " + ((ClassModel) abstractUMLConnectionModel.getTarget()).getName());
					}
					if (abstractUMLConnectionModel.getTarget() instanceof InterfaceModel) {
						System.out.println("    --> source: " + ((InterfaceModel) abstractUMLConnectionModel.getTarget()).getName());
					}
				}
				for (AbstractUMLConnectionModel abstractUMLConnectionModel : targets) {
					if (abstractUMLConnectionModel.getSource() instanceof ClassModel) {
						System.out.println("    --> target: " + ((ClassModel) abstractUMLConnectionModel.getSource()).getName());
					}
					if (abstractUMLConnectionModel.getSource() instanceof InterfaceModel) {
						System.out.println("    --> target: " + ((InterfaceModel) abstractUMLConnectionModel.getSource()).getName());
					}
				}
			}
			if (element instanceof AbstractUMLEntityModel) {
				validateModel((AbstractUMLEntityModel) element);
			}
		}
	}

}
