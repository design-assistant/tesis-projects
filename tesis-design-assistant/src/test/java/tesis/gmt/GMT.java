package tesis.gmt;

import java.io.PrintStream;
import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

import algorithms.GraphMatching;

public class GMT {

	public static void main(String[] args) {

		String propertiesPath = "./src/test/resources/gmt/properties/properties_fingerprint.prop";

		try {
			GraphMatching graphMatching = new GraphMatching(propertiesPath);

			// access private field distanceMatrix
			Field field = GraphMatching.class.getDeclaredField("distanceMatrix");
			field.setAccessible(true);
			double[][] d = (double[][]) field.get(graphMatching);

			DecimalFormat decFormat = (DecimalFormat) NumberFormat.getInstance(Locale.ENGLISH);
			decFormat.applyPattern("0.00000");

			PrintStream out = System.out;
			for (int i = 0; i < d.length; i++) {
				for (int j = 0; j < d[0].length; j++) {
					out.print(decFormat.format(d[i][j]));
					if (j + 1 < d[0].length) {
						out.print(" ");
					}
				}
				out.println();
			}
			out.flush();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
