package dao.hb.util;

import java.util.Iterator;
import java.util.NoSuchElementException;

import context.BaseObject;

@SuppressWarnings("unchecked")
public class PaginatedListIterator extends BaseObject implements Iterator {

	private static final long serialVersionUID = 1L;
	private PaginatedList list;
	private int i = 0;
	private Iterator iterator;

	public PaginatedListIterator(PaginatedList list) {
		this.list = list;
	}

	public boolean hasNext() {
		return i < list.size();
	}

	public Object next() {
		if (i == list.getFirstElement()) {
			iterator = list.getList().iterator();
		}

		if ((i >= list.getFirstElement()) && (i < list.getFirstElement() + list.getMaxElements())) {
			i++;
			return iterator.next();
		}

		if (hasNext()) {
			i++;
			return null;
		} else {
			throw new NoSuchElementException();
		}
	}

	public void remove() {
		throw new UnsupportedOperationException();
	}

}
