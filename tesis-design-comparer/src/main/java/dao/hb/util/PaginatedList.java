package dao.hb.util;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import context.BaseObject;

@SuppressWarnings("unchecked")
public class PaginatedList extends BaseObject implements List {
	private static final long serialVersionUID = 1L;
	private List list;
	private int firstElement, maxElements, size;

	public PaginatedList() {
	}

	public PaginatedList(List list, int firstElement, int maxElements, int size) {
		this.list = list;
		this.firstElement = firstElement;
		this.maxElements = maxElements;
		this.size = size;
	}

	public List getList() {
		return list;
	}

	public void setList(List list) {
		this.list = list;
	}

	public int getFirstElement() {
		return firstElement;
	}

	public void setFirstElement(int firstElement) {
		this.firstElement = firstElement;
	}

	public int getMaxElements() {
		return maxElements;
	}

	public void setMaxElements(int maxElements) {
		this.maxElements = maxElements;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public int getPageSize() {
		return list.size();
	}

	public int getPageNumber() {
		return getFirstElement() / getMaxElements();
	}

	public int getLastPageNumber() {
		return (size() - 1) / getMaxElements();
	}

	public Iterator iterator() {
		return new PaginatedListIterator(this);
	}

	public int size() {
		return size;
	}

	public int getSize() {
		return size();
	}

	public void add(int arg0, Object arg1) {
		throw new UnsupportedOperationException();
	}

	public boolean add(Object arg0) {
		throw new UnsupportedOperationException();
	}

	public boolean addAll(Collection arg0) {
		throw new UnsupportedOperationException();
	}

	public boolean addAll(int arg0, Collection arg1) {
		throw new UnsupportedOperationException();
	}

	public void clear() {
		throw new UnsupportedOperationException();

	}

	public boolean contains(Object arg0) {
		throw new UnsupportedOperationException();
	}

	public boolean containsAll(Collection arg0) {
		throw new UnsupportedOperationException();
	}

	public Object get(int arg0) {
		throw new UnsupportedOperationException();
	}

	public int indexOf(Object arg0) {
		throw new UnsupportedOperationException();
	}

	public boolean isEmpty() {
		throw new UnsupportedOperationException();
	}

	public int lastIndexOf(Object arg0) {
		throw new UnsupportedOperationException();
	}

	public ListIterator listIterator() {
		throw new UnsupportedOperationException();
	}

	public ListIterator listIterator(int arg0) {
		throw new UnsupportedOperationException();
	}

	public Object remove(int arg0) {
		throw new UnsupportedOperationException();
	}

	public boolean remove(Object arg0) {
		throw new UnsupportedOperationException();
	}

	public boolean removeAll(Collection arg0) {
		throw new UnsupportedOperationException();
	}

	public boolean retainAll(Collection arg0) {
		throw new UnsupportedOperationException();
	}

	public Object set(int arg0, Object arg1) {
		throw new UnsupportedOperationException();
	}

	public List subList(int arg0, int arg1) {
		throw new UnsupportedOperationException();
	}

	public Object[] toArray() {
		throw new UnsupportedOperationException();
	}

	public Object[] toArray(Object[] arg0) {
		throw new UnsupportedOperationException();
	}
}
