package dao.hb;

import model.group.QualityAttributeGroup;
import dao.IQualityAttributeGroupDAO;

public class QualityAttributeGroupDAO  extends GenericHBDAO<QualityAttributeGroup, Integer> implements IQualityAttributeGroupDAO{
	
	public QualityAttributeGroupDAO() {
		super(QualityAttributeGroup.class);
	}

}
