package dao.hb;

import java.util.List;

import model.group.PlainDesignBox;
import model.group.QualityAttribute;
import dao.IPlainDesignBoxDAO;

public class PlainDesignBoxDAO extends GenericHBDAO<PlainDesignBox, Integer> implements IPlainDesignBoxDAO {

	public PlainDesignBoxDAO() {
		super(PlainDesignBox.class);
	}


	
	@Override
	public List<PlainDesignBox> findAllByFullQualityAttributesAndComponentsCountAbdsorbedRelationsCountRange(
			List<QualityAttribute> attributes, int componentsMin, int componentsMax, int absorbedMin, int absorvedMax) {
		String attributeList = generateStringAttributeList(attributes);
		boolean wantsAttributes = attributes != null;
		boolean wantsComponentsCount = componentsMax > 0 && componentsMin > 0;
		boolean wantsAbsorbedRelations = absorbedMin > 0 && absorvedMax > 0;
		String query = "select distinct plain from PlainDesignBox as plain ";
		if (wantsAttributes) {
			query += "inner join plain.attributes as attrib ";
		}
		if (wantsComponentsCount || wantsAbsorbedRelations) {
			query += "inner join plain.designs as plaindesign ";
		}
		query += "where 1=1 ";
		if (wantsAttributes) {
			query += "and not exists (select qagroup from QualityAttributeGroup as qagroup where qagroup.plaindesignbox.id = plain.id "
					+ "and qagroup.attribute.name not in( " + attributeList + ")) and plain.attributes.size=" + attributes.size();
		}

		query = addExtraComponentsCriteria(componentsMin, componentsMax, absorbedMin, absorvedMax, wantsComponentsCount,
				wantsAbsorbedRelations, query);
		return findAllByHQL(query, true);

	}

	@Override
	public List<PlainDesignBox> findAllByLessQualityAttributesAndComponentsCountAbdsorbedRelationsCountRange(
			List<QualityAttribute> attributes, int componentsMin, int componentsMax, int absorbedMin, int absorbedMax) {
		String attributeList = generateStringAttributeList(attributes);
		boolean wantsAttributes = attributes != null;
		boolean wantsComponentsCount = componentsMax > 0 && componentsMin > 0;
		boolean wantsAbsorbedRelations = absorbedMin > 0 && absorbedMax > 0;
		String query = "select distinct plain from PlainDesignBox as plain ";
		if (wantsAttributes) {
			query += "inner join plain.attributes as attrib ";
		}
		if (wantsComponentsCount || wantsAbsorbedRelations) {
			query += "inner join plain.designs as plaindesign ";
		}
		query += "where 1=1 ";
		if (wantsAttributes) {
			query += "and not exists (select qagroup from QualityAttributeGroup as qagroup where qagroup.plaindesignbox.id = plain.id "
					+ "and qagroup.attribute.name not in( " + attributeList + ")) ";
		}
		query = addExtraComponentsCriteria(componentsMin, componentsMax, absorbedMin, absorbedMax, wantsComponentsCount,
				wantsAbsorbedRelations, query);
		return findAllByHQL(query, true);
	}

	@Override
	public List<PlainDesignBox> findAllByLimitQualityAttributesAndComponentsCountAbdsorbedRelationsCountRange(
			List<QualityAttribute> attributes, int componentsMin, int componentsMax, int absorbedMin, int absorvedMax, int limit) {
		String attributeList = generateStringAttributeList(attributes);
		boolean wantsAttributes = attributes != null;
		boolean wantsComponentsCount = componentsMax > 0 && componentsMin > 0;
		boolean wantsAbsorbedRelations = absorbedMin > 0 && absorvedMax > 0;
		String query = "select distinct plain from PlainDesignBox as plain ";
		if (wantsAttributes) {
			query += "inner join plain.attributes as attrib ";
		}
		if (wantsComponentsCount || wantsAbsorbedRelations) {
			query += "inner join plain.designs as plaindesign ";
		}
		query += "where 1=1 ";
		if (wantsAttributes) {
			query += "and (select count (qagroup) from QualityAttributeGroup as qagroup where qagroup.plaindesignbox.id = plain.id "
					+ "and qagroup.attribute.name  in( " + attributeList + ")) >=" + limit;
		}
		query = addExtraComponentsCriteria(componentsMin, componentsMax, absorbedMin, absorvedMax, wantsComponentsCount,
				wantsAbsorbedRelations, query);
		return findAllByHQL(query, true);
	}

	@Override
	public List<PlainDesignBox> findAllByMoreQualityAttributesAndComponentsCountAbdsorbedRelationsCountRange(
			List<QualityAttribute> attributes, int componentsMin, int componentsMax, int absorbedMin, int absorvedMax) {
		String attributeList = generateStringAttributeList(attributes);
		boolean wantsAttributes = attributes != null;
		boolean wantsComponentsCount = componentsMax > 0 && componentsMin > 0;
		boolean wantsAbsorbedRelations = absorbedMin > 0 && absorvedMax > 0;
		String query = "select distinct plain from PlainDesignBox as plain ";
		if (wantsAttributes) {
			query += "inner join plain.attributes as attrib ";
		}
		if (wantsComponentsCount || wantsAbsorbedRelations) {
			query += "inner join plain.designs as plaindesign ";
		}
		query += "where 1=1 ";
		if (wantsAttributes) {
			query += "and (select count (qagroup) from QualityAttributeGroup as qagroup where qagroup.plaindesignbox.id = plain.id "
					+ "and qagroup.attribute.name  in( " + attributeList + ")) =" + attributes.size();
		}
		query = addExtraComponentsCriteria(componentsMin, componentsMax, absorbedMin, absorvedMax, wantsComponentsCount,
				wantsAbsorbedRelations, query);
		return findAllByHQL(query, true);
	}

	private String addExtraComponentsCriteria(int componentsMin, int componentsMax, int absorbedMin, int absorvedMax,
			boolean wantsComponentsCount, boolean wantsAbsorbedRelations, String query) {
		if (wantsComponentsCount || wantsAbsorbedRelations) {
			query += " and plaindesign.name='up'";
			if (wantsComponentsCount) {
				query += "and plaindesign.componentsCount " + ">= " + componentsMin + " and plaindesign.componentsCount <= "
						+ componentsMax;
			}

			if (wantsAbsorbedRelations) {
				query += "and plaindesign.absorbedRelations " + ">= " + absorbedMin + " and plaindesign.absorbedRelations <= "
						+ absorvedMax;
			}
		}
		return query;
	}

	private String generateStringAttributeList(List<QualityAttribute> attributes) {
		String attributeList = "";
		for (QualityAttribute qa : attributes) {
			attributeList = attributeList + "'" + qa.getName() + "' , ";
		}
		attributeList = attributeList.substring(0, attributeList.length() - 2);
		return attributeList;
	}
	
}
