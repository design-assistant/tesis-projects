package dao.hb;

import model.data.generalStructure.classDiagram.Asociation;
import dao.IAsociationDAO;

public class AsociationDAO  extends GenericHBDAO<Asociation, Integer> implements IAsociationDAO{
	
	public AsociationDAO() {
		super(Asociation.class);
	}

}
