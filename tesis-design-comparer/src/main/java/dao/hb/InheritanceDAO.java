package dao.hb;

import model.data.generalStructure.classDiagram.Inheritance;
import dao.IInheritanceDAO;

public class InheritanceDAO  extends GenericHBDAO<Inheritance, Integer> implements IInheritanceDAO{
	
	public InheritanceDAO() {
		super(Inheritance.class);
	}

}
