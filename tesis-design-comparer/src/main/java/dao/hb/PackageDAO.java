package dao.hb;

import dao.IPackageDAO;

public class PackageDAO  extends GenericHBDAO<model.data.generalStructure.classDiagram.Package, Integer> implements IPackageDAO{
	
	public PackageDAO() {
		super(model.data.generalStructure.classDiagram.Package.class);
	}

}
