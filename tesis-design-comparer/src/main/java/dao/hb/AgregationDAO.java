package dao.hb;

import model.data.generalStructure.classDiagram.Agregation;
import dao.IAgregationDAO;

public class AgregationDAO extends GenericHBDAO<Agregation, Integer> implements IAgregationDAO {

	public AgregationDAO() {
		super(Agregation.class);
	}

}
