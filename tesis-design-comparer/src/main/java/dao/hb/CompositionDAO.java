package dao.hb;

import model.data.generalStructure.classDiagram.Composition;
import dao.ICompositionDAO;

public class CompositionDAO  extends GenericHBDAO<Composition, Integer> implements ICompositionDAO{
	
	public CompositionDAO() {
		super(Composition.class);
	}

}
