package dao.hb;

import model.data.generalStructure.classDiagram.Implementation;
import dao.IImplementationDAO;

public class ImplementationDAO  extends GenericHBDAO<Implementation, Integer> implements IImplementationDAO{
	
	public ImplementationDAO() {
		super(Implementation.class);
	}

}
