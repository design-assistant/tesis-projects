package dao.hb;

import model.data.generalStructure.classDiagram.Dependence;
import dao.IDependenceDAO;

public class DependenceDAO  extends GenericHBDAO<Dependence, Integer> implements IDependenceDAO{
	
	public DependenceDAO() {
		super(Dependence.class);
	}

}
