package dao.hb;

import model.data.generalStructure.classDiagram.ClassDiagram;
import dao.IClassDiagramDAO;

public class ClassDiagramDAO  extends GenericHBDAO<ClassDiagram, Integer> implements IClassDiagramDAO{
	
	public ClassDiagramDAO() {
		super(ClassDiagram.class);
	}

}
