package dao.hb;

import dao.IClassDAO;

public class ClassDAO  extends GenericHBDAO<model.data.generalStructure.classDiagram.Class, Integer> implements IClassDAO{
	
	public ClassDAO() {
		super(model.data.generalStructure.classDiagram.Class.class);
	}

}
