package dao.hb;

import model.data.generalStructure.classDiagram.Interface;
import dao.IInterfaceDAO;

public class InterfaceDAO  extends GenericHBDAO<Interface, Integer> implements IInterfaceDAO{
	
	public InterfaceDAO() {
		super(Interface.class);
	}

}
