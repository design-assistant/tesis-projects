package dao.hb;

import model.group.QualityAttribute;
import dao.IQualityAttributeDAO;

public class QualityAttributeDAO  extends GenericHBDAO<QualityAttribute, Integer> implements IQualityAttributeDAO{
	
	public QualityAttributeDAO() {
		super(QualityAttribute.class);
	}

}
