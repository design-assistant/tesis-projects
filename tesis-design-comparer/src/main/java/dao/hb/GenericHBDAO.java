package dao.hb;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.MatchMode;
import org.hibernate.metadata.ClassMetadata;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import dao.IGenericDAO;

public class GenericHBDAO<T extends Serializable, PK extends Serializable> extends HibernateDaoSupport implements IGenericDAO<T, PK> {

	private static Logger log = Logger.getLogger(GenericHBDAO.class);

	@SuppressWarnings("unchecked")
	private Class returnedClass;

	public GenericHBDAO() {

	}

	public GenericHBDAO(String returnedClass) {
		try {
			this.returnedClass = Class.forName(returnedClass);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	public GenericHBDAO(Class returnedClass) {
		this.returnedClass = returnedClass;

	}

	@SuppressWarnings("unchecked")
	public Class returnedClass() {
		return this.returnedClass;
	}

	public String className() {
		return this.returnedClass().getName();
	}

	public void delete(T obj) {
		log.debug("Objeto de tipo " + className() + " eliminando");
		getHibernateTemplate().delete(obj);
	}

	@SuppressWarnings("unchecked")
	public Collection<T> filter(Collection<T> collection, String filter) {
		return (Collection<T>) getHibernateTemplate().execute(getFilterCallback(collection, filter));
	}

	@SuppressWarnings("unchecked")
	public List<T> findAll() {
		log.debug("Obteniendo todos los objetos de tipo " + className());
		return getHibernateTemplate().find("from " + className());
	}

	@SuppressWarnings("unchecked")
	public List<T> findAll(String orderBy) {
		log.debug("Obteniendo todos los objetos de tipo " + className() + " ordenados por " + orderBy);
		String query = "from " + className();
		if (orderBy.trim().length() > 0) {
			query += " order by " + orderBy;
		}
		return getHibernateTemplate().find(query);

	}

	@SuppressWarnings("unchecked")
	public List<T> findByExample(T obj) {
		log.debug("Obteniendo todos los Objetos de tipo " + className() + " By Example");
		DetachedCriteria criteria = DetachedCriteria.forClass(returnedClass());
		criteria.add(Example.create(obj).ignoreCase().enableLike(MatchMode.ANYWHERE));
		return getHibernateTemplate().findByCriteria(criteria);
	}

	@SuppressWarnings("unchecked")
	public List<T> findById(Collection<PK> ids) {
		DetachedCriteria criteria = DetachedCriteria.forClass(returnedClass());
		ClassMetadata classMetadata = getSessionFactory().getClassMetadata(returnedClass());
		String idPropertyName = classMetadata.getIdentifierPropertyName();
		criteria.add(Expression.in(idPropertyName, ids));
		return getHibernateTemplate().findByCriteria(criteria);
	}

	@SuppressWarnings("unchecked")
	public T findById(PK id) {
		log.debug("obteniendo objeto de tipo " + className() + " by Id: " + id);
		return (T) getHibernateTemplate().get(returnedClass(), id);
	}

	@SuppressWarnings("unchecked")
	public List<T> findByProperty(String propertyName, Object value) {
		DetachedCriteria criteria = DetachedCriteria.forClass(returnedClass());
		criteria.add(Expression.like(propertyName, value));
		return getHibernateTemplate().findByCriteria(criteria);
	}

	public T findOneByProperty(String propertyName, Object value) {
		T obj = null;
		List<T> results = this.findByProperty(propertyName, value);
		if (results != null && results.size() > 0) {
			obj = results.get(0);
		}
		return obj;
	}

	@SuppressWarnings("unchecked")
	public List<T> findByPropertyContent(String propertyName, String value) {
		DetachedCriteria criteria = DetachedCriteria.forClass(returnedClass());
		criteria.add(Expression.ilike(propertyName, value, MatchMode.ANYWHERE));
		return getHibernateTemplate().findByCriteria(criteria);
	}

	public void persist(T obj) {
		log.debug("Persistiendo objeto de tipo " + className());
		getHibernateTemplate().saveOrUpdate(obj);
	}

	public void refresh(T obj) {
		getHibernateTemplate().refresh(obj);
	}

	@SuppressWarnings("unchecked")
	private static HibernateCallback getFilterCallback(final Collection collection, final String filter) {
		return new HibernateCallback() {

			public Object doInHibernate(Session session) throws HibernateException {
				return session.createFilter(collection, filter).list();
			}
		};
	}

	@SuppressWarnings("unchecked")
	public List<T> findAllByHQL(String query, boolean cacheable, List<String> lista) {
		return getSession().createQuery(query).setParameterList("lista", lista).list();
	}

	@SuppressWarnings("unchecked")
	public List<T> findAllByHQL(String query, boolean cacheable) {
		return getSession().createQuery(query).list();
	}

	@SuppressWarnings("unchecked")
	public List<T> findAllBySQL(String query, boolean cacheable) {
		return getSession().createSQLQuery(query).list();
	}

	@SuppressWarnings("unchecked")
	public List<T> findAllMaxByHQL(String query, int max) {

		Query q = getSession().createQuery(query);
		q.setMaxResults(max);
		return q.list();
	}

	@SuppressWarnings("unchecked")
	public T findOneByHQL(String query) {
		return (T) getSession().createQuery(query).uniqueResult();
	}

}