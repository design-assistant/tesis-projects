package dao.hb;

import model.data.generalStructure.classDiagram.Relation;
import dao.IRelationDAO;

public class RelationDAO  extends GenericHBDAO<Relation, Integer> implements IRelationDAO{
	
	public RelationDAO() {
		super(Relation.class);
	}

}
