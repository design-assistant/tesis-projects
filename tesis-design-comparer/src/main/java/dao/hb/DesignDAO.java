package dao.hb;

import model.data.generalStructure.Design;
import dao.IDesignDAO;

public class DesignDAO  extends GenericHBDAO<Design, Integer> implements IDesignDAO{
	
	public DesignDAO() {
		super(Design.class);
	}

}
