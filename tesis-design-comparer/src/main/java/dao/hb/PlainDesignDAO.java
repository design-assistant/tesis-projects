package dao.hb;

import model.group.PlainDesign;
import dao.IPlainDesignDAO;

public class PlainDesignDAO  extends GenericHBDAO<PlainDesign, Integer> implements IPlainDesignDAO{
	
	public PlainDesignDAO() {
		super(PlainDesign.class);
	}

}
