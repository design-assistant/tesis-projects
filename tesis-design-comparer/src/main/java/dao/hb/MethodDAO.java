package dao.hb;

import dao.IMethodDAO;

public class MethodDAO  extends GenericHBDAO<model.data.generalStructure.classDiagram.Method, Integer> implements IMethodDAO{
	
	public MethodDAO() {
		super(model.data.generalStructure.classDiagram.Method.class);
	}

}
