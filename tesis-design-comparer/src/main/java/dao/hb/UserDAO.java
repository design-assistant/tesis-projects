package dao.hb;

import dao.IUserDAO;

public class UserDAO extends GenericHBDAO<model.User, Integer> implements IUserDAO{
	
	public UserDAO() {
		super(model.User.class);
	}


}
