package dao;

import model.data.generalStructure.classDiagram.Implementation;


public interface IImplementationDAO extends IGenericDAO<Implementation, Integer> {

}
