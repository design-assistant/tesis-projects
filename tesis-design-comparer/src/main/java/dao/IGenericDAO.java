package dao;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;



public interface IGenericDAO<T extends Serializable, PK extends Serializable> extends DAO {

	@SuppressWarnings("unchecked")
	public Class returnedClass();

	public T findById(PK id);

	public Collection<T> filter(Collection<T> collection, String filter);

	public List<T> findAll();

	public List<T> findAll(String orderBy);

	public List<T> findByExample(T obj);

	public List<T> findById(Collection<PK> ids);

	public List<T> findByProperty(String propertyName, Object value);

	public List<T> findByPropertyContent(String propertyName, String value);

	public T findOneByProperty(String propertyName, Object value);

	public void persist(T obj);

	public void delete(T obj);

	public void refresh(T obj);

	public List<T> findAllByHQL(String query, boolean cacheable,List<String> lista);
	
	public List<T> findAllByHQL(String query, boolean cacheable);

	public List<T> findAllMaxByHQL(String query, int max);

	public T findOneByHQL(String query);


}