package dao;

import model.data.generalStructure.classDiagram.Method;

public interface IMethodDAO extends IGenericDAO<Method, Integer> {

}
