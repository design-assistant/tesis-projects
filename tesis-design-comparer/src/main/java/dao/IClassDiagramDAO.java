package dao;

import model.data.generalStructure.classDiagram.ClassDiagram;

public interface IClassDiagramDAO extends IGenericDAO<ClassDiagram, Integer> {

}
