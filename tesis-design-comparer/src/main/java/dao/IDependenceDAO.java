package dao;

import model.data.generalStructure.classDiagram.Dependence;


public interface IDependenceDAO extends IGenericDAO<Dependence, Integer> {

}
