package dao;

import model.data.generalStructure.classDiagram.Inheritance;


public interface IInheritanceDAO extends IGenericDAO<Inheritance, Integer> {

}
