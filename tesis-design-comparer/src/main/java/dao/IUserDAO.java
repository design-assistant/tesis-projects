package dao;

import model.User;

public interface IUserDAO extends IGenericDAO<User, Integer> {

}
