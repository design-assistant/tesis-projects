package dao;

import java.util.List;

import model.group.PlainDesignBox;
import model.group.QualityAttribute;

public interface IPlainDesignBoxDAO extends IGenericDAO<PlainDesignBox, Integer> {

	
	List<PlainDesignBox> findAllByFullQualityAttributesAndComponentsCountAbdsorbedRelationsCountRange(List<QualityAttribute> attributes,int componentsMin,int componentsMax, int absorbedMin, int absorvedMax);
	
	List<PlainDesignBox> findAllByLessQualityAttributesAndComponentsCountAbdsorbedRelationsCountRange(List<QualityAttribute> attributes,int componentsMin,int componentsMax, int absorbedMin, int absorvedMax);
	
	List<PlainDesignBox> findAllByMoreQualityAttributesAndComponentsCountAbdsorbedRelationsCountRange(List<QualityAttribute> attributes,int componentsMin,int componentsMax, int absorbedMin, int absorvedMax);
	
	List<PlainDesignBox> findAllByLimitQualityAttributesAndComponentsCountAbdsorbedRelationsCountRange(List<QualityAttribute> attributes,int componentsMin,int componentsMax, int absorbedMin, int absorvedMax, int limit);

}
