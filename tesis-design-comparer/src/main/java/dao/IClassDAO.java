package dao;

import model.data.generalStructure.classDiagram.Class;

public interface IClassDAO extends IGenericDAO<Class, Integer> {

}
