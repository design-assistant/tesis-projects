package dao;

import model.data.generalStructure.classDiagram.Package;


public interface IPackageDAO extends IGenericDAO<Package, Integer> {

}
