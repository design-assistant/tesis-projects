package dao;

import model.data.generalStructure.classDiagram.Composition;


public interface ICompositionDAO extends IGenericDAO<Composition, Integer> {

}
