package bo;

import java.util.List;

import model.group.QualityAttribute;

public interface IQualityAttributeBO {

	public void persist(QualityAttribute attribute);

	public QualityAttribute findByName(String name);
	public List<QualityAttribute> findAll();
}
