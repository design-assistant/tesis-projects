package bo;

import java.util.List;

import model.group.PlainDesignBox;
import model.group.QualityAttribute;

public interface IPlainDesignBoxBO {

	public void persist(PlainDesignBox design);

	public List<PlainDesignBox> findAllByFullQualityAttributesAndComponentsCountAbdsorbedRelationsCountRange(
			List<QualityAttribute> attributes, int componentsMin, int componentsMax, int absorbedMin, int absorvedMax);

	public List<PlainDesignBox> findAllByLessQualityAttributesAndComponentsCountAbdsorbedRelationsCountRange(
			List<QualityAttribute> attributes, int componentsMin, int componentsMax, int absorbedMin, int absorvedMax);

	public List<PlainDesignBox> findAllByLimitQualityAttributesAndComponentsCountAbdsorbedRelationsCountRange(
			List<QualityAttribute> attributes, int componentsMin, int componentsMax, int absorbedMin, int absorvedMax, int limit);

	public List<PlainDesignBox> findAllByMoreQualityAttributesAndComponentsCountAbdsorbedRelationsCountRange(
			List<QualityAttribute> attributes, int componentsMin, int componentsMax, int absorbedMin, int absorvedMax);

}
