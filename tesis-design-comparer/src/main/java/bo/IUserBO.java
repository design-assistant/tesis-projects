package bo;

import model.User;

public interface IUserBO {
	
	public void persist(User user);
	public User findByUser(String user);
}
