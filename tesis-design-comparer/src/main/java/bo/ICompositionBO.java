package bo;

import model.data.generalStructure.classDiagram.Composition;

public interface ICompositionBO {

	public void persist(Composition composition);
}
