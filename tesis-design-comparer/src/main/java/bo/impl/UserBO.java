package bo.impl;

import dao.IUserDAO;
import model.User;
import bo.IUserBO;

public class UserBO implements IUserBO{

	IUserDAO dao;
	
	public IUserDAO getDao() {
		return dao;
	}

	public void setDao(IUserDAO dao) {
		this.dao = dao;
	}
	
	@Override
	public void persist(User user) {
		dao.persist(user);
	}

	@Override
	public User findByUser(String user) {
		return dao.findOneByHQL("from User us where us.user='"+user+"'");
	}

}
