package bo.impl;

import model.data.generalStructure.classDiagram.ClassDiagram;
import bo.IClassDiagramBO;
import dao.IClassDiagramDAO;

public class ClassDiagramBO implements IClassDiagramBO {

	IClassDiagramDAO dao;

	public IClassDiagramDAO getDao() {
		return dao;
	}

	public void setDao(IClassDiagramDAO dao) {
		this.dao = dao;
	}

	@Override
	public void persist(ClassDiagram t) {
		dao.persist(t);
	}
}
