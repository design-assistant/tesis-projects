package bo.impl;

import dao.IAsociationDAO;
import model.data.generalStructure.classDiagram.Asociation;
import bo.IAsociationBO;

public class AsociationBO implements IAsociationBO {

	IAsociationDAO dao;

	public IAsociationDAO getDao() {
		return dao;
	}

	public void setDao(IAsociationDAO dao) {
		this.dao = dao;
	}

	@Override
	public void persist(Asociation t) {
		dao.persist(t);
	}

}
