package bo.impl;

import java.util.ArrayList;
import java.util.List;

import model.data.generalStructure.Design;
import model.data.generalStructure.Diagram;
import bo.IDesignBO;
import dao.IDesignDAO;

public class DesignBO implements IDesignBO {

	@Override
	public List<Design> findAll() {
		
		return dao.findAll();
	}

	IDesignDAO dao;

	@Override
	public void persist(Design design) {
		List<Diagram> temps= design.getDiagrams();
		design.setDiagrams(new ArrayList<Diagram>());
		dao.persist(design);
		for(Diagram diagram: temps){
			diagram.setDesign(design);
			diagram.save();
			design.getDiagrams().add(diagram);
		}
		dao.persist(design);
	}

	public IDesignDAO getDao() {
		return dao;
	}

	public void setDao(IDesignDAO dao) {
		this.dao = dao;
	}

}
