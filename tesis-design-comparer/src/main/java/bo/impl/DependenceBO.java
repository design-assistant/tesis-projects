package bo.impl;

import dao.IDependenceDAO;
import model.data.generalStructure.classDiagram.Dependence;
import bo.IDependenceBO;

public class DependenceBO implements IDependenceBO {

	IDependenceDAO dao;

	public IDependenceDAO getDao() {
		return dao;
	}

	public void setDao(IDependenceDAO dao) {
		this.dao = dao;
	}

	@Override
	public void persist(Dependence t) {
		dao.persist(t);
	}

}
