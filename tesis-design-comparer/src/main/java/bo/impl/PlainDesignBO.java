package bo.impl;

import java.util.List;

import model.group.PlainDesign;
import bo.IPlainDesignBO;
import dao.IPlainDesignDAO;

public class PlainDesignBO implements IPlainDesignBO {

	@Override
	public List<PlainDesign> findAll() {
		
		return dao.findAll();
	}

	IPlainDesignDAO dao;

	@Override
	public void persist(PlainDesign design) {
		dao.persist(design);

	}

	public IPlainDesignDAO getDao() {
		return dao;
	}

	public void setDao(IPlainDesignDAO dao) {
		this.dao = dao;
	}

}
