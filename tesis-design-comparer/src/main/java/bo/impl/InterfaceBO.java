package bo.impl;

import model.data.generalStructure.classDiagram.Interface;
import bo.IInterfaceBO;
import dao.IInterfaceDAO;

public class InterfaceBO implements IInterfaceBO {

	IInterfaceDAO dao;

	public IInterfaceDAO getDao() {
		return dao;
	}

	public void setDao(IInterfaceDAO dao) {
		this.dao = dao;
	}

	@Override
	public void persist(Interface t) {
		dao.persist(t);
	}
}
