package bo.impl;

import model.data.generalStructure.classDiagram.Package;
import bo.IPackageBO;
import dao.IPackageDAO;

public class PackageBO implements IPackageBO {

	IPackageDAO dao;

	public IPackageDAO getDao() {
		return dao;
	}

	public void setDao(IPackageDAO dao) {
		this.dao = dao;
	}

	@Override
	public void persist(Package t) {
		dao.persist(t);
	}

}
