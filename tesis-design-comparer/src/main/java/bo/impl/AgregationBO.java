package bo.impl;

import dao.IAgregationDAO;
import model.data.generalStructure.classDiagram.Agregation;
import bo.IAgregationBO;

public class AgregationBO implements IAgregationBO {

	IAgregationDAO dao;

	public IAgregationDAO getDao() {
		return dao;
	}

	public void setDao(IAgregationDAO dao) {
		this.dao = dao;
	}

	@Override
	public void persist(Agregation t) {
		dao.persist(t);
	}

}
