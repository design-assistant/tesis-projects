package bo.impl;

import model.group.QualityAttributeGroup;
import bo.IQualityAttributeGroupBO;
import dao.IQualityAttributeGroupDAO;

public class QualityAttributeGroupBO implements IQualityAttributeGroupBO {

	IQualityAttributeGroupDAO dao;
	
	
	public IQualityAttributeGroupDAO getDao() {
		return dao;
	}

	public void setDao(IQualityAttributeGroupDAO dao) {
		this.dao = dao;
	}

	@Override
	public void persist(QualityAttributeGroup attribute) {
		dao.persist(attribute);
		
	}



}
