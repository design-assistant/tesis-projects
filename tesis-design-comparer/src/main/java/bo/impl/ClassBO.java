package bo.impl;

import dao.IClassDAO;
import model.data.generalStructure.classDiagram.Class;
import bo.IClassBO;

public class ClassBO implements IClassBO {

	IClassDAO dao;

	public IClassDAO getDao() {
		return dao;
	}

	public void setDao(IClassDAO dao) {
		this.dao = dao;
	}

	@Override
	public void persist(Class t) {
		dao.persist(t);
	}

}
