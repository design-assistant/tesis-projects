package bo.impl;

import model.data.generalStructure.classDiagram.Method;
import bo.IMethodBO;
import dao.IMethodDAO;

public class MethodBO implements IMethodBO {

	IMethodDAO dao;

	public IMethodDAO getDao() {
		return dao;
	}

	public void setDao(IMethodDAO dao) {
		this.dao = dao;
	}

	@Override
	public void persist(Method t) {
		dao.persist(t);
	}

}
