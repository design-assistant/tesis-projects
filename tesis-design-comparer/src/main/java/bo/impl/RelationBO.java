package bo.impl;

import model.data.generalStructure.classDiagram.Relation;
import bo.IRelationBO;
import dao.IRelationDAO;

public class RelationBO implements IRelationBO {

	IRelationDAO dao;
	
	public IRelationDAO getDao() {
		return dao;
	}

	public void setDao(IRelationDAO dao) {
		this.dao = dao;
	}

	@Override
	public void persist(Relation relation) {
		dao.persist(relation);
		
	}

}
