package bo.impl;

import java.util.List;

import model.group.PlainDesignBox;
import model.group.QualityAttribute;
import bo.IPlainDesignBoxBO;
import dao.IPlainDesignBoxDAO;

public class PlainDesignBoxBO implements IPlainDesignBoxBO {

	private IPlainDesignBoxDAO dao;

	public IPlainDesignBoxDAO getDao() {
		return dao;
	}

	public void setDao(IPlainDesignBoxDAO dao) {
		this.dao = dao;
	}

	@Override
	public void persist(PlainDesignBox design) {
		dao.persist(design);

	}

	@Override
	public List<PlainDesignBox> findAllByFullQualityAttributesAndComponentsCountAbdsorbedRelationsCountRange(
			List<QualityAttribute> attributes, int componentsMin, int componentsMax, int absorbedMin, int absorvedMax) {
		return dao.findAllByFullQualityAttributesAndComponentsCountAbdsorbedRelationsCountRange(attributes, componentsMin, componentsMax,
				absorbedMin, absorvedMax);
	}

	@Override
	public List<PlainDesignBox> findAllByLessQualityAttributesAndComponentsCountAbdsorbedRelationsCountRange(
			List<QualityAttribute> attributes, int componentsMin, int componentsMax, int absorbedMin, int absorvedMax) {
		return dao.findAllByLessQualityAttributesAndComponentsCountAbdsorbedRelationsCountRange(attributes, componentsMin, componentsMax,
				absorbedMin, absorvedMax);
	}

	@Override
	public List<PlainDesignBox> findAllByLimitQualityAttributesAndComponentsCountAbdsorbedRelationsCountRange(
			List<QualityAttribute> attributes, int componentsMin, int componentsMax, int absorbedMin, int absorvedMax, int limit) {
		return dao.findAllByLimitQualityAttributesAndComponentsCountAbdsorbedRelationsCountRange(attributes, componentsMin, componentsMax,
				absorbedMin, absorvedMax, limit);
	}

	@Override
	public List<PlainDesignBox> findAllByMoreQualityAttributesAndComponentsCountAbdsorbedRelationsCountRange(
			List<QualityAttribute> attributes, int componentsMin, int componentsMax, int absorbedMin, int absorvedMax) {
		return dao.findAllByMoreQualityAttributesAndComponentsCountAbdsorbedRelationsCountRange(attributes, componentsMin, componentsMax,
				absorbedMin, absorvedMax);
	}

}
