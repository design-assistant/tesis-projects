package bo.impl;

import java.util.List;

import model.group.QualityAttribute;
import bo.IQualityAttributeBO;
import dao.IQualityAttributeDAO;

public class QualityAttributeBO implements IQualityAttributeBO {

	IQualityAttributeDAO dao;
	
	
	public IQualityAttributeDAO getDao() {
		return dao;
	}

	public void setDao(IQualityAttributeDAO dao) {
		this.dao = dao;
	}

	@Override
	public List<QualityAttribute> findAll() {
		return dao.findAll("name");
	}

	@Override
	public QualityAttribute findByName(String name) {
		return dao.findOneByHQL("from QualityAttribute a where a.name='"+name+"'");
	}

	@Override
	public void persist(QualityAttribute attribute) {
		dao.persist(attribute);
		
	}



}
