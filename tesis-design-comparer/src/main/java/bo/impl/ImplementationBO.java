package bo.impl;

import dao.IImplementationDAO;
import model.data.generalStructure.classDiagram.Implementation;
import bo.IImplementationBO;

public class ImplementationBO implements IImplementationBO {

	IImplementationDAO dao;

	public IImplementationDAO getDao() {
		return dao;
	}

	public void setDao(IImplementationDAO dao) {
		this.dao = dao;
	}

	@Override
	public void persist(Implementation t) {
		dao.persist(t);
	}

}
