package bo.impl;

import dao.ICompositionDAO;
import model.data.generalStructure.classDiagram.Composition;
import bo.ICompositionBO;

public class CompositionBO implements ICompositionBO {

	ICompositionDAO dao;

	public ICompositionDAO getDao() {
		return dao;
	}

	public void setDao(ICompositionDAO dao) {
		this.dao = dao;
	}

	@Override
	public void persist(Composition t) {
		dao.persist(t);
	}

}
