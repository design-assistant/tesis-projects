package bo.impl;

import dao.IInheritanceDAO;
import model.data.generalStructure.classDiagram.Inheritance;
import bo.IInheritanceBO;

public class InheritanceBO implements IInheritanceBO {

	IInheritanceDAO dao;

	public IInheritanceDAO getDao() {
		return dao;
	}

	public void setDao(IInheritanceDAO dao) {
		this.dao = dao;
	}

	@Override
	public void persist(Inheritance t) {
		dao.persist(t);
	}

}
