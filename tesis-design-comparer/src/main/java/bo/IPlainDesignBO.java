package bo;

import java.util.List;

import model.group.PlainDesign;

public interface IPlainDesignBO {

	public void persist(PlainDesign design);

	public List<PlainDesign> findAll();
}
