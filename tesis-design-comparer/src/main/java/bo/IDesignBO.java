package bo;

import java.util.List;

import model.data.generalStructure.Design;

public interface IDesignBO {

	public void persist(Design design);

	public List<Design> findAll();
}
