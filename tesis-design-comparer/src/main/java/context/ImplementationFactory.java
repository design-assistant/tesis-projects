package context;

import org.apache.commons.lang3.Validate;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;

public final class ImplementationFactory {
	private ImplementationFactory(){
		super();
	}

	private static final Logger LOGGER = Logger
			.getLogger(ImplementationFactory.class);


	private static ApplicationContext app;

	
	public static synchronized void setApplicationContext(final ApplicationContext ctx) {

		LOGGER.debug("setWebApplicationContext(XmlWebApplicationContext) - start");
		
		Validate.notNull(ctx, "El contexto no puede ser NULL!");		
		app = ctx;
		
		LOGGER.debug("setWebApplicationContext(XmlWebApplicationContext) - end");
	}
		
	public static ApplicationContext getApplicationContext(){
		return app;
	}
	
	public static Object getBean(String name)
	{
		return app.getBean(name);
	}
	
	
	
}
