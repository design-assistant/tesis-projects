package model.data.leveler;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import model.data.generalStructure.Component;
import model.data.generalStructure.Repository;
import model.data.generalStructure.classDiagram.Attribute;
import model.data.generalStructure.classDiagram.BoxComponent;
import model.data.generalStructure.classDiagram.Relation;

public class Hierarchy {

	private BoxComponent node;
	private int level;
	private List<Hierarchy> sons = new ArrayList<Hierarchy>();

	public Hierarchy() {
	}

	public Hierarchy(BoxComponent node, Vector<Hierarchy> sons, int level) {
		super();
		this.node = node;
		this.sons = sons;
		this.level = level;
	}

	public BoxComponent getNode() {
		return node;
	}

	public List<Hierarchy> getSons() {
		return sons;
	}

	public void setNode(BoxComponent node) {
		this.node = node;
	}

	public void setSons(List<Hierarchy> sons) {
		this.sons = sons;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public int getLevel() {
		return level;
	}

	/**
	 * Este metodo es el que agrega un nodo a la jerarquia sabiendo de quien
	 * debe ser hijo dicho nodo
	 * 
	 * @param father :
	 *            es el nodo que sera el padre del nodo a agregar
	 * @param son :
	 *            nodo hijo a agregar
	 * @return : retorna verdadero si se pudo agregar, falso en caso contrario
	 */
	public boolean insertSon(BoxComponent father, BoxComponent son) {
		if (node.getCompName().equals(father.getCompName())) {
			Hierarchy h = new Hierarchy(son, new Vector<Hierarchy>(), level + 1);
			sons.add(h);
			return true;
		} else {
			for (int i = 0; i < sons.size(); i++) {
				if (sons.get(i).insertSon(father, son))
					return true;
			}
		}
		return false;
	}

	/**
	 * Metodo que me informa si la jerarquia ya se encuentra aplanada hacia
	 * arriba
	 * 
	 * @return : verdadero si la jerarquia ya se encuentra aplanada, falso en
	 *         caso contrario
	 */
	public boolean isFlattenUP() {
		int val = getMaxLevel();
		if (val == 1)
			return true;
		return false;
	}

	/**
	 * Este metodo devuleve el n�mero m�ximo de niveles que tiene la jerarquia
	 * 
	 * @return : devuelve el n�mero m�ximo
	 */
	public int getMaxLevel() {
		if (sons.size() == 0)
			return level;
		else {
			int lev;
			int max = 0;
			for (int i = 0; i < sons.size(); i++) {
				lev = sons.get(i).getMaxLevel();
				if (max < lev)
					max = lev;
			}
			return max;
		}
	}

	/**
	 * Este m�todo levanta un nivel al padre
	 * 
	 * @param cantLevel :
	 *            n�mero del nivel que se quiere llevar al padre
	 */
	public void flattenLevelUp(int cantLevel) {
		if ((level + 1) == cantLevel) {
		//	List<Method> methods;
			List<Attribute> attributes;
			for (int i = 0; i < sons.size(); i++) {
		//		methods = sons.get(i).getNode().getUsefullMethods();
				attributes = sons.get(i).getNode().getUsefullAttributes();
				this.node.addAttributes(attributes);
				
		//		this.node.addMethods(methods);
				absorveRelations(i);
			}
			sons = new ArrayList<Hierarchy>();
		} else {
			for (int i = 0; i < sons.size(); i++)
				sons.get(i).flattenLevelUp(cantLevel);
		}
	}

	private void absorveRelations(int i) {
		List<Relation> relations=Repository.getInstance().getRelationsByComponent(sons.get(i).getNode().getCompName());
		for(int j=0;j<relations.size();j++){
			Relation r=relations.get(j);
			if(r.getTarget().getCompName().equals(node.getCompName())){
				String att = r.getNewAttributeStub(r.getSource().getBoxComponentName());
				Attribute attribute = new Attribute();
				attribute.setName(att);
				node.getAttributes().add(attribute);
			}
		}
		
	}

	public void flattenLevelDown(List<Component> leaf) {
	//	List<Method> methods = node.getUsefullMethods();
		List<Attribute> attributes = node.getUsefullAttributes();
		for (Hierarchy h : sons) {
			h.getNode().getAttributes().addAll(attributes);
		//	h.getNode().getMethods().addAll(methods);
			if (h.isSheet()) {
				leaf.add(h.getNode());
			}
			h.flattenLevelDown(leaf);
		}
	}
	
	public boolean isSheet(){
		return getSons().size() == 0;
	}

}
