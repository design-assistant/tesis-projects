package model.data.leveler;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import model.data.generalStructure.Component;
import model.data.generalStructure.Design;
import model.data.generalStructure.Diagram;
import model.data.generalStructure.Repository;
import model.data.generalStructure.classDiagram.Attribute;
import model.data.generalStructure.classDiagram.BoxComponent;
import model.data.generalStructure.classDiagram.ClassDiagram;
import model.data.generalStructure.classDiagram.ClassDiagramComponent;
import model.data.generalStructure.classDiagram.Method;
import model.data.generalStructure.classDiagram.Relation;

public class Leveler {

	public Leveler() {
	}

	public Design flattenUp(Design design) {
		Design ret = new Design();
		ret.setName(design.getName() + " UP");
		Vector<Component> staticComponents = new Vector<Component>();
		Repository rep = Repository.getInstance();
		staticComponents = rep.cloneRelations(); // get all relations cloned
		Diagram diag = analyze(staticComponents);
		ret.addDiagram(diag);
		return ret;
	}

	public Design flattenDown(Design design) {
		Design ret = new Design();
		ret.setName(design.getName() + " DOWN");
		Vector<Component> staticComponents = new Vector<Component>();
		Repository rep = Repository.getInstance();
		staticComponents = rep.cloneRelations();
		Diagram diag = analyzeDown(staticComponents);
		ret.addDiagram(diag);
		return ret;
	}

	/**
	 * Este metodo es el que genera el diagrama de clases aplanado hacia arriba
	 */

	private Diagram analyze(Vector<Component> aux) {
		ClassDiagram ret = new ClassDiagram("UP");
		Vector<Hierarchy> hierarchies = generateHierarchies(aux);
		for (int i = 0; i < hierarchies.size(); i++)
			flattenHierarchiesUP(hierarchies.get(i), aux);
		List<Method> methods;
		List<Attribute> relations;
		BoxComponent c;
		Vector<Component> temp = new Vector<Component>();
		int beforeSize = aux.size();
		for (int i = 0; i < aux.size() && aux.size() > 0; i++) {
			beforeSize = aux.size();
			methods = new ArrayList<Method>();
			if (i >= 0) {
				c = (BoxComponent) ((Relation) aux.get(i)).getSource();
				relations = absorbRelationship(c, aux, methods);
				c.getAttributes().addAll(relations);
				c.getMethods().addAll(methods);
				temp.add(c);
				i = i - (beforeSize - aux.size());
			}
		}
		for (int i = 0; i < hierarchies.size(); i++)
			aux.add(hierarchies.get(i).getNode());
		aux.addAll(temp);
		for (int i = 0; i < aux.size(); i++)
			ret.addComponent(aux.get(i));

		return ret;
	}

	private Diagram analyzeDown(Vector<Component> aux) {

		ClassDiagram ret = new ClassDiagram("DOWN");
		Vector<Hierarchy> hierarchies = generateHierarchies(aux);
		List<Component> comps = new ArrayList<Component>();
		List<Method> methods;
		List<Attribute> relations;
		BoxComponent c;
		Vector<Component> temp = new Vector<Component>();
		int beforeSize = aux.size();
		for (int i = 0; i < aux.size() && aux.size() > 0; i++) {
			beforeSize = aux.size();
			methods = new ArrayList<Method>();
			if (i >= 0) {
				c = (BoxComponent) ((Relation) aux.get(i)).getSource();
				relations = absorbRelationship(c, aux, methods);
				c.getAttributes().addAll(relations);
				c.getMethods().addAll(methods);
				temp.add(c);
				i = i - (beforeSize - aux.size());
			}
		}
		for (int i = 0; i < hierarchies.size(); i++) {
			comps.addAll(flattenHierarchiesDown(hierarchies.get(i), aux));
		}
		comps.addAll(temp);
		for (Component comp : comps) {
			ret.addComponent(comp);
		}

		return ret;
	}

	/**
	 * Metodo encargado de aplanar la jerarquia hacia arriba
	 * 
	 * @param hierarchy
	 *            : jerarquia a aplanar
	 * @param components
	 *            : vector que tiene los componentes del dise�o
	 */
	private void flattenHierarchiesUP(Hierarchy hierarchy,
			Vector<Component> components) {
		int cantLevel;
		while (!hierarchy.isFlattenUP()) {
			cantLevel = hierarchy.getMaxLevel();
			hierarchy.flattenLevelUp(cantLevel);
		}
	}

	private List<Component> flattenHierarchiesDown(Hierarchy hierarchy,
			Vector<Component> components) {
		List<Component> rets = new ArrayList<Component>();
		hierarchy.flattenLevelDown(rets);
		return rets;
	}

	/**
	 * Este metodo es el que selecciona los componentes que seran parte de la
	 * jerarquia e invocara al metodo que va armando la jerarquia
	 * 
	 * @param aux
	 *            : vector con los componentes
	 * @return : devuelve un vector con las jerarquias, en cada posicion hay una
	 *         jerarquia del dise�o
	 */
	private Vector<Hierarchy> generateHierarchies(Vector<Component> aux) {
		Hashtable<String, Component> hierarchical = new Hashtable<String, Component>();
		for (int i = 0; i < aux.size(); i++) {
			if (aux.get(i).isHierarchical()) {
				hierarchical.put(aux.get(i).getPath()
						+ aux.get(i).getCompName(), aux.get(i));
				aux.remove(i);
				i--;
			}
		}
		Vector<Hierarchy> hierarchies = fillHierarchies(hierarchical, aux);
		return hierarchies;
	}

	/**
	 * Este el metodo es el que se encarga de armar la jerarquia con todos los
	 * componentes sueltos
	 * 
	 * @param hierarchical
	 *            : conjunto de componentes que generaran la jerarquia
	 * @param components
	 *            : resto de los componentes del dise�o
	 * @return retorna un vector con las jerarquias que existan en el dise�o
	 */
	private Vector<Hierarchy> fillHierarchies(
			Hashtable<String, Component> hierarchical,
			Vector<Component> components) {
		Vector<Hierarchy> ret = new Vector<Hierarchy>();
		Hashtable<String, BoxComponent> fathers = getFathers(hierarchical);
		Hierarchy hierarchy;
		String key;
		for (Enumeration<String> e = fathers.keys(); e.hasMoreElements();) {
			key = e.nextElement();
			hierarchy = new Hierarchy(fathers.get(key),
					new Vector<Hierarchy>(), 1);
			deleteFromComponents(components, fathers.get(key));
			List<Method> methods = new ArrayList<Method>();
			List<Attribute> relations = absorbRelationship(fathers.get(key),
					components, methods);
			hierarchy.getNode().getAttributes().addAll(relations);
			hierarchy.getNode().getMethods().addAll(methods);
			ret.add(hierarchy);
		}
		Relation rel;
		while (!hierarchical.isEmpty()) {
			for (Enumeration<String> e = hierarchical.keys(); e
					.hasMoreElements();) {
				key = e.nextElement();
				rel = (Relation) hierarchical.get(key);
				if (validSon(rel, ret, components)) {
					hierarchical.remove(key);
					deleteFromComponents(components, rel.getTarget());
				}
			}
		}
		return ret;
	}

	private List<Attribute> absorbRelationship(BoxComponent source,
			Vector<Component> components, List<Method> methods) {
		List<Attribute> attributes = new ArrayList<Attribute>();
		List<Component> comps = new ArrayList<Component>();
		for (Component component : components) {
			String att = ((ClassDiagramComponent) component)
					.getNewAttributeStub(source.getCompName());
			if (att != null) {
				Attribute attribute = new Attribute();
				attribute.setName(att);
				attributes.add(attribute);
				comps.add(component);
			} else {
				String mt = ((ClassDiagramComponent) component)
						.getNewMethodStub(source.getCompName());
				if (mt != null) {
					Method m = new Method();
					m.setAmbient(null);
					m.setName(mt);
					if (!methods.contains(m)) {
						methods.add(m);
					}
					comps.add(component);
				}
			}

		}
		for (Component comp : comps) {
			deleteFromComponents(components, comp);
		}
		return attributes;

	}

	/**
	 * si es posible colgar el hijo de la relacion, porque ya esta el padre en
	 * la jerarquia se inserta y se devuelve verdadero, de otra forma se
	 * devuelve falso y no se inserta el hijo
	 * 
	 * @param rel
	 * @param ret
	 * @return
	 */
	private boolean validSon(Relation rel, Vector<Hierarchy> ret,
			Vector<Component> components) {
		for (int i = 0; i < ret.size(); i++) {

			if (ret.get(i).insertSon(rel.getSource(), rel.getTarget())) {
				List<Method> methods = new ArrayList<Method>();
				List<Attribute> relations = absorbRelationship(rel.getTarget(), components, methods);
				rel.getTarget().getAttributes().addAll(relations);
				rel.getTarget().getMethods().addAll(methods);
				return true;
			}

		}
		return false;
	}

	private void deleteFromComponents(Vector<Component> components,
			Component component) {
		boolean cut = false;
		for (int i = 0; i < components.size() && !cut; i++) {
			if (component.getCompName().equals(components.get(i).getCompName())) {
				components.remove(i);
				cut = true;
			}
		}
	}

	/**
	 * Este metodo me devuele los elementos que son padres en las jerarquias
	 * 
	 * @param hierarchical
	 * @return
	 */
	private Hashtable<String, BoxComponent> getFathers(
			Hashtable<String, Component> hierarchical) {
		Hashtable<String, BoxComponent> fathers = new Hashtable<String, BoxComponent>();
		Hashtable<String, BoxComponent> sons = new Hashtable<String, BoxComponent>();
		BoxComponent father;
		BoxComponent son;
		Relation rel;
		String key;
		for (Enumeration<String> e = hierarchical.keys(); e.hasMoreElements();) {
			key = e.nextElement();
			rel = (Relation) hierarchical.get(key);
			father = (BoxComponent) rel.getSource();
			son = rel.getTarget();
			if (!(sons.containsKey(father.getPath() + father.getCompName())))
				fathers.put(father.getPath() + father.getCompName(), father);
			if (fathers.containsKey(son.getPath() + son.getCompName()))
				fathers.remove(son.getPath() + son.getCompName());
			sons.put(son.getPath() + son.getCompName(), son);
		}
		return fathers;
	}

}
