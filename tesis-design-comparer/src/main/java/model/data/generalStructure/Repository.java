package model.data.generalStructure;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import model.data.generalStructure.classDiagram.BoxComponent;
import model.data.generalStructure.classDiagram.Relation;
import model.group.PlainDesign;
import model.group.PlainDesignBox;

public class Repository {

	private static Repository instance;
	private Vector<Component> relations;
	private Hashtable<String, BoxComponent> boxs = new Hashtable<String, BoxComponent>();
	private int diagramNum;
	private PlainDesign up;
	private PlainDesign down;
	private PlainDesignBox currentPDBox;
	private double currentUserDesignWeight;
	private List<String> fathers = new ArrayList<String>();
	private List<String> sons = new ArrayList<String>();	

	public double getCurrentUserDesignWeight() {
		return currentUserDesignWeight;
	}

	public void setCurrentUserDesignWeight(double currentUserDesignWeight) {
		this.currentUserDesignWeight = currentUserDesignWeight;
	}

	public PlainDesignBox getCurrentPDBox() {
		return currentPDBox;
	}

	public void setCurrentPDBox(PlainDesignBox currentPDBox) {
		this.currentPDBox = currentPDBox;
	}

	public PlainDesign getUp() {
		return up;
	}

	public void setUp(PlainDesign up) {
		this.up = up;
	}

	public PlainDesign getDown() {
		return down;
	}

	public void setDown(PlainDesign down) {
		this.down = down;
	}

	public int getDiagramNum() {
		return diagramNum++;
	}

	private void setDiagramNum(int diagramNum) {
		this.diagramNum = diagramNum;
	}

	private Repository() {
		relations = new Vector<Component>();
	}

	public static Repository getInstance() {
		if (instance == null) {
			instance = new Repository();
			instance.setDiagramNum(0);
		}
		return instance;
	}

	public Vector<Component> getRelations() {
		return relations;
	}

	public void setRelations(Vector<Component> relations) {
		this.relations = relations;
	}

	public static void setInstance(Repository instance) {
		Repository.instance = instance;
	}

	public void addRelation(Component component) {
		if (!relations.contains(component))
			relations.add(component);
	}

	public BoxComponent getBoxComponent(String name) {
		return boxs.get(name);
	}

	public void addBoxComponent(BoxComponent comp) {
		if (!boxs.containsKey(comp.getCompName())) {
			boxs.put(comp.getCompName(), comp);
		}
	}

	public Vector<Component> cloneRelations() {
		Vector<Component> rels = new Vector<Component>();
		Hashtable<String, BoxComponent> comps = new Hashtable<String, BoxComponent>();
		for (int i = 0; i < relations.size(); i++) {
			Relation r = (Relation) relations.get(i);
			Relation clon = (Relation) r.getCloneComponent();

			if (comps.containsKey(r.getSource().compName))
				clon.setSource(comps.get(r.getSource().getCompName()));
			else {
				BoxComponent bc = r.getSource().cloneBoxComponent();
				comps.put(bc.getCompName(), bc);
				clon.setSource(bc);
			}

			if (comps.containsKey(r.getTarget().compName))
				clon.setTarget(comps.get(r.getTarget().getCompName()));
			else {
				BoxComponent bc = r.getTarget().cloneBoxComponent();
				comps.put(bc.getCompName(), bc);
				clon.setTarget(bc);
			}
			rels.add(clon);
		}
		return rels;

	}
	
	public List<Relation> getRelationsByComponent(String name){
		List<Relation> rels= new ArrayList<Relation>();
		for (int i = 0; i < relations.size(); i++) {
			Relation r = (Relation) relations.get(i);
			if(r.getSource().getCompName().equals(name)){
				rels.add(r);
			}
		}
		return rels;
	}
	

	public void clearRepository() {
		instance = new Repository();
		instance.setDiagramNum(0);
	}

	public boolean containsFather(String compName) {
		if (fathers.contains(compName))
			return true;
		return false;
	}

	public void addFather(String compName) {
		fathers.add(compName);
	}

	public void clearCostInfo() {
		fathers = new ArrayList<String>();
		sons = new ArrayList<String>();
	}

	public boolean containsSons(String compName) {
		if (sons.contains(compName))
			return true;
		return false;
	}

	public void addSon(String compName) {
		sons.add(compName);
	}

	public boolean isFatherOrSon(String compName) {
		if ((fathers.contains(compName)) || (sons.contains(compName)))
			return true;
		return false;
	}

}
