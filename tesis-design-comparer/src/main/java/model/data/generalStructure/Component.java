package model.data.generalStructure;

import java.io.Serializable;

import model.group.PlainDesign;

import view.drawable.DrawableComponent;

public abstract class Component implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected String compName;
	protected int id;
	protected String path;// la direccion del paquete ej: org.com.coparador.pool.ClaseEjemplo
	protected Diagram diagram;
	protected PlainDesign plainDesign;

	public Diagram getDiagram() {
		return diagram;
	}

	public PlainDesign getPlainDesign() {
		return plainDesign;
	}

	public void setPlainDesign(PlainDesign plainDesign) {
		this.plainDesign = plainDesign;
	}

	public void setDiagram(Diagram diagram) {
		this.diagram = diagram;
	}

	public Component() {
	}

	public Component(String compName) {
		super();
		this.compName = compName;
		this.path = "default";
	}

	public Component(String compName, int id, String path) {
		super();
		this.compName = compName;
		this.id = id;
		this.path = path;
	}

	public Component(String compName, String path) {
		super();
		this.compName = compName;
		this.path = path;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCompName() {
		return compName;
	}

	public void setCompName(String compName) {
		this.compName = compName;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public abstract DrawableComponent getDrawable();

	/**
	 * Sirve para saber si el objeto puede ser asociado a una jerarquia
	 * 
	 * @return : booleano que dice si es o no valido en una jerarquia. Redefinido para la herencia
	 */
	public boolean isHierarchical() {
		return false;
	}

	public abstract Component getCloneComponent();

	public abstract void save();

	@Override
	public boolean equals(Object obj) {
		Component c = (Component) obj;
		return c.getCompName() != null && c.getCompName().equals(this.getCompName());
	}

	// TODOS DE ACA PARA ABAJO RETORNAN 0 LOS HIJOS QUE LOS NECESITAN LOS REDEFINEN

	public int getFathersCount() {
		return 0;
	}

	public int getSonsCount() {
		return 0;
	}

	public int getBoxComponentsCount() {
		return 0;
	}

	public int getInheritanceCount() {
		return 0;
	}

	public int getImplementationCount() {
		return 0;
	}

	public int getUsesCount() {
		return 0;
	}

	public int getAsociationCount() {
		return 0;
	}

	public int getCollectionsCount() {
		return 0;
	}

	public String getBoxComponentName() {
		return null;
	}

	public void analizeComponent(StringBuilder source, StringBuilder target) {
		target.append(this.compName);
	}

	public void setThroughput() {}

}
