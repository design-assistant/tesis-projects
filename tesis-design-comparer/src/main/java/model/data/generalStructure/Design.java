package model.data.generalStructure;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import model.data.generalStructure.classDiagram.BoxComponent;
import model.data.generalStructure.classDiagram.Relation;

import view.drawable.DrawableComponent;

public class Design implements Cloneable, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String name;
	private List<Diagram> diagrams = new ArrayList<Diagram>();
	protected long id;
	private List<String> sources = new ArrayList<String>();
	private List<String> targets = new ArrayList<String>();
	private List<String> sourcesAndTargets = new ArrayList<String>();
	private List<String> empties = new ArrayList<String>();
	private int sourcesOutput = 0;
	private int targetInPut = 0;
	private int sourcesAndTargetsOutput = 0;
	private int sourcesAndTargetsInput = 0;
	private List<BoxComponent> sourcesTroughPut = new ArrayList<BoxComponent>();
	private List<BoxComponent> targetsTroughPut = new ArrayList<BoxComponent>();
	private List<BoxComponent> sourcesAndTargetsTroughPut = new ArrayList<BoxComponent>();
	private List<BoxComponent> emptiesTroughPut = new ArrayList<BoxComponent>();
	private double success;

	public double getSuccess() {
		return success;
	}
public boolean isInterface(String name){
	return ((Diagram) diagrams.get(0)).isInterface(name);
}
	
	public void setSuccess(double success) {
		this.success = success;
	}

	public List<Relation> getRelations() {
		return this.getDiagrams().get(0).getRelations();
	}

	public List<BoxComponent> getSourcesTroughPut() {
		return sourcesTroughPut;
	}

	public void setSourcesTroughPut(List<BoxComponent> sourcesTroughPut) {
		this.sourcesTroughPut = sourcesTroughPut;
	}

	public List<BoxComponent> getTargetsTroughPut() {
		return targetsTroughPut;
	}

	public void setTargetsTroughPut(List<BoxComponent> targetsTroughPut) {
		this.targetsTroughPut = targetsTroughPut;
	}

	public List<BoxComponent> getSourcesAndTargetsTroughPut() {
		return sourcesAndTargetsTroughPut;
	}

	public void setSourcesAndTargetsTroughPut(List<BoxComponent> sourcesAndTargetsTroughPut) {
		this.sourcesAndTargetsTroughPut = sourcesAndTargetsTroughPut;
	}

	public List<BoxComponent> getEmptiesTroughPut() {
		return emptiesTroughPut;
	}

	public void setEmptiesTroughPut(List<BoxComponent> emptiesTroughPut) {
		this.emptiesTroughPut = emptiesTroughPut;
	}

	public void addSourcesOutPut() {
		sourcesOutput++;
	}

	public void addSourcesAndTargetsOutPut() {
		sourcesAndTargetsOutput++;
	}

	public void addSourcesAndTargetsInPut() {
		sourcesAndTargetsInput++;
	}

	public void addTargetInPut() {
		targetInPut++;
	}

	public void delSourcesOutPut() {
		sourcesOutput--;
	}

	public void delSourcesAndTargetsOutPut() {
		sourcesAndTargetsOutput--;
	}

	public void delSourcesAndTargetsInPut() {
		sourcesAndTargetsInput--;
	}

	public void delTargetInPut() {
		targetInPut--;
	}

	public int getSourcesOutput() {
		return sourcesOutput;
	}

	public void setSourcesOutput(int sourcesOutput) {
		this.sourcesOutput = sourcesOutput;
	}

	public int getTargetInPut() {
		return targetInPut;
	}

	public void setTargetInPut(int targetInPut) {
		this.targetInPut = targetInPut;
	}

	public int getSourcesAndTargetsOutput() {
		return sourcesAndTargetsOutput;
	}

	public void setSourcesAndTargetsOutput(int sourcesAndTargetsOutput) {
		this.sourcesAndTargetsOutput = sourcesAndTargetsOutput;
	}

	public int getSourcesAndTargetsInput() {
		return sourcesAndTargetsInput;
	}

	public void setSourcesAndTargetsInput(int sourcesAndTargetsInput) {
		this.sourcesAndTargetsInput = sourcesAndTargetsInput;
	}

	public List<String> getSources() {
		return sources;
	}

	public void setSources(List<String> sources) {
		this.sources = sources;
	}

	public List<String> getTargets() {
		return targets;
	}

	public void setTargets(List<String> targets) {
		this.targets = targets;
	}

	public List<String> getSourcesAndTargets() {
		return sourcesAndTargets;
	}

	public void setSourcesAndTargets(List<String> sourcesAndTargets) {
		this.sourcesAndTargets = sourcesAndTargets;
	}

	public List<String> getEmpties() {
		return empties;
	}

	public void setEmpties(List<String> empties) {
		this.empties = empties;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Design() {
	}

	public Design(String name, List<Diagram> diagrams) {
		super();
		this.name = name;
		this.diagrams = diagrams;
	}

	public List<Diagram> getDiagrams() {
		return diagrams;
	}

	public void setDiagrams(List<Diagram> diagrams) {
		this.diagrams = diagrams;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void addDiagram(Diagram diagram) {
		diagrams.add(diagram);
	}

	public List<DrawableComponent> createStaticsDrawables() {
		List<DrawableComponent> components = new ArrayList<DrawableComponent>();
		List<DrawableComponent> diagramComponents = new ArrayList<DrawableComponent>();
		for (int i = 0; i < diagrams.size(); i++) {
			diagramComponents = diagrams.get(i).createStaticDrawables();
			if (diagramComponents != null)
				components.addAll(diagramComponents);
		}
		return components;
	}

	public Vector<Vector<DrawableComponent>> createDinamicsDrawables() {
		Vector<Vector<DrawableComponent>> components = new Vector<Vector<DrawableComponent>>();
		Vector<DrawableComponent> drawable = new Vector<DrawableComponent>();
		for (int i = 0; i < diagrams.size(); i++) {
			drawable = diagrams.get(i).createDinamicDrawables();
			if (drawable != null)
				components.add(drawable);
		}
		return components;
	}

	public List<Component> getStaticComponents() {
		List<Component> ret = new ArrayList<Component>();
		for (int i = 0; i < diagrams.size(); i++)
			ret.addAll(diagrams.get(i).getStaticComponents());
		return ret;
	}

	public int getFathersCount() {
		return diagrams.get(0).getFathersCount();
	}

	public int getSonsCount() {
		return diagrams.get(0).getSonsCount();
	}

	public Integer getBoxComponentsCount() {
		return diagrams.get(0).getBoxComponentsCount();
	}

	public Integer getInheritanceCount() {
		return diagrams.get(0).getInheritanceCount();
	}

	public Integer getImplementationCount() {
		return diagrams.get(0).getImplementationCount();
	}

	public Integer getUsesCount() {
		return diagrams.get(0).getUsesCount();
	}

	public Integer getAsociationCount() {
		return diagrams.get(0).getAsociationCount();
	}

	public Integer getCollectionsCount() {
		return diagrams.get(0).getCollectionsCount();
	}

	public void separateRelationComponents() {
		diagrams.get(0).separateRelationComponents(this);
	}

	public void addStubComponent(Design otherDesign, String action) {
		diagrams.get(0).addStubComponent(otherDesign, action, this);
	}

	public void addSource(String action) {
		sources.add(action);
	}

	public void addTarget(String action) {
		targets.add(action);
	}

	public void addSourceAndTarget(String action) {
		sourcesAndTargets.add(action);
	}

	public void addEmpty(String action) {
		empties.add(action);
	}

	public void clearRelationComponents() {
		diagrams.get(0).clearRelations();
	}
}
