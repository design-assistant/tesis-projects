package model.data.generalStructure.sequenceDiagram;

import view.drawable.DrawableComponent;
import view.drawable.sequenceDiagram.DrawableCreation;


public class Creation extends Message{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Creation() {}
	
	public Creation(String creationName, Instance source, Instance target,int order) {
		super(creationName, source, target,order);
	}

	@Override
	public DrawableComponent getDrawable() {
		return new DrawableCreation(this); 
	}


}
