package model.data.generalStructure.sequenceDiagram;

import view.drawable.DrawableComponent;
import view.drawable.sequenceDiagram.DrawableInvocation;



public class Invocation extends Message{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Invocation() {}
	
	public Invocation(String invocationName, Instance source, Instance target,int order) {
		super(invocationName, source, target,order);
	}

	@Override
	public DrawableComponent getDrawable() {
		return new DrawableInvocation(this);
	}


}
