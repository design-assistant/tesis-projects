package model.data.generalStructure.sequenceDiagram;


import view.drawable.DrawableComponent;
import view.drawable.sequenceDiagram.DrawableInstance;
import model.data.generalStructure.classDiagram.Class;

public class Instance extends SequenceDiagramComponent{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Class reference;
	protected int length;
	
	public Instance() {
		length=0;
	}

	public Instance(String compName, Class reference) {
		super(compName);
		this.reference = reference;
		length=compName.length()+4;
	}

	public Class getReference() {
		return reference;
	}

	public void setReference(Class reference) {
		this.reference = reference;
	}

	@Override
	public DrawableComponent getDrawable() {
		return new DrawableInstance(this);
	}
	public int getLength() {
		return length;
	}
	public void setCompName(String compName) {
		this.compName = compName;
		length=compName.length()+4;
	}

		
}
