package model.data.generalStructure.sequenceDiagram;



import java.util.Hashtable;
import java.util.Vector;

import view.drawable.DrawableComponent;


import model.data.generalStructure.Component;
import model.data.generalStructure.Diagram;

public class SequenceDiagram extends Diagram{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public SequenceDiagram() {
		super();
	}

	public SequenceDiagram(String diagName) {
		super();
		this.diagName = diagName; 
	}

	public SequenceDiagram(String diagName, Hashtable<String, Component> components) {
		super(diagName,components);
	}


	@Override
	public Vector<DrawableComponent> createDinamicDrawables() {
		return super.createDrawables();
	}
	
	@Override
	public Vector<Component> getDinamicComponents() {
		return null;
	}

	@Override
	public void save() {
	}
}
