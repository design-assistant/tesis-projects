package model.data.generalStructure.sequenceDiagram;



public abstract class Message extends SequenceDiagramComponent{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Instance source;
	private Instance target;
	private int order;
	
	public Message() {}

	public Message(String messageName, Instance source, Instance target,int order) {
		super(messageName);
		this.source = source;
		this.target = target;
		this.order = order;
	}

	public void setOrder(int order) {
		this.order = order;
	}
	
	public int getOrder() {
		return order;
	}
	
	public Instance getSource() {
		return source;
	}

	public void setSource(Instance source) {
		this.source = source;
	}

	public Instance getTarget() {
		return target;
	}

	public void setTarget(Instance target) {
		this.target = target;
	}

}
