package model.data.generalStructure.sequenceDiagram;


import model.data.generalStructure.Component;

public abstract class SequenceDiagramComponent extends Component{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public SequenceDiagramComponent() {
		super();
	}
	
	public SequenceDiagramComponent(String compName) {
		super();
		this.compName = compName;

	}

	@Override
	public Component getCloneComponent() {
		return null;
	}

	@Override
	public void save() {
	}
	

}
