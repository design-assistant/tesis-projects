package model.data.generalStructure.sequenceDiagram;

import view.drawable.DrawableComponent;
import view.drawable.sequenceDiagram.DrawableDestroy;

public class Destroy extends Message{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Destroy() {}
	
	public Destroy(String destroyName, Instance source, Instance target,int order) {
		super(destroyName, source, target,order);
	}

	@Override
	public DrawableComponent getDrawable() {
		return new DrawableDestroy(this);
	}

	

}
