package model.data.generalStructure.sequenceDiagram;

import view.drawable.DrawableComponent;
import view.drawable.sequenceDiagram.DrawableReturn;



public class Return extends Message{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Return() {}
	
	public Return(String returnName, Instance source, Instance target,int order) {
		super(returnName, source, target,order);
	}

	@Override
	public DrawableComponent getDrawable() {
		return new DrawableReturn(this);
	}

	

}
