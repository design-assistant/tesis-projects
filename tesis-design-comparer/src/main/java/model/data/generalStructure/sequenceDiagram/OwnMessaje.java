package model.data.generalStructure.sequenceDiagram;

import view.drawable.DrawableComponent;
import view.drawable.sequenceDiagram.DrawableOwnMessage;



public class OwnMessaje extends Message{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public OwnMessaje() {}
	
	public OwnMessaje(String ownMessajeName, Instance source, Instance target,int order) {
		super(ownMessajeName, source, target, order);
	}

	@Override
	public DrawableComponent getDrawable() {
		return new DrawableOwnMessage(this);
	}

	
}
