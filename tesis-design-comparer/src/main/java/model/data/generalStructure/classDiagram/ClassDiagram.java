package model.data.generalStructure.classDiagram;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import model.data.generalStructure.Component;
import model.data.generalStructure.Design;
import model.data.generalStructure.Diagram;
import view.drawable.DrawableComponent;
import bo.IClassDiagramBO;
import context.ImplementationFactory;

public class ClassDiagram extends Diagram {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public void save() {
		IClassDiagramBO cdBO = (IClassDiagramBO) ImplementationFactory.getBean("classDiagramBO");
		this.setComps(new ArrayList<Component>());
		cdBO.persist(this);
		for (Enumeration<String> e = this.components.keys(); e.hasMoreElements();) {
			{
				ClassDiagramComponent component = (ClassDiagramComponent) this.components.get(e.nextElement());
				component.setDiagram(this);
				component.save();
				this.getComps().add(component);
			}
		}

	}
@Override
public boolean isInterface(String name) {
	for (Component component: this.comps){
		if(component.getCompName().equals(name)){
			return component.getClass().getName().endsWith("Interface");
		}
	}
	return false;
}


	public ClassDiagram() {
		super();
	}

	public ClassDiagram(String diagName) {
		super();
		this.diagName = diagName;
	}

	public ClassDiagram(String diagName, Hashtable<String, Component> components) {
		super(diagName, components);
	}

	@Override
	public Vector<DrawableComponent> createStaticDrawables() {
		return super.createDrawables();
	}

	@Override
	public Vector<Component> getStaticComponents() {
		return super.getComponentsVector();
	}

	@Override
	public void separateRelationComponents(Design design) {
		List<String> sources = new ArrayList<String>();
		List<String> targets = new ArrayList<String>();
		List<String> empties = new ArrayList<String>();
		List<String> sourcesAndTargets = new ArrayList<String>();
		List<BoxComponent> sourcesTroughPut = new ArrayList<BoxComponent>();
		List<BoxComponent> targetsTroughPut = new ArrayList<BoxComponent>();
		List<BoxComponent> sourcesAndTargetsTroughPut = new ArrayList<BoxComponent>();
		List<BoxComponent> emptiesTroughPut = new ArrayList<BoxComponent>();
		StringBuilder source = null;
		StringBuilder target = null;
		for (Component component : comps) {
			source = new StringBuilder("");
			target = new StringBuilder("");
			component.analizeComponent(source, target);
			if (!source.toString().equals("")) {
				Relation relation = (Relation) component;
				this.addRelation(relation);
				component.setThroughput();
				if ((!sources.contains(source.toString()) && !targets.contains(source.toString()))
						&& (!sourcesAndTargets.contains(source.toString()))) // NO ESTA EN NINGUNO&&
				{
					sources.add(source.toString());
					sourcesTroughPut.add(relation.getSource());
				} else if ((!sources.contains(source.toString())) && (!sourcesAndTargets.contains(source.toString()))) { // ESTA EN
					sourcesAndTargets.add(source.toString());
					targets.remove(source.toString());
					sourcesAndTargetsTroughPut.add(relation.getSource());
					targetsTroughPut.remove(relation.getSource());
				}
				if ((!sources.contains(target.toString()) && !targets.contains(target.toString()))
						&& (!sourcesAndTargets.contains(target.toString()))) // NO ESTA EN NINGUNO
				{
					targets.add(target.toString());
					targetsTroughPut.add(relation.getTarget());
				} else if ((!targets.contains(target.toString())) && (!sourcesAndTargets.contains(target.toString()))) {// ESTA EN SOURCE
					sourcesAndTargets.add(target.toString());
					sources.remove(target.toString());
					sourcesAndTargetsTroughPut.add(relation.getTarget());
					sourcesTroughPut.remove(relation.getTarget());
				}
				if (empties.size() != 0) {
					empties.remove(source.toString());
					emptiesTroughPut.remove(relation.getSource());
					if (empties.size() != 0) {
						empties.remove(target.toString());
						relation.getTarget();
					}
				}
			} else if (!target.toString().equals("")) {
				if (!(!sources.isEmpty() && sources.contains(target.toString()) || !targets.isEmpty()
						&& targets.contains(target.toString()) || !sourcesAndTargets.isEmpty()
						&& sourcesAndTargets.contains(target.toString()))) {
					empties.add(target.toString());
					emptiesTroughPut.add((BoxComponent) component);
				}
			}

		}
		int output = 0;
		int input = 0;
		for (int i = 0; i < sourcesTroughPut.size(); i++) {
			output += sourcesTroughPut.get(i).getOutPut();
		}
		design.setSourcesOutput(output);
		output = 0;

		for (int i = 0; i < sourcesAndTargetsTroughPut.size(); i++) {
			output += sourcesAndTargetsTroughPut.get(i).getOutPut();
			input += sourcesAndTargetsTroughPut.get(i).getInPut();
		}
		design.setSourcesAndTargetsInput(input);
		design.setSourcesAndTargetsOutput(output);
		input = 0;
		for (int i = 0; i < targetsTroughPut.size(); i++) {
			input += targetsTroughPut.get(i).getInPut();
		}
		design.setTargetInPut(input);
		design.setSources(sources);
		design.setSourcesAndTargets(sourcesAndTargets);
		design.setTargets(targets);

		design.setSourcesTroughPut(sourcesTroughPut);
		design.setSourcesAndTargetsTroughPut(sourcesAndTargetsTroughPut);
		design.setTargetsTroughPut(targetsTroughPut);
		design.setEmptiesTroughPut(emptiesTroughPut);

	}

	@Override
	public void addStubComponent(Design otherDesign, String action, Design design) {
		this.setDesign(design);
		if (this.getDesign().getSources().size() < otherDesign.getSources().size())
			this.getDesign().addSource(action);
		else if (this.getDesign().getTargets().size() < otherDesign.getTargets().size())
			this.getDesign().addTarget(action);
		else if (this.getDesign().getSourcesAndTargets().size() < otherDesign.getSourcesAndTargets().size())
			this.getDesign().addSourceAndTarget(action);
		else
			this.getDesign().addEmpty(action);

	}

}
