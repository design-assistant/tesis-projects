package model.data.generalStructure.classDiagram;

import java.util.ResourceBundle;

import model.data.generalStructure.Component;
import model.data.generalStructure.Repository;
import view.drawable.DrawableComponent;
import view.drawable.classDiagram.DrawableComposition;
import bo.ICompositionBO;
import context.ImplementationFactory;

public class Composition extends Relation {

	@SuppressWarnings("static-access")
	@Override
	public String getRelationDescription() {
		return this.COMPOSITION;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Composition() {
	}

	public Composition(boolean bool) {
		Repository rep = Repository.getInstance();
		if(!this.isAutoCreate())
		rep.addRelation(this);
	}
	public Composition(String compositionName, BoxComponent source, BoxComponent target) {
		super(compositionName, source, target);
	}

	@Override
	public DrawableComponent getDrawable() {
		return new DrawableComposition(this);
	}

	@Override
	public String getNewAttributeStub(String source) {
		if (this.getSource().getCompName().equals(source))
			return ResourceBundle.getBundle("sintaxisAbsorcion").getString("composicion")
					+ this.getTarget().getCompName();
		else
			return null;
	}

	@Override
	public void postSave() {
		ICompositionBO relationBO = (ICompositionBO) ImplementationFactory.getBean("compositionBO");
		relationBO.persist(this);

	}
	
	@Override
	public Component getCloneComponent() {
		Composition a= new Composition();
		a.setCompName(compName);
		return a;
	}
	
	@Override
	public int getCollectionsCount() {
		return 1;
	}

}
