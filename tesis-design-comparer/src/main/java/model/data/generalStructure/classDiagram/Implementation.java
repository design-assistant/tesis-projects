package model.data.generalStructure.classDiagram;

import model.data.generalStructure.Component;
import model.data.generalStructure.Repository;
import view.drawable.DrawableComponent;
import view.drawable.classDiagram.DrawableImplementation;
import bo.IImplementationBO;
import context.ImplementationFactory;

public class Implementation extends Relation {

	@SuppressWarnings("static-access")
	@Override
	public String getRelationDescription() {
		return this.IMPLEMENTATION;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Implementation() {
	

	}
	public Implementation(boolean bool) {
		Repository rep = Repository.getInstance();
		
		rep.addRelation(this);
	}

	public Implementation(String implementationName, BoxComponent source, BoxComponent target) {
		super(implementationName, source, target);
	}

	@Override
	public DrawableComponent getDrawable() {

		return new DrawableImplementation(this);
	}

	/**
	 * Sirve para saber si el objeto puede ser asociado a una jerarquia
	 * 
	 * @return : booleano que dice si es o no valido en una jerarquia. Redefinido para la herencia
	 */
	@Override
	public boolean isHierarchical() {
		return true;
	}

	@Override
	public void postSave() {
		IImplementationBO relationBO = (IImplementationBO) ImplementationFactory.getBean("implementationBO");
		relationBO.persist(this);

	}
	
	@Override
	public Component getCloneComponent() {
		Implementation a= new Implementation();
		a.setCompName(compName);
		return a;
	}
	
	@Override
	public int getFathersCount() {
		if (!Repository.getInstance().containsFather(this.getSource().getCompName()))
		{
			Repository.getInstance().addFather(this.getSource().getCompName());
			return 1;
		}
		return 0;
	}
	
	@Override
	public int getSonsCount() {
		if (!Repository.getInstance().containsSons(this.getTarget().getCompName()))
		{
			Repository.getInstance().addSon(this.getTarget().getCompName());
			return 1;
		}
		return 0;
	}
	
	@Override
	public int getImplementationCount() {
		return 1;
	}
}
