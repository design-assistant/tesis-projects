package model.data.generalStructure.classDiagram;

import java.util.ResourceBundle;

public class Attribute {

	private String name;
	private String ambient;
	private String typ;
	protected BoxComponent component;
	protected int id;
	private ResourceBundle properties = ResourceBundle
			.getBundle("sintaxisAbsorcion");

	public BoxComponent getComponent() {
		return component;
	}

	public void setComponent(BoxComponent component) {
		this.component = component;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Attribute(String name, String ambient, String type) {
		this.name = name;
		this.ambient = ambient;
		this.typ = type;
	}

	public Attribute() {

	}

	public String getName() {
		return name;
	}

	public void setAmbient(String ambient) {
		this.ambient = ambient;
	}

	public String getTyp() {
		return typ;
	}

	public String getAmbient() {
		return ambient;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setTyp(String typ) {
		this.typ = typ;
	}

	public String getLength() {
		String a = this.ambient != null ? this.ambient + " " : "";
		String t = this.typ != null ? this.typ + " " : "";
		return a + t + " " + name;
	}

	public boolean isFromOtherComponent() {

		if ((this.name.startsWith(properties.getString("agregacion")))
				|| (this.name.startsWith(properties.getString("asociacion")))
				|| (this.name.startsWith(properties.getString("composicion"))))
			return true;
		return false;
	}

	public String getNameIsClassOf() {
		String classOf = properties.getString("asociacion");
		String collectionOf = properties.getString("agregacion");
		String absorbedName = null;
		if (isFromOtherComponent())
			if (this.name.startsWith(classOf))
				absorbedName = this.name.substring(classOf.length(), name
						.length());
			else
				absorbedName = this.name.substring(collectionOf.length(), name
						.length());
		return absorbedName;
	}

	

}
