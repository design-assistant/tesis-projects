package model.data.generalStructure.classDiagram;

import java.util.ArrayList;
import java.util.List;

public abstract class BoxComponent extends ClassDiagramComponent {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected List<Method> methods = new ArrayList<Method>();
	protected int length;
	protected List<Attribute> attributes = new ArrayList<Attribute>();
	protected int inPut = 0;
	protected int outPut = 0;

	public BoxComponent() {
	}

	public int getInPut() {
		return inPut;
	}

	public void addInPut() {
		inPut++;
	}

	public void addOutPut() {
		outPut++;
	}

	public void setInPut(int inPut) {
		this.inPut = inPut;
	}

	public int getOutPut() {
		return outPut;
	}

	public void setOutPut(int outPut) {
		this.outPut = outPut;
	}

	public abstract boolean isClassProxy();

	public BoxComponent(String compName) {
		super(compName);

	}

	public List<Attribute> getAttributes() {
		for (int i = 0; i < attributes.size(); i++) {
			changeLength(attributes.get(i).getLength());
		}
		return attributes;
	}

	public void setAttributes(List<Attribute> attributes) {
		this.attributes = attributes;
	}

	/**
	 * Este metodo es para agregar nuevos atributos a la clase, usado cunado se hace la absorci�n de atributos
	 * 
	 * @param attributes :
	 *            atributos a agregar
	 */
	public void addAttributes(List<Attribute> attributes) {
		this.attributes.addAll(attributes);
	}

	public void addAtribute(Attribute t) {
		attributes.add(t);
	}

	public List<Attribute> getUsefullAttributes() {
		List<Attribute> ret = new ArrayList<Attribute>();
		for (int i = 0; i < attributes.size(); i++) {
			if ((attributes.get(i).getAmbient() != null) && (attributes.get(i).getTyp() != null))// cambio == por
				ret.add(attributes.get(i));
		}
		return ret;
	}

	protected List<Attribute> cloneAttributes() {
		List<Attribute> as = new ArrayList<Attribute>();
		for (Attribute a : attributes) {
			as.add(a);
		}
		return as;
	}

	@Override
	public void setCompName(String compName) {
		super.setCompName(compName);
		length = compName.length() + 4;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public int getLength() {
		return length;
	}

	protected void changeLength(String string) {
		if (this.length < string.length())
			length = string.length();
	}

	public List<Method> getMethods() {
		return methods;
	}

	public void setMethods(List<Method> ms) {
		if (ms != null) {
			for (int i = 0; i < ms.size(); i++) {
				changeLength(ms.get(i).getLength());
			}
			this.methods = ms;
		}
	}

	public List<Method> getUsefullMethods() {
		List<Method> ret = new ArrayList<Method>();
		for (int i = 0; i < methods.size(); i++) {
			if (methods.get(i).getAmbient() != null)
				ret.add(methods.get(i));
		}
		return ret;
	}

	public void addMethods(List<Method> methods) {
		this.methods.addAll(methods);
	}

	public abstract BoxComponent cloneBoxComponent();

	protected List<Method> cloneMethods() {
		List<Method> ms = new ArrayList<Method>();
		for (Method m : methods) {
			ms.add(m);
		}
		return ms;
	}

	public int getAbsorbedRelationsCount() {
		int ret = 0;
		for (int i = 0; i < attributes.size(); i++)
			if (attributes.get(i).isFromOtherComponent())
				ret++;
		return ret;
	}

	public int getUseAbsorbedRelationsCount() {
		int ret = 0;
		for (int i = 0; i < methods.size(); i++)
			if (methods.get(i).isUseOf())
				ret++;
		return ret;
	}

	public int getMultipleUseAbsorbedByComponentCount() {
		int ret = getUseAbsorbedRelationsCount();
		if (ret > 1)
			return ret;
		return 0;
	}

	public int getMultipleAbsorbedByComponentCount() {
		int ret = getAbsorbedRelationsCount();
		if (ret > 1)
			return ret;
		return 0;
	}

	@Override
	public String getBoxComponentName() {
		return this.getCompName();
	}

}
