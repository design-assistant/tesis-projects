package model.data.generalStructure.classDiagram;

import model.data.generalStructure.Component;
import model.data.generalStructure.Repository;
import view.drawable.DrawableComponent;
import view.drawable.classDiagram.DrawableInheritance;
import bo.IInheritanceBO;
import context.ImplementationFactory;

public class Inheritance extends Relation {

	@SuppressWarnings("static-access")
	@Override
	public String getRelationDescription() {
		return this.INHERITANCE;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Inheritance() {

	}

	public Inheritance(boolean bool) {
		Repository rep = Repository.getInstance();

		rep.addRelation(this);
	}

	public Inheritance(String inheritanceName, BoxComponent source, BoxComponent target) {
		super(inheritanceName, source, target);
	}

	@Override
	public DrawableComponent getDrawable() {
		return new DrawableInheritance(this);
	}

	/**
	 * Sirve para saber si el objeto puede ser asociado a una jerarquia
	 * 
	 * @return : booleano que dice si es o no valido en una jerarquia. Redefinido para la herencia
	 */
	public boolean isHierarchical() {
		return true;
	}

	@Override
	public void postSave() {
		IInheritanceBO relationBO = (IInheritanceBO) ImplementationFactory.getBean("inheritanceBO");
		relationBO.persist(this);
	}

	@Override
	public Component getCloneComponent() {
		Inheritance a = new Inheritance();
		a.setCompName(compName);
		return a;
	}

	@Override
	public int getFathersCount() {
		if (!Repository.getInstance().containsFather(this.getSource().getCompName())) {
			Repository.getInstance().addFather(this.getSource().getCompName());
			return 1;
		}
		return 0;
	}

	@Override
	public int getSonsCount() {
		if (!Repository.getInstance().containsSons(this.getTarget().getCompName())) {
			Repository.getInstance().addSon(this.getTarget().getCompName());
			return 1;
		}
		return 0;
	}

	@Override
	public int getInheritanceCount() {
		return 1;
	}

}
