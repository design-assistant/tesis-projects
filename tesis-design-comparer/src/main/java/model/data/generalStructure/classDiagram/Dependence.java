package model.data.generalStructure.classDiagram;

import java.util.ResourceBundle;

import model.data.generalStructure.Component;
import model.data.generalStructure.Repository;
import view.drawable.DrawableComponent;
import view.drawable.classDiagram.DrawableDependence;
import bo.IDependenceBO;
import context.ImplementationFactory;

public class Dependence extends Relation {

	@SuppressWarnings("static-access")
	@Override
	public String getRelationDescription() {
		return this.USE;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Dependence() {
		
	}
	
	public Dependence(boolean bool) {
		Repository rep = Repository.getInstance();
		
		rep.addRelation(this);
	}

	public Dependence(String dependenceName, BoxComponent source, BoxComponent target) {
		super(dependenceName, source, target);
	}

	@Override
	public DrawableComponent getDrawable() {
		return new DrawableDependence(this);
	}

	@Override
	public String getNewMethodStub(String source) {
		if (this.getSource().getCompName().equals(source))
			return ResourceBundle.getBundle("sintaxisAbsorcion").getString("method") + this.getTarget().getCompName();
		else
			return null;
	}

	@Override
	public void postSave() {

		IDependenceBO relationBO = (IDependenceBO) ImplementationFactory.getBean("dependenceBO");
		relationBO.persist(this);

	}
	@Override
	public Component getCloneComponent() {
		Dependence a= new Dependence();
		a.setCompName(compName);
		return a;
	}
 
	@Override
	public int getUsesCount() {
		return 1;
	}
	
}
