package model.data.generalStructure.classDiagram;

import java.util.ResourceBundle;

import model.data.generalStructure.Component;
import model.data.generalStructure.Repository;
import view.drawable.DrawableComponent;
import view.drawable.classDiagram.DrawableAgregation;
import bo.IAgregationBO;
import context.ImplementationFactory;

public class Agregation extends Relation {

	
	@SuppressWarnings("static-access")
	@Override
	public String getRelationDescription() {
	return this.AGREGATION;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public Agregation() {
	
	}
	public Agregation(boolean bool) {
		Repository rep = Repository.getInstance();
		if(!this.isAutoCreate())
		rep.addRelation(this);
	}

	public Agregation(String agregationName, BoxComponent source, BoxComponent target) {
		super(agregationName, source, target);
	}

	@Override
	public DrawableComponent getDrawable() {
		return new DrawableAgregation(this);
	}

	@Override
	public String getNewAttributeStub(String source) {
		if (this.getSource().getCompName().equals(source))
			return ResourceBundle.getBundle("sintaxisAbsorcion").getString("agregacion")
					+ this.getTarget().getCompName();
		else
			return null;
	}

	@Override
	public void postSave() {
		IAgregationBO relationBO = (IAgregationBO) ImplementationFactory.getBean("agregationBO");
		relationBO.persist(this);

	}
	@Override
	public Component getCloneComponent() {
		Agregation a= new Agregation();
		a.setCompName(compName);
		return a;
	}
	
	@Override
	public int getCollectionsCount() {
		return 1;
	}

}
