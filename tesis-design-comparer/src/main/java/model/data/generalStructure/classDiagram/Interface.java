package model.data.generalStructure.classDiagram;

import java.util.List;

import model.data.generalStructure.Repository;

import view.drawable.DrawableComponent;
import view.drawable.classDiagram.DrawableClass;
import view.drawable.classDiagram.DrawableInterface;
import bo.IInterfaceBO;
import context.ImplementationFactory;

public class Interface extends BoxComponent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Interface() {
	}

	public Interface(String interfaceName, List<Method> methods) {
		super(interfaceName);
		this.methods = methods;
	}

	@Override
	public DrawableComponent getDrawable() {
		if (this.attributes.size() > 0)
			return new DrawableClass(this);
		return new DrawableInterface(this);
	}

	@Override
	public void save() {
		if (this.id == 0) {
			IInterfaceBO iBO = (IInterfaceBO) ImplementationFactory.getBean("interfaceBO");
			for (int i = 0; i < methods.size(); i++) {
				Method m = (Method) methods.get(i);
				m.setComponent(this);

			}
			for (int i = 0; i < attributes.size(); i++) {
				Attribute m = (Attribute) attributes.get(i);
				m.setComponent(this);

			}
			iBO.persist(this);
		}
	}

	@Override
	public BoxComponent cloneBoxComponent() {
		Interface i = new Interface();
		i.setCompName(this.compName);
		i.setMethods(cloneMethods());
		return i;
	}

	@Override
	public boolean isClassProxy() {
		return true;
	}
	
	@Override
	public int getBoxComponentsCount() {
		if (!Repository.getInstance().isFatherOrSon(this.getCompName()))
			return 1;
		return 0;
	}
	
}
