package model.data.generalStructure.classDiagram;

import java.util.List;

import model.data.generalStructure.Repository;

import view.drawable.DrawableComponent;
import view.drawable.classDiagram.DrawableClass;
import bo.IClassBO;
import context.ImplementationFactory;

public class Class extends BoxComponent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected boolean abst;

	public Class() {
	}

	public Class(String className, List<Method> methods, List<Attribute> attributes, boolean isAbstract) {
		super(className);
		this.methods = methods;
		this.attributes = attributes;
		this.abst = isAbstract;
	}

	public boolean isAbst() {
		return abst;
	}

	public void setAbst(boolean abst) {
		this.abst = abst;
	}

	@Override
	public DrawableComponent getDrawable() {
		return new DrawableClass(this);
	}

	@Override
	public void save() {
		if (this.id == 0) {
			IClassBO classBO = (IClassBO) ImplementationFactory.getBean("classBO");
			for (int i = 0; i < methods.size(); i++) {
				Method m = (Method) methods.get(i);
				m.setComponent(this);
			}
			for (int i = 0; i < attributes.size(); i++) {
				Attribute m = (Attribute) attributes.get(i);
				m.setComponent(this);
			}
			classBO.persist(this);
		}
	}

	@Override
	public BoxComponent cloneBoxComponent() {
		Class cl = new Class();
		cl.setAbst(this.isAbst());
		cl.setCompName(this.compName);
		cl.setAttributes(cloneAttributes());
		cl.setMethods(cloneMethods());
		return cl;
	}

	@Override
	public boolean isClassProxy() {
		return false;
	}

	@Override
	public int getBoxComponentsCount() {
		if (!Repository.getInstance().isFatherOrSon(this.getCompName()))
			return 1;
		return 0;
	}
	
	
	
}
