package model.data.generalStructure.classDiagram;

import java.util.ArrayList;
import java.util.List;

import view.drawable.DrawableComponent;
import view.drawable.classDiagram.DrawablePackage;
import bo.IPackageBO;
import context.ImplementationFactory;


public class Package extends BoxComponent{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<ClassDiagramComponent> packageComponents = new ArrayList<ClassDiagramComponent>();
	
	public Package() {}

	public Package(String packageName,List<ClassDiagramComponent> packageComponents) {
		super(packageName);
		this.packageComponents = packageComponents;
	}

	public List<ClassDiagramComponent> getPackageComponents() {
		return packageComponents;
	}

	public void setPackageComponents(List<ClassDiagramComponent> packageComponents) {
		this.packageComponents = packageComponents;
	}

	@Override
	public DrawableComponent getDrawable() {
		return new DrawablePackage(this);
	}

	@Override
	public void save() {
		IPackageBO pBO=(IPackageBO)ImplementationFactory.getBean("packageBO");
		pBO.persist(this);
		
	}

	@Override
	public BoxComponent cloneBoxComponent() {
		return null;
	}

	@Override
	public boolean isClassProxy() {
		return true;
	}
}
