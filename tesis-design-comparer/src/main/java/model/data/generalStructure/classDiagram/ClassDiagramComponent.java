package model.data.generalStructure.classDiagram;

import model.data.generalStructure.Component;

public abstract class ClassDiagramComponent extends Component{


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ClassDiagramComponent() {
		super();
	}
	
	public ClassDiagramComponent(String compName) {
		super(compName);
	}
	
	public String getNewAttributeStub(String source){
		return null;
	}

	public String getNewMethodStub(String compName) {
		return null;
	}

	@Override
	public Component getCloneComponent() {
		return null;
	}

	
	
}
