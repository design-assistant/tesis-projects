package model.data.generalStructure.classDiagram;

public abstract class Relation extends ClassDiagramComponent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected BoxComponent source;
	protected BoxComponent target;

	public static final String ASOCIATION = "Asociation";
	public static final String AGREGATION = "Agregation";
	public static final String COMPOSITION = "Composition";
	public static final String USE = "Use";
	public static final String INHERITANCE = "Inheritance";
	public static final String IMPLEMENTATION = "Implementation";

	public abstract String getRelationDescription();
	
	protected boolean autoCreate;

	public boolean isAutoCreate() {
		return autoCreate;
	}

	public void setAutoCreate(boolean autoCreate) {
		this.autoCreate = autoCreate;
	}

	public Relation() {
	}

	public Relation(String relationName, BoxComponent source, BoxComponent target) {
		super(relationName);
		this.source = source;
		this.target = target;
	}

	public BoxComponent getSource() {
		return source;
	}

	public void setSource(BoxComponent source) {
		this.source = source;
	}

	public BoxComponent getTarget() {
		return this.target;
	}

	public void setTarget(BoxComponent target) {
		this.target = target;
	}

	@Override
	public void save() {
		if (this.source.getId() == 0) {
			this.source.setDiagram(this.getDiagram());
			this.source.save();
		}
		if (this.target.getId() == 0) {
			this.target.setDiagram(this.getDiagram());
			this.target.save();
		}
		postSave();

	}

	public abstract void postSave();

	@Override
	public void analizeComponent(StringBuilder source, StringBuilder target) {
		source.append(this.getSource().getCompName());
		target.append(this.getTarget().getCompName());
	}

	@Override
	public void setThroughput() {
		this.source.addOutPut();
		this.target.addInPut();
	}
}
