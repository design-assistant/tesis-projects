package model.data.generalStructure.classDiagram;

import java.io.Serializable;
import java.util.ResourceBundle;

import context.ImplementationFactory;

import bo.IMethodBO;

public class Method  implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected String name;
	protected String ambient;
	protected String parameters;
	protected String typ;
	protected boolean abst;
	protected BoxComponent component;
	protected int id;
	private ResourceBundle properties = ResourceBundle
	.getBundle("sintaxisAbsorcion");
	

	public int getId() {
		return id;
	}

	public boolean isAbst() {
		return abst;
	}

	public void setAbst(boolean abst) {
		this.abst = abst;
	}

	public void setId(int id) {
		this.id = id;
	}

	public BoxComponent getComponent() {
		return component;
	}

	public void setComponent(BoxComponent component) {
		this.component = component;
	}

	public Method() {
	}

	public Method(String name, String ambient, String parameters, String typ, boolean isAbstract) {
		super();
		this.name = name;
		this.ambient = ambient;
		this.parameters = parameters;
		this.typ = typ;
		this.abst = isAbstract;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAmbient() {
		return ambient;
	}

	public void setAmbient(String ambient) {
		this.ambient = ambient;
	}

	public String getParameters() {
		return parameters;
	}

	public void setParameters(String parameters) {
		this.parameters = parameters;
	}

	public String getTyp() {
		return typ;
	}

	public void setTyp(String typ) {
		this.typ = typ;
	}

	public boolean isAbstract() {
		return abst;
	}

	public void setAbstract(boolean isAbstract) {
		this.abst = isAbstract;
	}

	public String getLength() {
		String a = this.ambient != null ? this.ambient + " " : "";
		String t = this.typ != null ? this.typ + " " : "";
		if(parameters==null || parameters.equals("null"))
			parameters="";
		return a + name + "(" + parameters + "): " + t;
	}

	public void save() {
		IMethodBO mbo=(IMethodBO)ImplementationFactory.getBean("methodBO");
		mbo.persist(this);
		
	}

	public boolean isUseOf() {
		if ((this.name.startsWith(properties.getString("method"))))
			return true;
		return false;
	}
	
	public boolean isFromOtherComponent() {

		if ((this.name.startsWith(properties.getString("method"))))
			return true;
		return false;
	}

	public String getNameIsUseOf() {
		String useOf = properties.getString("method");
		String absorbedName = null;
		if (isFromOtherComponent())
			if (this.name.startsWith(useOf))
				absorbedName = this.name.substring(useOf.length(), name
						.length());
		return absorbedName;
	}
	
	@Override
	public boolean equals(Object obj) {
		try
		{
			Method m=(Method)obj;
			return m.getName().equals(this.getName());
		}
		catch(ClassCastException e){
			return false;
		}
		
	}

}
