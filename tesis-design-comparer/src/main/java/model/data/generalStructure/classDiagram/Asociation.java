package model.data.generalStructure.classDiagram;

import java.util.ResourceBundle;

import model.data.generalStructure.Component;
import model.data.generalStructure.Repository;
import view.drawable.DrawableComponent;
import view.drawable.classDiagram.DrawableAsociation;
import bo.IAsociationBO;
import context.ImplementationFactory;

public class Asociation extends Relation {

	@SuppressWarnings("static-access")
	@Override
	public String getRelationDescription() {
		return this.ASOCIATION;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Asociation() {
	}
	public Asociation(boolean bool) {
		Repository rep = Repository.getInstance();
		
		rep.addRelation(this);
	}

	public Asociation(String asociationName, BoxComponent source, BoxComponent target) {
		super(asociationName, source, target);
	}

	@Override
	public DrawableComponent getDrawable() {
		return new DrawableAsociation(this);
	}

	@Override
	public String getNewAttributeStub(String source) {
		if (this.getSource().getCompName().equals(source))
			return ResourceBundle.getBundle("sintaxisAbsorcion").getString("asociacion")
					+ this.getTarget().getCompName();
		else
			return null;
	}

	@Override
	public void postSave() {

		IAsociationBO relationBO = (IAsociationBO) ImplementationFactory.getBean("asociationBO");
		relationBO.persist(this);

	}
	
	@Override
	public Component getCloneComponent() {
		Asociation a= new Asociation();
		a.setCompName(compName);
		return a;
	}
	
	@Override
	public int getAsociationCount() {
		return 1;
	}

}
