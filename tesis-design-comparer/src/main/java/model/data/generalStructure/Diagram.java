package model.data.generalStructure;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import model.data.generalStructure.classDiagram.Relation;

import view.drawable.DrawableComponent;

public abstract class Diagram implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected String diagName;
	protected Hashtable<String, Component> components = new Hashtable<String, Component>();
	protected List<Component> comps = new ArrayList<Component>();
	protected int id;
	protected Design design;
	private List<Relation> relations = new ArrayList<Relation>();

	protected void addRelation(Relation relation) {
		this.relations.add(relation);
	}

	public List<Relation> getRelations() {
		return relations;
	}

	public Design getDesign() {
		return design;
	}

	public void setDesign(Design design) {
		this.design = design;
	}

	public List<Component> getComps() {
		return comps;
	}

	public void setComps(List<Component> comps) {
		this.comps = comps;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Diagram() {
	}

	public Diagram(String diagName, Hashtable<String, Component> components) {
		super();
		this.diagName = diagName;
		this.components = components;
	}

	public String getDiagName() {
		return diagName;
	}

	public void setDiagName(String diagName) {
		this.diagName = diagName;
	}

	public Hashtable<String, Component> getComponents() {
		return components;
	}

	public void setComponents(Hashtable<String, Component> components) {
		this.components = components;
		makeComps();
	}

	private void makeComps() {
		for (Enumeration<String> e = components.keys(); e.hasMoreElements();) {
			comps.add(components.get(e.nextElement()));
		}

	}

	/**
	 * 
	 * @param component
	 */
	public void addComponent(Component component) {
		if (component != null) {
			components.put(component.getPath() + component.getCompName(), component);
			comps.add(component);
		}
	}

	public void makeComponents() {
		for (Component component : comps) {
			components.put(component.getPath(), component);
		}

	}

	public Vector<DrawableComponent> createDrawables() {
		Vector<DrawableComponent> ret = new Vector<DrawableComponent>();
		String key;
		if (components.size() > 0) {
			for (Enumeration<String> e = components.keys(); e.hasMoreElements();) {
				key = e.nextElement();
				ret.add(components.get(key).getDrawable());
			}
		} else if (comps.size() > 0) {
			for (int i = 0;i < comps.size();i++) {
				ret.add(comps.get(i).getDrawable());
			}
		}
		return ret;
	}

	public Vector<DrawableComponent> createStaticDrawables() {
		return null;
	}

	public Vector<DrawableComponent> createDinamicDrawables() {
		return null;
	}

	public Vector<Component> getStaticComponents() {
		return null;
	}

	public Vector<Component> getDinamicComponents() {
		return null;
	}

	public Vector<Component> getComponentsVector() {
		Vector<Component> ret = new Vector<Component>();
		String key;
		if (!components.isEmpty()) {
			for (Enumeration<String> e = components.keys(); e.hasMoreElements();) {
				key = e.nextElement();
				ret.add(components.get(key));
			}
		} else {
			for (int i = 0; i < comps.size(); i++) {
				ret.add(comps.get(i));
			}
		}
		return ret;
	}

	public abstract void save();

	public int getFathersCount() {
		int fathers = 0;
		for (Component comp : comps)
			fathers += comp.getFathersCount();
		return fathers;
	}

	public int getSonsCount() {
		int sons = 0;
		for (Component comp : comps)
			sons += comp.getSonsCount();
		return sons;
	}

	public Integer getBoxComponentsCount() {
		int box = 0;
		for (Component comp : comps)
			box += comp.getBoxComponentsCount();
		return box;
	}

	public Integer getInheritanceCount() {
		int inheritance = 0;
		for (Component comp : comps)
			inheritance += comp.getInheritanceCount();
		return inheritance;
	}

	public Integer getImplementationCount() {
		int implementation = 0;
		for (Component comp : comps)
			implementation += comp.getImplementationCount();
		return implementation;
	}

	public Integer getUsesCount() {
		int uses = 0;
		for (Component comp : comps)
			uses += comp.getUsesCount();
		return uses;
	}

	public Integer getAsociationCount() {
		int asociation = 0;
		for (Component comp : comps)
			asociation += comp.getAsociationCount();
		return asociation;
	}

	public Integer getCollectionsCount() {
		int collections = 0;
		for (Component comp : comps)
			collections += comp.getCollectionsCount();
		return collections;
	}

	public void separateRelationComponents(Design design) {
	}

	public void addStubComponent(Design otherDesign, String action, Design design) {
	}
	
	public void clearRelations(){
		this.relations = new ArrayList<Relation>();
	}

	public boolean isInterface(String name) {
		return false;
	}

}
