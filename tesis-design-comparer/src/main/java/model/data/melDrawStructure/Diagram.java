package model.data.melDrawStructure;

import java.util.Enumeration;
import java.util.Hashtable;


public abstract class Diagram{
	protected String name;
	@SuppressWarnings("unchecked")
	protected Hashtable components;
	
	public Diagram() {
		
	}
	public Diagram(String name){
		this.name=name;
	}
	
	public String getName() {
		return name;
	}
	@SuppressWarnings("unchecked")
	public Hashtable getComponents() {
		return components;
	}
	public void setName(String name) {
		this.name = name;
	}
	@SuppressWarnings("unchecked")
	public void setComponents(Hashtable components) {
		this.components = components;
	}
	@SuppressWarnings("unchecked")
	public model.data.generalStructure.Diagram marshall() {
		DiagramComponent aux;
		String key;
		model.data.generalStructure.Diagram generalDiagram = getGeneralDiagram();
		model.data.generalStructure.Component generalComponent;
		for (Enumeration<String> e = components.keys();e.hasMoreElements();)
		{
			key = e.nextElement();
			aux = (DiagramComponent)components.get(key);
			if (aux.isValid(aux.getComponentName()))
			{
				generalComponent = aux.marshall();
				generalDiagram.addComponent(generalComponent);
			}
		}
		generalDiagram.setDiagName(name);
		return generalDiagram;
	}
	protected abstract model.data.generalStructure.Diagram getGeneralDiagram();
	
}
