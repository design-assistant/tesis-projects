package model.data.melDrawStructure.statics;

import model.data.generalStructure.Component;
import model.data.generalStructure.Repository;
import model.data.generalStructure.classDiagram.BoxComponent;
import model.data.generalStructure.classDiagram.ClassDiagramComponent;

public abstract class Relation extends StaticComponent {

	private StaticTerminal source;
	private StaticTerminal target;

	public Relation() {
		super();
	}

	public Relation(String componentName, StaticTerminal source2, StaticTerminal target2) {
		super(componentName);
		this.source = source2;
		this.target = target2;
	}

	@Override
	public String getComponentName() {
		return super.getComponentName();
	}

	@Override
	public void setComponentName(String componentName) {
		super.setComponentName(componentName);
	}

	public StaticTerminal getSource() {
		return source;
	}

	public StaticTerminal getTarget() {
		return target;
	}

	public void setSource(StaticTerminal source) {
		this.source = source;
	}

	public void setTarget(StaticTerminal target) {
		this.target = target;
	}

	@Override
	public Component marshall() {
		String name = this.getSource().getComponentName();
		BoxComponent source = Repository.getInstance().getBoxComponent(name);
		if (source== null) {
			 source = (BoxComponent) this.getSource().marshall();
			 Repository.getInstance().addBoxComponent(source);
		}
		name = this.getTarget().getComponentName();
		BoxComponent target = Repository.getInstance().getBoxComponent(name);
		if (target== null) {
			 target = (BoxComponent) this.getTarget().marshall();
			 Repository.getInstance().addBoxComponent(target);
		}
		model.data.generalStructure.classDiagram.Relation generalRelation = getSpecificRelation();
		generalRelation.setCompName(this.componentName);
		generalRelation.setSource(source);
		generalRelation.setTarget(target);
		generalRelation.setAutoCreate(true);
		return generalRelation;
	}

	protected abstract model.data.generalStructure.classDiagram.Relation getSpecificRelation();

	protected abstract model.data.generalStructure.classDiagram.Relation getSpecificRelation(ClassDiagramComponent component);

}
