 package model.data.melDrawStructure.statics;

public class Attribute {
	private String name;
	private String ambient;
	private String type;
	
	public Attribute(String name, String ambient, String type) {
		this.name = name;
		this.ambient = ambient;
		this.type = type;
	}
	
	public Attribute() {
	
	}
	public String getName() {
		return name;
	}

	public void setAmbient(String ambient) {
		this.ambient = ambient;
	}
	
	public String getType() {
		return type;
	}
	public String getAmbient() {
		return ambient;
	}
	public void setName(String name) {
		this.name = name;
	}

	public void setType(String type) {
		this.type = type;
	}

	public model.data.generalStructure.classDiagram.Attribute marshall() {
		model.data.generalStructure.classDiagram.Attribute generalAttribute = new model.data.generalStructure.classDiagram.Attribute(this.name,this.ambient,this.type);
		return generalAttribute;
	}

}
