package model.data.melDrawStructure.statics;

import java.util.Hashtable;

import model.data.generalStructure.Diagram;

public class ClassDiagram extends StaticDiagram{
	
	@SuppressWarnings("unchecked")
	public ClassDiagram() {
		super();
		this.components = new Hashtable();
	}

	@SuppressWarnings("unchecked")
	public ClassDiagram(String name) {
		super(name);
		this.components=new Hashtable();
	}
	

	@SuppressWarnings("unchecked")
	@Override
	public Hashtable getComponents() {
		return super.getComponents();
	}
	
	@Override
	public String getName() {
		return super.getName();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void setComponents(Hashtable components) {
		super.setComponents(components);
	}
	
	@Override
	public void setName(String name) {
		super.setName(name);
	}

	@Override
	protected Diagram getGeneralDiagram() {
		return new model.data.generalStructure.classDiagram.ClassDiagram(this.getName());
	}
	
			
}
