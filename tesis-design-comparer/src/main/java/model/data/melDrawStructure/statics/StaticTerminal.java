package model.data.melDrawStructure.statics;


public abstract class StaticTerminal extends StaticComponent {
	
	public StaticTerminal() {
		super();
	}
	
	public StaticTerminal(String componentName){
		super(componentName);
	}
	
	@Override
	public String getComponentName() {
		return super.getComponentName();
	}
	
	@Override
	public void setComponentName(String componentName) {
		super.setComponentName(componentName);
	}
	
	
	
}
