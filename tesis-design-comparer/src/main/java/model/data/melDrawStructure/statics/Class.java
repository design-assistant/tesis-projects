package model.data.melDrawStructure.statics;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import model.data.generalStructure.Component;
import model.data.generalStructure.Repository;
import model.data.generalStructure.classDiagram.ClassDiagramComponent;

public class Class extends StaticTerminal {

	@SuppressWarnings("unchecked")
	private Vector methods;
	@SuppressWarnings("unchecked")
	private Vector attributes;

	@SuppressWarnings("unchecked")
	public Class() {
		super();
		methods = new Vector();
		attributes = new Vector();
	}

	@SuppressWarnings("unchecked")
	public Class(String name, Vector attributes, Vector methods) {
		super(name);
		this.attributes = attributes;
		this.methods = methods;
	}

	@SuppressWarnings("unchecked")
	public Vector getAttributes() {
		return attributes;
	}

	@SuppressWarnings("unchecked")
	public void setAttributes(Vector attributes) {
		this.attributes = attributes;
	}

	@Override
	public String getComponentName() {
		return super.getComponentName();
	}

	@Override
	public void setComponentName(String componentName) {
		super.setComponentName(componentName);
	}

	@SuppressWarnings("unchecked")
	public Vector getMethods() {
		if (methods != null)
			return methods;
		else
			return null;
	}

	@SuppressWarnings("unchecked")
	public void setMethods(Vector methods) {
		this.methods = methods;
	}

	@Override
	public Component marshall() {
		Repository rep= Repository.getInstance();
		if(rep.getBoxComponent(this.componentName)!=null)
			return rep.getBoxComponent(this.componentName);
		model.data.generalStructure.classDiagram.Class generalClass = new model.data.generalStructure.classDiagram.Class();
		generalClass.setCompName(this.getComponentName());
		generalClass.setAbst(this.obtainAbtract());
		List<model.data.generalStructure.classDiagram.Method> generalMethods = new ArrayList<model.data.generalStructure.classDiagram.Method>();
		List<model.data.generalStructure.classDiagram.Attribute> generalAttributes = new ArrayList<model.data.generalStructure.classDiagram.Attribute>();
		Method method;
		for (int i = 0; i < methods.size(); i++) {
			method = (Method) methods.get(i);
			generalMethods.add(method.marshall());
		}
		Attribute attribute;
		for (int i = 0; i < attributes.size(); i++) {
			attribute = (Attribute) attributes.get(i);
			generalAttributes.add(attribute.marshall());
		}
		generalClass.setAttributes(generalAttributes);
		
		generalClass.setMethods(generalMethods);
		rep.addBoxComponent(generalClass);
		return generalClass;
	}

	@SuppressWarnings("unchecked")
	public void create(ClassDiagramComponent component){
		model.data.generalStructure.classDiagram.Class generalClass = (model.data.generalStructure.classDiagram.Class)component;
		this.setComponentName(generalClass.getCompName());
		this.methods = new Vector();
		this.attributes = new Vector();
		Method method;
		for (int i = 0; i < generalClass.getMethods().size(); i++) {
			method = new Method(generalClass.getMethods().get(i).getName(),
					generalClass.getMethods().get(i).getAmbient(), generalClass
							.getMethods().get(i).getParameters(), generalClass
							.getMethods().get(i).getTyp(), generalClass.getCompName());
			if(generalClass.getMethods().get(i).isAbstract())
				method.setAbstract(true);
			methods.add(method);
		}
		Attribute attribute;
		for (int i = 0; i < generalClass.getAttributes().size(); i++) {
			attribute = new Attribute(generalClass.getAttributes().get(i)
					.getName(), generalClass.getAttributes().get(i)
					.getAmbient(), generalClass.getAttributes().get(i)
					.getTyp());
			attributes.add(attribute);
		}
		
	}

	@SuppressWarnings("unchecked")
	public boolean obtainAbtract() {
		for (Method m : (Vector<Method>) this.methods) {
			if (m.isAbstract())
				return true;
		}
		return false;
	}



}
