package model.data.melDrawStructure.statics;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import model.data.generalStructure.Component;
import model.data.generalStructure.Repository;

public class Interface extends StaticTerminal {

	@SuppressWarnings("unchecked")
	protected Vector methods;
	
	@SuppressWarnings("unchecked")
	public Interface(){
		super();
		this.methods = new Vector();
	}
	
	@SuppressWarnings("unchecked")
	public Interface(String interfaceName,Vector methods){
		super(interfaceName);
		this.methods = methods;
	}
	
	@SuppressWarnings("unchecked")
	public Vector getMethods() {
		return methods;
	}
	
	@SuppressWarnings("unchecked")
	public void setMethods(Vector methods) {
		this.methods = methods;
	}
	
	@Override
	public String getComponentName() {
		return super.getComponentName();
	}
	
	@Override
	public void setComponentName(String componentName) {
		super.setComponentName(componentName);
	}

	@Override
	public Component marshall() {
		Repository rep= Repository.getInstance();
		if(rep.getBoxComponent(this.componentName)!=null)
			return rep.getBoxComponent(this.componentName);
		model.data.generalStructure.classDiagram.Interface generalInterface = new model.data.generalStructure.classDiagram.Interface();
		generalInterface.setCompName(this.getComponentName());
		List<model.data.generalStructure.classDiagram.Method> generalMethods = new ArrayList<model.data.generalStructure.classDiagram.Method>();
		Method method;
		for (int i = 0;i < methods.size();i++)
		{
			method = (Method)methods.get(i);
			generalMethods.add(method.marshall());
		}
		generalInterface.addAttributes(new ArrayList<model.data.generalStructure.classDiagram.Attribute>());
		generalInterface.setMethods(generalMethods);
		rep.addBoxComponent(generalInterface);
		return generalInterface;
	}

	

}
