package model.data.melDrawStructure.statics;

import model.data.generalStructure.classDiagram.Asociation;
import model.data.generalStructure.classDiagram.ClassDiagramComponent;

public class AsociationRelation extends Relation{

	public AsociationRelation(){
		super();
	}
	
	public AsociationRelation(String componentName, StaticTerminal source2, StaticTerminal target2) {
		super(componentName,source2,target2);
	}

	@Override
	protected model.data.generalStructure.classDiagram.Relation getSpecificRelation() {
		return new model.data.generalStructure.classDiagram.Asociation(true);
	}

	@Override
	protected model.data.generalStructure.classDiagram.Relation getSpecificRelation(ClassDiagramComponent component) {
		return (Asociation)component;
	}
	
}
