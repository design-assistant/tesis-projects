package model.data.melDrawStructure.statics;

public class Method {
	private String name;
	private String ambient;
	private String parameters;
	private String type;
	private String own;
	private boolean isAbstract;

	public Method(String name, String ambient, String parameters, String type, String own) {
		this.name = name;
		this.ambient = ambient;
		this.parameters = parameters;
		if((type!=null)&&(type.trim().length()>0))
			this.type = type;
		else
			this.type="void";
		this.own=own;
		isAbstract=false;
	}

	public Method() {}

	public void setAmbient(String ambient) {
		this.ambient = ambient;
	}
	public String getName() {
		return name;
	}
	public String getParameters() {
		return parameters;
	}
	public String getType() {
		return type;
	}
	public String getAmbient() {
		return ambient;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setParameters(String parameters) {
		this.parameters = parameters;
	}
	public void setPublic(String ambient) {
		this.ambient = ambient;
	}
	public void setType(String type) {
		this.type = type;
	}
	public boolean isAbstract() {
		return isAbstract;
	}
	public void setAbstract(boolean isAbstract) {
		this.isAbstract = isAbstract;
	}

	public String getOwn() {
		return own;
	}

	public void setOwn(String own) {
		this.own = own;
	}

	public model.data.generalStructure.classDiagram.Method marshall() {
		model.data.generalStructure.classDiagram.Method generalMethod = new model.data.generalStructure.classDiagram.Method(this.name,this.ambient,this.parameters,this.type,this.isAbstract);
		return generalMethod;
	}

}
