package model.data.melDrawStructure.statics;



import model.data.generalStructure.Component;
import model.data.generalStructure.Repository;

public class Package extends StaticTerminal {

	public Package() {
		super();
	}
	
	public Package(String packageName){
		super(packageName);
	}

	@Override
	public String getComponentName() {
		return super.getComponentName();
	}
	
	@Override
	public void setComponentName(String componentName) {
		super.setComponentName(componentName);
	}

	@Override
	public Component marshall() {
		Repository rep= Repository.getInstance();
		if(rep.getBoxComponent(this.componentName)!=null)
			return rep.getBoxComponent(this.componentName);
		model.data.generalStructure.classDiagram.Package generalPackage = new model.data.generalStructure.classDiagram.Package();
		generalPackage.setCompName(this.getComponentName());
		rep.addBoxComponent(generalPackage);
		return generalPackage;
	}

	
	
	
}
