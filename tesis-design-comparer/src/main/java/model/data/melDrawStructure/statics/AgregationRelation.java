package model.data.melDrawStructure.statics;

import model.data.generalStructure.classDiagram.Agregation;
import model.data.generalStructure.classDiagram.ClassDiagramComponent;

public class AgregationRelation extends Relation{

	public AgregationRelation(){
		super();
	}
	
	public AgregationRelation(String componentName, StaticTerminal source2, StaticTerminal target2) {
		super(componentName,source2,target2);
	}

	@Override
	protected model.data.generalStructure.classDiagram.Relation getSpecificRelation() {
		return new model.data.generalStructure.classDiagram.Agregation(true);
	}

	@Override
	protected model.data.generalStructure.classDiagram.Relation getSpecificRelation(ClassDiagramComponent component) {
		return (Agregation)component;
	}


	

}
