package model.data.melDrawStructure.statics;

import model.data.melDrawStructure.DiagramComponent;
import model.data.melDrawStructure.Repository;

public abstract class StaticComponent extends DiagramComponent {

	public StaticComponent() {
		super();
	}
	
	public StaticComponent(String componentName){
		super(componentName);
	}
	
	@Override
	public String getComponentName() {
		return super.getComponentName();
	}
	
	@Override
	public void setComponentName(String componentName) {
		super.setComponentName(componentName);
	}
	
	@Override
	public boolean isValid(String componentName) {
		return !Repository.getRepository().containsStaticComponent(componentName);
	}
	
	
	
}
