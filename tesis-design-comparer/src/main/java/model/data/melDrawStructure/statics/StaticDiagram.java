package model.data.melDrawStructure.statics;

import java.util.Hashtable;

import model.data.melDrawStructure.Diagram;

public abstract class StaticDiagram extends Diagram {
		
	public StaticDiagram() {
		super();
	}
	
	public StaticDiagram(String name){
		super(name);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Hashtable getComponents() {
		return super.getComponents();
	}
	
	@Override
	public void setName(String name) {
		super.setName(name);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void setComponents(Hashtable components) {
		super.setComponents(components);
	}
	
	@Override
	public String getName() {
		return super.getName();
	}

}
