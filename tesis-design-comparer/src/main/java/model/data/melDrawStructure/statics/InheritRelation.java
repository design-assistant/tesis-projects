package model.data.melDrawStructure.statics;

import model.data.generalStructure.classDiagram.ClassDiagramComponent;
import model.data.generalStructure.classDiagram.Inheritance;

public class InheritRelation extends Relation{

	public InheritRelation(){
		super();
	}
	
	public InheritRelation(String componentName, StaticTerminal source2, StaticTerminal target2) {
		super(componentName,source2,target2);
	}

	@Override
	protected model.data.generalStructure.classDiagram.Relation getSpecificRelation() {
		return new model.data.generalStructure.classDiagram.Inheritance(true);
	}

	@Override
	protected model.data.generalStructure.classDiagram.Relation getSpecificRelation(ClassDiagramComponent component) {
		return (Inheritance)component;
	}

	

	


}
