package model.data.melDrawStructure;

import java.io.FileReader;


import org.exolab.castor.xml.Unmarshaller;



public class Project {

	public Project() {}
	
	public DesignProject decode(String rout)
	{
		DesignProject project= new DesignProject();
		try {
		      project = (DesignProject)Unmarshaller.unmarshal(DesignProject.class, new FileReader(rout));
		} catch (Exception e) {
			e.printStackTrace();
		}
		    return project;
	}
	
}
