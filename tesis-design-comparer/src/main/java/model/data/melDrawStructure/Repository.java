package model.data.melDrawStructure;

import java.util.Vector;

public class Repository {

	private static Repository repository;
	private Vector<String> staticComponents = new Vector<String>();


	public static Repository getRepository() {
		if (repository == null)
			repository = new Repository();
		return repository;
	}

	public boolean containsStaticComponent(String component) {
		if (staticComponents.contains(component))
			return true;
		else {
			addStaticComponent(component);
			return false;
		}
	}

	public void addStaticComponent(String component) {
		staticComponents.add(component);
	}

	public void clearRepository() {
		repository= new Repository();
		
	}

	

	
}
