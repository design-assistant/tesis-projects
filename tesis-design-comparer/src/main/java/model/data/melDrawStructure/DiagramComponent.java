package model.data.melDrawStructure;

public abstract class DiagramComponent{
	
	protected String componentName;
	
	public DiagramComponent() {    
		
	}
	
	public DiagramComponent(String componentName){
		this.componentName = componentName;
	}

	public String getComponentName() {
		return componentName;
	}
	
	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}

	public abstract model.data.generalStructure.Component marshall();

	public abstract boolean isValid(String componentName2);
	
	

}
