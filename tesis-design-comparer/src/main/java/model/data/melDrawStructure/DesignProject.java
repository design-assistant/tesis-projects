package model.data.melDrawStructure;

import java.util.Vector;

import model.data.generalStructure.Design;

public class DesignProject {
	String name;
	Vector<Diagram> components;

	public DesignProject() {
	}

	public DesignProject(String name, Vector<Diagram> components) {
		super();
		this.name = name;
		this.components = components;
	}

	public Vector<Diagram> getComponents() {
		return components;
	}

	public String getName() {
		return name;
	}

	public void setComponents(Vector<Diagram> components) {
		this.components = components;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Este m�todo es el que se encarga de migrar la estructura del usuario, en
	 * este caso de la aplicaci�n LogicPatternsCase, a la estructura de general
	 * de DesignComparer
	 */
	public Design marshall() {
		Design design = new Design();
		design.setName(name);
		for (int i = 0; i < components.size(); i++)
			design.addDiagram(components.get(i).marshall());
		return design;
	}

}
