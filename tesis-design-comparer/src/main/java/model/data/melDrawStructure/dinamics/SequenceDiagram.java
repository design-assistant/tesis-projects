package model.data.melDrawStructure.dinamics;

import java.util.Hashtable;

import model.data.generalStructure.Diagram;

public class SequenceDiagram extends DinamicDiagram{

	@SuppressWarnings("unchecked")
	public SequenceDiagram() {
		super();
		this.components = new Hashtable();
	}
	
	@SuppressWarnings("unchecked")
	public SequenceDiagram(String name) {
		super(name);
		this.components=new Hashtable();
	}

	@Override
	protected Diagram getGeneralDiagram() {
		return new model.data.generalStructure.sequenceDiagram.SequenceDiagram(this.getName());
	}

}
