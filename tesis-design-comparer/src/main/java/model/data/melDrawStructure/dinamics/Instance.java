package model.data.melDrawStructure.dinamics;

import model.data.generalStructure.Component;
import model.data.melDrawStructure.statics.Class;

public class Instance extends DinamicTerminal{

	private Class reference;
	private String diagName;
	
	public Instance() {
		super();
	}
	
	public Instance (String componentName,Class refeer,String diagName){
		super(componentName);
		this.reference = refeer;
		this.diagName = diagName;
	}
	
	@Override
	public String getComponentName() {
		return super.getComponentName();
	}
	
	public String getDiagName() {
		return diagName;
	}
	
	@Override
	public void setComponentName(String componentName) {
		super.setComponentName(componentName);
	}
	
	public void setDiagName(String diagName) {
		this.diagName = diagName;
	}
	
	public void setReference(Class reference) {
		this.reference = reference;
	}
	
	public Class getReference() {
		return reference;
	}

	@Override
	public Component marshall() {
		model.data.generalStructure.sequenceDiagram.Instance generalInstance = new model.data.generalStructure.sequenceDiagram.Instance();
		generalInstance.setCompName(this.componentName);
		generalInstance.setReference((model.data.generalStructure.classDiagram.Class)this.reference.marshall());
		return generalInstance;
	}
	
}
