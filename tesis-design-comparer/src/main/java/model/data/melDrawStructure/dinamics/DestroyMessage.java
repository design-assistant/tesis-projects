package model.data.melDrawStructure.dinamics;

public class DestroyMessage extends Message{
	
	public DestroyMessage() {
		super();
	}
	
	public DestroyMessage(String componentName,String diagName,DinamicTerminal source, DinamicTerminal target){
		super(componentName,diagName,source,target);
	}

	@Override
	protected model.data.generalStructure.sequenceDiagram.Message getSpecificMessage() {
		return new model.data.generalStructure.sequenceDiagram.Destroy();
	}



}
