package model.data.melDrawStructure.dinamics;

public class ReturnMessage extends Message{
	
	public ReturnMessage() {
		super();
	}
	
	public ReturnMessage(String componentName,String diagName,DinamicTerminal source, DinamicTerminal target){
		super(componentName,diagName,source,target);
	}

	@Override
	protected model.data.generalStructure.sequenceDiagram.Message getSpecificMessage() {
		return new model.data.generalStructure.sequenceDiagram.Return();
	}


	
}
