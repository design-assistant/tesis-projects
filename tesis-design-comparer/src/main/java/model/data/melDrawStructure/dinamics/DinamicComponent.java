package model.data.melDrawStructure.dinamics;

import model.data.melDrawStructure.DiagramComponent;


public abstract class DinamicComponent extends DiagramComponent{
	
	public DinamicComponent() {
		super();
	}
	
	public DinamicComponent(String componentName){
		super(componentName);
	}
	
	@Override
	public String getComponentName() {
		return super.getComponentName();
	}
	
	@Override
	public void setComponentName(String componentName) {
		super.setComponentName(componentName);
	}
	
	@Override
	public boolean isValid(String componentName2) {
		return true;
	}
}
