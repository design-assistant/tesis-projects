package model.data.melDrawStructure.dinamics;

public class InvokeMessage extends Message{

	public InvokeMessage() {
		super();
	}
	
	public InvokeMessage(String componentName,String diagName,DinamicTerminal source, DinamicTerminal target){
		super(componentName,diagName,source,target);
	}

	@Override
	protected model.data.generalStructure.sequenceDiagram.Message getSpecificMessage() {
		return new model.data.generalStructure.sequenceDiagram.Invocation();
	}

	

}
