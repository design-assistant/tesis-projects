package model.data.melDrawStructure.dinamics;

import model.data.generalStructure.Component;
import model.data.generalStructure.sequenceDiagram.Instance;


public abstract class Message extends DinamicComponent{

	private DinamicTerminal source;
	private DinamicTerminal target;
		
	public Message() {
		super();
	}
	
	public Message(String componentName,String diagName,DinamicTerminal source, DinamicTerminal target){
		super(componentName);
		this.source = source;
		this.target = target;
	}	
			
	@Override
	public void setComponentName(String componentName) {
		super.setComponentName(componentName);
	}
	
	@Override
	public String getComponentName() {
		return super.getComponentName();
	}
	
	public DinamicTerminal getSource() {
		return source;
	}
	
	public DinamicTerminal getTarget() {
		return target;
	}
	
	 public void setSource(DinamicTerminal source) {
		this.source = source;
	}
	 
	 public void setTarget(DinamicTerminal target) {
		this.target = target;
	}

	@Override
	public Component marshall(){
		model.data.generalStructure.sequenceDiagram.Instance source = (Instance) this.getSource().marshall();
		model.data.generalStructure.sequenceDiagram.Instance target = (Instance)this.getTarget().marshall();
		model.data.generalStructure.sequenceDiagram.Message generalMessage = getSpecificMessage();
		generalMessage.setCompName(this.componentName);
		generalMessage.setSource(source);
		generalMessage.setTarget(target);
		int order = Integer.parseInt(this.componentName.split(":")[0]);
		generalMessage.setOrder(order);
		return generalMessage;
	}

	protected abstract model.data.generalStructure.sequenceDiagram.Message getSpecificMessage();
}
