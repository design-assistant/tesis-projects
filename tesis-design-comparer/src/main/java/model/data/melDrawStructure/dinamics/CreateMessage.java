package model.data.melDrawStructure.dinamics;

public class CreateMessage extends Message{
	
	public CreateMessage() {
		super();
	}
	
	public CreateMessage(String componentName,String diagName,DinamicTerminal source, DinamicTerminal target){
		super(componentName,diagName,source,target);
	}

	@Override
	protected model.data.generalStructure.sequenceDiagram.Message getSpecificMessage() {
		return new model.data.generalStructure.sequenceDiagram.Creation();
	}





}
