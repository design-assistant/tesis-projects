package model.data.melDrawStructure.dinamics;


public abstract class DinamicTerminal extends DinamicComponent{

	public DinamicTerminal() {
		super();
	}
	
	public DinamicTerminal(String componentName){
		super(componentName);
	}
	
	@Override
	public String getComponentName() {
		return super.getComponentName();
	}
	
	@Override
	public void setComponentName(String componentName) {
		super.setComponentName(componentName);
	}
	
}
