package model.data.melDrawStructure.dinamics;

import java.util.Hashtable;

import model.data.melDrawStructure.Diagram;

public abstract class DinamicDiagram extends Diagram{

	public DinamicDiagram() {
		super();
	}

	public DinamicDiagram(String name){
		super(name);
	}

	@Override
	public String getName() {
		
		return super.getName();
	}
	
	@Override
	public void setName(String name) {
		
		super.setName(name);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Hashtable getComponents() {
		return super.getComponents();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void setComponents(Hashtable components) {
		super.setComponents(components);
	}
	

}
