package model.data.melDrawStructure.dinamics;

public class AutoMessage extends Message{

	public AutoMessage() {
		super();
	}
	
	public AutoMessage(String componentName,String diagName,DinamicTerminal source, DinamicTerminal target){
		super(componentName,diagName,source,target);
	}

	@Override
	protected model.data.generalStructure.sequenceDiagram.Message getSpecificMessage() {
		return new model.data.generalStructure.sequenceDiagram.OwnMessaje();
	}

	
}
