package model.data.melDrawStructure.graphicListeners;


/**
 * 
 * Esta interface debera ser implemetnada por todas aquellas unevas interfaces
 *  graficas de nuevos posibles diagramas  que quieran ser persistidos y recuperados.
 */

public interface ScreenInvoker {
	
	public void invoke(String name);
	
}
