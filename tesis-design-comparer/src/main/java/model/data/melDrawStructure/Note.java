package model.data.melDrawStructure;

import model.data.generalStructure.Component;

public class Note extends DiagramComponent {

	public String text;


	public Note(){
		super();
	}

	public Note(String noteName,String noteText){
		super(noteName);
		this.text = noteText;
	}

	@Override
	public String getComponentName() {
		return super.getComponentName();
	}

	public String getText() {
		return text;
	}

	@Override
	public void setComponentName(String componentName) {
		super.setComponentName(componentName);
	}

	public void setText(String text) {
		this.text = text;
	}

	@Override
	public Component marshall() {
		return null;
	}

	@Override
	public boolean isValid(String componentName2) {
		return true;
	}



}