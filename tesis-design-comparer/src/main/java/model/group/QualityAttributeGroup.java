package model.group;

import java.io.Serializable;

public class QualityAttributeGroup implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private int success;
	private QualityAttribute attribute;
	private PlainDesignBox plaindesignbox;

	
	public PlainDesignBox getPlaindesignbox() {
		return plaindesignbox;
	}

	public void setPlaindesignbox(PlainDesignBox plaindesignbox) {
		this.plaindesignbox = plaindesignbox;
	}

	public QualityAttributeGroup() {}
	
	public QualityAttributeGroup(int success,QualityAttribute qa)
	{
		this.success = success;
		this.attribute = qa;
	}

	public QualityAttribute getAttribute() {
		return attribute;
	}

	public void setAttribute(QualityAttribute attribute) {
		this.attribute = attribute;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getSuccess() {
		return success;
	}

	public void setSuccess(int success) {
		this.success = success;
	}
	
	
}
