package model.group;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import model.data.generalStructure.Component;
import model.data.generalStructure.classDiagram.BoxComponent;

public class PlainDesign implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private PlainDesignBox box;
	private String name;
	private List<Component> components;
	private int componentsCount;
	private int absorbedRelations;
	private int internalAbsorbedRelations;
	private int internalUseAbsorbedRelations;
	private int useAbsorbedRelations;
	private List<String> componentsNames;
	private int multipleUseAbsorbedByComponent;
	private int multipleAbsorbedByComponent;
		
	public String getName() {
		return name;
	}
	
	public int getInternalUseAbsorbedRelations() {
		return internalUseAbsorbedRelations;
	}

	public void setInternalUseAbsorbedRelations(int internalUseAbsorbedRelations) {
		this.internalUseAbsorbedRelations = internalUseAbsorbedRelations;
	}



	public int getMultipleAbsorbedByComponent() {
		return multipleAbsorbedByComponent;
	}

	public void setMultipleAbsorbedByComponent(int multipleAbsorbedByComponent) {
		this.multipleAbsorbedByComponent = multipleAbsorbedByComponent;
	}

	public int getMultipleUseAbsorbedByComponent() {
		return multipleUseAbsorbedByComponent;
	}

	public void setMultipleUseAbsorbedByComponent(int multipleUseAbsorbedByComponent) {
		this.multipleUseAbsorbedByComponent = multipleUseAbsorbedByComponent;
	}

	public int getInternalAbsorbedRelations() {
		return internalAbsorbedRelations;
	}

	public int getUseAbsorbedRelations() {
		return useAbsorbedRelations;
	}

	public void setUseAbsorbedRelations(int useAbsorbedRelations) {
		this.useAbsorbedRelations = useAbsorbedRelations;
	}

	public void setInternalAbsorbedRelations(int internalAbsorbedRelations) {
		this.internalAbsorbedRelations = internalAbsorbedRelations;
	}

	public int getComponentsCount() {
		return componentsCount;
	}

	public void setComponentsCount(int componentsCount) {
		this.componentsCount = componentsCount;
	}

	public int getAbsorbedRelations() {
		return absorbedRelations;
	}

	public void setAbsorbedRelations(int absorbedRelations) {
		this.absorbedRelations = absorbedRelations;
	}

	public void setName(String name) {
		this.name = name;
	}

	public PlainDesignBox getBox() {
		return box;
	}

	public void setBox(PlainDesignBox box) {
		this.box = box;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<Component> getComponents() {
		return components;
	}

	public void setComponents(List<Component> components) {
		this.components = components;
	}
		
	public PlainDesign() {
		components = new ArrayList<Component>();
		componentsCount = 0;
		absorbedRelations = 0;
		internalAbsorbedRelations = 0;
		componentsNames = new ArrayList<String>();
		useAbsorbedRelations = 0;
		multipleUseAbsorbedByComponent = 0;
		internalUseAbsorbedRelations = 0;
		multipleAbsorbedByComponent = 0;
	}

	public void addComponent(BoxComponent boxComponent) {
		componentsCount++;
		componentsNames.add(boxComponent.getCompName());
		components.add(boxComponent);
		absorbedRelations += boxComponent.getAbsorbedRelationsCount();
		useAbsorbedRelations += boxComponent.getUseAbsorbedRelationsCount();
		multipleUseAbsorbedByComponent += boxComponent.getMultipleUseAbsorbedByComponentCount();
		multipleAbsorbedByComponent += boxComponent.getMultipleAbsorbedByComponentCount();
		
	}

	public void createInternalAbsorbedRelation() {
		BoxComponent component;
		String componentClassOf;
		String componentUseOf;
		for (int i = 0;i < components.size();i++)
		{
			component = (BoxComponent)components.get(i);
			for (int j = 0;j < component.getAttributes().size();j++)
			{
				componentClassOf = component.getAttributes().get(j).getNameIsClassOf();
				if (componentClassOf != null)
				{
					if (componentsNames.contains(componentClassOf))
						internalAbsorbedRelations++;
				}
			}
			for (int j = 0;j < component.getMethods().size();j++)
			{
				componentUseOf = component.getMethods().get(j).getNameIsUseOf();
				if (componentUseOf != null)
				{
					if (componentsNames.contains(componentUseOf))
						internalUseAbsorbedRelations++;
				}
			}
			
		}
	}

	public void getTotalAbsorbed() {
		absorbedRelations += useAbsorbedRelations;
	}

}
