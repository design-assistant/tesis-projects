package model.group;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import model.data.generalStructure.Design;
import presenter.comparition.IDesignBoxComparator;
import presenter.comparition.WeightComparator;
import presenter.criteria.CostValue;
import presenter.criteria.DinamicValue;
import presenter.criteria.WeightValue;

public class PlainDesignBox implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<PlainDesign> designs;
	private List<QualityAttributeGroup> attributes;
	private Design design;
	private float success;
	private int id;
	private DinamicValue dinamicComparitionValue;
	private DinamicValue dinamicTransformationValue;
	private IDesignBoxComparator comparator;

	public DinamicValue getDinamicTransformationValue() {
		return dinamicTransformationValue;
	}

	public int getId() {
		return id;
	}

	public Design getDesign() {
		return design;
	}

	public void setDesign(Design design) {
		this.design = design;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<PlainDesign> getDesigns() {
		return designs;
	}

	public void setDesigns(List<PlainDesign> designs) {
		this.designs = designs;
	}

	public void initializeContext() {
		//SE SACO LA INYECCION PORQUE SIEMPRE SE INYECTABA EL MISMO ELEMENTO Y FALLABA LA COMPARACION
		dinamicComparitionValue = (DinamicValue) new WeightValue();//ImplementationFactory.getBean("weightCriteria");
		comparator = (IDesignBoxComparator) new WeightComparator();//ImplementationFactory.getBean("designComparator");
		dinamicComparitionValue.setPlain(this);
		dinamicTransformationValue = (DinamicValue) new CostValue();//ImplementationFactory.getBean("costCriteria");
		dinamicTransformationValue.setPlain(this);
	}

	public PlainDesignBox() {
		designs = new ArrayList<PlainDesign>();
		attributes = new ArrayList<QualityAttributeGroup>();

	}

	public PlainDesign getDesignUp() {
		return getDesignByName("up");
	}

	private PlainDesign getDesignByName(String name) {
		if (designs != null) {
			for (int i = 0; i < designs.size(); i++) {
				PlainDesign design = designs.get(i);
				if (design.getName().equals(name))
					return design;
			}
		}
		return null;
	}

	public PlainDesign getDesignDown() {
		return getDesignByName("down");
	}

	public List<QualityAttributeGroup> getAttributes() {
		return attributes;
	}

	public void setAttributes(List<QualityAttributeGroup> attributes) {
		this.attributes = attributes;
	}

	public float getSuccess() {
		return success;
	}

	public void setSuccess(float success) {
		this.success = success;
	}

	public void addQualityAttributeGroup(QualityAttributeGroup qag) {
		attributes.add(qag);
	}

	public void generateSuccessValue() {
		success = 0;
		for (int i = 0; i < attributes.size(); i++)
			success = attributes.get(i).getSuccess() + success;
		if (attributes.size() != 0)
			success = success / attributes.size();
	}

	public DinamicValue getDinamicComparitionValue() {
		return dinamicComparitionValue;
	}

	public IDesignBoxComparator getComparator() {
		return comparator;
	}

	public Object getComparitionValue(PlainDesignBox userPlainDesign) {

		return comparator.compare(userPlainDesign, this);
	}

	public double getSuccessForUserAttributes(List<String> attributes) {
		boolean cut;
		double acumulated = 0;
		int count = 0;
		for (int i = 0;i < attributes.size();i++)
		{
			cut = false;
			for (int j = 0;j < this.attributes.size() && !cut;j++)
			{
				if (attributes.get(i).equals(this.attributes.get(j).getAttribute().getName()))
				{
					cut = true;
					count++;
					acumulated += this.attributes.get(j).getSuccess(); 
				}
			}
			if (!cut) // LE ASIGNA 0 AL ATRIBUTO DE CALIDAD QUE ELIGE EL USUARIO QUE NO SE TIENE EN CUENTA PARA LA APP
				count++;
		}
		if (count == 0)
			return 0;
		else
			return acumulated/count;
	}

	

}
