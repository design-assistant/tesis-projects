package presenter;

import java.util.Enumeration;
import java.util.Hashtable;

import presenter.exceptions.DuplicateAttributeException;

import model.User;
import model.data.generalStructure.Repository;
import model.group.PlainDesignBox;
import model.group.QualityAttribute;
import model.group.QualityAttributeGroup;
import bo.IQualityAttributeBO;
import bo.IUserBO;
import context.ImplementationFactory;



public class AdminUserPresenter extends UserControlPresenter {

	private PlainDesignBox pdbox; 
	
	public boolean userValidName(String userName) {
		IUserBO userBO = (IUserBO) ImplementationFactory.getBean("userBO");
		if (userBO.findByUser(userName) == null) 
			return true;
		return false;
	}

	public void addUser(String userName, String pass) {
		User user = new User(userName, pass);
		user.save();
	}

	public boolean isValidUser(String userName, String pass) {
		IUserBO userBO = (IUserBO) ImplementationFactory.getBean("userBO");
		User us = userBO.findByUser(userName);
		if (us == null)
			return false;
		else
		{
			if (pass.equals(us.getPass()))
				return true;
			return false;
		}
	}
	public void addAttribute(String attributeName) throws DuplicateAttributeException{
		IQualityAttributeBO qaBO = (IQualityAttributeBO) ImplementationFactory.getBean("qualityAttributeBO");
		if(qaBO.findByName(attributeName)!=null){
			throw new DuplicateAttributeException();
		}
		QualityAttribute qa= new QualityAttribute();
		qa.setName(attributeName);
		qaBO.persist(qa);
	}

	public void saveEvaluation(Hashtable<String, Integer> rankings) {
		pdbox = new PlainDesignBox();
		IQualityAttributeBO qBO = (IQualityAttributeBO) ImplementationFactory.getBean("qualityAttributeBO");
		QualityAttribute qa;
		QualityAttributeGroup qaGroup;
		String qaName;
		for (Enumeration<String> e = rankings.keys();e.hasMoreElements();)
		{
			qaName = e.nextElement();
			qa = qBO.findByName(qaName);
			qaGroup = new QualityAttributeGroup(rankings.get(qaName).intValue(),qa);
			pdbox.addQualityAttributeGroup(qaGroup);
		}
		pdbox.getDesigns().add(Repository.getInstance().getDown());
		pdbox.getDesigns().add(Repository.getInstance().getUp());
		pdbox.generateSuccessValue();
		Repository.getInstance().setCurrentPDBox(pdbox);
	}

	


	

}
