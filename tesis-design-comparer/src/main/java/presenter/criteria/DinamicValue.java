package presenter.criteria;

import model.group.PlainDesignBox;

public abstract class DinamicValue {

	protected Object value;
	protected PlainDesignBox plain;

	public Object getValue() {
		if (value == null)
			value = calculateValue(plain);
		return value;
	}

	public PlainDesignBox getPlain() {
		return plain;
	}

	public void setPlain(PlainDesignBox plain) {
		this.plain = plain;
	}

	protected abstract Object calculateValue(PlainDesignBox plain);

}
