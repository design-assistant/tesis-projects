package presenter.criteria;

import java.util.ResourceBundle;

import model.group.PlainDesign;
import model.group.PlainDesignBox;

public class WeightValue extends DinamicValue {

	private static final String weightTableRoot = "comparitionWeightTable";
	
	@Override
	protected Object calculateValue(PlainDesignBox plain) {
		PlainDesign up = plain.getDesignUp();
		PlainDesign down = plain.getDesignDown();

		ResourceBundle properties = ResourceBundle.getBundle(weightTableRoot);
		// Cargar los valores por las cajas
		double weightValueUp = new Integer(properties.getString("component")) * up.getComponentsCount();
		double weightValueDown = new Integer(properties.getString("component")) * down.getComponentsCount();

		// Sumar valor por las absorbidas multiples por caja
		weightValueUp += new Integer(properties.getString("absorbed.multiple.by.component.use")) * up.getMultipleUseAbsorbedByComponent();
		weightValueUp += new Integer(properties.getString("absorbed.multiple.by.component")) * up.getMultipleAbsorbedByComponent();
		weightValueDown += new Integer(properties.getString("absorbed.multiple.by.component.use")) * down.getMultipleUseAbsorbedByComponent();
		weightValueDown += new Integer(properties.getString("absorbed.multiple.by.component")) * down.getMultipleAbsorbedByComponent();

		// Sumar valor por las absorbidas internas del dise�o aplanado
		weightValueUp += new Integer(properties.getString("absorbed.internal.use")) * up.getInternalUseAbsorbedRelations();
		weightValueUp += new Integer(properties.getString("absorbed.internal")) * (up.getInternalAbsorbedRelations() - up.getInternalUseAbsorbedRelations());
		weightValueDown += new Integer(properties.getString("absorbed.internal.use")) * down.getInternalUseAbsorbedRelations();
		weightValueDown += new Integer(properties.getString("absorbed.internal")) * (down.getInternalAbsorbedRelations() - down.getInternalUseAbsorbedRelations());
		
		// Sumar valor por las absorbidas externas
		weightValueUp += new Integer(properties.getString("absorbed.use")) * (up.getUseAbsorbedRelations() - up.getInternalUseAbsorbedRelations());
		weightValueUp += new Integer(properties.getString("absorbed")) * ((up.getAbsorbedRelations() - up.getUseAbsorbedRelations())- up.getInternalAbsorbedRelations());
		weightValueDown += new Integer(properties.getString("absorbed.use")) * (down.getUseAbsorbedRelations() - down.getInternalUseAbsorbedRelations());
		weightValueDown += new Integer(properties.getString("absorbed")) * ((down.getAbsorbedRelations() - down.getUseAbsorbedRelations())- down.getInternalAbsorbedRelations());

		// Se considera mas informativo el aplanado hacia arriba, por lo tanto se lo toma completo y el aplanado hacia abajo se lo considera dividido 2
		weightValueDown = weightValueDown / 2;
		return new Double((weightValueDown + weightValueUp) / 2);
	}

	
	

}
