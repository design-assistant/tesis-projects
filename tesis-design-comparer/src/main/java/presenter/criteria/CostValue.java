package presenter.criteria;

import java.util.ResourceBundle;

import model.data.generalStructure.Repository;
import model.group.PlainDesignBox;

public class CostValue extends DinamicValue {

	private static final String costTableRoot = "transformationCostTable";
	
	@Override
	protected Object calculateValue(PlainDesignBox plain) {
		ResourceBundle properties = ResourceBundle.getBundle(costTableRoot);
		Repository.getInstance().clearCostInfo();
		int costValue = plain.getDesign().getFathersCount() * new Integer(properties.getString("father"));
		costValue += plain.getDesign().getSonsCount() * new Integer(properties.getString("son"));
		costValue += plain.getDesign().getBoxComponentsCount() * new Integer(properties.getString("box"));
		costValue += plain.getDesign().getInheritanceCount() * new Integer(properties.getString("relation.inheritance"));
		costValue += plain.getDesign().getImplementationCount() * new Integer(properties.getString("relation.implements"));
		costValue += plain.getDesign().getUsesCount() * new Integer(properties.getString("relation.use"));
		costValue += plain.getDesign().getAsociationCount() * new Integer(properties.getString("relation.asociation"));
		costValue += plain.getDesign().getCollectionsCount() * new Integer(properties.getString("relation.collection"));
		return new Integer(costValue);
	}

	
	

}
