package presenter;

import java.util.Enumeration;

import model.data.generalStructure.Design;
import model.data.generalStructure.Diagram;
import model.data.generalStructure.Repository;
import model.data.generalStructure.classDiagram.BoxComponent;
import model.data.leveler.Leveler;
import model.data.melDrawStructure.DesignProject;
import model.data.melDrawStructure.Project;
import model.group.PlainDesign;
import model.group.PlainDesignBox;
import presenter.marshalling.Marshaller;
import presenter.marshalling.mellDraw.MellDrawMarshaller;
import view.Configuration;
import bo.IDesignBO;
import bo.IPlainDesignBoxBO;
import context.ImplementationFactory;

public abstract class UserControlPresenter extends WindowPresenter {

	private Marshaller marshaller = new MellDrawMarshaller();
	private Design design;
	

	
	
	

	public Marshaller getMarshaller() {
		return marshaller;
	}

	public void setMarshaller(Marshaller marshaller) {
		this.marshaller = marshaller;
	}

	public Design getDesign() {
		return design;
	}

	public void setDesign(Design design) {
		this.design = design;
	}

	public boolean isActive() {

		return design != null;
	}

	public void clearContents() {
		this.design = null;
		Repository.getInstance().clearRepository();
	}

	public void loadDesign(String path) {

		Project project = new Project();
		DesignProject dp = project.decode(path);
		for (int i = 0; i < dp.getComponents().size(); i++)
			Configuration.addDiagramsNames(dp.getComponents().get(i).getName());
		Configuration.proyectName = dp.getName();
		loadDesign(getMarshaller().marshall(dp));
	}

	public void loadDesign(Design design) {
		setDesign(design);
		Design upDesign = plainUp();
		Design downDesign = plainDown();
		Repository.getInstance().setUp(makePlainDesign(upDesign, "up"));
		Repository.getInstance().setDown(makePlainDesign(downDesign, "down"));
	}
	
	protected PlainDesign makePlainDesign(Design design, String name) {
		PlainDesign pd = new PlainDesign();
		pd.setName(name);
		for (int i = 0; i < design.getDiagrams().size(); i++) {
			Diagram diagram = design.getDiagrams().get(i);
			for (Enumeration<String> e = diagram.getComponents().keys(); e.hasMoreElements();) {
				pd.addComponent((BoxComponent) diagram.getComponents().get(e.nextElement()));
			}
		}
		pd.getTotalAbsorbed();
		pd.createInternalAbsorbedRelation();
		return pd;
	}

	public void save() {
		IDesignBO designBO = (IDesignBO) ImplementationFactory.getBean("designBO");
		designBO.persist(design);
		IPlainDesignBoxBO pboxBO = (IPlainDesignBoxBO) ImplementationFactory.getBean("plainDesignBoxBO");
		PlainDesignBox pbox = Repository.getInstance().getCurrentPDBox();
		pbox.setDesign(design);
		pboxBO.persist(pbox);
	}
	
	
	public Design plainUp(){
		Leveler leveler = new Leveler();
		return leveler.flattenUp(design);
	}
	
	public Design plainDown(){
		Leveler leveler = new Leveler();
		return leveler.flattenDown(design);
	}
}
