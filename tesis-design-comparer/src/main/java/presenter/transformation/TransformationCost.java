package presenter.transformation;

import java.util.ResourceBundle;

public class TransformationCost {

	public static int box;
	public static int inheritance;
	public static int implementations;
	public static int use;
	public static int asociation;
	public static int collection;
	public static int modInheritance;
	public static int modImplementation;
	public static int modUse;
	public static int modAsociation;
	public static int modCollection;
	
	public TransformationCost() {
		ResourceBundle resource = ResourceBundle.getBundle("transformationCostTable");
		box = Integer.parseInt(resource.getString("box"));
		inheritance = Integer.parseInt(resource.getString("relation.inheritance"));
		implementations = Integer.parseInt(resource.getString("relation.implements"));
		use = Integer.parseInt(resource.getString("relation.use"));
		asociation= Integer.parseInt(resource.getString("relation.asociation"));
		collection = Integer.parseInt(resource.getString("relation.collection"));
		modInheritance = Integer.parseInt(resource.getString("mod.relation.inheritance"));
		modImplementation= Integer.parseInt(resource.getString("mod.relation.implements"));
		modAsociation = Integer.parseInt(resource.getString("mod.relation.asociation"));	
		modUse = Integer.parseInt(resource.getString("mod.relation.use"));
		modCollection = Integer.parseInt(resource.getString("mod.relation.collection"));
	}
	
}
