package presenter.transformation;

import java.util.ArrayList;
import java.util.List;

public class Combination {

	private List<String> namesCombination = new ArrayList<String>();

	public List<String> getNamesCombination() {
		return namesCombination;
	}

	public void setNamesCombination(List<String> namesCombination) {
		this.namesCombination = namesCombination;
	}

	public void add(String nameCombination) {
		namesCombination.add(nameCombination);
	}

	public void addAll(List<String> nameCombination) {
		namesCombination.addAll(nameCombination);
	}

}
