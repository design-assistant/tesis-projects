package presenter.transformation;

public class RelationTransformAction extends TransformAction {

	private String target;
	private String relationType;

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public String getRelationType() {
		return relationType;
	}

	public void setRelationType(String relationType) {
		this.relationType = relationType;
	}

	public RelationTransformAction() {
	}

	public RelationTransformAction(String target, String relationType, String source, String action) {
		super();
		this.source = source;
		this.action = action;
		this.target = target;
		this.relationType = relationType;
	}

	@Override
	public String toString() {
		if (action.equals(TransformationGenerator.add) || action.equals(TransformationGenerator.delete))
			return "Debe " + action + " la relacion del tipo " + relationType + " entre " + source + " y " +target;
		else 
			return "La relacion entre " + source + " y " + target + " se debe " + action + " por una relacion del tipo " +relationType;
	}

}
