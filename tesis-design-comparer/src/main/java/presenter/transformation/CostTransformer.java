package presenter.transformation;

import model.group.PlainDesignBox;

public class CostTransformer implements IDesignTransformer {
	
	public CostTransformer() {}
	

	
	@Override
	public Object obtainTransformationValue(PlainDesignBox userPlainDesign, PlainDesignBox appPlainDesign) {
		int userValue  = (Integer)userPlainDesign.getDinamicTransformationValue().getValue();
		int appValue = (Integer)appPlainDesign.getDinamicTransformationValue().getValue();
		int compareValue  = Math.abs(userValue - appValue);
		return new Integer(compareValue);
	}

}
