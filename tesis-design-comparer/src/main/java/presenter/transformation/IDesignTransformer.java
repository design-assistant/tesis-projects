package presenter.transformation;

import model.group.PlainDesignBox;

public interface IDesignTransformer {

	public Object obtainTransformationValue(PlainDesignBox userPlainDesign, PlainDesignBox appPlainDesign);
	
}
