package presenter.transformation.ports;

import java.util.List;

import model.data.generalStructure.Design;
import model.data.generalStructure.classDiagram.BoxComponent;

public class EmptyPort extends RelationPort {

	public EmptyPort(Design userDesign, Design appDesign) {
		super(userDesign, appDesign, EMPTY);
	}

	@Override
	public int getDiference() {
		difference=this.userDesign.getEmpties().size() - this.appDesign.getEmpties().size();
		return difference;
	}

	@Override
	public void relatedToSource() {
		relatedToAll(this.userDesign.getSources(), this.userDesign.getSourcesTroughPut());
	}

	@Override
	public void relatedToSourceAndTarget() {
		relatedToAll(this.userDesign.getTargets(), this.userDesign.getSourcesAndTargetsTroughPut());
	}

	@Override
	public void relatedToTarget() {
		relatedToAll(this.userDesign.getSourcesAndTargets(), this.userDesign.getTargetsTroughPut());
	}

	private void relatedToAll(List<String> portNamesList, List<BoxComponent> portComponentList) {
		portNamesList.add(this.userDesign.getEmpties().remove(0));
		portComponentList.add(this.userDesign.getEmptiesTroughPut().remove(0));
	}

}
