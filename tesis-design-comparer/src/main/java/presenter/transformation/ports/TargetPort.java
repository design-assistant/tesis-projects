package presenter.transformation.ports;

import model.data.generalStructure.Design;
import model.data.generalStructure.classDiagram.BoxComponent;

public class TargetPort extends RelationPort {

	public TargetPort(Design userDesign, Design appDesign) {
		super(userDesign, appDesign,TARGET);
	}

	@Override
	public int getDiference() {
		difference= this.userDesign.getTargets().size() - this.appDesign.getTargets().size();
		return difference;
	}

	@Override
	public void relatedToEmpty() {
		BoxComponent component = getLessInPut(this.userDesign.getTargetsTroughPut(), this.userDesign.getTargets());
		this.userDesign.getEmpties().add(component.getCompName());
		this.userDesign.getEmptiesTroughPut().add(component);
	}

	@Override
	public void relatedToSource() {
		BoxComponent component = getLessInPut(this.userDesign.getTargetsTroughPut(), this.userDesign.getTargets());
		this.userDesign.getSources().add(component.getCompName());
		this.userDesign.getSourcesTroughPut().add(component);
	}

	@Override
	public void relatedToSourceAndTarget() {
		BoxComponent component = getLessInPut(this.userDesign.getTargetsTroughPut(), this.userDesign.getTargets());
		this.userDesign.getSourcesAndTargets().add(component.getCompName());
		this.userDesign.getSourcesAndTargetsTroughPut().add(component);
	}
	


}
