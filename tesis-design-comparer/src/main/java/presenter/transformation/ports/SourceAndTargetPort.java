package presenter.transformation.ports;

import java.util.List;

import model.data.generalStructure.Design;
import model.data.generalStructure.classDiagram.BoxComponent;

public class SourceAndTargetPort extends RelationPort {

	public SourceAndTargetPort(Design userDesign, Design appDesign) {
		super(userDesign,appDesign,SOURCEANDTARGET);
	}
	
	@Override
	public int getDiference() {
		difference= this.userDesign.getSourcesAndTargets().size() - this.appDesign.getSourcesAndTargets().size();
		return difference;
	}

	private BoxComponent getLessInPutLessOutPut(List<BoxComponent> troughPutList, List<String> namesList) {
		int min = Integer.MAX_VALUE;
		BoxComponent minInPutOutPut = null;
		for (int i = 0; i < troughPutList.size(); i++) {
			if (min > troughPutList.get(i).getInPut() + troughPutList.get(i).getOutPut()) {
				min = troughPutList.get(i).getInPut() + troughPutList.get(i).getOutPut();
				minInPutOutPut = troughPutList.get(i);
			}
		}
		troughPutList.remove(minInPutOutPut);
		namesList.remove(minInPutOutPut.getCompName());
		return minInPutOutPut;
	}

	private BoxComponent getLessInPutMoreOutPut(List<BoxComponent> troughPutList, List<String> namesList) {
		int min = Integer.MAX_VALUE;
		BoxComponent minInPutOutPut = null;
		for (int i = 0; i < troughPutList.size(); i++) {
			if (min > troughPutList.get(i).getInPut()) {
				min = troughPutList.get(i).getInPut();
				minInPutOutPut = troughPutList.get(i);
			} else if (min == troughPutList.get(i).getInPut()) {
				if (minInPutOutPut.getOutPut() < troughPutList.get(i).getOutPut()) {
					min = troughPutList.get(i).getInPut();
					minInPutOutPut = troughPutList.get(i);
				}
			}
		}
		troughPutList.remove(minInPutOutPut);
		namesList.remove(minInPutOutPut.getCompName());
		return minInPutOutPut;
	}

	private BoxComponent getLessOutPutMoreInPut(List<BoxComponent> troughPutList, List<String> namesList) {
		int min = Integer.MAX_VALUE;
		BoxComponent minInPutOutPut = null;
		for (int i = 0; i < troughPutList.size(); i++) {
			if (min > troughPutList.get(i).getOutPut()) {
				min = troughPutList.get(i).getOutPut();
				minInPutOutPut = troughPutList.get(i);
			} else if (min == troughPutList.get(i).getOutPut()) {
				if (minInPutOutPut.getInPut() < troughPutList.get(i).getInPut()) {
					min = troughPutList.get(i).getOutPut();
					minInPutOutPut = troughPutList.get(i);
				}
			}
		}
		troughPutList.remove(minInPutOutPut);
		namesList.remove(minInPutOutPut.getCompName());
		return minInPutOutPut;
	}

	@Override
	public void relatedToEmpty() {
		BoxComponent component = getLessInPutLessOutPut(this.userDesign.getSourcesAndTargetsTroughPut(), this.userDesign
				.getSourcesAndTargets());
		this.userDesign.getEmpties().add(component.getCompName());
		this.userDesign.getEmptiesTroughPut().add(component);
	}

	@Override
	public void relatedToSource() {
		BoxComponent component = getLessInPutMoreOutPut(this.userDesign.getSourcesAndTargetsTroughPut(), this.userDesign
				.getSourcesAndTargets());
		this.userDesign.getSources().add(component.getCompName());
		this.userDesign.getSourcesTroughPut().add(component);
	}

	@Override
	public void relatedToTarget() {
		BoxComponent component = getLessOutPutMoreInPut(this.userDesign.getSourcesAndTargetsTroughPut(), this.userDesign
				.getSourcesAndTargets());
		this.userDesign.getTargets().add(component.getCompName());
		this.userDesign.getTargetsTroughPut().add(component);
	}



}
