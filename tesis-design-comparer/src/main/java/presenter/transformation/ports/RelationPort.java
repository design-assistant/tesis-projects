package presenter.transformation.ports;

import java.util.List;

import model.data.generalStructure.Design;
import model.data.generalStructure.classDiagram.BoxComponent;

public abstract class RelationPort implements Comparable<RelationPort> {

	protected Design userDesign;
	protected Design appDesign;
	protected static final int SOURCE = 0;
	protected static final int TARGET =1;
	protected static final int SOURCEANDTARGET = 2;
	protected static final int EMPTY = 3;
	protected int difference;

	private int relationPort;

	public int getRelationPort() {
		return relationPort;
	}

	public RelationPort() {

	}

	public RelationPort(Design userDesign, Design appDesign, int relationPort) {
		this.userDesign = userDesign;
		this.appDesign = appDesign;
		this.relationPort = relationPort;
	}

	public Design getUserDesign() {
		return userDesign;
	}

	public void setUserDesign(Design userDesign) {
		this.userDesign = userDesign;
	}

	public Design getAppDesign() {
		return appDesign;
	}

	public void setAppDesign(Design appDesign) {
		this.appDesign = appDesign;
	}

	public abstract int getDiference();

	public void relatedToEmpty() {
	}

	public void relatedToSource() {
	}

	public void relatedToTarget() {
	}

	public void relatedToSourceAndTarget() {
	}

	protected BoxComponent getLessOutPut(List<BoxComponent> troughPutList, List<String> namesList) {
		int min = Integer.MAX_VALUE;
		BoxComponent minOutPut = null;
		for (int i = 0; i < troughPutList.size(); i++) {
			if (min > troughPutList.get(i).getOutPut()) {
				min = troughPutList.get(i).getOutPut();
				minOutPut = troughPutList.get(i);
			}
		}
		troughPutList.remove(minOutPut);
		namesList.remove(minOutPut.getCompName());
		return minOutPut;
	}

	protected BoxComponent getLessInPut(List<BoxComponent> troughPutList, List<String> namesList) {
		int min = Integer.MAX_VALUE;
		BoxComponent minInPut = null;
		for (int i = 0; i < troughPutList.size(); i++) {
			if (min > troughPutList.get(i).getInPut()) {
				min = troughPutList.get(i).getInPut();
				minInPut = troughPutList.get(i);
			}
		}
		troughPutList.remove(minInPut);
		namesList.remove(minInPut.getCompName());
		return minInPut;
	}

	@Override
	public int compareTo(RelationPort relationPort) {
		if (this.getDiference() > relationPort.getDiference())
			return -1;
		else if (this.getDiference() == relationPort.getDiference())
			return 0;
		return 1;
	}

	public void relate(int relationPort) {
		if (relationPort == SOURCE)
			relatedToSource();
		else if (relationPort == TARGET)
			relatedToTarget();
		else if (relationPort == SOURCEANDTARGET)
			relatedToSourceAndTarget();
		else if (relationPort == EMPTY)
			relatedToEmpty();
	}

}
