package presenter.transformation.ports;

import model.data.generalStructure.Design;
import model.data.generalStructure.classDiagram.BoxComponent;

public class SourcePort extends RelationPort {

	public SourcePort(Design userDesign, Design appDesign) {
		super(userDesign, appDesign, SOURCE);
	}

	@Override
	public int getDiference() {
		difference = this.userDesign.getSources().size() - this.appDesign.getSources().size();
		return difference;
	}

	@Override
	public void relatedToEmpty() {
		BoxComponent component = getLessOutPut(this.userDesign.getSourcesTroughPut(), this.userDesign.getSources());
		this.userDesign.getEmpties().add(component.getCompName());
		this.userDesign.getEmptiesTroughPut().add(component);
	}

	@Override
	public void relatedToSourceAndTarget() {
		BoxComponent component = getLessOutPut(this.userDesign.getSourcesTroughPut(), this.userDesign.getSources());
		this.userDesign.getSourcesAndTargets().add(component.getCompName());
		this.userDesign.getSourcesAndTargetsTroughPut().add(component);
	}

	@Override
	public void relatedToTarget() {
		BoxComponent component = getLessOutPut(this.userDesign.getSourcesTroughPut(), this.userDesign.getSources());
		this.userDesign.getTargets().add(component.getCompName());
		this.userDesign.getTargetsTroughPut().add(component);
	}

}
