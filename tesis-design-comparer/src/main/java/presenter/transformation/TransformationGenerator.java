package presenter.transformation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;

import model.data.generalStructure.Design;
import model.data.generalStructure.classDiagram.Relation;
import presenter.transformation.ports.EmptyPort;
import presenter.transformation.ports.RelationPort;
import presenter.transformation.ports.SourceAndTargetPort;
import presenter.transformation.ports.SourcePort;
import presenter.transformation.ports.TargetPort;

public class TransformationGenerator {

	final static String add = "Agregar";
	final static String delete = "Eliminar";
	final static String add_class = "Agregar la clase";
	final static String delete_class = "Eliminar la clase";
	final static String add_interface = "Agregar la interface";
	final static String delete_interface = "Eliminar la interface";

	final static String modify = "Modificar";
	final static String modify_to_Interface = "Cambiar a interface la clase ";
	final static String modify_to_Class = "Cambiar a clase la interface ";
	final static int MAX_COLLECTION_SIZE = 20;
	final static int MAX_COLLECTION_GROUP = 7;
	private int currentCost = Integer.MAX_VALUE;
	private TransformationCost transformationCostValues = new TransformationCost();
	private List<TransformAction> transformations = new ArrayList<TransformAction>();

	public TransformationGenerator() {
	}

	public List<TransformAction> transform(Design userDesign, Design appDesign) {
		transformations = new ArrayList<TransformAction>();
		userDesign.clearRelationComponents();
		appDesign.clearRelationComponents();
		userDesign.separateRelationComponents();
		appDesign.separateRelationComponents();

		int userDesignBoxComponentsCount = userDesign.getSources().size()
		+ userDesign.getTargets().size()
		+ userDesign.getSourcesAndTargets().size()
		+ userDesign.getEmpties().size();
		int appDesignBoxComponentsCount = appDesign.getSources().size()
		+ appDesign.getTargets().size()
		+ appDesign.getSourcesAndTargets().size()
		+ appDesign.getEmpties().size();

		if (userDesignBoxComponentsCount < appDesignBoxComponentsCount)
			for (int i = appDesignBoxComponentsCount
					- userDesignBoxComponentsCount; i > 0; i--)
				userDesign.addStubComponent(appDesign, "#" + i);
		else
			for (int i = userDesignBoxComponentsCount
					- appDesignBoxComponentsCount; i > 0; i--)
				appDesign.addStubComponent(userDesign, "#" + i);

		startTransformation(userDesign, appDesign);
		return transformations;
	}

	private void startTransformation(Design userDesign, Design appDesign) {
		List<List<Hashtable<String, String>>> renamesCombinations = new ArrayList<List<Hashtable<String, String>>>();
		// EMPAREJADO
		SourcePort sourcePort = new SourcePort(userDesign, appDesign);
		TargetPort targetPort = new TargetPort(userDesign, appDesign);
		SourceAndTargetPort sourceAndTargetPort = new SourceAndTargetPort(
				userDesign, appDesign);
		EmptyPort emptyPort = new EmptyPort(userDesign, appDesign);
		List<RelationPort> portList = new ArrayList<RelationPort>();
		portList.add(sourcePort);
		portList.add(targetPort);
		portList.add(sourceAndTargetPort);
		portList.add(emptyPort);
		Collections.sort(portList);
		for (int i = 0; i < portList.size(); i++) {
			for (int k = i + 1; k < portList.size(); k++)
				for (int j = 0; j <= portList.get(i).getDiference(); j++) {
					if (portList.get(i).getDiference() > 0)
						if (portList.get(k).getDiference() != 0)
						{
							portList.get(i).relate(
									portList.get(k).getRelationPort());
							j--;
						}
				}
		}
		// se contempla ademas si tiene un solo elemento la lista.
		if (appDesign.getSources().size() > 1)
			renamesCombinations.add(getRenamesCombinations(userDesign
					.getSources(), appDesign.getSources()));
		else if (appDesign.getSources().size() == 1)
			renamesCombinations.add(getUniqueCombination(userDesign
					.getSources(), appDesign.getSources()));
		if (appDesign.getTargets().size() > 1)
			renamesCombinations.add(getRenamesCombinations(userDesign
					.getTargets(), appDesign.getTargets()));
		else if (appDesign.getTargets().size() == 1)
			renamesCombinations.add(getUniqueCombination(userDesign
					.getTargets(), appDesign.getTargets()));
		if (appDesign.getSourcesAndTargets().size() > 1)
			renamesCombinations.add(getRenamesCombinations(userDesign
					.getSourcesAndTargets(), appDesign.getSourcesAndTargets()));
		else if (appDesign.getSourcesAndTargets().size() == 1)
			renamesCombinations.add(getUniqueCombination(userDesign
					.getSourcesAndTargets(), appDesign.getSourcesAndTargets()));
		if (appDesign.getEmpties().size() > 0)
			renamesCombinations.add(getEmptiesRenames(userDesign.getEmpties(),
					appDesign.getEmpties()));

		// ordena
		Hashtable<String, String> totalCombination = new Hashtable<String, String>();
		if (renamesCombinations.size() > 0) {
			for (int i = 0; i < renamesCombinations.get(0).size(); i++) {

				if (renamesCombinations.size() > 1
						&& renamesCombinations.get(1).size() > 0) {
					for (int j = 0; j < renamesCombinations.get(1).size(); j++) {

						if (renamesCombinations.size() > 2
								&& renamesCombinations.get(2).size() > 0) {
							for (int k = 0; k < renamesCombinations.get(2)
							.size(); k++) {
								if (renamesCombinations.size() > 3
										&& renamesCombinations.get(3).size() > 0) {
									for (int m = 0; m < renamesCombinations
									.get(3).size(); m++) {
										totalCombination = new Hashtable<String, String>();
										addToCombination(renamesCombinations
												.get(0).get(i),
												totalCombination);
										addToCombination(renamesCombinations
												.get(1).get(j),
												totalCombination);
										addToCombination(renamesCombinations
												.get(2).get(k),
												totalCombination);
										addToCombination(renamesCombinations
												.get(3).get(m),
												totalCombination);
										buildTransmormation(totalCombination,
												userDesign, appDesign);
									}
								} else {
									totalCombination = new Hashtable<String, String>();
									addToCombination(renamesCombinations.get(0)
											.get(i), totalCombination);
									addToCombination(renamesCombinations.get(1)
											.get(j), totalCombination);
									addToCombination(renamesCombinations.get(2)
											.get(k), totalCombination);
									buildTransmormation(totalCombination,
											userDesign, appDesign);
								}
							}
						} else {
							totalCombination = new Hashtable<String, String>();
							addToCombination(renamesCombinations.get(0).get(i),
									totalCombination);
							addToCombination(renamesCombinations.get(1).get(j),
									totalCombination);
							buildTransmormation(totalCombination, userDesign,
									appDesign);
						}
					}

				} else {
					totalCombination = new Hashtable<String, String>();
					addToCombination(renamesCombinations.get(0).get(i),
							totalCombination);
					buildTransmormation(totalCombination, userDesign, appDesign);
				}

			}
		}
	}

	@SuppressWarnings("static-access")
	private void buildTransmormation(
			Hashtable<String, String> totalCombination, Design userDesign,
			Design appDesign) {
		List<TransformAction> candidateTransformations = new ArrayList<TransformAction>();
		List<Relation> userRelations = new ArrayList<Relation>();
		userRelations.addAll(userDesign.getRelations());
		List<Relation> appRelations = new ArrayList<Relation>();
		appRelations.addAll(appDesign.getRelations());
		String source = null;
		String target = null;
		String relationDescription = null;
		int cost = 0;
		TransformAction transformationAction;
		// AGREGA COSTO CLASES
		if (cost <= currentCost) {
			String key;
			String value;
			for (Enumeration<String> e = totalCombination.keys(); e
			.hasMoreElements()
			&& cost <= currentCost;) {
				key = e.nextElement();
				value = totalCombination.get(key);
				if (key.startsWith("#")) {
					if(userDesign.isInterface(value))
					{
						transformationAction = createBoxAction(this.delete_interface,
								totalCombination.get(key));
					}
					else
					{
						transformationAction = createBoxAction(this.delete_class,
								totalCombination.get(key));
					}
					cost += transformationAction.getCost();
					candidateTransformations.add(transformationAction);
				} else 	if (value.startsWith("#")) {
					if(appDesign.isInterface(key))
					{
						transformationAction = createBoxAction(this.add_interface, value);
					}
					else
					{
						transformationAction = createBoxAction(this.add_class, value);
					}
					cost += transformationAction.getCost();
					candidateTransformations.add(transformationAction);
				}
				else{
					if(appDesign.isInterface(key)&&!userDesign.isInterface(value))
					{
						transformationAction = createBoxAction(this.modify_to_Interface, value);
						cost += transformationAction.getCost();
						candidateTransformations.add(transformationAction);
					}
					else if(!appDesign.isInterface(key)&&userDesign.isInterface(value))
					{
						transformationAction = createBoxAction(this.modify_to_Class, value);
						cost += transformationAction.getCost();
						candidateTransformations.add(transformationAction);
					}
				}
			}
		}
		for (int i = 0; i < appRelations.size() && cost <= currentCost; i++) {
			source = appRelations.get(i).getSource().getCompName();
			target = appRelations.get(i).getTarget().getCompName();
			relationDescription = appRelations.get(i).getRelationDescription();
			source = totalCombination.get(source);
			target = totalCombination.get(target);
			List<Relation> involucratedRelations = getInvolucratedRelations(
					source, target, userRelations);
			if (involucratedRelations.size() == 0) {
				transformationAction = createRelationAction(this.add, source,
						target, relationDescription);
				cost += transformationAction.getCost();
				candidateTransformations.add(transformationAction);
			} else {
				Relation similarRelation = getSimilarRelation(
						involucratedRelations, relationDescription);
				if (!similarRelation.getRelationDescription().equals(
						relationDescription)) {
					transformationAction = createRelationAction(this.modify,
							source, target, relationDescription);
					cost += transformationAction.getCost();
					candidateTransformations.add(transformationAction);
				}
				userRelations.remove(similarRelation);
			}
		}
		for (int i = 0; i < userRelations.size() && cost <= currentCost; i++) {
			transformationAction = createRelationAction(this.delete,
					userRelations.get(i).getSource().getCompName(),
					userRelations.get(i).getTarget().getCompName(),
					userRelations.get(i).getRelationDescription());
			cost += transformationAction.getCost();
			candidateTransformations.add(transformationAction);
		}

		if (cost < currentCost) {
			currentCost = cost;
			if (cost != 0)
				this.transformations = candidateTransformations;
			else {
				this.transformations = new ArrayList<TransformAction>();
				this.transformations.add(new EqualTransformation());
			}
		} else if (cost == currentCost && cost != 0) {
			if (candidateTransformations.size() < this.transformations.size())
				this.transformations = candidateTransformations;
		}

	}

	@SuppressWarnings("static-access")
	private TransformAction createBoxAction(String actionName, String boxName) {
		BoxTransformAction action = new BoxTransformAction();
		action.setSource(boxName);
		action.setAction(actionName);
		action.setCost(this.transformationCostValues.box);
		return action;
	}

	private TransformAction createRelationAction(String actionName,
			String source, String target, String relationDescription) {
		RelationTransformAction action = new RelationTransformAction();
		action.setSource(source);
		action.setTarget(target);
		action.setAction(actionName);
		action.setRelationType(relationDescription);
		action.setCost(getCost(actionName, relationDescription));
		return action;
	}

	@SuppressWarnings("static-access")
	private int getCost(String actionName, String relationDescription) {
		if (actionName.equals(this.modify)) {
			if ((relationDescription.equals(Relation.AGREGATION))
					|| (relationDescription.equals(Relation.COMPOSITION)))
				return this.transformationCostValues.modCollection;
			else if (relationDescription.equals(Relation.ASOCIATION))
				return this.transformationCostValues.modAsociation;
			else if (relationDescription.equals(Relation.USE))
				return this.transformationCostValues.modUse;
			else if (relationDescription.equals(Relation.IMPLEMENTATION))
				return this.transformationCostValues.modImplementation;
			else if (relationDescription.equals(Relation.INHERITANCE))
				return this.transformationCostValues.modInheritance;

		} else {
			if ((relationDescription.equals(Relation.AGREGATION))
					|| (relationDescription.equals(Relation.COMPOSITION)))
				return this.transformationCostValues.collection;
			else if (relationDescription.equals(Relation.ASOCIATION))
				return this.transformationCostValues.asociation;
			else if (relationDescription.equals(Relation.USE))
				return this.transformationCostValues.use;
			else if (relationDescription.equals(Relation.IMPLEMENTATION))
				return this.transformationCostValues.implementations;
			else if (relationDescription.equals(Relation.INHERITANCE))
				return this.transformationCostValues.inheritance;
		}
		return 0;
	}

	private Relation getSimilarRelation(List<Relation> involucratedRelations,
			String relationDescription) {
		Relation relation = null;
		if (involucratedRelations.size() == 1) {
			return involucratedRelations.get(0);
		} else {
			relation = involucratedRelations.get(0);
			if (relation.getRelationDescription().equals(relationDescription))
				return relation;
			else {
				for (int i = 1; i < involucratedRelations.size(); i++) {
					if (involucratedRelations.get(i).getRelationDescription()
							.equals(relationDescription))
						return involucratedRelations.get(i);
				}
			}
		}
		return relation;
	}

	private List<Relation> getInvolucratedRelations(String source,
			String target, List<Relation> userRelations) {
		List<Relation> relations = new ArrayList<Relation>();
		for (int i = 0; i < userRelations.size(); i++) {
			if (userRelations.get(i).getSource().getCompName().equals(source)
					&& userRelations.get(i).getTarget().getCompName().equals(
							target)) {
				relations.add(userRelations.get(i));
			}
		}
		return relations;
	}

	private void addToCombination(Hashtable<String, String> partialCombination,
			Hashtable<String, String> totalCombination) {
		for (Enumeration<String> e = partialCombination.keys(); e
		.hasMoreElements();) {
			String key = e.nextElement();
			totalCombination.put(key, partialCombination.get(key));
		}

	}

	private List<Hashtable<String, String>> getEmptiesRenames(
			List<String> userComponentsNames, List<String> appComponentsNames) {
		List<Hashtable<String, String>> combinationsTable = new ArrayList<Hashtable<String, String>>();
		List<Combination> combinations = new ArrayList<Combination>();
		Combination combination = new Combination();
		combination.addAll(appComponentsNames);
		combinations.add(combination);
		completeCombination(userComponentsNames, combinationsTable,
				combinations);
		return combinationsTable;
	}

	private List<Hashtable<String, String>> getRenamesCombinations(
			List<String> userComponentsNames, List<String> appComponentsNames) {
		List<Hashtable<String, String>> combinationsTable = new ArrayList<Hashtable<String, String>>();
		List<String> separatedComponents = new ArrayList<String>();
		separateComponents(userComponentsNames, separatedComponents);
		int combinationMax = userComponentsNames.size();
		List<Combination> combinations = null;
		if (combinationMax > 1) {
			combinations = createCombinations(appComponentsNames,
					combinationMax);
			completeAbsoluteCombinations(appComponentsNames, combinations);
		} else if (combinationMax == 1) {
			combinations = new ArrayList<Combination>();
			for (int i = 0; i < appComponentsNames.size(); i++) {
				Combination combination = new Combination();
				combination.add(appComponentsNames.get(i));
				combinations.add(combination);
			}
			completeAbsoluteCombinations(appComponentsNames, combinations);
		} else if (combinationMax == 0 && appComponentsNames.size() > 0) {
			combinations = new ArrayList<Combination>();
			Combination combination = new Combination();
			combination.addAll(appComponentsNames);
			combinations.add(combination);
		}
		userComponentsNames.addAll(separatedComponents);
		completeCombination(userComponentsNames, combinationsTable,
				combinations);
		return combinationsTable;
	}

	private List<Hashtable<String, String>> getUniqueCombination(
			List<String> userComponentsNames, List<String> appComponentsNames) {
		Hashtable<String, String> uniqueCombination = new Hashtable<String, String>();
		uniqueCombination.put(appComponentsNames.get(0), userComponentsNames
				.get(0));
		List<Hashtable<String, String>> simpleCombinationList = new ArrayList<Hashtable<String, String>>();
		simpleCombinationList.add(uniqueCombination);
		return simpleCombinationList;
	}

	private void completeCombination(List<String> userComponentsNames,
			List<Hashtable<String, String>> combinationsTable,
			List<Combination> combinations) {
		Hashtable<String, String> combinationCase;
		for (Combination combination : combinations) {
			combinationCase = new Hashtable<String, String>();
			for (int i = 0; i < userComponentsNames.size(); i++) {
				combinationCase.put(combination.getNamesCombination().get(i),
						userComponentsNames.get(i));
			}
			combinationsTable.add(combinationCase);
		}
	}

	private void completeAbsoluteCombinations(List<String> appComponentsNames,
			List<Combination> combinations) {
		for (int i = 0; i < combinations.size(); i++) {
			List<String> cloneList = new ArrayList<String>();
			cloneList.addAll(appComponentsNames);
			Combination combination = combinations.get(i);
			for (int j = 0; j < combination.getNamesCombination().size(); j++) {
				cloneList.remove(combination.getNamesCombination().get(j));
			}
			combination.addAll(cloneList);
		}

	}

	private void separateComponents(List<String> userComponentsNames,
			List<String> separatedComponents) {
		for (int i = 0; i < userComponentsNames.size(); i++) {
			if (userComponentsNames.get(i).startsWith("#")) {
				separatedComponents.add(userComponentsNames.remove(i));
				i--;
			}
		}
	}

	private List<Combination> createCombinations(
			List<String> appDesignComponentNames, int combinationMax) {
		List<Combination> combinations = new ArrayList<Combination>();
		List<int[]> permutations = new ArrayList<int[]>();
		int collectionCount = appDesignComponentNames.size();
		if (combinationMax >= MAX_COLLECTION_GROUP) // Cota de elementos maximos
		{
			combinationMax = MAX_COLLECTION_GROUP;
			if (collectionCount > MAX_COLLECTION_SIZE)
				combinationMax--;
		}
		createPermutations(collectionCount, combinationMax,
				permutations);
		Object[] appNames = (Object[]) appDesignComponentNames.toArray();
		for (int i = 0; i < permutations.size(); i++) {
			int[] permutation = (int[]) permutations.get(i);
			Combination combination = new Combination();
			for (int j = 0; j < permutation.length; j++) {
				combination.add(appNames[permutation[j]].toString());
			}
			combinations.add(combination);
		}
		return combinations;
	}

	private int createPermutations(int n, int m, List<int[]> lista) {
		int i, j;
		int k = 1;
		boolean flag;
		int[] temp = new int[n + 1];
		for (i = 0; i <= m; i++)
			temp[i] = 0;
		do {
			flag = true;
			temp[m - 1] = temp[m - 1] + 1;
			for (i = m - 1; i > 0; i--) {
				if (temp[i] >= n) {
					temp[i] = 0;
					temp[i - 1] = temp[i - 1] + 1;
				}
			}
			for (i = 0; i < m && flag; i++)
				for (j = i + 1; j < m && flag; j++) {
					if (temp[i] == temp[j])
						flag = false;
				}
			if ((temp[0] < n) && (flag)) {
				int[] aux = new int[m];
				for (i = 0; i < m; i++) {
					aux[i] = temp[i];
				}
				lista.add(aux);
				k++;
			}
		} while (temp[0] < n);
		return k - 1;
	}
}
