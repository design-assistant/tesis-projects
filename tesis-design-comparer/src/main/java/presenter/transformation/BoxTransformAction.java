package presenter.transformation;

public class BoxTransformAction extends TransformAction {
	private String boxType;

	public BoxTransformAction() {}

	public BoxTransformAction(String boxType, String source, String action) {
		super();
		this.source = source;
		this.boxType = boxType;
		this.action = action;
	}

	public String getBoxType() {
		return boxType;
	}

	public void setBoxType(String boxType) {
		this.boxType = boxType;
	}

	
}
