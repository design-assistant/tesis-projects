package presenter.comparition;

import model.group.PlainDesignBox;

public interface IDesignBoxComparator {

	public Object compare(PlainDesignBox userPlainDesign, PlainDesignBox appPlainDesign);
 
}
