package presenter.comparition;

import model.group.PlainDesignBox;

public class WeightComparator implements IDesignBoxComparator{


	@Override
	public Object compare(PlainDesignBox userPlainDesign,PlainDesignBox appPlainDesign) {
		double userValue  = (Double)userPlainDesign.getDinamicComparitionValue().getValue();
		double appValue = (Double)appPlainDesign.getDinamicComparitionValue().getValue();
		double compareValue  = Math.abs(userValue - appValue);
		return new Double(compareValue);
	}
 
	
}
