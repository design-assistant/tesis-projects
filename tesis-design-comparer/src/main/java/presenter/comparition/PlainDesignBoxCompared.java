package presenter.comparition;

import model.group.PlainDesignBox;

public class PlainDesignBoxCompared implements Comparable<PlainDesignBoxCompared> {

	private PlainDesignBox plain;
	private double comparationValue;
	private double transformationValue;
	private String[] details;



	public String[] getDetails() {
		return details;
	}

	public void setDetails(String[] details) {
		this.details = details;
	}

	public double getTransformationValue() {
		return transformationValue;
	}

	public void setTransformationValue(int transformationValue) {
		this.transformationValue = transformationValue;
	}

	public PlainDesignBoxCompared(PlainDesignBox plain, double comparationValue) {
		this.plain = plain;
		this.comparationValue = comparationValue;
	}

	public PlainDesignBoxCompared() {
	}

	public PlainDesignBox getPlain() {
		return plain;
	}

	public void setPlain(PlainDesignBox plain) {
		this.plain = plain;
	}

	public double getComparationValue() {
		return comparationValue;
	}

	public void setComparationValue(double comparationValue) {
		this.comparationValue = comparationValue;
	}

	@Override
	public int compareTo(PlainDesignBoxCompared o) {
		if ((this.comparationValue < o.comparationValue) && (this.transformationValue < o.transformationValue))
		{
			return -1;
		}
		if ((this.comparationValue > o.comparationValue) && (this.transformationValue > o.transformationValue))
		{
			return 1;
		}
		if ((this.comparationValue < o.comparationValue) && (this.transformationValue == o.transformationValue)) 
		{
			return -1;
		}
		if ((this.comparationValue == o.comparationValue) && (this.transformationValue > o.transformationValue)) 
		{
			return 1;
		}
		if ((this.comparationValue > o.comparationValue) && (this.transformationValue == o.transformationValue)) 
		{
			return 1;
		}
		if ((this.comparationValue == o.comparationValue) && (this.transformationValue < o.transformationValue)) 
		{
			return -1;
		}
		double cost;
		double transformation;
		if ((this.comparationValue < o.comparationValue) && (this.transformationValue > o.transformationValue)) {
			cost = this.comparationValue / o.comparationValue;
			transformation = o.transformationValue / this.transformationValue;
			if (cost <= transformation)
			{
				return -1;
			}
			if (this.getPlain().getDesign().getName().equals("Observer") || (o.getPlain().getDesign().getName().equals("Observer")))
			return 1;
		}
		if ((this.comparationValue > o.comparationValue) && (this.transformationValue < o.transformationValue)) {
			cost = o.comparationValue / this.comparationValue;
			transformation = this.transformationValue / o.transformationValue;
			if (cost >= transformation)
			{
				return -1;
			}
			if (this.getPlain().getDesign().getName().equals("Observer") || (o.getPlain().getDesign().getName().equals("Observer")))
			return 1;
		}
		if(this.getPlain().getDesign().getSuccess()<=o.getPlain().getDesign().getSuccess()){
			return 1;
		}
		else{
			return -1;
		}
	}

}
