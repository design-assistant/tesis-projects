package presenter;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import model.data.generalStructure.Component;
import model.data.generalStructure.Design;
import model.data.generalStructure.Repository;
import model.group.PlainDesign;
import model.group.PlainDesignBox;
import model.group.QualityAttribute;
import presenter.logic.AbstractDesignComparer;
import presenter.transformation.TransformationGenerator;
import presenter.transformation.TransformAction;
import bo.IPlainDesignBoxBO;
import context.ImplementationFactory;

public class SimpleUserPresenter extends UserControlPresenter {

	private PlainDesignBox userDesignBox;
	private List<String> attributes = new ArrayList<String>();
	private int qualityAttributeCount;
	private int qualityAttributeKindOfMatching;
	private double absorbedRelationsLimit;
	private double componentsCountLimit;
	private AbstractDesignComparer comparer;
	private List<Design> finalResult;

	public SimpleUserPresenter() {
	}

	public List<Design> getFinalResult() {
		return finalResult;
	}

	public void setFinalResult(List<Design> finalResult) {
		this.finalResult = finalResult;
	}

	public AbstractDesignComparer getComparer() {
		return comparer;
	}

	public void setComparer(AbstractDesignComparer comparer) {
		this.comparer = comparer;
	}

	public PlainDesignBox getUserDesignBox() {
		return userDesignBox;
	}

	public void setUserDesignBox(PlainDesignBox userDesignBox) {
		this.userDesignBox = userDesignBox;
	}

	public void compare() {
		List<QualityAttribute> atts = null;
		if (this.attributes != null) {
			atts = new ArrayList<QualityAttribute>();
			for (int i = 0; i < this.attributes.size(); i++) {
				QualityAttribute q = new QualityAttribute();
				q.setName(attributes.get(i));
				atts.add(q);
			}
		}
		int percentageIntValue;
		int componentsMin;
		int componentsMax;
		if (this.componentsCountLimit != -1) {
			percentageIntValue = generatePercentageIntValue(this.componentsCountLimit, Repository.getInstance().getUp()
					.getComponentsCount());
			componentsMin = Repository.getInstance().getUp().getComponentsCount() - percentageIntValue;
			if (componentsMin <= 0)
				componentsMin = 1;
			componentsMax = Repository.getInstance().getUp().getComponentsCount() + percentageIntValue;
		} else {
			componentsMin = -1;
			componentsMax = -1;
		}
		int absorbedMin;
		int absorbedMax;
		if (this.absorbedRelationsLimit != -1) {
			percentageIntValue = generatePercentageIntValue(this.absorbedRelationsLimit, Repository.getInstance().getUp()
					.getAbsorbedRelations());
			absorbedMin = Repository.getInstance().getUp().getAbsorbedRelations() - percentageIntValue;
			if (absorbedMin <= 0)
				absorbedMin = 1;
			absorbedMax = Repository.getInstance().getUp().getAbsorbedRelations() + percentageIntValue;
		} else {
			absorbedMin = -1;
			absorbedMax = -1;
		}

		// Query a la BD
		IPlainDesignBoxBO pdbBO = (IPlainDesignBoxBO) ImplementationFactory.getBean("plainDesignBoxBO");
		List<PlainDesignBox> plains;
		if (this.qualityAttributeKindOfMatching == 1) // FULL
			plains = pdbBO.findAllByFullQualityAttributesAndComponentsCountAbdsorbedRelationsCountRange(atts, componentsMin, componentsMax,
					absorbedMin, absorbedMax);
		else if (this.qualityAttributeKindOfMatching == 2) // LESS
			plains = pdbBO.findAllByLessQualityAttributesAndComponentsCountAbdsorbedRelationsCountRange(atts, componentsMin, componentsMax,
					absorbedMin, absorbedMax);
		else if (this.qualityAttributeKindOfMatching == 3) // MORE
			plains = pdbBO.findAllByMoreQualityAttributesAndComponentsCountAbdsorbedRelationsCountRange(atts, componentsMin, componentsMax,
					absorbedMin, absorbedMax);
		else
			// LIMIT
			plains = pdbBO.findAllByLimitQualityAttributesAndComponentsCountAbdsorbedRelationsCountRange(atts, componentsMin,
					componentsMax, absorbedMin, absorbedMax, qualityAttributeCount);
		finalResult = comparer.compare(plains, userDesignBox,attributes);
		}
	
	

	/**
	 * Este metodo se encarga de obtener el n�mero entero del porcentaje ingresado por el usuario para armar el rango de cantidad de cajas
	 * o el rango de relaciones absorbidas en el cual caeran los dise�os v�lidos obtenidos por el query. Para esto se usa la cantidad de
	 * componentes (cajas) o la cantidad de relaciones absorbidas que tiene el dise�o aplanado UP.
	 * 
	 * @param percentage :
	 *            porcentaje
	 * @param componentsCount :
	 * @return : retorna n�mero entero para armar rango
	 */
	private int generatePercentageIntValue(double percentage, int componentsCount) {
		double componentsCountDoubleIncrease = (percentage * componentsCount) / 100;
		int componentsConuntIncrease = new Double(componentsCountDoubleIncrease).intValue();
		String[] decimalPartSplit = new Double(componentsCountDoubleIncrease).toString().split("\\.");
		if (new Integer(decimalPartSplit[1].substring(0, 1)) >= 5)
			componentsConuntIncrease++;
		return componentsConuntIncrease;
	}

	public void clearViewComparationSettings() {
		attributes = null;
		qualityAttributeCount = -1;
		qualityAttributeKindOfMatching = 1;
		absorbedRelationsLimit = -1;
		componentsCountLimit = -1;

	}

	public void setAbsorbedRelationsLimit(double absorbedRelationsLimit) {
		this.absorbedRelationsLimit = absorbedRelationsLimit;
	}

	public void setComponentsCountLimit(double componentsCountLimit) {
		this.componentsCountLimit = componentsCountLimit;
	}

	public void setQualityAttributes(List<String> attributes, int qualityAttributeCount, int qualityAttributeKindOfMatching) {
		this.attributes = attributes;
		this.qualityAttributeCount = qualityAttributeCount;
		this.qualityAttributeKindOfMatching = qualityAttributeKindOfMatching;
	}

	@Override
	public void loadDesign(String path) {
		super.loadDesign(path);
		loadUserDesign();
	}

	@Override
	public void loadDesign(Design design) {
		Repository.getInstance().setRelations(new Vector<Component>(design.getRelations()));
		super.loadDesign(design);
		loadUserDesign();
	}

	private void loadUserDesign() {
		// ARMA EL PAQUETE CON EL DISE�O INGRESADO POR EL USUARIO
		userDesignBox = new PlainDesignBox();
		userDesignBox.initializeContext();
		userDesignBox.setDesign(getDesign());
		List<PlainDesign> designs = new ArrayList<PlainDesign>();
		designs.add(Repository.getInstance().getUp());
		designs.add(Repository.getInstance().getDown());
		userDesignBox.setDesigns(designs);
		// la siguiente linea ya setea el valor en el userDesignBoxCriterio
		userDesignBox.getDinamicComparitionValue().getValue();
		userDesignBox.getDinamicTransformationValue().getValue();
	}

	public List<TransformAction> getTransformations(Design design) {
		 TransformationGenerator transformer = new TransformationGenerator();
		 List<TransformAction> transformActions = new ArrayList<TransformAction>();
	
		 transformActions = transformer.transform(userDesignBox.getDesign(), design);
		 return transformActions;
	}

	public List<String> getAttributes() {
		return attributes;
	}

}
