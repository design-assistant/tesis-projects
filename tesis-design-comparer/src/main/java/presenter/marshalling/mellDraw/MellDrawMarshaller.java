package presenter.marshalling.mellDraw;


import model.data.generalStructure.Design;
import model.data.melDrawStructure.DesignProject;
import model.data.melDrawStructure.Repository;
import presenter.marshalling.Marshaller;

public class MellDrawMarshaller implements Marshaller {

	public Design marshall(Object design) {
		Repository.getRepository().clearRepository();
		return ((DesignProject)design).marshall();
	}

	

}
