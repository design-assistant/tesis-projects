package presenter.marshalling;

import model.data.generalStructure.Design;

public interface Marshaller {

	public Design marshall(Object design);

}
