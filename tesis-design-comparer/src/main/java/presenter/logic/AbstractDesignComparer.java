package presenter.logic;

import java.util.List;

import presenter.transformation.IDesignTransformer;

import model.data.generalStructure.Design;
import model.group.PlainDesignBox;

public abstract class AbstractDesignComparer {

	protected IDesignTransformer transformer;
	
	public abstract List<Design> compare(List<PlainDesignBox> designs,PlainDesignBox userDesign, List<String> attributes);

	public IDesignTransformer getTransformer() {
		return transformer;
	}

	public void setTransformer(IDesignTransformer transformer) {
		this.transformer = transformer;
	}
	
	
	
}
