package presenter.logic.local;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import model.data.generalStructure.Design;
import model.group.PlainDesignBox;
import presenter.comparition.PlainDesignBoxCompared;
import presenter.logic.AbstractDesignComparer;
import presenter.transformation.CostTransformer;

public class LocalComparer extends AbstractDesignComparer {

	private final int changeItems = 8;

	@Override
	public List<Design> compare(List<PlainDesignBox> designs,
			PlainDesignBox userDesign, List<String> attributes) {
		List<PlainDesignBoxCompared> partialResult = new ArrayList<PlainDesignBoxCompared>();
		partialResult = obtainFinalResult(designs, userDesign, attributes);
		Collections.sort(partialResult);
		List<Design> finalResult = new ArrayList<Design>();
		for (PlainDesignBoxCompared compared : partialResult) {
			finalResult.add(compared.getPlain().getDesign());
		}

		return finalResult;
	}

	public List<PlainDesignBoxCompared> obtainFinalResult(
			List<PlainDesignBox> designs, PlainDesignBox userDesign,
			List<String> attributes) {
		List<PlainDesignBoxCompared> comparatedsPlainDesignBox = new ArrayList<PlainDesignBoxCompared>();
		PlainDesignBoxCompared comparated;
		for (PlainDesignBox plain : designs) {
			plain.initializeContext();
			comparated = new PlainDesignBoxCompared();
			comparated.setPlain(plain);
			comparated.setComparationValue((Double) plain
					.getComparitionValue(userDesign));
			comparated.setDetails(obtainChangeDetails(userDesign, plain));
			this.transformer = new CostTransformer();
			comparated.setTransformationValue((Integer) this.transformer
					.obtainTransformationValue(userDesign, plain));
			plain.getDesign().setSuccess(
					plain.getSuccessForUserAttributes(attributes));
			comparatedsPlainDesignBox.add(comparated);

		}
		return comparatedsPlainDesignBox;
	}

	private String[] obtainChangeDetails(PlainDesignBox userDesign,
			PlainDesignBox plain) {
		String[] changeDetails = new String[changeItems];

		int numberDiff = userDesign.getDesign().getBoxComponentsCount()
				- plain.getDesign().getBoxComponentsCount();
		String diffDetail = Math.abs(numberDiff) + " clases/interfaces"
				+ getAddORDel(numberDiff);
		changeDetails[0] = diffDetail;

		numberDiff = userDesign.getDesign().getFathersCount()
				- plain.getDesign().getFathersCount();
		diffDetail = Math.abs(numberDiff) + " clases/interfaces padre"
				+ getAddORDel(numberDiff);
		changeDetails[1] = diffDetail;

		numberDiff = userDesign.getDesign().getSonsCount()
				- plain.getDesign().getSonsCount();
		diffDetail = Math.abs(numberDiff) + " clases hijo"
				+ getAddORDel(numberDiff);
		changeDetails[2] = diffDetail;

		numberDiff = userDesign.getDesign().getCollectionsCount()
				- plain.getDesign().getCollectionsCount();
		diffDetail = Math.abs(numberDiff) + " agregaciones o composiciones"
				+ getAddORDel(numberDiff);
		changeDetails[3] = diffDetail;

		numberDiff = userDesign.getDesign().getAsociationCount()
				- plain.getDesign().getAsociationCount();
		diffDetail = Math.abs(numberDiff) + " asociaciones"
				+ getAddORDel(numberDiff);
		changeDetails[4] = diffDetail;

		numberDiff = userDesign.getDesign().getImplementationCount()
				- plain.getDesign().getImplementationCount();
		diffDetail = Math.abs(numberDiff) + " implementaciones"
				+ getAddORDel(numberDiff);
		changeDetails[5] = diffDetail;

		numberDiff = userDesign.getDesign().getInheritanceCount()
				- plain.getDesign().getInheritanceCount();
		diffDetail = Math.abs(numberDiff) + " extensiones"
				+ getAddORDel(numberDiff);
		changeDetails[6] = diffDetail;

		numberDiff = userDesign.getDesign().getUsesCount()
				- plain.getDesign().getUsesCount();
		diffDetail = Math.abs(numberDiff) + " dependencias"
				+ getAddORDel(numberDiff);
		changeDetails[7] = diffDetail;

		return changeDetails;

	}

	private String getAddORDel(int classDiff) {
		if (classDiff < 0)
			return "  faltante/s.";
		return "  sobrante/s.";
	}
}
