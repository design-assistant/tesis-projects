package presenter.exceptions;

public class DuplicateAttributeException  extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Override
	public String getMessage() {
		return "El atributo de calidad ya existe";
	}

}
