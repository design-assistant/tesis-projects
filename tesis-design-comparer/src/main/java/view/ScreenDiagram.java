package view;


import java.util.Hashtable;
import java.util.Vector;
import javax.swing.JScrollPane;
import org.jgraph.JGraph;
import org.jgraph.graph.DefaultCellViewFactory;
import org.jgraph.graph.DefaultGraphModel;
import org.jgraph.graph.GraphLayoutCache;
import org.jgraph.graph.GraphModel;
import view.drawable.DrawableComponent;


public abstract class ScreenDiagram {

	protected JScrollPane panel;
	protected JGraph graph;
	protected Vector<DrawableComponent> components = new Vector<DrawableComponent>();
	protected Hashtable<String,Object> painted = new Hashtable<String,Object>();
	protected String diagramName;
	
	public ScreenDiagram() {
		super();
		panel = new JScrollPane();
		panel.setBounds(0, 0, Configuration.widthEditor, Configuration.heightEditor);
		GraphModel model = new DefaultGraphModel();
		GraphLayoutCache view = new GraphLayoutCache(model,new DefaultCellViewFactory());
		graph=new JGraph(model,view);
		panel.setViewportView(graph);
	}

	public JScrollPane getPanel() {
		return panel;
	}

	public JGraph getGraph() {
		return graph;
	}

	public void addComponent(DrawableComponent drawable) {
		components.add(drawable);
	}

	public abstract void paint();
	
	public String getDiagramName() {
		return diagramName;
	}
	public void setDiagramName(String diagramName) {
		this.diagramName = diagramName;
	}
	

}