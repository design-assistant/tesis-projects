package view;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import presenter.AdminUserPresenter;




/**
* This code was edited or generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a corporation,
* company or business for any purpose whatever) then you
* should purchase a license for each developer using Jigloo.
* Please visit www.cloudgarden.com for details.
* Use of Jigloo implies acceptance of these licensing terms.
* A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
* THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
* LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
*/
public class Login extends javax.swing.JDialog {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextField jTextFieldUsuario;
	private JButton jButtonCancelar;
	private JButton jButtonAceptar;
	private JPasswordField jPasswordFieldPass;
	private JLabel jLabelContrasena;
	private JLabel jLabelUsuario;
	private AdminWindow father;
	private AdminUserPresenter presenter;
	
	
	public Login(AdminWindow adminWindow,AdminUserPresenter presenter) {
		super();
		initGUI();
		this.setModal(true);
		this.setLocationRelativeTo(null);
		this.father = adminWindow;
		this.presenter = presenter;
	}
	
	

	private void initGUI() {
		try {
			{
				this.setModal(true);
				this.setLocation(new java.awt.Point(100, 100));
				getContentPane().setLayout(null);
				this.setTitle("Login");
				this.setResizable(false);
				{
					jTextFieldUsuario = new JTextField();
					getContentPane().add(jTextFieldUsuario);
					jTextFieldUsuario.setBounds(91, 21, 126, 28);
//					jTextFieldUsuario.setText("admin");
					jTextFieldUsuario.addKeyListener(new KeyAdapter() {
						public void keyPressed(KeyEvent evt) {
							jTextFieldUsuarioKeyPressed(evt);
						}
					});
				}
				{
					jLabelUsuario = new JLabel();
					getContentPane().add(jLabelUsuario);
					jLabelUsuario.setText("Usuario:");
					jLabelUsuario.setBounds(21, 21, 49, 28);
				}
				{
					jLabelContrasena = new JLabel();
					getContentPane().add(jLabelContrasena);
					jLabelContrasena.setText("Contrase�a: ");
					jLabelContrasena.setBounds(21, 56, 84, 28);
				}
				{
					jPasswordFieldPass = new JPasswordField();
					getContentPane().add(jPasswordFieldPass);
					jPasswordFieldPass.setBounds(91, 56, 126, 28);
//					jPasswordFieldPass.setText("123456");
					jPasswordFieldPass.addKeyListener(new KeyAdapter() {
						public void keyPressed(KeyEvent evt) {
							jPasswordFieldPassKeyPressed(evt);
						}
					});
				}
				{
					jButtonAceptar = new JButton();
					getContentPane().add(jButtonAceptar);
					jButtonAceptar.setToolTipText("Aceptar");
					jButtonAceptar.setIcon(new ImageIcon(getClass().getClassLoader()
						.getResource("images/iconos/ok.png")));
					jButtonAceptar.setBounds(70, 105, 42, 42);
					jButtonAceptar.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent evt) {
							logIn(evt);
						}
					});
				}
				{
					jButtonCancelar = new JButton();
					getContentPane().add(jButtonCancelar);
					jButtonCancelar.setToolTipText("Cancelar");
					jButtonCancelar.setIcon(new ImageIcon(getClass().getClassLoader()
						.getResource("images/iconos/cancelar.png")));
					jButtonCancelar.setBounds(129, 105, 42, 42);
					jButtonCancelar.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent evt) {
							cancelLogIn(evt);
						}
					});
				}
			}
			this.setSize(246, 181);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void logIn(ActionEvent evt) {
		verifyUser();
	}

	private void verifyUser() {
		@SuppressWarnings("unused")
		AcceptMessage message;
		String pass = "";
		for (int i = 0;i < jPasswordFieldPass.getPassword().length;i++)
			pass = pass + jPasswordFieldPass.getPassword()[i];
		if (!jTextFieldUsuario.getText().equals("") && (!pass.equals("")))
		{
			if (presenter.isValidUser(jTextFieldUsuario.getText().trim(),pass))
			{
				this.father.setLogin(true);
				this.dispose();
			}
			else
				message = new AcceptMessage("Usuario/Contrase�a inv�lido",274);
		}
		else
			message = new AcceptMessage("Ingrese los datos de usuario",274);
	}
	
	private void jTextFieldUsuarioKeyPressed(KeyEvent evt) {
		if (evt.getKeyCode() == 10)		
			verifyUser();
	}
	
	private void jPasswordFieldPassKeyPressed(KeyEvent evt) {
		if (evt.getKeyCode() == 10)		
			verifyUser();
	}
	
	private void cancelLogIn(ActionEvent evt) {
		this.father.setLogin(false);
		this.dispose();
	}

}
