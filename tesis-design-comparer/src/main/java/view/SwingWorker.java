package view;
import javax.swing.SwingUtilities;


public abstract class SwingWorker {
    private Object value;
    private Thread thread;

    /** 
     * Computa el valor que ha de ser retornado por el m�todo get
     */
    public abstract Object construct();

    /**
     * Llamado por despachador de eventos de threads luego de que el m�todo
     * construct haya efectuado el retorno
     */
    public void finished() {
    }

    /**
     * Retorna el valor creado por el m�todo construct  
     */
    public Object get() {
        while (true) {
            Thread t;
            synchronized (SwingWorker.this) {
                t = thread;
                if (t == null) {
                    return value;
                }
            }
            try {
                t.join();
            }
            catch (InterruptedException e) {
            }
        }
    }

    /**
     * Arranca el thread que llamar� al m�todo construct y luego finalizar� 
     */
    public SwingWorker() {
        final Runnable doFinished = new Runnable() {
           public void run() { finished(); }
        };

        Runnable doConstruct = new Runnable() { 
            public void run() {
                synchronized(SwingWorker.this) {
                    value = construct();
                    thread = null;
                }
                SwingUtilities.invokeLater(doFinished);
            }
        };

        thread = new Thread(doConstruct);
        thread.start();
    }
}

