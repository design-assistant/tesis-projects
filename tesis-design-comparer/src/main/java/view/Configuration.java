package view;

import java.util.Vector;


public final class Configuration {


	public static final int widthSpace=7;//espacio entre letra y letra...
	public static final int heightSpace=15;//espacio entre renglon y renglon...
	public static final int widthEditor=800;//tama�o de la pantalla de visualizacion - Ancho
	public static final int heightEditor=600;//tama�o de la pantalla de visualizacion - Largo
	public static int relationalComponents = 0;
	public static final int TabWidth = 650;
	public static final int TabHeight = 532;
	public static String proyectName = "";
	public static Vector<String> diagramsNames = new Vector<String>();
	public static int packageBodyWidth=40;
	public static int packageBodyHeight=30;
	public static int maxMessages=30;
	public static int instanceSpace=150;
	public static final int moreLineIntance=300;
	public static void addDiagramsNames(String name) {
		diagramsNames.add(name);
	}
	public static String getDiagramName(int i) {
		if (diagramsNames.size() < i)
			return "";
		else
			return diagramsNames.get(i);
	}
	
}
