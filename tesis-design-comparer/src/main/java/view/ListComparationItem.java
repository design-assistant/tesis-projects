package view;

import javax.swing.ImageIcon;

public class ListComparationItem extends ListItem{

	private String alert;
	private String alertType;
	
	public ListComparationItem(ImageIcon icon, String value,String alert, String alertType) {
		this.icon = icon;
		this.value = value;
		this.alert = alert;
		this.alertType=alertType;
	}
	
	public ListComparationItem(ImageIcon icon,String alert,String alertType) {
		this.icon = icon;
		this.alert = alert;
		this.alertType=alertType;
	}
	
	public String getAlert() {
		return alert;
	}

	public String getAlertType() {
		return alertType;
	}

	
}
