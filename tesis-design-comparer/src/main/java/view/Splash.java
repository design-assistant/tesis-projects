package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JProgressBar;
import javax.swing.Timer;
import javax.swing.WindowConstants;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import context.ImplementationFactory;

/**
* This code was edited or generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a corporation,
* company or business for any purpose whatever) then you
* should purchase a license for each developer using Jigloo.
* Please visit www.cloudgarden.com for details.
* Use of Jigloo implies acceptance of these licensing terms.
* A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
* THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
* LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
*/
public class Splash extends javax.swing.JDialog  {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JProgressBar jProgressBar1;
	private Timer timer;
	private Task task;
	private JLabel jLabel1;
	private JLabel jLabel2;
	private JLabel jLabel3;
	private boolean end = false;
	public final static int ONE_SECOND = 1000;

	public Splash(int taskTime,JFrame frame) {
		super();
		task = new Task(taskTime);
		jProgressBar1 = new JProgressBar(0, task.getLengthOfTask());
		jProgressBar1.setValue(0);
		jProgressBar1.setStringPainted(true);
		initGUI();
		timer = new Timer(ONE_SECOND, new TimerListener(this,frame));
        task.go();
        timer.start();
        this.setVisible(true);
        ImplementationFactory.setApplicationContext(new ClassPathXmlApplicationContext("applicationContext.xml"));
        end = true;
	}
	
	private void initGUI() {
		try {
			{
				getContentPane().setLayout(null);
				getContentPane().setBackground(new java.awt.Color(218,198,171));
				this.setResizable(false);
				this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
				this.setFocusable(false);
				this.setFocusTraversalKeysEnabled(false);
				{
					jLabel2 = new JLabel();
					getContentPane().add(jLabel2);
					jLabel2.setText("Comparación y Transformación de  Diagramas de Diseño");
					jLabel2.setBounds(19, 7, 556, 27);
					jLabel2.setFont(new java.awt.Font("Segoe UI",1,20));
				}
				{
					getContentPane().add(jProgressBar1);
					{
						jLabel1 = new JLabel();
						getContentPane().add(jLabel1);
						jLabel1.setBounds(98, 42, 420, 315);
						jLabel1.setIcon(new ImageIcon(getClass().getClassLoader().getResource("images/420px-UML_Diagrams.jpg")));
					}
					jProgressBar1.setBounds(91, 371, 420, 14);
					jProgressBar1.setBackground(new java.awt.Color(
						255,
						255,
						255));
					jProgressBar1.setForeground(new java.awt.Color(128,64,64));
					jProgressBar1.setIndeterminate(true);
				}
				{
					jLabel3 = new JLabel();
					getContentPane().add(jLabel3);
					jLabel3.setText("Tesis de grado en Ingeniería de Sistemas. Marco Stella - Lucas Tiritilli. UNICEN. Tandil. Junio de 2010");
					jLabel3.setBounds(28, 396, 602, 23);
				}

				this.setFocusCycleRoot(false);
				this.setFocusableWindowState(false);
				this.setUndecorated(true);
				jProgressBar1.setStringPainted(true);
				jProgressBar1.setString("");

			}
			this.setSize(589, 426);
			this.setLocationRelativeTo(null);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	class TimerListener implements ActionListener {
		Splash pb;
	      JFrame main;
	    	public TimerListener(Splash bar, JFrame frame) {
				pb = bar;
				main = frame;
			}

			public void actionPerformed(ActionEvent evt) {
				jProgressBar1.setValue(task.getCurrent());
	        
	            if (task.done()) {
	                timer.stop();
	                main.setEnabled(true);
	                if (end)
	                	pb.dispose();
	                else
	                {
	        			jProgressBar1.setValue(0);
	        			timer.start();
	                }
	            }
	        }
	    }
	
}
