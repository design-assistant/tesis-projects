package view;

import java.awt.Color;
import java.awt.Component;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

class MyCellRenderer extends JLabel implements ListCellRenderer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MyCellRenderer() {
		setOpaque(true);
	}

	public Component getListCellRendererComponent(JList list, Object value, // value to display
			int index, // cell index
			boolean iss, // is selected
			boolean chf) // cell has focus?
	{
		
		setText(((ListItem) value).getValue());
		setIcon(((ListItem)value).getIcon());

		
		if (iss) {
			setBorder(BorderFactory.createLineBorder(Color.blue, 2));
		} else {
			setBorder(BorderFactory.createLineBorder(list.getBackground(), 2));
		}
		return this;
	}
}