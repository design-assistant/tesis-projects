package view;

public class Task {
	    private int lengthOfTask;
	    private int current = 0;
	    private String statMessage;

	    Task (int taskTime) {
	        lengthOfTask = taskTime;
	    }

	    void go() {
	        current = 0;
	        @SuppressWarnings("unused")
			final SwingWorker worker = new SwingWorker() {
	            public Object construct() {
	                return new ActualTask();
	            }
	        };
	    }

	    int getLengthOfTask() {
	        return lengthOfTask;
	    }

	    int getCurrent() {
	        return current;
	    }

	    void stop() {
	        current = lengthOfTask;
	    }

	    boolean done() {
	        if (current >= lengthOfTask)
	            return true;
	        else
	            return false;
	    }

	    String getMessage() {
	        return statMessage;
	    }

	    class ActualTask {
	        ActualTask () {

	            while (current < lengthOfTask) {
	                try {
	                    Thread.sleep(50); 
	                    current += Math.random() * 100;
	                } catch (InterruptedException e) {
	                }
	            }
	        }
	    }
}
