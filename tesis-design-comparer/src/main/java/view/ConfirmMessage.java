package view;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

/**
* This code was edited or generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a corporation,
* company or business for any purpose whatever) then you
* should purchase a license for each developer using Jigloo.
* Please visit www.cloudgarden.com for details.
* Use of Jigloo implies acceptance of these licensing terms.
* A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
* THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
* LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
*/
public class ConfirmMessage extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JButton jButtonCancel;
	private JButton jButtonOk;
	private JLabel jLabelMessage;
	private JLabel jLabel1;
	private int selection = JOptionPane.CANCEL_OPTION;

	public ConfirmMessage(String message,int size) {
		initGUI();
		this.setModal(true);
		this.setSize(size, 139);
		jButtonOk.setBounds((size/2)-50, 56, 40, 40);
		jButtonCancel.setBounds((size/2)+10, 56, 40, 40);
		jLabelMessage.setText(message);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}
	
	private void initGUI() {
		try {
			{
				getContentPane().setLayout(null);
				this.setTitle("Mensaje");
				this.setResizable(false);
				{
					jLabel1 = new JLabel();
					getContentPane().add(jLabel1);
					jLabel1.setBounds(10, 21, 35, 28);
					jLabel1.setIcon(new ImageIcon(getClass().getClassLoader().getResource("images/iconos/alert.png")));

				}
				{
					jLabelMessage = new JLabel();
					getContentPane().add(jLabelMessage);
					jLabelMessage.setHorizontalAlignment(JLabel.LEFT);
					jLabelMessage.setBounds(49, 28, 900, 21);
				}
				{
					jButtonOk = new JButton();
					getContentPane().add(jButtonOk);
					jButtonOk.setIcon(new ImageIcon(getClass().getClassLoader()
							.getResource("images/iconos/ok.png")));
					jButtonOk.setToolTipText("Aceptar");
					jButtonOk.setBounds(119, 56, 40, 40);
					jButtonOk.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent evt) {
							jButtonOkActionPerformed(evt);
						}
					});
				}
				{
					jButtonCancel = new JButton();
					jButtonCancel.setIcon(new ImageIcon(getClass().getClassLoader()
							.getResource("images/iconos/cancelar.png")));
					jButtonCancel.setToolTipText("Cancelar");
					getContentPane().add(jButtonCancel);
					jButtonCancel.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent evt) {
							jButtonCancelActionPerformed(evt);
						}
					});
					jButtonCancel.setBounds(56, 56, 42, 42);
				}
			}
			{
				this.setSize(274, 139);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void jButtonOkActionPerformed(ActionEvent evt) {
		selection = JOptionPane.OK_OPTION;
		this.dispose();
	}
	
	private void jButtonCancelActionPerformed(ActionEvent evt) {
		selection = JOptionPane.CANCEL_OPTION;
		this.dispose();
	}

	public int getAnswer() {
		return selection;
	}
}
