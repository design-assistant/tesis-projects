package view.drawable.sequenceDiagram;

import java.awt.geom.Rectangle2D;
import java.util.Hashtable;
import java.util.List;

import org.jgraph.graph.DefaultEdge;
import org.jgraph.graph.DefaultGraphCell;
import org.jgraph.graph.GraphConstants;
import model.data.generalStructure.Component;
import model.data.generalStructure.sequenceDiagram.Message;

public class DrawableCreation extends DrawableInvocation {

	public DrawableCreation(Component component) {
		super(component);
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void SpecificDraw(DefaultGraphCell sourceCell, DefaultGraphCell targetCell, Hashtable<String, Object> painted) {

		String source = ((Message) this.getDiagramComponent()).getSource().getPath() + ":"
				+ ((Message) this.getDiagramComponent()).getSource().getCompName();
		String target = ((Message) this.getDiagramComponent()).getTarget().getPath() + ":"
				+ ((Message) this.getDiagramComponent()).getTarget().getCompName();

		DefaultEdge edge = new DefaultEdge(this.visibleName());

		DefaultGraphCell aux2 = (DefaultGraphCell) (((DrawableInstance) painted.get(target)).getDrawed());

		edge.setSource(((DrawableInstance) painted.get(source)).getNextElement().getChildAt(0));

		edge.setTarget(((DefaultGraphCell) aux2.getChildAt(0)).getChildAt(0));

		List list = aux2.getChildren();

		DefaultGraphCell head = (DefaultGraphCell) aux2.getChildAt(0);
		DefaultGraphCell inline = (DefaultGraphCell) list.get(((DrawableInstance) painted.get(source)).getCurrentLevel());

		Rectangle2D rec = GraphConstants.getBounds(inline.getAttributes());
		Rectangle2D rec2 = GraphConstants.getBounds(head.getAttributes());

		GraphConstants.setBounds(head.getAttributes(), new Rectangle2D.Double(rec.getX() - (rec2.getWidth() - 1) / 3, rec.getY() + 10, rec2
				.getWidth(), rec2.getHeight()));

		GraphConstants.setLabelAlongEdge(edge.getAttributes(), true);
		GraphConstants.setSelectable(edge.getAttributes(), false);
		int arrow = GraphConstants.ARROW_SIMPLE;
		GraphConstants.setLineEnd(edge.getAttributes(), arrow);
		GraphConstants.setEndFill(edge.getAttributes(), true);
		GraphConstants.setLineWidth(edge.getAttributes(), 1);
		moreSpecificDrawMessage(edge);
		DefaultGraphCell print = new DefaultGraphCell();
		print.add(edge);
		print.add(aux2);

		GraphConstants.setSelectable(print.getAttributes(), false);

		GraphConstants.setSelectable(print.getAttributes(), false);
		GraphConstants.setResize(print.getAttributes(), false);
		GraphConstants.setGroupOpaque(print.getAttributes(), true);
		SequenceLevelManager.getInstance().addLevel();
		drawed = print;
	}

}
