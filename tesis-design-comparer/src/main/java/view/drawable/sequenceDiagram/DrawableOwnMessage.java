package view.drawable.sequenceDiagram;

import java.awt.geom.Rectangle2D;
import java.util.Hashtable;
import org.jgraph.graph.DefaultEdge;
import org.jgraph.graph.DefaultGraphCell;
import org.jgraph.graph.GraphConstants;
import model.data.generalStructure.Component;
import model.data.generalStructure.sequenceDiagram.Message;


public class DrawableOwnMessage extends DrawableInvocation {

	public DrawableOwnMessage(Component component) {
		super(component);
	}


	@Override
	protected void SpecificDraw(DefaultGraphCell sourceCell,DefaultGraphCell targetCell, Hashtable<String, Object> painted) {
		String source = ((Message) this.getDiagramComponent()).getSource().getPath()+ ":"+ ((Message) this.getDiagramComponent()).getSource().getCompName();
		String target = ((Message) this.getDiagramComponent()).getTarget().getPath()+ ":"+ ((Message) this.getDiagramComponent()).getTarget().getCompName();
		DefaultGraphCell total=new DefaultGraphCell();
		DefaultGraphCell aux1 = new DefaultGraphCell();

		//primera relacion con celda invisible
		GraphConstants.setBounds(aux1.getAttributes(),new Rectangle2D.Double(((DrawableInstance)painted.get(target)).getX(),((DrawableInstance)painted.get(target)).getHeight(((((DrawableInstance)painted.get(source)).getCurrentLevel())))+1,0,0));
		GraphConstants.setSelectable(aux1.getAttributes(), false);
		aux1.addPort();
		DefaultEdge temp = new DefaultEdge();
		setEdgeData(temp);
		temp.setSource(((DrawableInstance)painted.get(source)).getNextElement().getChildAt(0));
		temp.setTarget(aux1.getChildAt(0));

		//relacion entre 2 celdas invisibles
		SequenceLevelManager.getInstance().addLevel();
		DefaultGraphCell aux2 = new DefaultGraphCell();
		GraphConstants.setBounds(aux2.getAttributes(),new Rectangle2D.Double(((DrawableInstance)painted.get(target)).getX(),((DrawableInstance)painted.get(target)).getHeight(((((DrawableInstance)painted.get(source)).getCurrentLevel())))+1,0,0));
		GraphConstants.setSelectable(aux2.getAttributes(), false);
		aux2.addPort();
		DefaultEdge temp2 = new DefaultEdge(visibleName());
		setEdgeData(temp2);
		temp2.setSource(aux1.getChildAt(0));
		temp2.setTarget(aux2.getChildAt(0));

		//relacion final entre celda invisible y linea de vida
		DefaultEdge end = new DefaultEdge();
		end.setSource(aux2.getChildAt(0));
		end.setTarget(((DrawableInstance)painted.get(target)).getNextElement().getChildAt(0));
		setEdgeData(end);
		int arrow = GraphConstants.ARROW_SIMPLE;
		GraphConstants.setLineEnd(end.getAttributes(), arrow);

		total.add(temp);
		total.add(aux1);
		total.add(temp2);
		total.add(end);
		GraphConstants.setSelectable(total.getAttributes(), false);
		drawed=total;//el objeto que inserta en el graph
		SequenceLevelManager.getInstance().addLevel();
	}

	public void setEdgeData(DefaultEdge edge) {
		GraphConstants.setSelectable(edge.getAttributes(), false);
		GraphConstants.setLabelAlongEdge(edge.getAttributes(),true);
		int arrow = GraphConstants.ARROW_NONE;
		GraphConstants.setLineEnd(edge.getAttributes(), arrow);
		GraphConstants.setEndFill(edge.getAttributes(), true);
		GraphConstants.setLineWidth(edge.getAttributes(),1);
	}

}
