package view.drawable.sequenceDiagram;

import org.jgraph.graph.DefaultEdge;
import org.jgraph.graph.GraphConstants;
import model.data.generalStructure.Component;


public class DrawableReturn extends DrawableInvocation{

	public DrawableReturn(Component component) {
		super(component);
	}

@Override
protected void moreSpecificDrawMessage(DefaultEdge edge) {
	float[] dash= new float[2];
	dash[0] = 5;
	dash[1] = 5;
	GraphConstants.setDashPattern(edge.getAttributes(),dash);
}

@Override
public String visibleName() {
	return "";
}
	

}
