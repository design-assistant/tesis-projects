package view.drawable.sequenceDiagram;

import java.awt.geom.Rectangle2D;
import java.util.Hashtable;
import java.util.List;

import javax.swing.BorderFactory;

import org.jgraph.graph.DefaultEdge;
import org.jgraph.graph.DefaultGraphCell;
import org.jgraph.graph.GraphConstants;

import view.Configuration;

import model.data.generalStructure.Component;
import model.data.generalStructure.sequenceDiagram.Message;

public class DrawableDestroy extends DrawableInvocation {

	public DrawableDestroy(Component component) {
		super(component);
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void SpecificDraw(DefaultGraphCell sourceCell,DefaultGraphCell targetCell, Hashtable<String, Object> painted) {
		
			String source = ((Message) this.getDiagramComponent()).getSource().getPath()+ ":"+ ((Message) this.getDiagramComponent()).getSource().getCompName();
			String target = ((Message) this.getDiagramComponent()).getTarget().getPath()+ ":"+ ((Message) this.getDiagramComponent()).getTarget().getCompName();
			DefaultEdge edge = new DefaultEdge(this.visibleName());
			int count = ((DrawableInstance)painted.get(source)).getCurrentLevel() - 1;
			DefaultGraphCell aux2 = (DefaultGraphCell)(((DrawableInstance)painted.get(target)).getDrawed());
			for (int i = count;i < Configuration.maxMessages;i++)
				aux2.remove(count);
			List list =aux2.getChildren();
			DefaultGraphCell inline= (DefaultGraphCell)list.get(((DrawableInstance)painted.get(source)).getCurrentLevel());
			Rectangle2D rec = GraphConstants.getBounds(inline.getAttributes());
			GraphConstants.setBounds(((DefaultGraphCell)aux2.getChildAt(count+1)).getAttributes(),new Rectangle2D.Double(rec.getX()+rec.getWidth()/2-2,((DrawableInstance)painted.get(target)).getHeight(((((DrawableInstance)painted.get(source)).getCurrentLevel())))-6,0,0));
			GraphConstants.setBorder(((DefaultGraphCell)aux2.getChildAt(count+1)).getAttributes(),BorderFactory.createEmptyBorder());
			((DefaultGraphCell)aux2.getChildAt(count+1)).setUserObject("X");
			GraphConstants.setSelectable(((DefaultGraphCell)aux2.getChildAt(count+1)).getAttributes(),false);
			edge.setSource(((DrawableInstance)painted.get(source)).getNextElement().getChildAt(0));
			edge.setTarget(((DefaultGraphCell)aux2.getChildAt(count+1)).getChildAt(0));
			GraphConstants.setLabelAlongEdge(edge.getAttributes(), true);
			GraphConstants.setSelectable(edge.getAttributes(), false);
			int arrow = GraphConstants.ARROW_SIMPLE;
			GraphConstants.setLineEnd(edge.getAttributes(), arrow);
			GraphConstants.setEndFill(edge.getAttributes(), true);
			GraphConstants.setLineWidth(edge.getAttributes(), 1);
			moreSpecificDrawMessage(edge);
			DefaultGraphCell print = new DefaultGraphCell();
			print.add(edge);
			print.add(aux2);
			GraphConstants.setSelectable(print.getAttributes(), false);
			GraphConstants.setSelectable(print.getAttributes(), false);
			GraphConstants.setResize(print.getAttributes(), false);
			GraphConstants.setGroupOpaque(print.getAttributes(), true);
			SequenceLevelManager.getInstance().addLevel();
			drawed = print;
	}

}
