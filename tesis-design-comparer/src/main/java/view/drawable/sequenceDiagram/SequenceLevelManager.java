package view.drawable.sequenceDiagram;

public class SequenceLevelManager {
	private static SequenceLevelManager instance;
	private int currentLevel;

	private SequenceLevelManager() {
		currentLevel = 2;
	}

	public static SequenceLevelManager getInstance() {
		if (instance == null) {
			instance = new SequenceLevelManager();
		}
		return instance;
	}

	public int nextLevel() {
		return currentLevel;
	}

	public void addLevel() {
		currentLevel++;
	}

	public int currentLevel() {
		return currentLevel;
	}
}
