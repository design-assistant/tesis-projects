package view.drawable.sequenceDiagram;

import java.awt.Point;
import java.util.Enumeration;
import java.util.Hashtable;

import model.data.generalStructure.Component;
import model.data.generalStructure.sequenceDiagram.Message;

public abstract class DrawableMessage extends DrawableDinamicComponent {

	public DrawableMessage(Component component) {
		super(component);
	}

	@Override
	public boolean draw(Hashtable<String, Object> painted,Enumeration<Point> enumerator) {
		return false;
	}

	@Override
	public void allocate(ScreenDinamicDiagram screenDinamicDiagram) {
		screenDinamicDiagram.addMessage(this);
	}


	public boolean compareTo(DrawableMessage message) {
		int myOrder = ((Message)this.getDiagramComponent()).getOrder();
		int messageOrder = ((Message)message.getDiagramComponent()).getOrder();
		if (myOrder <= messageOrder)
			return true;
		return false;

	}

	public String visibleName(){
		String[] name=this.getDiagramComponent().getCompName().split(":");
		return name[1];
	}

}
