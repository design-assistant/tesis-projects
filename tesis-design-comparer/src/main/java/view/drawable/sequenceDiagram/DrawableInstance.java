package view.drawable.sequenceDiagram;

import java.awt.Color;
import java.awt.Point;
import java.awt.geom.Rectangle2D;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import javax.swing.BorderFactory;


import org.jgraph.graph.DefaultEdge;
import org.jgraph.graph.DefaultGraphCell;
import org.jgraph.graph.DefaultPort;
import org.jgraph.graph.GraphConstants;
import view.Configuration;
import model.data.generalStructure.Component;
import model.data.generalStructure.sequenceDiagram.Instance;

public class DrawableInstance extends DrawableDinamicComponent {

	private DefaultGraphCell endDrawed;
	private int endX = 0;
	private int endY = 0;
	private Point p;
	private Integer x; 
	private Vector<Integer> heights = new Vector<Integer>();
	private int level = 0;

	public DrawableInstance(Component component) {
		super(component);
	}

	@Override
	public void allocate(ScreenDinamicDiagram screenDinamicDiagram) {
		screenDinamicDiagram.addInstance(this);
	}

	public void more() {
		int width = ((Instance) getDiagramComponent()).getLength()
		* Configuration.widthSpace;
		int height = Configuration.heightSpace;
		endY = endY + Configuration.moreLineIntance;
		GraphConstants.setBounds(this.endDrawed.getAttributes(),
				new Rectangle2D.Double(endX, endY, width, height));
	}

	public int getLevel() {
		return level;
	}
	
	public void setLevel(int level) {
		this.level = level;
	System.err.println("la instancia "+diagramComponent.getCompName()+" fue seteada en el nivel "+level);
	}
	
	public Point nextPoint() {
		p.y = p.y + 30;
		return p;
	}
	public Integer getX() {
		return x;
	}
	
	public int getHeight(int position){
		return heights.get(position);
	}
	@Override
	public boolean draw(Hashtable<String, Object> painted,
			Enumeration<Point> enumerator) {
		int width = ((Instance) getDiagramComponent()).getLength()
		* Configuration.widthSpace;
		int height = Configuration.heightSpace;
		p = enumerator.nextElement();
		int x = p.x;
		int y = p.y;
		p.x = p.x + width;
		p.y = p.y + height;
		this.x = x + width / 3 - 1 + 30;

		// celda de nombre de instancia.
		DefaultGraphCell total = new DefaultGraphCell();
		
		DefaultGraphCell cellClass = new DefaultGraphCell(":"
				+ getDiagramComponent().getCompName());
		GraphConstants.setBounds(cellClass.getAttributes(),
				new Rectangle2D.Double(x, y, width, height));
		GraphConstants.setSelectable(cellClass.getAttributes(), false);
		GraphConstants.setEditable(cellClass.getAttributes(), false);
		GraphConstants.setResize(cellClass.getAttributes(), false);
		GraphConstants.setGroupOpaque(cellClass.getAttributes(), true);
		GraphConstants.setBorder(cellClass.getAttributes(), BorderFactory
				.createLineBorder(Color.BLACK, 1));
		DefaultPort port = new DefaultPort();
		cellClass.add(port);
		total.add(cellClass);

		// celdas de foco
		for (int i = 0; i < Configuration.maxMessages; i++) {
			DefaultGraphCell foco = new DefaultGraphCell();
			y = y + height + 1;
			heights.add(new Integer(y));
			GraphConstants.setBounds(foco.getAttributes(),
					new Rectangle2D.Double(x + width / 3 - 1, y, width / 3,
							height+20));
			graphSettings(foco);
			GraphConstants.setBorder(foco.getAttributes(), BorderFactory
					.createMatteBorder(0, 0, 0, 0, Color.BLACK));
			foco.addPort();
			total.add(foco);

		}

		// celda invisible al final de la linea de vida
		DefaultGraphCell fin = new DefaultGraphCell();
		GraphConstants.setBounds(fin.getAttributes(), new Rectangle2D.Double(x
				+ width / 2, y, 0, 0));
		graphSettings(fin);
		GraphConstants.setBorder(fin.getAttributes(), BorderFactory
				.createLineBorder(Color.BLACK, 1));
		fin.addPort();

		// linea de vida
		DefaultEdge bridge = new DefaultEdge();
		bridge.setSource(cellClass.getChildAt(0));
		bridge.setTarget(fin.getChildAt(0));
		int arrow2 = GraphConstants.ARROW_NONE;
		GraphConstants.setLineEnd(bridge.getAttributes(), arrow2);
		GraphConstants.setSelectable(bridge.getAttributes(), false);
		float[] punteada = new float[2];
		punteada[0] = 10;
		punteada[1] = 10;
		GraphConstants.setDashPattern(bridge.getAttributes(), punteada);
		total.add(fin);
		total.add(bridge);

		// consideraciones generales
		painted.put((getDiagramComponent()).getPath() + ":"
				+ (getDiagramComponent()).getCompName(), this);
		GraphConstants.setGroupOpaque(total.getAttributes(), true);
		GraphConstants.setSizeable(total.getAttributes(), false);
		GraphConstants.setMoveable(total.getAttributes(), false);
		
		this.drawed = total;
		return true;
	}

	private void graphSettings(DefaultGraphCell fin) {
		GraphConstants.setEditable(fin.getAttributes(), false);
		GraphConstants.setResize(fin.getAttributes(), false);
		GraphConstants.setAutoSize(fin.getAttributes(), true);

	}

	public DefaultGraphCell getNextElement()
	{
		return (DefaultGraphCell)((DefaultGraphCell)this.drawed).getChildAt(SequenceLevelManager.getInstance().nextLevel()-level);
	}

	public int getCurrentLevel() {
		return SequenceLevelManager.getInstance().currentLevel();
	}


}
