package view.drawable.sequenceDiagram;



import java.awt.Point;
import java.util.Enumeration;
import java.util.Vector;

import model.data.generalStructure.sequenceDiagram.Message;
import view.ScreenDiagram;
import view.drawable.DrawableComponent;
import view.drawable.enumerators.SequenceEnumeration;



public class ScreenDinamicDiagram extends ScreenDiagram {

	private Vector<DrawableInstance> instances = new Vector<DrawableInstance>();
	private Vector<DrawableMessage> messages = new Vector<DrawableMessage>();

	public ScreenDinamicDiagram() {
		super();
	}

	@Override
	public void addComponent(DrawableComponent drawable) {
		super.addComponent(drawable);
		((DrawableDinamicComponent)drawable).allocate(this);
	}

	public void addInstance(DrawableInstance instance){
		instances.add(instance);
	}

	public void addMessage(DrawableMessage message){
		boolean cut = false;
		if (messages.size() == 0)
			messages.add(message);
		else
		{
			for (int i = 0;i < messages.size() && !cut;i++)
			{
				if (message.compareTo(messages.get(i)))
				{
					messages.add(i, message);
					cut = true;
				}
			}
			if (!cut)
				messages.add(message);
		}
	}

	public void  testMore()
	{
		instances.get(0).more();
	}

	public void paint() {
		Enumeration<Point> enumeration = new SequenceEnumeration<Point>();
		reallocateIntances();
		for (int i=0; i< instances.size();i++){
			if(instances.get(i).draw(painted,enumeration))
				if(instances.get(i).getDrawed()!=null)
				{
					graph.getGraphLayoutCache().insert(instances.get(i).getDrawed());
					graph.setDisconnectOnMove(false);
					graph.setDisconnectable(false);
					graph.setSelectionEnabled(false);
				}
		}
		for (int i=0; i< messages.size();i++){
			if(messages.get(i).draw(painted,enumeration))
				if(messages.get(i).getDrawed()!=null)
				{
					graph.getGraphLayoutCache().insert(messages.get(i).getDrawed());
					graph.setDisconnectOnMove(false);
					graph.setDisconnectable(false);
				}
		}
	}

	private void reallocateIntances() {
		Vector<DrawableInstance> ordered=new Vector<DrawableInstance>();
		for(DrawableMessage message : messages){
			if(!containsInstance(((Message)message.getDiagramComponent()).getSource().getCompName(),ordered))
			{
				ordered.add(getInstance(((Message)message.getDiagramComponent()).getSource().getCompName()));
				ordered.add(getInstance(((Message)message.getDiagramComponent()).getTarget().getCompName()));
			}
			else
				if(!containsInstance(((Message)message.getDiagramComponent()).getTarget().getCompName(),ordered))
				{
					ordered.add(getInstance(((Message)message.getDiagramComponent()).getTarget().getCompName()));
				}
		}
		instances=ordered;
	}

	private boolean containsInstance(String instanceName, Vector<DrawableInstance> reallocated){
		for(DrawableInstance instance :reallocated)
		{
			if(instance.getDiagramComponent().getCompName().equals(instanceName))
				return true;
		}
		return false;
	}

	public DrawableInstance getInstance(String instanceName){
		for (DrawableInstance instance : instances){
			if(instance.getDiagramComponent().getCompName().equals(instanceName))
				return instance;
		}
		return null;
	}

}
