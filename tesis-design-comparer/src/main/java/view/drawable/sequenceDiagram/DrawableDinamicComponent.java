package view.drawable.sequenceDiagram;

import model.data.generalStructure.Component;
import view.ScreenDiagram;
import view.drawable.DrawableComponent;

public abstract class DrawableDinamicComponent extends DrawableComponent{

	public DrawableDinamicComponent(Component component) {
		super(component);
	}
	
	@Override
	public ScreenDiagram getDiagram() {
		return new ScreenDinamicDiagram();
	}

	public abstract void allocate(ScreenDinamicDiagram screenDinamicDiagram); 

}
