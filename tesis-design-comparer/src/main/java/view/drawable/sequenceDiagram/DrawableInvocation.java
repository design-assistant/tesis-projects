package view.drawable.sequenceDiagram;

import java.awt.Point;
import java.util.Enumeration;
import java.util.Hashtable;

import org.jgraph.graph.DefaultEdge;
import org.jgraph.graph.DefaultGraphCell;
import org.jgraph.graph.DefaultPort;
import org.jgraph.graph.GraphConstants;
import view.drawable.DrawableComponent;
import model.data.generalStructure.Component;
import model.data.generalStructure.sequenceDiagram.Message;

public class DrawableInvocation extends DrawableMessage {

	public DrawableInvocation(Component component) {
		super(component);
	}

	@Override
	public boolean draw(Hashtable<String, Object> painted,
			Enumeration<Point> enumerator) {
		String source = ((Message) this.getDiagramComponent()).getSource()
		.getPath()
		+ ":"
		+ ((Message) this.getDiagramComponent()).getSource()
		.getCompName();
		String target = ((Message) this.getDiagramComponent()).getTarget()
		.getPath()
		+ ":"
		+ ((Message) this.getDiagramComponent()).getTarget()
		.getCompName();
		if ((painted.containsKey(source)) && (painted.containsKey(target))) {
			DefaultGraphCell sourceCell = (DefaultGraphCell)((DrawableComponent)painted.get(source)).getDrawed();;
			DefaultGraphCell targetCell = (DefaultGraphCell)((DrawableComponent)painted.get(target)).getDrawed();

			DefaultPort sourcePort = new DefaultPort();
			DefaultPort targetPort = new DefaultPort();
			sourceCell.add(sourcePort);
			targetCell.add(targetPort);
			SpecificDraw(sourceCell, targetCell,painted);
			return true;
		} else
			return false;
	}

	protected void SpecificDraw(DefaultGraphCell sourceCell,
			DefaultGraphCell targetCell, Hashtable<String, Object> painted) {
			String source = ((Message) this.getDiagramComponent()).getSource().getPath()+ ":"+ ((Message) this.getDiagramComponent()).getSource().getCompName();
			String target = ((Message) this.getDiagramComponent()).getTarget().getPath()+ ":"+ ((Message) this.getDiagramComponent()).getTarget().getCompName();
			DefaultEdge edge = new DefaultEdge(this.visibleName());
			edge.setSource(((DrawableInstance)painted.get(source)).getNextElement().getChildAt(0));
			edge.setTarget(((DrawableInstance)painted.get(target)).getNextElement().getChildAt(0));
			SequenceLevelManager.getInstance().addLevel();
			GraphConstants.setLabelAlongEdge(edge.getAttributes(), true);
			GraphConstants.setSelectable(edge.getAttributes(), false);
			int arrow = GraphConstants.ARROW_SIMPLE;
			GraphConstants.setLineEnd(edge.getAttributes(), arrow);
			GraphConstants.setEndFill(edge.getAttributes(), true);
			GraphConstants.setLineWidth(edge.getAttributes(), 1);
			moreSpecificDrawMessage(edge);
			drawed = edge;
	}

	protected void moreSpecificDrawMessage(DefaultEdge edge) {
	}
	
	
}
