package view.drawable;

import java.awt.Point;
import java.util.Hashtable;

public class PointComponentManager {

	private Hashtable<String, Point> points;
public static PointComponentManager instance;	

	private PointComponentManager() {
points= new Hashtable<String, Point>();
	}

	public void addPoint(String componentName, Point point) {
		points.put(componentName, point);
	}

	public Point getPoint(String componentName) {
		return points.get(componentName);
	}
	
	public static PointComponentManager getInstance(){
		if(instance==null)
			instance= new PointComponentManager();
		return instance;
	}

	public void clearPoints() {
		points = new Hashtable<String, Point>();
	}

}
