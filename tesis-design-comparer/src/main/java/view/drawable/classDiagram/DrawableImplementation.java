package view.drawable.classDiagram;

import org.jgraph.graph.DefaultEdge;
import org.jgraph.graph.DefaultGraphCell;
import org.jgraph.graph.DefaultPort;
import org.jgraph.graph.GraphConstants;

import model.data.generalStructure.Component;

public class DrawableImplementation extends DrawableRelation {

	public DrawableImplementation(Component component) {
		super(component);
	}

	@Override
	protected void SpecificDraw(DefaultGraphCell sourceCell,
			DefaultGraphCell targetCell) {
		DefaultEdge edge = new DefaultEdge();
		DefaultPort targetPort = (DefaultPort) targetCell.getChildAt(targetCell
				.getChildCount() - 1);
		DefaultPort sourcePort = (DefaultPort) sourceCell.getChildAt(sourceCell
				.getChildCount() - 1);
		changeEdgeStyle(sourcePort, targetPort, edge);
		edge.setSource(targetCell.getChildAt(targetCell.getChildCount() - 1));
		edge.setTarget(sourceCell.getChildAt(sourceCell.getChildCount() - 1));
		int arrow = GraphConstants.ARROW_TECHNICAL;
		GraphConstants.setLineEnd(edge.getAttributes(), arrow);
		GraphConstants.setEndFill(edge.getAttributes(), false);
		GraphConstants.setOpaque(edge.getAttributes(), true);
		float[] dash = new float[2];
		dash[0] = 5;
		dash[1] = 5;
		GraphConstants.setDashPattern(edge.getAttributes(), dash);
		drawed = edge;
	}

}
