package view.drawable.classDiagram;

import java.awt.Color;
import java.awt.Point;
import java.awt.geom.Rectangle2D;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.swing.BorderFactory;

import org.jgraph.graph.DefaultGraphCell;
import org.jgraph.graph.GraphConstants;

import model.data.generalStructure.Component;
import model.data.generalStructure.classDiagram.BoxComponent;
import view.Configuration;

public class DrawablePackage extends DrawableStaticComponent {

	public DrawablePackage(Component component) {
		super(component);
		Configuration.relationalComponents++;
	}

	@Override
	public boolean draw(Hashtable<String,Object> painted, Enumeration<Point> enumerator) {
		Point point = enumerator.nextElement();
		super.storeDrawablePoints(this.diagramComponent.getCompName(), point);
		nextX =  point.getX();
		nextY =  point.getY();
		width =((BoxComponent)getDiagramComponent()).getLength()*Configuration.widthSpace;
		height=Configuration.heightSpace;
		x=0;
		y=0;
		DefaultGraphCell ret =new DefaultGraphCell();
		DefaultGraphCell cellPackageName= new DefaultGraphCell(((BoxComponent)getDiagramComponent()).getCompName());
		y = editPackageComponent(width, height, x, y, cellPackageName);
		GraphConstants.setSelectable(cellPackageName.getAttributes(), false);
		GraphConstants.setResize(cellPackageName.getAttributes(), false);
		GraphConstants.setGroupOpaque(cellPackageName.getAttributes(), true);
		GraphConstants.setBorder(cellPackageName.getAttributes(), BorderFactory.createLineBorder(Color.BLACK, 1));
		ret.add(cellPackageName);
		
		
		DefaultGraphCell cellPackageBody = new DefaultGraphCell();
		y = editPackageComponent(width+Configuration.packageBodyWidth,height+Configuration.packageBodyHeight, x, y, cellPackageBody);
		GraphConstants.setSelectable(cellPackageBody.getAttributes(), false);
		GraphConstants.setResize(cellPackageBody.getAttributes(), false);
		GraphConstants.setGroupOpaque(cellPackageBody.getAttributes(), true);
		GraphConstants.setBorder(cellPackageBody.getAttributes(), BorderFactory.createLineBorder(Color.BLACK, 1));
		ret.add(cellPackageBody);
		GraphConstants.setGroupOpaque(ret.getAttributes(), true);//habilita las configuraciones de borde, color, etc...
		GraphConstants.setSizeable(ret.getAttributes(),false);
		GraphConstants.setSelectable(ret.getAttributes(), true);
		painted.put(((BoxComponent)getDiagramComponent()).getPath()+":"+((BoxComponent)getDiagramComponent()).getCompName(), this);
		this.drawed=ret;
		return true;

	}
	protected int editPackageComponent(int ancho, int largo, int x, int y, DefaultGraphCell cellClass) {
		GraphConstants.setBounds(cellClass.getAttributes(),new Rectangle2D.Double(nextX,nextY,ancho,largo));
		GraphConstants.setSelectable(cellClass.getAttributes(),false);
		nextY = nextY + largo;
		return (int) nextY;
	}

}
