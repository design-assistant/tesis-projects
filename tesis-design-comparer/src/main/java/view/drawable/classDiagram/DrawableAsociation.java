package view.drawable.classDiagram;

import org.jgraph.graph.DefaultEdge;
import org.jgraph.graph.DefaultGraphCell;
import org.jgraph.graph.DefaultPort;
import org.jgraph.graph.GraphConstants;

import model.data.generalStructure.Component;

public class DrawableAsociation extends DrawableRelation {

	public DrawableAsociation(Component component) {
		super(component);
	}

	@Override
	protected void SpecificDraw(DefaultGraphCell sourceCell,
			DefaultGraphCell targetCell) {
		DefaultEdge edge = new DefaultEdge();
		DefaultPort sourcePort = (DefaultPort) targetCell.getChildAt(targetCell
				.getChildCount() - 1);
		DefaultPort targetPort = (DefaultPort) sourceCell.getChildAt(sourceCell
				.getChildCount() - 1);
		changeEdgeStyle(sourcePort, targetPort, edge);
		edge.setSource(sourceCell.getChildAt(sourceCell.getChildCount() - 1));
		edge.setTarget(targetCell.getChildAt(targetCell.getChildCount() - 1));
		int arrow = GraphConstants.ARROW_CLASSIC;
		GraphConstants.setLineEnd(edge.getAttributes(), arrow);
		GraphConstants.setEndFill(edge.getAttributes(), true);
		GraphConstants.setOpaque(edge.getAttributes(), true);
		drawed = edge;
	}

}
