package view.drawable.classDiagram;

import model.data.generalStructure.Component;

import org.jgraph.graph.DefaultEdge;
import org.jgraph.graph.DefaultGraphCell;
import org.jgraph.graph.DefaultPort;
import org.jgraph.graph.GraphConstants;

public class DrawableInheritance extends DrawableRelation {

	public DrawableInheritance(Component component) {
		super(component);

	}

	@Override
	protected void SpecificDraw(DefaultGraphCell sourceCell,
			DefaultGraphCell targetCell) {
		DefaultEdge edge = new DefaultEdge();
		DefaultPort targetPort = (DefaultPort) targetCell.getChildAt(targetCell
				.getChildCount() - 1);
		DefaultPort sourcePort = (DefaultPort) sourceCell.getChildAt(sourceCell
				.getChildCount() - 1);
		changeEdgeStyle(sourcePort, targetPort,edge);
		edge.setSource(targetPort);
		edge.setTarget(sourcePort);
		int arrow = GraphConstants.ARROW_TECHNICAL;
		GraphConstants.setLineEnd(edge.getAttributes(), arrow);
		GraphConstants.setEndFill(edge.getAttributes(), false);
		GraphConstants.setOpaque(edge.getAttributes(), true);
		drawed = edge;

	}

	

}
