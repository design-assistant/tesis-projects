package view.drawable.classDiagram;


import org.jgraph.graph.DefaultEdge;
import org.jgraph.graph.DefaultGraphCell;
import org.jgraph.graph.DefaultPort;
import org.jgraph.graph.GraphConstants;

import model.data.generalStructure.Component;

public class DrawableComposition extends DrawableRelation {

	public DrawableComposition(Component component) {
		super(component);
	}

	@Override
	protected void SpecificDraw(DefaultGraphCell sourceCell,
			DefaultGraphCell targetCell) {
		DefaultEdge edge = new DefaultEdge();
		DefaultPort sourcePort = (DefaultPort) targetCell.getChildAt(targetCell
				.getChildCount() - 1);
		DefaultPort targetPort = (DefaultPort) sourceCell.getChildAt(sourceCell
				.getChildCount() - 1);
		changeEdgeStyle(sourcePort, targetPort,edge);
		edge.setSource(targetCell.getChildAt(targetCell.getChildCount()-1));
		edge.setTarget(sourceCell.getChildAt(sourceCell.getChildCount()-1));
		int arrow = GraphConstants.ARROW_DIAMOND;
		GraphConstants.setLineEnd(edge.getAttributes(), arrow);
		GraphConstants.setEndFill(edge.getAttributes(), true);
		GraphConstants.setOpaque(edge.getAttributes(), true);
		drawed=edge;
		
	}





}
