package view.drawable.classDiagram;

import java.awt.Color;
import java.awt.Font;
import java.util.List;

import javax.swing.BorderFactory;

import model.data.generalStructure.Component;
import model.data.generalStructure.classDiagram.Attribute;
import model.data.generalStructure.classDiagram.BoxComponent;
import model.data.generalStructure.classDiagram.Class;
import model.data.generalStructure.classDiagram.Method;

import org.jgraph.graph.DefaultGraphCell;
import org.jgraph.graph.GraphConstants;

public class DrawableClass extends DrawableInterface {

	private List<Attribute> attributes = ((model.data.generalStructure.classDiagram.BoxComponent) this.diagramComponent)
			.getAttributes();

	public DrawableClass(Component component) {
		super(component);
	}

	protected void addAttributes(DefaultGraphCell ret) {
		// la celda que contiene las celdas de los atributos
		DefaultGraphCell cellAtributos = new DefaultGraphCell();
		GraphConstants.setSelectable(cellAtributos.getAttributes(), false);
		GraphConstants.setResize(cellAtributos.getAttributes(), false);
		GraphConstants.setGroupOpaque(cellAtributos.getAttributes(), true);
		GraphConstants.setBorder(cellAtributos.getAttributes(), BorderFactory
				.createLineBorder(Color.BLACK, 1));
		// por cada atributo una nueva celda...
		if (attributes.size() == 0) {
			DefaultGraphCell dgc = new DefaultGraphCell("");
			y = editClassComponent(width, height, x, y, dgc);
			cellAtributos.add(dgc);
		} else {
			for (int i = 0; i < attributes.size(); i++) {
				DefaultGraphCell dgc = new DefaultGraphCell(attributes.get(i)
						.getLength());
				y = editClassComponent(width, height, x, y, dgc);
				cellAtributos.add(dgc);
			}
		}
		ret.add(cellAtributos);
	}

	@Override
	public DefaultGraphCell specificName() {
		if (!(((BoxComponent)this.diagramComponent).isClassProxy()) && ((Class) this.diagramComponent).isAbst()) {
			DefaultGraphCell cell = new DefaultGraphCell(
					((BoxComponent) getDiagramComponent()).getCompName());
			GraphConstants.setFont(cell.getAttributes(), new Font(
					Font.DIALOG, Font.ITALIC, 12));
			return cell;
		}
		return super.specificName();

	}
	
	
	@Override
	protected void specificMethods(DefaultGraphCell dgc, Method m) {
		if (m.isAbstract())
			GraphConstants.setFont(dgc.getAttributes(), new Font(
					Font.DIALOG, Font.ITALIC, 12));
	}

}
