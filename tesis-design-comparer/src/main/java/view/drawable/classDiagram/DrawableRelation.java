package view.drawable.classDiagram;

import java.awt.Point;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;

import model.data.generalStructure.Component;
import model.data.generalStructure.classDiagram.Relation;

import org.jgraph.graph.DefaultEdge;
import org.jgraph.graph.DefaultGraphCell;
import org.jgraph.graph.DefaultPort;
import org.jgraph.graph.GraphConstants;

import view.drawable.DrawableComponent;


public abstract class DrawableRelation extends DrawableStaticComponent {

	public DrawableRelation(Component component) {
		super(component);
	}

	
	@Override
	public boolean draw(Hashtable<String,Object> painted, Enumeration<Point> enumerator) {
		Point point = new Point(0,0);
		super.storeDrawablePoints(this.getDiagramComponent().getCompName(),point);
		String source=((Relation)this.getDiagramComponent()).getSource().getPath()+":"+((Relation)this.getDiagramComponent()).getSource().getCompName();
		String target=((Relation)this.getDiagramComponent()).getTarget().getPath()+":"+((Relation)this.getDiagramComponent()).getTarget().getCompName();
		if((painted.containsKey(source))&&(painted.containsKey(target)))
		{
			DefaultGraphCell sourceCell=(DefaultGraphCell)((DrawableComponent)painted.get(source)).getDrawed();
			DefaultGraphCell targetCell=(DefaultGraphCell)((DrawableComponent)painted.get(target)).getDrawed();
			SpecificDraw(sourceCell, targetCell);
			return true;
		}
		else
			return false;

	}

	protected abstract void SpecificDraw(DefaultGraphCell sourceCell,DefaultGraphCell targetCell);

	@SuppressWarnings("unchecked")
	protected void changeEdgeStyle(DefaultPort sourcePort, DefaultPort targetPort,DefaultEdge e) {
		GraphConstants.setSelectable(e.getAttributes(), false);
		if (!sourcePort.getEdges().isEmpty()) {
			for (Iterator it = sourcePort.getEdges().iterator(); it.hasNext();) {
				DefaultEdge edge = (DefaultEdge) it.next();
				if (edge.getSource().equals(targetPort)
						|| edge.getTarget().equals(targetPort)) {
					GraphConstants.setRouting(e.getAttributes(), GraphConstants.ROUTING_SIMPLE);
					return;
				}
			}
		}
		GraphConstants.setRouting(e.getAttributes(), GraphConstants.ROUTING_DEFAULT);
	}
}
