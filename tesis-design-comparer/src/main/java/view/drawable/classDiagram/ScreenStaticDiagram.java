package view.drawable.classDiagram;

import java.awt.Point;
import java.util.Enumeration;

import view.ScreenDiagram;
import view.drawable.DrawableComponent;
import view.drawable.PointComponentManager;

import view.drawable.enumerators.CoordinateEnumeration;




public class ScreenStaticDiagram extends ScreenDiagram {

	public ScreenStaticDiagram() {
		super();
	}


	public void paint() {
		
		Enumeration<Point> enumeration = new  CoordinateEnumeration<Point>();
		PointComponentManager.getInstance().clearPoints();
		for (int i=0;i<components.size();i++)
		{
			if(components.get(i).draw(painted,enumeration))
			{
				if(components.get(i).getDrawed()!=null)
				{
					graph.getGraphLayoutCache().insert(components.get(i).getDrawed());
					graph.setDisconnectOnMove(false);
					graph.setDisconnectable(false);
				}
			}
			else
			{
				DrawableComponent component=components.get(i);
				components.remove(i);
				components.add(component);
				i--;
			}
		}
	}
	
}
