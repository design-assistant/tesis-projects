package view.drawable.classDiagram;

import java.awt.Color;
import java.awt.Point;
import java.awt.geom.Rectangle2D;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.tree.MutableTreeNode;

import model.data.generalStructure.Component;
import model.data.generalStructure.classDiagram.BoxComponent;
import model.data.generalStructure.classDiagram.Method;

import org.jgraph.graph.DefaultGraphCell;
import org.jgraph.graph.DefaultPort;
import org.jgraph.graph.GraphConstants;

import view.Configuration;

public class DrawableInterface extends DrawableStaticComponent {


	public DrawableInterface(Component component) {
		super(component);
		Configuration.relationalComponents++;
	}

	@Override
	public boolean draw(Hashtable<String,Object> painted, Enumeration<Point> enumerator) {
		Point point = enumerator.nextElement();
		super.storeDrawablePoints(this.diagramComponent.getCompName(), point);
		nextX =  point.getX();
		nextY =  point.getY();
		List<Method> methods=((BoxComponent)getDiagramComponent()).getMethods();

		//la clase tiene el tama�o maximo(en caracteres) de sus atributos y metodos...
		width =((BoxComponent)getDiagramComponent()).getLength()*Configuration.widthSpace;
		height=Configuration.heightSpace;
		x=0;
		y=0;
		DefaultGraphCell ret =new DefaultGraphCell();

		//insertamos la celda con nombre de clase
		DefaultGraphCell cellClass= specificName();
		y = editClassComponent(width, height, x, y, cellClass);
		GraphConstants.setSelectable(cellClass.getAttributes(), false);
		GraphConstants.setResize(cellClass.getAttributes(), false);
		GraphConstants.setGroupOpaque(cellClass.getAttributes(), true);
		GraphConstants.setBorder(cellClass.getAttributes(), BorderFactory.createLineBorder(Color.BLACK, 1));
		ret.add(cellClass);
    	addAttributes(ret);
		DefaultGraphCell cellMetodos = getMethods(methods);
		ret.add(cellMetodos);
		GraphConstants.setGroupOpaque(ret.getAttributes(), true);//habilita las configuraciones de borde, color, etc...
		GraphConstants.setBorder(ret.getAttributes(), BorderFactory.createLineBorder(Color.BLACK,2));
		GraphConstants.setSizeable(ret.getAttributes(),false);
		GraphConstants.setSelectable(ret.getAttributes(), true);
		DefaultPort port= new DefaultPort();
		ret.add(port);
		port.setParent((MutableTreeNode) ret.getRoot());
		painted.put(((BoxComponent)getDiagramComponent()).getPath()+":"+((BoxComponent)getDiagramComponent()).getCompName(), this);
		this.drawed=ret;
		return true;
	}



	protected void addAttributes(DefaultGraphCell ret) {
	}

	private DefaultGraphCell getMethods(List<Method> methods) {
		//la celda con las celdas por cada metodo..
		DefaultGraphCell cellMetodos=new DefaultGraphCell();
		GraphConstants.setSelectable(cellMetodos.getAttributes(),false);
		GraphConstants.setResize(cellMetodos.getAttributes(), false);
		GraphConstants.setGroupOpaque(cellMetodos.getAttributes(), true);//habilita las configuraciones de borde, color, etc...
		GraphConstants.setBorder(cellMetodos.getAttributes(), BorderFactory.createLineBorder(Color.BLACK, 1));
		//por cada metodo una nueva celda...
		if (methods.size() == 0)
		{
			DefaultGraphCell dgc=new DefaultGraphCell("");
			y = editClassComponent(width, height, x, y, dgc);
			cellMetodos.add(dgc);
		}
		else
		{
			for (int i = 0; i < methods.size(); i++) {
				DefaultGraphCell dgc=new DefaultGraphCell(methods.get(i).getLength());
				specificMethods(dgc,methods.get(i));
				y = editClassComponent(width, height, x, y, dgc);
				cellMetodos.add(dgc);		
			}
		}
		return cellMetodos;
	}


	protected void specificMethods(DefaultGraphCell dgc, Method m) {
		}

	/**
	 * Da las configuraciones de tama�o y posicion a una celda (defaultGraphCell)
	 * @param ancho  ( ancho de la celda creada)
	 * @param largo   (largo de la celda creada)
	 * @param x       (posicion de x)
	 * @param y      (posicion de y)
	 * @param cellClass (celda a editar)
	 * @return (y incrementada para dibujar en el proximo renglon...)
	 */
	protected int editClassComponent(int ancho, int largo, int x, int y, DefaultGraphCell cellClass) {
		GraphConstants.setBounds(cellClass.getAttributes(),new Rectangle2D.Double(nextX,nextY,ancho,largo));
		GraphConstants.setSelectable(cellClass.getAttributes(),false);
		nextY = nextY + largo;
		return (int) nextY;
	}
	
	public DefaultGraphCell  specificName(){
		return new DefaultGraphCell(((BoxComponent)getDiagramComponent()).getCompName());
	}

}
