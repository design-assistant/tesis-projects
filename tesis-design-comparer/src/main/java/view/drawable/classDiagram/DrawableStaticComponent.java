package view.drawable.classDiagram;


import model.data.generalStructure.Component;

import view.ScreenDiagram;
import view.drawable.DrawableComponent;

public abstract class DrawableStaticComponent extends DrawableComponent{

	protected int width;
	protected int height;
	protected int x;
	protected int y;
	protected double nextY;
	protected double nextX;
	public DrawableStaticComponent(Component component) {
		super(component);
	}

	@Override
	public ScreenDiagram getDiagram() {
		return new ScreenStaticDiagram();
	}


}
