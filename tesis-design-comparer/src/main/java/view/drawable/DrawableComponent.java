package view.drawable;

import java.awt.Point;
import java.util.Enumeration;
import java.util.Hashtable;

import view.ScreenDiagram;


import model.data.generalStructure.Component;



public abstract class DrawableComponent {

	protected Component diagramComponent;
	protected Object drawed;
	
	public DrawableComponent(Component component) {
		this.diagramComponent=component;
	}

	public Component getDiagramComponent() {
		return diagramComponent;
	}
	
	public abstract boolean draw(Hashtable<String,Object> painted, Enumeration<Point> enumerator);
	
	public Object getDrawed() {
		return drawed;
	}

	public abstract ScreenDiagram getDiagram();
	
	
	protected void storeDrawablePoints(String componentName, Point point)
	{
		PointComponentManager.getInstance().addPoint(componentName, point);
	}
	
}
