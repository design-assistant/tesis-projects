package view.drawable.enumerators;

import java.util.Enumeration;
import java.util.List;
import java.util.Vector;

import view.drawable.DrawableComponent;


public class DesignEnumeration implements Enumeration<Vector<DrawableComponent>> {

	private Vector<Vector<DrawableComponent>> components=new Vector<Vector<DrawableComponent>>();
	private int i;
	public DesignEnumeration(List<DrawableComponent> statics,Vector<Vector<DrawableComponent>> dinamics) {
		i=0;
		Vector<DrawableComponent> comps= new Vector<DrawableComponent>();
		comps.addAll(statics);
		components.add(comps);
		for (int i=0;i<dinamics.size();i++)
		{
			components.add(dinamics.get(i));
		}
	}

	public boolean hasMoreElements() {
		return i<components.size();
	}

	public Vector<DrawableComponent> nextElement() {
		Vector<DrawableComponent> vector =components.get(i);
		i++;
		return vector;
	}

}
