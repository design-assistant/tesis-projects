package view.drawable.enumerators;

import java.awt.Point;
import java.util.Enumeration;

import view.Configuration;

public class SequenceEnumeration<P> implements Enumeration<Point> {

	private int y;
	private int x;

	public SequenceEnumeration() {
		y=10;
		x=-100;
	}
	@Override
	public boolean hasMoreElements() {
		return true;
	}

	@Override
	public Point nextElement() {
		x=x+Configuration.instanceSpace;
		return new Point(x,y);
	}

}
