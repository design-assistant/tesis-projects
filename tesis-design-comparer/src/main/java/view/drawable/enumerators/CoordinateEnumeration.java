package view.drawable.enumerators;

import java.awt.Point;
import java.util.Enumeration;

import view.Configuration;



public class CoordinateEnumeration<P> implements Enumeration<Point>{

	private double angle;
	private double x;
	private double y;
	private int components;
	private int cursor;
	private double radio;
	
	
	public CoordinateEnumeration() {
		super();
		components = Configuration.relationalComponents;
		double max = Math.max(Configuration.TabHeight,Configuration.TabWidth);
		max = max /2;
		x = max / 2; // pone en el (0,0)
		y = max / 2;
		angle = 360 / components;
		radio = max / 2;
	}

	public boolean hasMoreElements() {
		return cursor < components;
	}

	public Point nextElement() {
		Point p = new Point((int)(x+radio*Math.cos(Math.toRadians((cursor+1)*angle))),(int) (y+radio*Math.sin(Math.toRadians((1+cursor)*angle))));
		cursor ++;
		return p;
	}
}
