package view.drawable;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import model.data.generalStructure.Design;
import view.drawable.enumerators.DesignEnumeration;


public class DrawableDesign {
	private List<DrawableComponent> classComponents;
	private Vector<Vector<DrawableComponent>>  sequences;
	private Design design;

	public DrawableDesign(Design design) {
		classComponents=new ArrayList<DrawableComponent>();
		sequences=new Vector<Vector<DrawableComponent>>();
		this.design = design;
	}

	public DesignEnumeration getEnumeration()
	{
		return new DesignEnumeration(classComponents,sequences);
	}

	public void createDrawables() {
		classComponents =design.createStaticsDrawables();	
		sequences=design.createDinamicsDrawables();
	}


}
