package view;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

/**
* This code was edited or generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a corporation,
* company or business for any purpose whatever) then you
* should purchase a license for each developer using Jigloo.
* Please visit www.cloudgarden.com for details.
* Use of Jigloo implies acceptance of these licensing terms.
* A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
* THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
* LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
*/
public class About extends javax.swing.JDialog {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JLabel jLabel1;
	private JLabel jLabel3;
	private JLabel jLabel4;
	private JLabel jLabel5;
	private JLabel jLabel2;
	private JLabel jLabel7;
	private JLabel jLabel6;

	
	public About() {
		initGUI();
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}
	
	private void initGUI() {
		try {
			{
				getContentPane().setLayout(null);
				this.setTitle("Acerca de...");
				{
					jLabel1 = new JLabel();
					getContentPane().add(jLabel1);
					jLabel1.setText("Comparaci�n y transformaci�n de diagramas de dise�o");
					jLabel1.setBounds(7, 7, 506, 28);
					jLabel1.setFont(new java.awt.Font("Arial",1,18));
				}
				{
					jLabel3 = new JLabel();
					getContentPane().add(jLabel3);
					jLabel3.setText("Tesis de Grado en Ingenier�a de Sistemas.");
					jLabel3.setBounds(44, 41, 314, 28);
					jLabel3.setFont(new java.awt.Font("Arial Black",0,11));
				}
				{
					jLabel4 = new JLabel();
					getContentPane().add(jLabel4);
					jLabel4.setText("Marco Stella  - mdstella@gmail.com");
					jLabel4.setBounds(7, 111, 301, 14);
					jLabel4.setFont(new java.awt.Font("Arial Black",0,11));
				}
				{
					jLabel5 = new JLabel();
					getContentPane().add(jLabel5);
					jLabel5.setText("Lucas Tiritilli  - ltiritilli@gmail.com");
					jLabel5.setBounds(7, 126, 280, 17);
					jLabel5.setFont(new java.awt.Font("Arial Black",0,11));
				}
				{
					jLabel6 = new JLabel();
					getContentPane().add(jLabel6);
					jLabel6.setText("Unicen 2010");
					jLabel6.setBounds(7, 144, 91, 14);
					jLabel6.setFont(new java.awt.Font("Arial Black",0,11));
				}
				{
					jLabel7 = new JLabel();
					getContentPane().add(jLabel7);
					jLabel7.setBounds(354, 34, 147, 129);
					jLabel7.setIcon(new ImageIcon(getClass().getClassLoader().getResource("images/Unicen.gif")));
				}
				{
					jLabel2 = new JLabel();
					getContentPane().add(jLabel2);
					jLabel2.setText("�rea de Dise�o de Software");
					jLabel2.setFont(new java.awt.Font("Arial Black",0,11));
					jLabel2.setBounds(143, 69, 205, 28);
				}
			}
			this.setSize(540, 202);
			this.setModal(true);
			getContentPane().setBackground(new java.awt.Color(255,255,255));
			this.setResizable(false);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
