package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import presenter.AdminUserPresenter;

/**
 * This code was edited or generated using CloudGarden's Jigloo SWT/Swing GUI Builder, which is free for non-commercial use. If Jigloo is
 * being used commercially (ie, by a corporation, company or business for any purpose whatever) then you should purchase a license for each
 * developer using Jigloo. Please visit www.cloudgarden.com for details. Use of Jigloo implies acceptance of these licensing terms. A
 * COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED LEGALLY FOR ANY CORPORATE OR COMMERCIAL
 * PURPOSE.
 */
public class AddUserDialog extends javax.swing.JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextField jTextFieldUsuario;
	private JButton jButtonCancelar;
	private JPasswordField jPasswordFieldPass2;
	private JLabel jLabelRepetir;
	private JLabel jLabelRepContrasena;
	private JButton jButtonAceptar;
	private JPasswordField jPasswordFieldPass;
	private JLabel jLabelContrasena;
	private JLabel jLabelUsuario;
	private AdminUserPresenter presenter;

	public AddUserDialog(JFrame frame, AdminUserPresenter presenter) {
		super(frame);
		initGUI();
		this.setLocationRelativeTo(null);
		this.presenter = presenter;
	}

	private void initGUI() {
		try {
			{
				this.setModal(true);
				this.setLocation(new java.awt.Point(100, 100));
				getContentPane().setLayout(null);
				this.setTitle("Agregar usuario");
				this.setResizable(false);
				{
					jTextFieldUsuario = new JTextField();
					getContentPane().add(jTextFieldUsuario);
					jTextFieldUsuario.setBounds(91, 21, 126, 28);
					jTextFieldUsuario.addKeyListener(new KeyAdapter() {
						public void keyPressed(KeyEvent evt) {
							jTextFieldUsuarioKeyPressed(evt);
						}
					});
				}
				{
					jLabelUsuario = new JLabel();
					getContentPane().add(jLabelUsuario);
					jLabelUsuario.setText("Usuario");
					jLabelUsuario.setBounds(21, 21, 70, 28);
				}
				{
					jLabelContrasena = new JLabel();
					getContentPane().add(jLabelContrasena);
					jLabelContrasena.setText("Contraseņa");
					jLabelContrasena.setBounds(21, 56, 70, 28);
				}
				{
					jPasswordFieldPass = new JPasswordField();
					getContentPane().add(jPasswordFieldPass);
					jPasswordFieldPass.setBounds(91, 56, 126, 28);
					jPasswordFieldPass.addKeyListener(new KeyAdapter() {
						public void keyPressed(KeyEvent evt) {
							jPasswordFieldPassKeyPressed(evt);
						}
					});
				}
				{
					jButtonAceptar = new JButton();
					getContentPane().add(jButtonAceptar);
					jButtonAceptar.setIcon(new ImageIcon(getClass().getClassLoader()
							.getResource("images/iconos/ok.png")));
					jButtonAceptar.setToolTipText("Aceptar");
					jButtonAceptar.setBounds(74, 133, 42, 42);
					jButtonAceptar.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent evt) {
							jButtonAceptarActionPerformed(evt);
						}
					});
				}
				{
					jLabelRepContrasena = new JLabel();
					getContentPane().add(jLabelRepContrasena);
					jLabelRepContrasena.setText("Contraseņa");
					jLabelRepContrasena.setBounds(21, 101, 70, 28);
				}
				{
					jLabelRepetir = new JLabel();
					getContentPane().add(jLabelRepetir);
					jLabelRepetir.setText("Repetir");
					jLabelRepetir.setBounds(33, 85, 49, 28);
				}
				{
					jPasswordFieldPass2 = new JPasswordField();
					getContentPane().add(jPasswordFieldPass2);
					jPasswordFieldPass2.setBounds(91, 91, 126, 28);
					jPasswordFieldPass2.addKeyListener(new KeyAdapter() {
						public void keyPressed(KeyEvent evt) {
							jPasswordFieldPass2KeyPressed(evt);
						}
					});
				}
				{
					jButtonCancelar = new JButton();
					getContentPane().add(jButtonCancelar);
					jButtonCancelar.setIcon(new ImageIcon(getClass().getClassLoader()
							.getResource("images/iconos/cancelar.png")));
					jButtonCancelar.setToolTipText("Cancelar");
					jButtonCancelar.setBounds(133, 133, 42, 42);
					jButtonCancelar.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent evt) {
							jButtonCancelarActionPerformed(evt);
						}
					});
				}
			}
			this.setSize(246, 216);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void jButtonAceptarActionPerformed(ActionEvent evt) {
		addUser();
	}

	private void addUser() {
		@SuppressWarnings("unused")
		AcceptMessage message;
		if (presenter.userValidName(jTextFieldUsuario.getText().trim())){
			String pass = "";
			String pass2 = "";
			for (int i = 0; i < jPasswordFieldPass.getPassword().length; i++)
				pass = pass + jPasswordFieldPass.getPassword()[i];
			for (int i = 0; i < jPasswordFieldPass2.getPassword().length; i++)
				pass2 = pass2 + jPasswordFieldPass2.getPassword()[i];
			if ((!jTextFieldUsuario.getText().equals("")) && (!pass.equals("")) && (!pass2.equals(""))) {
				if (pass.equals(pass2)) {
					presenter.addUser(jTextFieldUsuario.getText().trim(), pass);
					this.dispose();
				} else
					message = new AcceptMessage("Verifique los datos ingresados",274);
			
			} else
				message = new AcceptMessage("Ingrese los datos de usuario",274);
		} else
			message = new AcceptMessage("El usuario ingresado ya existe",274);
	}

	private void jTextFieldUsuarioKeyPressed(KeyEvent evt) {
		if (evt.getKeyCode() == 10)
			addUser();
	}

	private void jPasswordFieldPassKeyPressed(KeyEvent evt) {
		if (evt.getKeyCode() == 10)
			addUser();
	}

	private void jPasswordFieldPass2KeyPressed(KeyEvent evt) {
		if (evt.getKeyCode() == 10)
			addUser();
	}

	private void jButtonCancelarActionPerformed(ActionEvent evt) {
		this.dispose();
	}

}
