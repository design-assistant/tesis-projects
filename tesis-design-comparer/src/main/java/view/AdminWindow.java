package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JTabbedPane;
import javax.swing.WindowConstants;

import presenter.AdminUserPresenter;
import context.ImplementationFactory;

/**
 * This code was edited or generated using CloudGarden's Jigloo SWT/Swing GUI
 * Builder, which is free for non-commercial use. If Jigloo is being used
 * commercially (ie, by a corporation, company or business for any purpose
 * whatever) then you should purchase a license for each developer using Jigloo.
 * Please visit www.cloudgarden.com for details. Use of Jigloo implies
 * acceptance of these licensing terms. A COMMERCIAL LICENSE HAS NOT BEEN
 * PURCHASED FOR THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED LEGALLY FOR
 * ANY CORPORATE OR COMMERCIAL PURPOSE.
 */
public class AdminWindow extends Window<AdminUserPresenter> {

	private static final long serialVersionUID = 1L;

	{
		try {
			javax.swing.UIManager
					.setLookAndFeel("com.jgoodies.looks.plastic.Plastic3DLookAndFeel");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private JButton btnOpenFile;
	private JButton jButtonAgregarUser;
	private JButton jButtonLogOut;
	private JButton btnEvaluar;
	private JButton btnSave;
	private boolean login = false;
	private Comparator father;

	public AdminWindow(Comparator comparator) {
		super();
		presenter = (AdminUserPresenter) ImplementationFactory
				.getBean("adminPresenter");
		this.father = comparator;
		Login inst = new Login(this,presenter);
		inst.setVisible(true);
		if (login)
		{
			initGUI();
			this.father.addTab(this);
		}
		else
		{
			this.father.removeAllTabs();
			this.father.enableMenu();
			this.presenter.clearContents();
		}
		
	}
	
	public void setLogin(boolean login) {
		this.login = login;
	}


	private void initGUI() {
		try {
			setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
			getContentPane().setLayout(null);
			this.setTitle("Pantalla de administrador");
			this.setResizable(false);
			{
				btnOpenFile = new JButton();
				getContentPane().add(btnOpenFile);
				btnOpenFile.setIcon(new ImageIcon(getClass().getClassLoader()
						.getResource("images/iconos/abrir.png")));
				btnOpenFile.setToolTipText("Abrir dise�o");
				btnOpenFile.setBounds(164, 7, 40, 40);
				btnOpenFile.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent evt) {
						openFile(evt);
					}
				});
			}
			{
				tabs = new JTabbedPane();
				getContentPane().add(tabs);
				tabs.setBounds(19, 49, 738, 504);
			}
			{
				btnSave = new JButton();
				getContentPane().add(btnSave);
				btnSave.setIcon(new ImageIcon(getClass().getClassLoader()
						.getResource("images/iconos/guardar.png")));
				btnSave.setToolTipText("Guardar dise�o");
				btnSave.setBounds(350, 7, 40, 40);
				btnSave.setEnabled(false);
				btnSave.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent evt) {
						saveFile(evt);
					}
				});
			}

			{
				btnEvaluar = new JButton();
				getContentPane().add(btnEvaluar);
				btnEvaluar.setIcon(new ImageIcon(getClass().getClassLoader()
						.getResource("images/iconos/evaluar.png")));
				btnEvaluar.setToolTipText("Evaluar dise�o");
				btnEvaluar.setBounds(257, 7, 40, 40);
				btnEvaluar.setEnabled(false);
				btnEvaluar.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent evt) {
						evaluate(evt);
					}
				});
			}
			{
				jButtonAgregarUser = new JButton();
				getContentPane().add(jButtonAgregarUser);
				jButtonAgregarUser.setIcon(new ImageIcon(getClass().getClassLoader()
						.getResource("images/iconos/agregarUsuario.png")));
				jButtonAgregarUser.setToolTipText("Agregar usuario");
				jButtonAgregarUser.setBounds(443, 7, 40, 40);
				jButtonAgregarUser.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent evt) {
						addUser(evt);
					}
				});
			}
			{
				jButtonLogOut = new JButton();
				getContentPane().add(jButtonLogOut);
				jButtonLogOut.setIcon(new ImageIcon(getClass().getClassLoader()
						.getResource("images/iconos/logout.png")));
				jButtonLogOut.setToolTipText("Cerrar sesi�n");
				jButtonLogOut.setBounds(536, 7, 40, 40);
				jButtonLogOut.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent evt) {
						jButtonLogOutActionPerformed(evt);
					}
				});
			}
			pack();
			this.setSize(818, 650);
			jButtonAgregarUser
					.setPreferredSize(new java.awt.Dimension(784, 574));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void openFile(ActionEvent evt) {
		boolean canClear = true;
		if (presenter.isActive()) {
			ConfirmMessage message = new ConfirmMessage("Si todav�a no ha salvado cambios, los mismos se perder�n. Desea continuar?",470);
			int answer = message.getAnswer();
			if (isNOOption(answer)) {
				canClear = false;
			} else {
				presenter.clearContents();
				btnSave.setEnabled(false);
				btnEvaluar.setEnabled(false);
				this.delTabs();
			}
		}
		if (canClear) {
			JFileChooser op = new JFileChooser();
			FFilter mtcFilter;
			mtcFilter = new FFilter("mtc", "Entrada Comparador");
			op.addChoosableFileFilter(mtcFilter);
			op.setAcceptAllFileFilterUsed(false);
			int option = op.showOpenDialog(this);
			String path = "";
			if (isOKOption(option)) {
				path = op.getSelectedFile().toString();
				presenter.loadDesign(path);
				painter = new Painter(presenter.getDesign());
				painter.draw(this, 0);
				btnEvaluar.setEnabled(true);
			}
		}

	}

	private void saveFile(ActionEvent evt) {
		try {
			presenter.save();
			btnEvaluar.setEnabled(false);
			btnSave.setEnabled(false);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void evaluate(ActionEvent evt) {
		JFrame frame = new JFrame();
		AdminQualityAttributeDialog inst = new AdminQualityAttributeDialog(
				frame, this,presenter);
		inst.setVisible(true);
	}

	private void jButtonLogOutActionPerformed(ActionEvent evt) {
		this.father.removeAllTabs();
		this.father.enableMenu();
		this.presenter.clearContents();
	}

	private void addUser(ActionEvent evt) {
		JFrame frame = new JFrame();
		AddUserDialog inst = new AddUserDialog(frame,presenter);
		inst.setVisible(true);
	}

	/**
	 * This method should return an instance of this class which does NOT
	 * initialize it's GUI elements. This method is ONLY required by Jigloo if
	 * the superclass of this class is abstract or non-public. It is not needed
	 * in any other situation.
	 */
	public static Object getGUIBuilderInstance() {
		return new AdminWindow(Boolean.FALSE);
	}

	/**
	 * This constructor is used by the getGUIBuilderInstance method to provide
	 * an instance of this class which has not had it's GUI elements initialized
	 * (ie, initGUI is not called in this constructor).
	 */
	public AdminWindow(Boolean initGUI) {
		super();
	}

	public void enableSave() {
		btnSave.setEnabled(true);
	}

	@Override
	public String getTabName(int size) {
		return "Diagrama a evaluar";
	}

	public void logOut() {
		jButtonLogOutActionPerformed(null);
	}
}
