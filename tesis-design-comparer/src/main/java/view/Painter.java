package view;

import java.util.Vector;

import model.data.generalStructure.Design;
import model.data.generalStructure.Repository;
import view.drawable.DrawableComponent;
import view.drawable.DrawableDesign;
import view.drawable.enumerators.DesignEnumeration;
import view.drawable.sequenceDiagram.ScreenDinamicDiagram;

public class Painter {

	private Vector<ScreenDiagram> screens = new Vector<ScreenDiagram>();
	private Design design;

	public Painter(Design design) {
		this.design = design;
	}

	@SuppressWarnings("unchecked")
	public void draw(Window window,int size){
		//SETEA EN 0 PARA CUANDO REDIBUJA MAS DE UN DISE�O ASI SE DIBUJA EN EL MISMO LUGAR Y NO SE ACHICA
		Configuration.relationalComponents=0;
		DrawableDesign drawableDesign = new DrawableDesign(design);
		drawableDesign.createDrawables();
		ScreenDiagram screen;
		Vector<DrawableComponent> components;
		int num = Repository.getInstance().getDiagramNum();
		for (DesignEnumeration e = drawableDesign.getEnumeration();e.hasMoreElements();)
		{
			components = e.nextElement();
			if (components.size() > 0)
			{
				screen = getDiagram(components.get(0));
				screen.setDiagramName("Diagrama " + num);
				for(int i=0;i<components.size();i++)
					screen.addComponent(components.get(i));
				screen.paint();
				screens.add(screen);
				window.addTab(window.getTabName(size),screen.getPanel());
			}
		}
	}

	private ScreenDiagram getDiagram(DrawableComponent drawableComponent) {
		return drawableComponent.getDiagram();
	}
	
	public void more()
	{
		((ScreenDinamicDiagram) screens.get(1)).testMore();
	}

}
