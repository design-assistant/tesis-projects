package view;

import javax.swing.ImageIcon;

public abstract class  ListItem {
	protected String value;
	
	protected ImageIcon icon;

	public ListItem() {}
	
	public void setValue(String value) {
		this.value = value;
	}

	public ImageIcon getIcon() {
		return icon;
	}

	public String getValue() {
		return value;
	}
	
}
