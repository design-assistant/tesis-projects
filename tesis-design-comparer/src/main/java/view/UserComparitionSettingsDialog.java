package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import model.group.QualityAttribute;
import bo.IQualityAttributeBO;
import context.ImplementationFactory;

/**
 * This code was edited or generated using CloudGarden's Jigloo SWT/Swing GUI Builder, which is free for non-commercial use. If Jigloo is
 * being used commercially (ie, by a corporation, company or business for any purpose whatever) then you should purchase a license for each
 * developer using Jigloo. Please visit www.cloudgarden.com for details. Use of Jigloo implies acceptance of these licensing terms. A
 * COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED LEGALLY FOR ANY CORPORATE OR COMMERCIAL
 * PURPOSE.
 */
public class UserComparitionSettingsDialog extends javax.swing.JDialog {

	/**
	 * 
	 */
	private boolean windowClose = true;
	private static final long serialVersionUID = 1L;
	private JScrollPane jScrollPane1;
	private JList jList1;
	private JScrollPane jScrollPane2;
	private JRadioButton jRadioButtonFull;
	private ButtonGroup buttonGroupQAKindOfMatching;
	private JTextField jTextFieldComponentCountLimit;
	private JLabel jLabel2ComponentsCount;
	private JLabel jLabel1ComponentsCount;
	private JLabel jLabel3;
	private JLabel jLabel3AbsorbedRelations;
	private JLabel jLabel3ComponentsCount;
	private JTextField jTextFieldAbsorbedRelationLimit;
	private JLabel jLabel2AbsorbedRelations;
	private JLabel jLabel1AbsorbedRelations;
	private JSeparator jSeparator3;
	private JLabel jLabel1QA;
	private JSeparator jSeparator2;
	private JSeparator jSeparator1;
	private JCheckBox jCheckBoxAbsorbedRelations;
	private JCheckBox jCheckBoxComponentsCount;
	private JButton jButton1;
	private JLabel jLabel1;
	private JButton btnSave;
	private JTextField jTextFieldQALimit;
	private JRadioButton jRadioButtonLimit;
	private JRadioButton jRadioButtonMore;
	private JRadioButton jRadioButtonLess;
	private JLabel jLabel2QA;
	private JButton btnAddAttribute;
	private JList jList2;
	DefaultListModel jList1Model;
	DefaultListModel jList2Model;
	private int[] selectedIndex;
	private UserWindow userWindow;

	public UserComparitionSettingsDialog(JFrame frame, UserWindow userWindow) {
		super(frame);
		initGUI();
		this.setModal(true);
		this.setLocationRelativeTo(null);
		this.userWindow = userWindow;
	}

	private void initGUI() {
		try {
			{
				getContentPane().setLayout(null);
				this.setResizable(false);
				this.setTitle("Parametros de configuraci�n");
				this.addWindowListener(new WindowAdapter() {
					public void windowClosing(WindowEvent evt) {
						rootWindowClosing(evt);
					}
				});
				{
					{
						buttonGroupQAKindOfMatching = new ButtonGroup();
					}
					jScrollPane1 = new JScrollPane();
					getContentPane().add(jScrollPane1);
					jScrollPane1.setBounds(406, 56, 147, 133);
					jScrollPane1.getVerticalScrollBar().setEnabled(true);
					jScrollPane1.getHorizontalScrollBar().setEnabled(true);
					{
						IQualityAttributeBO qaBO = (IQualityAttributeBO) ImplementationFactory.getBean("qualityAttributeBO");
						jList1Model = new DefaultListModel();
						for (QualityAttribute qa : qaBO.findAll())
							jList1Model.addElement(qa);
						jList1 = new JList();
						jScrollPane1.setViewportView(jList1);
						jList1.setModel(jList1Model);
						jList1.setBounds(70, 84, 140, 126);
						jList1.addKeyListener(new KeyAdapter() {
							public void keyPressed(KeyEvent evt) {
								jList1KeyPressed(evt);
							}
						});
					}
				}
				{
					jScrollPane2 = new JScrollPane();
					getContentPane().add(jScrollPane2);
					jScrollPane2.setBounds(637, 56, 147, 133);
					jScrollPane2.getVerticalScrollBar().setEnabled(true);
					jScrollPane2.getHorizontalScrollBar().setEnabled(true);
					{
						jList2Model = new DefaultListModel();
						jList2 = new JList();
						jScrollPane2.setViewportView(jList2);
						jList2.setModel(jList2Model);
					}
				}
				{
					btnAddAttribute = new JButton();
					getContentPane().add(btnAddAttribute);
					btnAddAttribute.setToolTipText("Agregar atributo");
					btnAddAttribute.setIcon(new ImageIcon(getClass().getClassLoader()
						.getResource("images/iconos/agregar.png")));
					btnAddAttribute.setBounds(574, 77, 40, 40);
					btnAddAttribute.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent evt) {
							btnAddAttributeActionPerformed(evt);
						}
					});
				}
				{
					jButton1 = new JButton();
					getContentPane().add(jButton1);
					jButton1.setToolTipText("Quitar atributo");
					jButton1.setIcon(new ImageIcon(getClass().getClassLoader()
						.getResource("images/iconos/quitar.png")));
					jButton1.setBounds(573, 133, 40, 40);
					jButton1.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent evt) {
							btnRemoveAttributeActionPerformed(evt);
						}

					});
				}
				{
					btnSave = new JButton();
					getContentPane().add(btnSave);
					btnSave.setToolTipText("Realizar b�squeda");
					btnSave.setIcon(new ImageIcon(getClass().getClassLoader()
						.getResource("images/iconos/ok.png")));
					btnSave.setBounds(371, 369, 42, 42);
					btnSave.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent evt) {
							saveComparationSettings(evt);
						}
					});
				}
				{
					jLabel1 = new JLabel();
					getContentPane().add(jLabel1);
					jLabel1.setText("Seleccione los atributos por los cuales desea comparar");
					jLabel1.setBounds(21, 14, 322, 28);
					jLabel1.setFont(new java.awt.Font("Tahoma",1,11));
				}
				{
					jCheckBoxComponentsCount = new JCheckBox();
					getContentPane().add(jCheckBoxComponentsCount);
					jCheckBoxComponentsCount.setText("Grado de Cercan�a");
					jCheckBoxComponentsCount.setBounds(21, 63, 154, 21);
					jCheckBoxComponentsCount.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent evt) {
							jCheckBoxComponentsCountActionPerformed(evt);
						}
					});
				}
				{
					jCheckBoxAbsorbedRelations = new JCheckBox();
					getContentPane().add(jCheckBoxAbsorbedRelations);
					jCheckBoxAbsorbedRelations.setText("Grado de Acoplamiento");
					jCheckBoxAbsorbedRelations.setBounds(21, 98, 217, 21);
					jCheckBoxAbsorbedRelations.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent evt) {
							jCheckBoxAbsorbedRelationsActionPerformed(evt);
						}
					});
				}
				{
					jSeparator1 = new JSeparator();
					getContentPane().add(jSeparator1);
					jSeparator1.setBounds(392, 0, 7, 364);
					jSeparator1.setOrientation(SwingConstants.VERTICAL);
				}
				{
					jSeparator2 = new JSeparator();
					getContentPane().add(jSeparator2);
					jSeparator2.setBounds(0, 273, 791, 14);
				}
				{
					jLabel1QA = new JLabel();
					getContentPane().add(jLabel1QA);
					jLabel1QA.setText("Atributos de Calidad");
					jLabel1QA.setBounds(406, 14, 140, 28);
					jLabel1QA.setFont(new java.awt.Font("Tahoma",1,11));
				}
				{
					jSeparator3 = new JSeparator();
					getContentPane().add(jSeparator3);
					jSeparator3.setBounds(0, 364, 791, 14);
				}
				{
					jLabel1ComponentsCount = new JLabel();
					getContentPane().add(jLabel1ComponentsCount);
					jLabel1ComponentsCount.setText("Grado de Cercan�a");
					jLabel1ComponentsCount.setBounds(21, 280, 147, 28);
					jLabel1ComponentsCount.setFont(new java.awt.Font("Tahoma",1,11));
					jLabel1ComponentsCount.setEnabled(false);
				}
				{
					jLabel2ComponentsCount = new JLabel();
					getContentPane().add(jLabel2ComponentsCount);
					jLabel2ComponentsCount.setText("Valor (porcentaje)");
					jLabel2ComponentsCount.setBounds(31, 322, 116, 28);
					jLabel2ComponentsCount.setEnabled(false);
				}
				{
					jTextFieldComponentCountLimit = new JTextField();
					getContentPane().add(jTextFieldComponentCountLimit);
					jTextFieldComponentCountLimit.setBounds(146, 322, 35, 28);
					jTextFieldComponentCountLimit.setEnabled(false);
				}
				{
					jRadioButtonFull = new JRadioButton();
					getContentPane().add(jRadioButtonFull);
					jRadioButtonFull.setText("Por exactitud");
					jRadioButtonFull.setBounds(399, 224, 119, 21);
					buttonGroupQAKindOfMatching.add(jRadioButtonFull);
					jRadioButtonFull.setSelected(true);
				}
				{
					jLabel2QA = new JLabel();
					getContentPane().add(jLabel2QA);
					jLabel2QA.setText("Tipo de busqueda");
					jLabel2QA.setBounds(406, 196, 147, 28);
					jLabel2QA.setFont(new java.awt.Font("Tahoma",1,11));
				}
				{
					jRadioButtonLess = new JRadioButton();
					getContentPane().add(jRadioButtonLess);

					jRadioButtonLess.setText("Por inclusi�n inversa");
					jRadioButtonLess.setBounds(567, 224, 182, 21);
					buttonGroupQAKindOfMatching.add(jRadioButtonLess);
				}
				{
					jRadioButtonMore = new JRadioButton();
					getContentPane().add(jRadioButtonMore);
					jRadioButtonMore.setText("Por inclusi�n total");
					jRadioButtonMore.setBounds(399, 245, 168, 21);
					buttonGroupQAKindOfMatching.add(jRadioButtonMore);
				}
				{
					jRadioButtonLimit = new JRadioButton();
					getContentPane().add(jRadioButtonLimit);
					jRadioButtonLimit.setText("Por inclusi�n parcial (cantidad)");
					jRadioButtonLimit.setBounds(567, 245, 182, 21);
					buttonGroupQAKindOfMatching.add(jRadioButtonLimit);
					jRadioButtonLimit.addChangeListener(new ChangeListener() {
						public void stateChanged(ChangeEvent evt) {
							jRadioButtonLimitStateChanged(evt);
						}
					});
				}
				{
					jTextFieldQALimit = new JTextField();
					getContentPane().add(jTextFieldQALimit);
					jTextFieldQALimit.setBounds(749, 241, 35, 28);
					jTextFieldQALimit.setEnabled(false);
				}
				{
					jLabel1AbsorbedRelations = new JLabel();
					getContentPane().add(jLabel1AbsorbedRelations);
					jLabel1AbsorbedRelations.setText("Grado de Acoplamiento");
					jLabel1AbsorbedRelations.setBounds(406, 280, 231, 28);
					jLabel1AbsorbedRelations.setFont(new java.awt.Font("Tahoma",1,11));
					jLabel1AbsorbedRelations.setEnabled(false);
				}
				{
					jLabel2AbsorbedRelations = new JLabel();
					getContentPane().add(jLabel2AbsorbedRelations);
					jLabel2AbsorbedRelations.setText("Valor (porcentaje)");
					jLabel2AbsorbedRelations.setBounds(466, 322, 108, 28);
					jLabel2AbsorbedRelations.setEnabled(false);
				}
				{
					jTextFieldAbsorbedRelationLimit = new JTextField();
					getContentPane().add(jTextFieldAbsorbedRelationLimit);
					jTextFieldAbsorbedRelationLimit.setBounds(574, 322, 35, 28);
					jTextFieldAbsorbedRelationLimit.setEnabled(false);
				}
				{
					jLabel3ComponentsCount = new JLabel();
					getContentPane().add(jLabel3ComponentsCount);
					jLabel3ComponentsCount.setText("%");
					jLabel3ComponentsCount.setBounds(183, 322, 14, 28);
					jLabel3ComponentsCount.setEnabled(false);
				}
				{
					jLabel3AbsorbedRelations = new JLabel();
					getContentPane().add(jLabel3AbsorbedRelations);
					jLabel3AbsorbedRelations.setText("%");
					jLabel3AbsorbedRelations.setBounds(611, 322, 14, 28);
					jLabel3AbsorbedRelations.setEnabled(false);
				}
				{
					jLabel3 = new JLabel();
					getContentPane().add(jLabel3);
					jLabel3.setText("Atributos de Calidad");
					jLabel3.setFont(new java.awt.Font("Tahoma", 1, 11));
					jLabel3.setEnabled(false);
					jLabel3.setBounds(406, 14, 140, 28);
				}
			}
			this.setSize(799, 440);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void btnAddAttributeActionPerformed(ActionEvent evt) {
		if (jList1.getSelectedIndex() != -1) {
			selectedIndex = jList1.getSelectedIndices();
			for (int i = selectedIndex.length - 1;i >= 0;i--)
			{
				jList2Model.addElement(jList1Model.get(selectedIndex[i]));
				jList1Model.remove(selectedIndex[i]);
			}
		}
	}

	private void btnRemoveAttributeActionPerformed(ActionEvent evt) {
		if (jList2.getSelectedIndex() != -1) {
			for (int i = jList2.getSelectedIndices().length-1;i >= 0;i--)
			{
				jList1Model.addElement(jList2Model.get(jList2.getSelectedIndices()[i]));
				jList2Model.remove(jList2.getSelectedIndices()[i]);
			}
		}

	}


	private void saveComparationSettings(ActionEvent evt) {
		boolean submit = verifyErrors();
		if (submit)
		{
			userWindow.cleanComparationSettings();
			List<String> attributes = new ArrayList<String>();;
			int qualityAttributeCount = -1;
			int qualityAttributeKindOfMatching=1;
			for (int i = 0;i < jList2Model.size();i++)
				attributes.add(jList2Model.get(i).toString());
			if (jRadioButtonFull.isSelected())
				qualityAttributeKindOfMatching = 1;
			else if (jRadioButtonLess.isSelected())
					qualityAttributeKindOfMatching = 2;
			else if (jRadioButtonMore.isSelected())
					qualityAttributeKindOfMatching = 3;
			else if (jRadioButtonLimit.isSelected())
				{
					qualityAttributeKindOfMatching = 4;
					qualityAttributeCount = new Integer(jTextFieldQALimit.getText()).intValue();
				}
			userWindow.setQualityAttributes(attributes,qualityAttributeCount,qualityAttributeKindOfMatching);
			if (jCheckBoxComponentsCount.isSelected())
			{
				double componentsCountLimit = new Double(jTextFieldComponentCountLimit.getText()).doubleValue();
				userWindow.setComponentsCountLimit(componentsCountLimit);
			}
			if (jCheckBoxAbsorbedRelations.isSelected())
			{
				double absorbedRelationsLimit = new Double(jTextFieldAbsorbedRelationLimit.getText()).doubleValue();
				userWindow.setAbsorbedRelationsLimit(absorbedRelationsLimit);
			}
			windowClose = false;
			this.dispose();
		}
	}
	
	private boolean verifyErrors() {
		@SuppressWarnings("unused")
		AcceptMessage message;
		if (jList2Model.size() == 0)
		{
			message = new AcceptMessage("Al menos debe elegir un atributo de calidad",300);
			return false;
		}
		if (jRadioButtonLimit.isSelected())
		{
			try {
				int qualityAttributeCount = new Integer(jTextFieldQALimit.getText()).intValue();
				if ((qualityAttributeCount >= jList2Model.size()) || (qualityAttributeCount <= 0))
				{
					message = new AcceptMessage("El valor seleccionado para atributos de calidad debe ser un n�mero entero menor a la cantidad seleccionada de los mismos y mayor a 0 para la opci�n elegida",900);
					return false;
				}
			} catch (Exception e) {
				message = new AcceptMessage("El valor seleccionado para atributos de calidad debe ser un n�mero entero menor a la cantidad seleccionada de los mismos y mayor a 0 para la opci�n elegida",900);
				return false;
			}
			
		}
		if (jCheckBoxComponentsCount.isSelected())
		{
			try {
				double componentCountLimit = new Double(jTextFieldComponentCountLimit.getText()).doubleValue();
				if ((componentCountLimit > 100) || (componentCountLimit < 0))
				{
					message = new AcceptMessage("El valor seleccionado como umbral de diferencia debe ser un n�mero real menor a 100 y superior a 0",600);
					return false;
				}
			} catch (Exception e) {
				message = new AcceptMessage("El valor seleccionado como umbral de diferencia debe ser un n�mero real menor a 100 y superior a 0",600);
				return false;
			}
		}
		if (jCheckBoxAbsorbedRelations.isSelected())
		{
			try {
				double absorbedRelationsLimit = new Double(jTextFieldAbsorbedRelationLimit.getText()).doubleValue();
				if ((absorbedRelationsLimit > 100) || (absorbedRelationsLimit < 0))
				{
					message = new AcceptMessage("El valor seleccionado para el grado de relaci�n entre componentes debe ser un n�mero real menor a 100 y superior a 0",700);
					return false;
				}
			} catch (Exception e) {
				message = new AcceptMessage("El valor seleccionado para el grado de relaci�n entre componentes debe ser un n�mero real menor a 100 y superior a 0",700);
				return false;
			}
		}
		return true;
	}
	
	private void jCheckBoxComponentsCountActionPerformed(ActionEvent evt) {
		if (jCheckBoxComponentsCount.isSelected())
		{
			jLabel1ComponentsCount.setEnabled(true);
			jLabel2ComponentsCount.setEnabled(true);
			jLabel3ComponentsCount.setEnabled(true);
			jTextFieldComponentCountLimit.setEnabled(true);			
		}
		else
		{
			jLabel1ComponentsCount.setEnabled(false);
			jLabel2ComponentsCount.setEnabled(false);
			jLabel3ComponentsCount.setEnabled(false);
			jTextFieldComponentCountLimit.setEnabled(false);		
		}
	}
	
	private void jCheckBoxAbsorbedRelationsActionPerformed(ActionEvent evt) {
		if (jCheckBoxAbsorbedRelations.isSelected())
		{
			jLabel1AbsorbedRelations.setEnabled(true);
			jLabel2AbsorbedRelations.setEnabled(true);
			jLabel3AbsorbedRelations.setEnabled(true);
			jTextFieldAbsorbedRelationLimit.setEnabled(true);
		}
		else
		{
			jLabel1AbsorbedRelations.setEnabled(false);
			jLabel2AbsorbedRelations.setEnabled(false);
			jLabel3AbsorbedRelations.setEnabled(false);
			jTextFieldAbsorbedRelationLimit.setEnabled(false);
		}
	}
	
	private void jRadioButtonLimitStateChanged(ChangeEvent evt) {
		if (jRadioButtonLimit.isSelected())
			jTextFieldQALimit.setEnabled(true);
		else
			jTextFieldQALimit.setEnabled(false);
	}
		
	private void rootWindowClosing(WindowEvent evt) {
		if (windowClose)
		{
			ConfirmMessage message = new ConfirmMessage("�Desea cancelar la comparaci�n?",240);
			int op = message.getAnswer();
			if ((JOptionPane.CANCEL_OPTION == op) || (JOptionPane.CLOSED_OPTION == op))
				setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
			else
			{
				setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
				userWindow.dontCompare();
			}
		}
	}
	
	private void jList1KeyPressed(KeyEvent evt) {
		if (evt.getKeyCode() == 10) //ENTER
			btnAddAttributeActionPerformed(null);
	}

}
