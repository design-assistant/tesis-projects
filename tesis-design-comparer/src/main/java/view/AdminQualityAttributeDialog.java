package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Hashtable;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import model.data.generalStructure.Repository;
import model.group.QualityAttribute;
import model.group.QualityAttributeGroup;
import presenter.AdminUserPresenter;
import presenter.exceptions.DuplicateAttributeException;
import bo.IQualityAttributeBO;
import context.ImplementationFactory;

/**
 * This code was edited or generated using CloudGarden's Jigloo SWT/Swing GUI Builder, which is free for non-commercial use. If Jigloo is
 * being used commercially (ie, by a corporation, company or business for any purpose whatever) then you should purchase a license for each
 * developer using Jigloo. Please visit www.cloudgarden.com for details. Use of Jigloo implies acceptance of these licensing terms. A
 * COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED LEGALLY FOR ANY CORPORATE OR COMMERCIAL
 * PURPOSE.
 */
public class AdminQualityAttributeDialog extends javax.swing.JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JScrollPane jScrollPane1;
	private JList jList1;
	private JScrollPane jScrollPane2;
	private JTextField txtAttribute;
	private JButton jButtonAddAttribute;
	private JButton jButton1;
	private JButton btnSave;
	private JButton btnAddAttribute;
	private JList jList2;
	private DefaultListModel attributesModel;
	private DefaultListModel selectedAttributesModel;
	public int temporalRank;
	public Hashtable<String, Integer> rankings = new Hashtable<String, Integer>();
	private int[] selectedIndex;
	private AdminWindow father;
	private AdminUserPresenter presenter;


	public int getTemporalRank() {
		return temporalRank;
	}

	public AdminQualityAttributeDialog(JFrame frame,AdminWindow father, AdminUserPresenter presenter) {
		super(frame);
		initGUI();
		this.setModal(true);
		this.father = father;
		this.setLocationRelativeTo(null);
		this.presenter = presenter;
	}

	private void initGUI() {
		try {
			{
				getContentPane().setLayout(null);
				this.setTitle("Evaluar");
				this.setResizable(false);
				{
					jScrollPane1 = new JScrollPane();
					getContentPane().add(jScrollPane1);
					jScrollPane1.setBounds(7, 7, 147, 133);
					{
						getAllAttributes();
						jList1 = new JList();
						jScrollPane1.setViewportView(jList1);
						jList1.setModel(attributesModel);
						jList1.setBounds(70, 84, 140, 126);
						jList1.addKeyListener(new KeyAdapter() {
							public void keyPressed(KeyEvent evt) {
								jList1KeyPressed(evt);
							}
						});
					}
				}
				{
					jScrollPane2 = new JScrollPane();
					getContentPane().add(jScrollPane2);
					jScrollPane2.setBounds(224, 7, 147, 133);
					{
						selectedAttributesModel = new DefaultListModel();
						jList2 = new JList();
						jScrollPane2.setViewportView(jList2);
						jList2.setModel(selectedAttributesModel);
						jList2.setCellRenderer(new MyCellRenderer());
						if (Repository.getInstance().getCurrentPDBox() != null)
						{
							List<QualityAttributeGroup> attributes = Repository.getInstance().getCurrentPDBox().getAttributes();
							for (int i = 0;i < attributes.size();i++)
							{
								ListQAItem li;
								if (attributes.get(i).getSuccess() >= 7)
									li = new ListQAItem(new ImageIcon(getClass().getClassLoader().getResource("images/verde.gif")));
								else if (attributes.get(i).getSuccess() <= 3)
										li = new ListQAItem(new ImageIcon(getClass().getClassLoader().getResource("images/rojo.gif")));
									else
										li = new ListQAItem(new ImageIcon(getClass().getClassLoader().getResource("images/amarillo.gif")));
								li.setQa(attributes.get(i).getAttribute());
								selectedAttributesModel.addElement(li);
								rankings.put(attributes.get(i).getAttribute().getName(),attributes.get(i).getSuccess());
								boolean cut = false;
								for (int j = 0;j < attributesModel.getSize() && !cut;j++)
								{
									if (((QualityAttribute)attributesModel.get(j)).getName().equals(attributes.get(i).getAttribute().getName()))
									{
										attributesModel.remove(j);
										cut = true;
									}
								}
							}
						}
					}
				}
				{
					btnAddAttribute = new JButton();
					getContentPane().add(btnAddAttribute);
					btnAddAttribute.setIcon(new ImageIcon(getClass().getClassLoader()
							.getResource("images/iconos/agregar.png")));
					btnAddAttribute.setToolTipText("Agregar atributo");
					btnAddAttribute.setBounds(169, 21, 40, 40);
					btnAddAttribute.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent evt) {
							calificateAttributes(evt);
						}
					});
				}
				{
					jButton1 = new JButton();
					getContentPane().add(jButton1);
					jButton1.setIcon(new ImageIcon(getClass().getClassLoader()
							.getResource("images/iconos/quitar.png")));
					jButton1.setToolTipText("Quitar atributo");
					jButton1.setBounds(168, 84, 42, 42);
					jButton1.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent evt) {
							btnRemoveAttributeActionPerformed(evt);
						}

					});
				}
				{
					btnSave = new JButton();
					getContentPane().add(btnSave);
					btnSave.setIcon(new ImageIcon(getClass().getClassLoader()
							.getResource("images/iconos/ok.png")));
					btnSave.setToolTipText("Aceptar");
					btnSave.setBounds(327, 146, 42, 42);
					btnSave.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent evt) {
							saveEvaluation(evt);
						}
					});
				}
				{
					jButtonAddAttribute = new JButton();
					getContentPane().add(jButtonAddAttribute);
					jButtonAddAttribute.setBounds(168, 146, 42, 42);
					jButtonAddAttribute.setIcon(new ImageIcon(getClass().getClassLoader()
							.getResource("images/iconos/mas.png")));
					jButtonAddAttribute.setToolTipText("Agregar atributo");
					jButtonAddAttribute.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent evt) {
							addQualityAttribute(evt);
						}
					});
				}
				{
					txtAttribute = new JTextField();
					getContentPane().add(txtAttribute);
					txtAttribute.setBounds(7, 156, 147, 22);
					txtAttribute.addKeyListener(new KeyAdapter() {
						public void keyPressed(KeyEvent evt) {
							txtAttributeKeyPressed(evt);
						}
					});
				}
			}
			this.setSize(386, 230);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void getAllAttributes() {
		IQualityAttributeBO qaBO = (IQualityAttributeBO) ImplementationFactory.getBean("qualityAttributeBO");
		attributesModel = new DefaultListModel();
		for (QualityAttribute qa : qaBO.findAll())
			if(!rankings.containsKey(qa.getName()))
				attributesModel.addElement(qa);
	}

	private void calificateAttributes(ActionEvent evt) {
		if (jList1.getSelectedIndex() != -1) {
			selectedIndex = jList1.getSelectedIndices();
			JFrame frame = new JFrame();
			QualityAttributeRank inst = new QualityAttributeRank(frame);
			inst.setFather(this);
			inst.setVisible(true);
		}
	}

	private void btnRemoveAttributeActionPerformed(ActionEvent evt) {
		if (jList2.getSelectedIndex() != -1) {
			for (int i = jList2.getSelectedIndices().length-1;i >= 0;i--)
			{
			//	attributesModel.addElement(((ListQAItem)selectedAttributesModel.get(jList2.getSelectedIndices()[i])).getQa());
				rankings.remove(((ListItem)selectedAttributesModel.get(jList2.getSelectedIndices()[i])).getValue());
				selectedAttributesModel.remove(jList2.getSelectedIndices()[i]);
				getAllAttributes();
				jList1.setModel(attributesModel);
				txtAttribute.setText("");
			}
		}

	}

	public void genericAdd(int success) {
		ListQAItem li;
		ImageIcon image;
		if (success >= 7)
			image = new ImageIcon(getClass().getClassLoader().getResource("images/verde.gif"));
		else if (success <= 3)
				image = new ImageIcon(getClass().getClassLoader().getResource("images/rojo.gif"));
			else
				image = new ImageIcon(getClass().getClassLoader().getResource("images/amarillo.gif"));
		for (int i = selectedIndex.length - 1;i >= 0;i--)
		{
			String item = attributesModel.get(selectedIndex[i]).toString();
			rankings.put(item, new Integer(success));
			li = new ListQAItem(image);
			li.setQa((QualityAttribute) attributesModel.get(selectedIndex[i]));
			selectedAttributesModel.addElement(li);
			attributesModel.remove(selectedIndex[i]);
		}		
	}

	private void saveEvaluation(ActionEvent evt) {
		if (selectedAttributesModel.size() > 0)
		{
			presenter.saveEvaluation(rankings);
			father.enableSave();
			this.dispose();
		}
		else{
			@SuppressWarnings("unused")
			AcceptMessage message = new AcceptMessage("Debe calificar el dise�o al menos con un atributo de calidad.",400);
		}
		
	}
	
	public void setTemporalRank(int temporalRank) {
		this.temporalRank = temporalRank;
	}
	
	private void addQualityAttribute(ActionEvent evt) {
		if(txtAttribute.getText().trim().length()==0)
		{
			new AcceptMessage("Debe especificar un nuevo atributo de calidad ", 300);
		}
		else{
			try{
				presenter.addAttribute(txtAttribute.getText());
				getAllAttributes();
				jList1.setModel(attributesModel);
				txtAttribute.setText("");
			}
			catch(DuplicateAttributeException e){
				new AcceptMessage(e.getMessage(), 230);
			}
		}
	}
	
	private void jList1KeyPressed(KeyEvent evt) {
		if (evt.getKeyCode() == 10) //ENTER
			calificateAttributes(null);
	}
	
	private void txtAttributeKeyPressed(KeyEvent evt) {
		if (evt.getKeyCode() == 10)
			addQualityAttribute(null);
	}
}
