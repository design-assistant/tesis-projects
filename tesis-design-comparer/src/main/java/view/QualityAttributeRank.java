package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Vector;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 * This code was edited or generated using CloudGarden's Jigloo SWT/Swing GUI Builder, which is free for non-commercial use. If Jigloo is
 * being used commercially (ie, by a corporation, company or business for any purpose whatever) then you should purchase a license for each
 * developer using Jigloo. Please visit www.cloudgarden.com for details. Use of Jigloo implies acceptance of these licensing terms. A
 * COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED LEGALLY FOR ANY CORPORATE OR COMMERCIAL
 * PURPOSE.
 */
public class QualityAttributeRank extends javax.swing.JDialog {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Vector<JLabel> stars = new Vector<JLabel>();
	@SuppressWarnings("unused")
	private ButtonGroup btnRank;
	private JButton btnOK;
	private Vector<Boolean> starsSelection = new Vector<Boolean>();
	private JLabel jLabelBad;
	private JLabel jLabelRegular;
	private JLabel jLabelGood;
	private AdminQualityAttributeDialog father;
	private JLabel jLabelSt10;
	private JLabel jLabelSt9;
	private JLabel jLabelSt8;
	private JLabel jLabelSt7;
	private JLabel jLabelSt6;
	private JLabel jLabelSt5;
	private JLabel jLabelSt0;
	private JLabel jLabelSt4;
	private JLabel jLabelSt3;
	private JLabel jLabelSt2;
	private JLabel jLabelSt1;
	private int rank;

	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	public QualityAttributeRank(JFrame frame) {
		super(frame);
		initGUI();
		this.setModal(true);
		this.setLocationRelativeTo(null);
		inicVectors();
	}

	private void initGUI() {
		try {
			{
				btnRank = new ButtonGroup();
				setRank(1);
			}
			{
				getContentPane().setLayout(null);
				this.setTitle("Calificar");
				this.setModal(true);
				this.setResizable(false);
				{
					btnOK = new JButton();
					getContentPane().add(btnOK);
					btnOK.setIcon(new ImageIcon(getClass().getClassLoader()
							.getResource("images/iconos/ok.png")));
					btnOK.setToolTipText("Aceptar");
					btnOK.setBounds(189, 70, 42, 42);
					btnOK.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent evt) {
							saveCalification(evt);
						}
					});
				}

				{
					jLabelSt1 = new JLabel();
					getContentPane().add(jLabelSt1);
					jLabelSt1.setBounds(49, 14, 35, 28);
					jLabelSt1.setIcon(new ImageIcon(getClass().getClassLoader()
						.getResource("images/star.gif")));
					jLabelSt1.addMouseListener(new MouseAdapter() {
						public void mouseClicked(MouseEvent evt) {
							jLabelSt1MouseClicked(evt);
						}
					});
				}
				{
					jLabelSt2 = new JLabel();
					getContentPane().add(jLabelSt2);
					jLabelSt2.setBounds(84, 14, 35, 28);
					jLabelSt2.setIcon(new ImageIcon(getClass().getClassLoader()
						.getResource("images/star.gif")));
					jLabelSt2.addMouseListener(new MouseAdapter() {
						public void mouseClicked(MouseEvent evt) {
							jLabelSt2MouseClicked(evt);
						}
					});
				}
				{
					jLabelSt3 = new JLabel();
					getContentPane().add(jLabelSt3);
					jLabelSt3.setBounds(119, 14, 35, 28);
					jLabelSt3.setIcon(new ImageIcon(getClass().getClassLoader()
						.getResource("images/star.gif")));
					jLabelSt3.addMouseListener(new MouseAdapter() {
						public void mouseClicked(MouseEvent evt) {
							jLabelSt3MouseClicked(evt);
						}
					});
				}
				{
					jLabelSt4 = new JLabel();
					getContentPane().add(jLabelSt4);
					jLabelSt4.setBounds(154, 14, 35, 28);
					jLabelSt4.setIcon(new ImageIcon(getClass().getClassLoader()
						.getResource("images/star.gif")));
					jLabelSt4.addMouseListener(new MouseAdapter() {
						public void mouseClicked(MouseEvent evt) {
							jLabelSt4MouseClicked(evt);
						}
					});
				}
				{
					jLabelSt5 = new JLabel();
					getContentPane().add(jLabelSt5);
					jLabelSt5.setBounds(189, 14, 35, 28);
					jLabelSt5.setIcon(new ImageIcon(getClass().getClassLoader()
						.getResource("images/star.gif")));
					jLabelSt5.addMouseListener(new MouseAdapter() {
						public void mouseClicked(MouseEvent evt) {
							jLabelSt5MouseClicked(evt);
						}
					});
				}
				{
					jLabelSt6 = new JLabel();
					getContentPane().add(jLabelSt6);
					jLabelSt6.setBounds(224, 14, 35, 28);
					jLabelSt6.setIcon(new ImageIcon(getClass().getClassLoader()
						.getResource("images/emptyStar.gif")));
					jLabelSt6.addMouseListener(new MouseAdapter() {
						public void mouseClicked(MouseEvent evt) {
							jLabelSt6MouseClicked(evt);
						}
					});
				}
				{
					jLabelSt7 = new JLabel();
					getContentPane().add(jLabelSt7);
					jLabelSt7.setBounds(259, 14, 35, 28);
					jLabelSt7.setIcon(new ImageIcon(getClass().getClassLoader()
						.getResource("images/emptyStar.gif")));
					jLabelSt7.addMouseListener(new MouseAdapter() {
						public void mouseClicked(MouseEvent evt) {
							jLabelSt7MouseClicked(evt);
						}
					});
				}
				{
					jLabelSt8 = new JLabel();
					getContentPane().add(jLabelSt8);
					jLabelSt8.setBounds(294, 14, 35, 28);
					jLabelSt8.setIcon(new ImageIcon(getClass().getClassLoader()
						.getResource("images/emptyStar.gif")));
					jLabelSt8.addMouseListener(new MouseAdapter() {
						public void mouseClicked(MouseEvent evt) {
							jLabelSt8MouseClicked(evt);
						}
					});
				}
				{
					jLabelSt9 = new JLabel();
					getContentPane().add(jLabelSt9);
					jLabelSt9.setBounds(329, 14, 35, 28);
					jLabelSt9.setIcon(new ImageIcon(getClass().getClassLoader()
						.getResource("images/emptyStar.gif")));
					jLabelSt9.addMouseListener(new MouseAdapter() {
						public void mouseClicked(MouseEvent evt) {
							jLabelSt9MouseClicked(evt);
						}
					});
				}
				{
					jLabelSt10 = new JLabel();
					getContentPane().add(jLabelSt10);
					jLabelSt10.setBounds(364, 14, 35, 28);
					jLabelSt10.setIcon(new ImageIcon(getClass()
						.getClassLoader().getResource("images/emptyStar.gif")));
					jLabelSt10.addMouseListener(new MouseAdapter() {
						public void mouseClicked(MouseEvent evt) {
							jLabelSt10MouseClicked(evt);
						}
					});
				}
				{
					jLabelSt0 = new JLabel();
					getContentPane().add(jLabelSt0);
					jLabelSt0.setIcon(new ImageIcon(getClass().getClassLoader()
						.getResource("images/star.gif")));
					jLabelSt0.setBounds(14, 14, 35, 28);
					jLabelSt0.addMouseListener(new MouseAdapter() {
						public void mouseClicked(MouseEvent evt) {
							jLabelSt0MouseClicked(evt);
						}
					});
				}
				{
					jLabelGood = new JLabel();
					getContentPane().add(jLabelGood);
					jLabelGood.setText("BUENO");
					jLabelGood.setBounds(315, 42, 63, 28);
				}
				{
					jLabelRegular = new JLabel();
					getContentPane().add(jLabelRegular);
					jLabelRegular.setText("REGULAR");
					jLabelRegular.setBounds(189, 42, 63, 28);
				}
				{
					jLabelBad = new JLabel();
					getContentPane().add(jLabelBad);
					jLabelBad.setText("MALO");
					jLabelBad.setBounds(77, 42, 42, 28);
				}

			}
			this.setSize(421, 146);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void saveCalification(ActionEvent evt) {
		father.setTemporalRank(getSuccess());
		father.genericAdd(getSuccess());
		this.dispose();
	}

	public AdminQualityAttributeDialog getFather() {
		return father;
	}

	public void setFather(AdminQualityAttributeDialog father) {
		this.father = father;
	}

	
	private void jLabelSt0MouseClicked(MouseEvent evt) {
		int index = stars.indexOf(jLabelSt0);
		paint(index);
	}
	
	private void jLabelSt1MouseClicked(MouseEvent evt) {
		int index = stars.indexOf(jLabelSt1);
		paint(index);
	}
	
	private void jLabelSt2MouseClicked(MouseEvent evt) {
		int index = stars.indexOf(jLabelSt2);
		paint(index);
	}
	
	private void jLabelSt3MouseClicked(MouseEvent evt) {
		int index = stars.indexOf(jLabelSt3);
		paint(index);
	}
	
	private void jLabelSt4MouseClicked(MouseEvent evt) {
		int index = stars.indexOf(jLabelSt4);
		paint(index);
	}
	
	private void jLabelSt5MouseClicked(MouseEvent evt) {
		int index = stars.indexOf(jLabelSt5);
		paint(index);
	}
	
	private void jLabelSt6MouseClicked(MouseEvent evt) {
		int index = stars.indexOf(jLabelSt6);
		paint(index);
	}
	
	private void jLabelSt7MouseClicked(MouseEvent evt) {
		int index = stars.indexOf(jLabelSt7);
		paint(index);
	}
	
	private void jLabelSt8MouseClicked(MouseEvent evt) {
		int index = stars.indexOf(jLabelSt8);
		paint(index);
	}
	
	private void jLabelSt9MouseClicked(MouseEvent evt) {
		int index = stars.indexOf(jLabelSt9);
		paint(index);
	}
	
	private void jLabelSt10MouseClicked(MouseEvent evt) {
		int index = stars.indexOf(jLabelSt10);
		paint(index);
	}

	private void paint(int index) {
		for (int i = 0;i <= index;i++)
		{
			stars.get(i).setIcon(new ImageIcon(getClass().getClassLoader().getResource("images/star.gif")));
			starsSelection.remove(i);
			starsSelection.insertElementAt(new Boolean(true),i);
		}
		for (int i = index+1;i < stars.size();i++)
		{
			stars.get(i).setIcon(new ImageIcon(getClass().getClassLoader().getResource("images/emptyStar.gif")));
			starsSelection.remove(i);
			starsSelection.insertElementAt(new Boolean(false),i);
		}
	}
	
	private int getSuccess() {
		for (int i = starsSelection.size()-1;i >= 0;i--)
			if (starsSelection.get(i).booleanValue())
				return i;
		return 1;
	}
	
	private void inicVectors() {
		for (int i = 0;i < 11;i++)
			starsSelection.add(new Boolean(false));
		stars.add(jLabelSt0);
		stars.add(jLabelSt1);
		stars.add(jLabelSt2);
		stars.add(jLabelSt3);
		stars.add(jLabelSt4);
		stars.add(jLabelSt5);
		stars.add(jLabelSt6);
		stars.add(jLabelSt7);
		stars.add(jLabelSt8);
		stars.add(jLabelSt9);
		stars.add(jLabelSt10);
		paintFive();
	}
	private void paintFive() {
		for (int i = 0;i <= 5;i++)
		{
			starsSelection.remove(i);
			starsSelection.insertElementAt(new Boolean(true),i);
		}
	}
}
