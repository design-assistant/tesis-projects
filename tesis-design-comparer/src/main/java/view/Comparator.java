package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JInternalFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JTabbedPane;

/**
 * This code was edited or generated using CloudGarden's Jigloo SWT/Swing GUI Builder, which is free for non-commercial use. If Jigloo is
 * being used commercially (ie, by a corporation, company or business for any purpose whatever) then you should purchase a license for each
 * developer using Jigloo. Please visit www.cloudgarden.com for details. Use of Jigloo implies acceptance of these licensing terms. A
 * COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED LEGALLY FOR ANY CORPORATE OR COMMERCIAL
 * PURPOSE.
 */
public class Comparator extends javax.swing.JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	{
		try {
			javax.swing.UIManager.setLookAndFeel("com.jgoodies.looks.plastic.Plastic3DLookAndFeel");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private JMenuBar jMenuBar1;
	private JMenuItem jMenuItemUser;
	private JMenuItem jMenuItemLogOut;
	private JMenuItem jMenuItemAbout;
	private JMenu jMenuHelp;
	private JMenuItem jMenuItemExit;
	private JTabbedPane jTabbedPane1;
	private JMenuItem jMenuItemAdmin;
	private JMenu jMenuSession;
	private AdminWindow adminWindow = null;
	private UserWindow userWindow = null;
	
	/**
	 * Auto-generated main method to display this JFrame
	 */
	public static void main(String[] args) {
		@SuppressWarnings("unused")
		Comparator inst = new Comparator();
	}

	public Comparator() {
		super();
		initGUI();
		this.setLocationRelativeTo(null);
		
		@SuppressWarnings("unused")
		Splash splash = new Splash(500,this);
		this.setVisible(true);
		this.jMenuSession.setEnabled(true);
		this.jMenuHelp.setEnabled(true);
		jMenuItemLogOut.setEnabled(false);
	}

	private void initGUI() {
		try {
			getContentPane().setLayout(null);
			this.setTitle("Design Comparer");
			this.setResizable(false);
			this.addWindowListener(new WindowAdapter() {
				public void windowClosing(WindowEvent evt) {
					rootWindowClosing(evt);
				}
			});
			{
				jTabbedPane1 = new JTabbedPane();
				getContentPane().add(jTabbedPane1);
				jTabbedPane1.setBounds(0, -25, 784, 623);
			}
			{
				jMenuBar1 = new JMenuBar();
				setJMenuBar(jMenuBar1);
				jMenuBar1.setPreferredSize(new java.awt.Dimension(392, 27));
				jMenuBar1.setEnabled(false);
				{
					jMenuSession = new JMenu();
					jMenuBar1.add(jMenuSession);
					jMenuSession.setText("Sesion");
					jMenuSession.setEnabled(false);
					{
						jMenuItemAdmin = new JMenuItem();
						jMenuSession.add(jMenuItemAdmin);
						jMenuItemAdmin.setText("Administrador");
						jMenuItemAdmin.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent evt) {
								adminLogin(evt);
							}
						});
					}
					{
						jMenuItemUser = new JMenuItem();
						jMenuSession.add(jMenuItemUser);
						jMenuItemUser.setText("Novato");
						jMenuItemUser.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent evt) {
								userLogin(evt);
							}
						});
					}
					{
						jMenuItemLogOut = new JMenuItem();
						jMenuSession.add(jMenuItemLogOut);
						jMenuItemLogOut.setText("Cerrar sesion");
						jMenuItemLogOut.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent evt) {
								jMenuItemLogOutActionPerformed(evt);
							}
						});
						this.setEnabled(false);
					}
					{
						jMenuItemExit = new JMenuItem();
						jMenuSession.add(jMenuItemExit);
						jMenuItemExit.setText("Salir");
						jMenuItemExit.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent evt) {
								jMenuItemExitActionPerformed(evt);
							}
						});
					}
				}
				{
					jMenuHelp = new JMenu();
					jMenuBar1.add(jMenuHelp);
					jMenuHelp.setText("Ayuda");
					jMenuHelp.setEnabled(false);
					{
						jMenuItemAbout = new JMenuItem();
						jMenuHelp.add(jMenuItemAbout);
						jMenuItemAbout.setText("Acerca de ...");
						jMenuItemAbout.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent evt) {
								jMenuItemAboutActionPerformed(evt);
							}
						});
					}
				}
			}
			pack();
			this.setSize(792, 649);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	private void adminLogin(ActionEvent evt) {
		jMenuItemAdmin.setEnabled(false);
		jMenuItemUser.setEnabled(false);
		jMenuItemLogOut.setEnabled(true);
		adminWindow = new AdminWindow(this);
	}

	public void addTab(JInternalFrame window) {
		jTabbedPane1.add(window);
	}

	public void removeAllTabs() {
		this.jTabbedPane1.removeAll();
	}

	private void userLogin(ActionEvent evt) {
		jMenuItemAdmin.setEnabled(false);
		jMenuItemUser.setEnabled(false);
		jMenuItemLogOut.setEnabled(true);
		userWindow = new UserWindow(this);
		this.addTab(userWindow);
	}
	
	private void rootWindowClosing(WindowEvent evt) {
		if (!jMenuItemAdmin.isEnabled() || !jMenuItemUser.isEnabled())
			this.setDefaultCloseOperation(javax.swing.JFrame.DO_NOTHING_ON_CLOSE);
		else
			System.exit(0);
	}

	public void enableMenu() {
		jMenuItemAdmin.setEnabled(true);
		jMenuItemUser.setEnabled(true);
		jMenuItemLogOut.setEnabled(false);
	}
	
	private void jMenuItemExitActionPerformed(ActionEvent evt) {
		System.exit(0);
	}
	
	private void jMenuItemAboutActionPerformed(ActionEvent evt) {
		@SuppressWarnings("unused")
		About about = new About();
	}
	
	private void jMenuItemLogOutActionPerformed(ActionEvent evt) {
		if (userWindow != null)
			userWindow.logOut();
		if (adminWindow != null)
			adminWindow.logOut();
	}

}
