package view;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;

/**
* This code was edited or generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a corporation,
* company or business for any purpose whatever) then you
* should purchase a license for each developer using Jigloo.
* Please visit www.cloudgarden.com for details.
* Use of Jigloo implies acceptance of these licensing terms.
* A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
* THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
* LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
*/
public class AcceptMessage extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JButton jButtonOk;
	private JLabel jLabelMessage;
	private JLabel jLabel1;
	private String type="alert";

	public AcceptMessage(String message,int size) {
		initMessage(message, size);
	}
	private void initMessage(String message, int size) {
		initGUI();
		this.setModal(true);
		this.setSize(size, 139);
		jButtonOk.setBounds((size/2)-20, 56, 40, 40);
		jLabelMessage.setText(message);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}
	public AcceptMessage(String message,String type,int size) {
	this.type=type;
	initMessage(message,size);
	}
	
	private void initGUI() {
		try {
			{
				getContentPane().setLayout(null);
				this.setTitle("Mensaje");
				this.setResizable(false);
				{
					jLabel1 = new JLabel();
					getContentPane().add(jLabel1);
					jLabel1.setBounds(10, 21, 35, 28);
					jLabel1.setIcon(new ImageIcon(getClass().getClassLoader().getResource("images/iconos/"+type+".png")));

				}
				{
					jLabelMessage = new JLabel();
					getContentPane().add(jLabelMessage);
					jLabelMessage.setHorizontalAlignment(JLabel.LEFT);
					jLabelMessage.setBounds(49, 28, 900, 21);
				}
				{
					jButtonOk = new JButton();
					getContentPane().add(jButtonOk);
					jButtonOk.setIcon(new ImageIcon(getClass().getClassLoader()
							.getResource("images/iconos/ok.png")));
					jButtonOk.setToolTipText("Aceptar");
					jButtonOk.setBounds(119, 56, 40, 40);
					jButtonOk.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent evt) {
							jButtonOkActionPerformed(evt);
						}
					});
				}
			}
			{
				this.setSize(274, 139);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void jButtonOkActionPerformed(ActionEvent evt) {
		this.dispose();
	}
}
