package view;

import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;

import presenter.WindowPresenter;

public class Window<T extends WindowPresenter> extends javax.swing.JInternalFrame {

	protected Painter painter;

	protected T presenter;

	public T getPresenter() {
		return presenter;
	}

	public void setPresenter(T presenter) {
		this.presenter = presenter;
	}

	private static final long serialVersionUID = 1L;
	protected JTabbedPane tabs = new JTabbedPane();

	public void addTab(String name, JScrollPane panel) {
		tabs.addTab(name, panel);
	}

	protected void delTabs() {
		tabs.removeAll();
	}

	protected boolean isOKOption(int option) {
		return (option == JOptionPane.OK_OPTION || option == JOptionPane.YES_OPTION);
	}

	protected boolean isNOOption(int option) {
		return (option == JOptionPane.NO_OPTION || option == JOptionPane.CANCEL_OPTION || option == JOptionPane.CLOSED_OPTION);
	}

	public String getTabName(int size) {
		return "";
	}

}
