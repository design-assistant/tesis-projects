package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.ListSelectionModel;
import javax.swing.WindowConstants;

import model.data.generalStructure.Design;
import presenter.SimpleUserPresenter;
import presenter.transformation.TransformAction;
import context.ImplementationFactory;

/**
 * This code was edited or generated using CloudGarden's Jigloo SWT/Swing GUI
 * Builder, which is free for non-commercial use. If Jigloo is being used
 * commercially (ie, by a corporation, company or business for any purpose
 * whatever) then you should purchase a license for each developer using Jigloo.
 * Please visit www.cloudgarden.com for details. Use of Jigloo implies
 * acceptance of these licensing terms. A COMMERCIAL LICENSE HAS NOT BEEN
 * PURCHASED FOR THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED LEGALLY FOR
 * ANY CORPORATE OR COMMERCIAL PURPOSE.
 */
public class UserWindow extends Window<SimpleUserPresenter> {

	private static final long serialVersionUID = 1L;
	private JButton jButtonComparar;
	private JScrollPane jScrollPane1;
	private JList jListResults;
	private JScrollPane jScrollPane2;
	private JLabel lblAttributes;
	private JList attributesList;
	private JScrollPane jScrollPane3;
	private JList jListTransformationList;
	private JButton btnOpenFile;
	private JButton btnTransform;
	private boolean compare = true;
	private JButton jButtonLogOut;
	private Comparator father;
	private DefaultListModel jListResultsModel;
	private DefaultListModel jListTransformationListModel;
	private DefaultComboBoxModel attributesModel;
	private List<Design> result = new ArrayList<Design>();
	private JLabel lblResults;
	private JLabel lblActions;
	String[] attributes;
	{
		try {
			javax.swing.UIManager
					.setLookAndFeel("com.jgoodies.looks.plastic.Plastic3DLookAndFeel");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public UserWindow(Comparator comparator) {
		super();
		presenter = (SimpleUserPresenter) ImplementationFactory
				.getBean("userPresenter");
		initGUI();
		this.father = comparator;
	}

	private void initGUI() {
		try {
			setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
			getContentPane().setLayout(null);
			this.setTitle("Pantalla de usuario");
			this.setResizable(false);
			{
				tabs = new JTabbedPane();
				getContentPane().add(tabs);
				tabs.setBounds(19, 49, 738, 400);
			}
			lblResults=new JLabel("Resultados");
			getContentPane().add(lblResults);
			lblResults.setBounds(19, 450, 200, 15);
			{
				jScrollPane1 = new JScrollPane();
				getContentPane().add(jScrollPane1);
				jScrollPane1.setBounds(19, 464, 136, 100);
				jScrollPane1.getVerticalScrollBar().setEnabled(true);
				jScrollPane1.getHorizontalScrollBar().setEnabled(true);
				{
					jListResultsModel = new DefaultListModel();
					jListResults = new JList();
					jScrollPane1.setViewportView(jListResults);
					jListResults.setModel(jListResultsModel);
					jListResults.setCellRenderer(new MyCellRenderer());
				}
			}
			{
				jButtonLogOut = new JButton();
				getContentPane().add(jButtonLogOut);
				jButtonLogOut.setBounds(446, 7, 40, 40);
				jButtonLogOut.setIcon(new ImageIcon(getClass().getClassLoader()
						.getResource("images/iconos/logout.png")));
				jButtonLogOut.setToolTipText("Cerrar sesi�n");
				jButtonLogOut.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent evt) {
						logOut(evt);
					}
				});
			}
			lblActions=new JLabel("Acciones");
			getContentPane().add(lblActions);
			lblActions.setBounds(205, 450, 114, 15);
			{
				jScrollPane2 = new JScrollPane();
				getContentPane().add(jScrollPane2);
				jScrollPane2.setBounds(205, 464, 430, 100);
			}
			{
				btnTransform = new JButton();
				getContentPane().add(btnTransform);
				btnTransform.setIcon(new ImageIcon(getClass().getClassLoader()
						.getResource("images/iconos/transformar.png")));
				btnTransform.setBounds(159, 494, 42, 42);
				btnTransform.setToolTipText("Transformar Dise�o");
				btnTransform.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent evt) {
						transform(evt);
					}

				});
			}

			{
				jButtonComparar = new JButton();
				getContentPane().add(jButtonComparar);
				jButtonComparar.setBounds(353, 7, 40, 40);
				jButtonComparar.setIcon(new ImageIcon(getClass().getClassLoader()
						.getResource("images/iconos/comparar.png")));
				jButtonComparar.setToolTipText("Configurar comparaci�n");
				jButtonComparar.setEnabled(false);
				jButtonComparar.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent evt) {
						compare(evt);
					}
				});
			}
			{
				btnOpenFile = new JButton();
				getContentPane().add(btnOpenFile);
				btnOpenFile.setIcon(new ImageIcon(getClass().getClassLoader()
						.getResource("images/iconos/abrir.png")));
				btnOpenFile.setToolTipText("Abrir dise�o");
				btnOpenFile.setBounds(260, 7, 40, 40);
				btnOpenFile.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent evt) {
						openFile(evt);
					}
				});
			}
			{
				jScrollPane3 = new JScrollPane();
				getContentPane().add(jScrollPane3);
				jScrollPane3.setBounds(642, 464, 116, 100);
				{
					attributesModel = new DefaultComboBoxModel();
					attributesList = new JList();
					jScrollPane3.setViewportView(attributesList);
					attributesList.setModel(attributesModel);
					attributesList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
				}
			}
			{
				lblAttributes = new JLabel();
				getContentPane().add(lblAttributes);
				lblAttributes.setText("Atributos de Calidad");
				lblAttributes.setEnabled(false);
				lblAttributes.setBounds(643, 450, 142, 15);
			}
			pack();
			this.setSize(818, 650);
			jScrollPane2.getVerticalScrollBar().setEnabled(true);
			jScrollPane2.getVerticalScrollBar().setBounds(0, 0, 0, 0);
			jScrollPane2.getHorizontalScrollBar().setEnabled(true);
			jScrollPane2.getHorizontalScrollBar().setBounds(0, 0, 0, 0);
			{
				jListTransformationListModel = new DefaultListModel();
				jListTransformationList = new JList();
				jScrollPane2.setViewportView(jListTransformationList);
				jListTransformationList.setModel(jListTransformationListModel);
			}
			btnTransform.setEnabled(false);
			lblActions.setEnabled(false);
			lblResults.setEnabled(false);
			lblAttributes.setEnabled(false);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void openFile(ActionEvent evt) {
		boolean canClear = true;
		if (presenter.isActive()) {
			ConfirmMessage message = new ConfirmMessage("Est� a punto de cambiar el dise�o a ser comparado. Desea continuar?",435);
			int answer = message.getAnswer();
			if (isNOOption(answer)) {
				canClear = false;
			} else {
				presenter.clearContents();
				this.delTabs();
				removeTransformations();
				removeResults();
				attributesModel=new DefaultComboBoxModel();
				attributesList.setModel(attributesModel);
			}
		}
		if (canClear) {
			JFileChooser op = new JFileChooser();
			FFilter mtcFilter;
			mtcFilter = new FFilter("mtc", "Entrada Comparador");
			op.addChoosableFileFilter(mtcFilter);
			op.setAcceptAllFileFilterUsed(false);
			int option = op.showOpenDialog(this);
			String rout = "";
			btnTransform.setEnabled(false);
			lblActions.setEnabled(false);
			lblResults.setEnabled(false);
			lblAttributes.setEnabled(false);
			if (isOKOption(option)) {
				rout = op.getSelectedFile().toString();
				presenter.loadDesign(rout);
				painter = new Painter(presenter.getDesign());
				painter.draw(this, 0);
				jButtonComparar.setEnabled(true);

			}
		}
	}

	/**
	 * This method should return an instance of this class which does NOT
	 * initialize it's GUI elements. This method is ONLY required by Jigloo if
	 * the superclass of this class is abstract or non-public. It is not needed
	 * in any other situation.
	 */
	public static Object getGUIBuilderInstance() {
		return new UserWindow(Boolean.FALSE);
	}

	/**
	 * This constructor is used by the getGUIBuilderInstance method to provide
	 * an instance of this class which has not had it's GUI elements initialized
	 * (ie, initGUI is not called in this constructor).
	 */
	public UserWindow(Boolean initGUI) {
		super();
	}

	public void setQualityAttributes(List<String> attributes,
			int qualityAttributeCount, int qualityAttributeKindOfMatching) {
		presenter.setQualityAttributes(attributes, qualityAttributeCount,
				qualityAttributeKindOfMatching);
	}

	private void compare(ActionEvent evt) {
		removeResults();
		removeTransformations();
		btnTransform.setEnabled(false);
		btnTransform.setEnabled(false);
		lblActions.setEnabled(false);
		lblAttributes.setEnabled(false);
		lblResults.setEnabled(false);
		JFrame frame = new JFrame();
		UserComparitionSettingsDialog inst = new UserComparitionSettingsDialog(
				frame, this);
		inst.setVisible(true);
		if (compare) {
			presenter.compare();
			result = presenter.getFinalResult();
			attributesModel= new DefaultComboBoxModel(presenter.getAttributes().toArray());
			attributesList.setModel(attributesModel);
			ListItem li;
			if (result.size() > 0)
			{
				for (int i = 0; i < result.size(); i++) {
					if (result.get(i).getSuccess() >= 7)
						li = new ListComparationItem(
								new ImageIcon(getClass().getClassLoader()
										.getResource("images/verde.gif")),
								"La transformaci�n lo aproxima a un buen dise�o para los atributos buscados","ok");
					else if (result.get(i).getSuccess() <= 3)
						li = new ListComparationItem(
								new ImageIcon(getClass().getClassLoader()
										.getResource("images/rojo.gif")),
								"La transformaci�n lo aproxima a un mal dise�o para los atributos buscados","cancelar");
					else
						li = new ListComparationItem(new ImageIcon(getClass().getClassLoader()
								.getResource("images/amarillo.gif")),
								"La transformaci�n lo aproxima a un dise�o regular para los atributos buscados","alert");
					li.setValue(result.get(i).getName());
					jListResultsModel.addElement(li);
				}
	
				btnTransform.setEnabled(true);
				lblActions.setEnabled(true);
				lblAttributes.setEnabled(true);
				lblResults.setEnabled(true);
			}
			else{
				@SuppressWarnings("unused")
				AcceptMessage message = new AcceptMessage("La b�squeda realizada no retorna resultados",300);
			}
			
		} else
			compare = true;
	}

	/**
	 * Este metodo limpia las variables que se usan para armar el query para
	 * traer PlainDesignBox de la BD
	 */
	public void cleanComparationSettings() {
		presenter.clearViewComparationSettings();

	}

	public void setComponentsCountLimit(double componentsCountLimit) {
		presenter.setComponentsCountLimit(componentsCountLimit);

	}

	public void setAbsorbedRelationsLimit(double absorbedRelationsLimit) {
		presenter.setAbsorbedRelationsLimit(absorbedRelationsLimit);

	}

	public void dontCompare() {
		compare = false;
	}

	private void logOut(ActionEvent evt) {
		this.father.removeAllTabs();
		this.father.enableMenu();
		this.presenter.clearContents();
	}

	private void transform(ActionEvent evt) {
		@SuppressWarnings("unused")
		AcceptMessage message;
		if (jListResults.getSelectedIndex() < 0)
			message = new AcceptMessage("Debe seleccionar un dise�o",220);
		else {
			removeTransformations();
			List<TransformAction> transformations = this.presenter
					.getTransformations(result.get(jListResults
							.getSelectedIndex()));
			for (int i = 0; i < transformations.size(); i++) {
				jListTransformationListModel.addElement(transformations.get(i)
						.toString());
			}
			painter = new Painter(result.get(jListResults.getSelectedIndex()));
			painter.draw(this, tabs.getTabCount());
			ListComparationItem item =(ListComparationItem)jListResultsModel
			.get(jListResults.getSelectedIndex());
			message = new AcceptMessage(item.getAlert(),item.getAlertType(),480);
		}
	}

	private void removeTransformations() {
		for (int i = 0; i < jListTransformationListModel.size(); i++) {
			jListTransformationListModel.remove(i);
			i--;
		}
	}

	private void removeResults() {
		for (int i = 0; i < jListResultsModel.size(); i++) {
			jListResultsModel.remove(i);
			i--;
		}
	}

	@Override
	public String getTabName(int size) {
		if (size == 0)
			return "Diagrama a comparar";
		else
			return "Diagrama a transformar " + size;
	}

	public void logOut() {
		logOut(null);
	}

	public String[] getAttributes() {
		return attributes;
	}

	public void setAttributes(String[] attributes) {
		this.attributes = attributes;
	}
	
	
}
