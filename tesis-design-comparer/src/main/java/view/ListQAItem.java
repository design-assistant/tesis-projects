package view;

import javax.swing.ImageIcon;

import model.group.QualityAttribute;

public class ListQAItem extends ListItem{
	
	private QualityAttribute qa;

	public ListQAItem(ImageIcon icon) {
		this.icon = icon;
	}
	
	public ListQAItem(QualityAttribute qa,ImageIcon icon) {
		this.icon = icon;
		this.value = qa.getName();
		this.qa = qa;
	}

	public QualityAttribute getQa() {
		return qa;
	}

	public void setQa(QualityAttribute qa) {
		this.qa = qa;
		this.value = qa.getName();
	}
		

}
