# SQL Manager 2010 Lite for MySQL 4.5.1.3
# ---------------------------------------
# Host     : localhost
# Port     : 3306
# Database : tesis_db_3


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES latin1 */;

SET FOREIGN_KEY_CHECKS=0;

DROP DATABASE IF EXISTS `tesis_db_3`;

CREATE DATABASE `tesis_db_3`
    CHARACTER SET 'latin1'
    COLLATE 'latin1_swedish_ci';

USE `tesis_db_3`;

#
# Structure for the `agregation` table : 
#

CREATE TABLE `agregation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=96 DEFAULT CHARSET=latin1;

#
# Structure for the `asociation` table : 
#

CREATE TABLE `asociation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=128 DEFAULT CHARSET=latin1;

#
# Structure for the `attribute` table : 
#

CREATE TABLE `attribute` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) DEFAULT NULL,
  `typ` varchar(20) DEFAULT NULL,
  `ambient` varchar(20) DEFAULT NULL,
  `componentId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

#
# Structure for the `boxcomponent` table : 
#

CREATE TABLE `boxcomponent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=133 DEFAULT CHARSET=latin1;

#
# Structure for the `class` table : 
#

CREATE TABLE `class` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `abst` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=133 DEFAULT CHARSET=latin1;

#
# Structure for the `classdiagram` table : 
#

CREATE TABLE `classdiagram` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

#
# Structure for the `classdiagramcomponent` table : 
#

CREATE TABLE `classdiagramcomponent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=133 DEFAULT CHARSET=latin1;

#
# Structure for the `component` table : 
#

CREATE TABLE `component` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `compname` varchar(50) DEFAULT NULL,
  `path` varchar(100) DEFAULT NULL,
  `diagramId` int(11) DEFAULT NULL,
  `plainDesignId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=133 DEFAULT CHARSET=latin1;

#
# Structure for the `composition` table : 
#

CREATE TABLE `composition` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=50 DEFAULT CHARSET=latin1;

#
# Structure for the `dependence` table : 
#

CREATE TABLE `dependence` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=97 DEFAULT CHARSET=latin1;

#
# Structure for the `design` table : 
#

CREATE TABLE `design` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

#
# Structure for the `diagram` table : 
#

CREATE TABLE `diagram` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `diagname` varchar(50) DEFAULT NULL,
  `designId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

#
# Structure for the `implementation` table : 
#

CREATE TABLE `implementation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=91 DEFAULT CHARSET=latin1;

#
# Structure for the `inheritance` table : 
#

CREATE TABLE `inheritance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=127 DEFAULT CHARSET=latin1;

#
# Structure for the `interface` table : 
#

CREATE TABLE `interface` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=102 DEFAULT CHARSET=latin1;

#
# Structure for the `method` table : 
#

CREATE TABLE `method` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) DEFAULT NULL,
  `ambient` varchar(20) DEFAULT NULL,
  `typ` varchar(20) DEFAULT NULL,
  `abst` bit(1) NOT NULL DEFAULT b'0',
  `parameters` varchar(100) DEFAULT NULL,
  `componentId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=46 DEFAULT CHARSET=latin1;

#
# Structure for the `package` table : 
#

CREATE TABLE `package` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

#
# Structure for the `plaindesign` table : 
#

CREATE TABLE `plaindesign` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(4) DEFAULT NULL,
  `boxId` int(11) DEFAULT NULL,
  `absorbedRelations` int(11) NOT NULL DEFAULT '0',
  `componentsCount` int(11) NOT NULL DEFAULT '0',
  `internalAbsorbedRelations` int(11) DEFAULT NULL,
  `internalUseAbsorbedRelations` int(11) DEFAULT NULL,
  `useAbsorbedRelations` int(11) DEFAULT NULL,
  `multipleUseAbsorbedByComponent` int(11) DEFAULT NULL,
  `multipleAbsorbedByComponent` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

#
# Structure for the `plaindesignbox` table : 
#

CREATE TABLE `plaindesignbox` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `success` float NOT NULL DEFAULT '5',
  `designId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

#
# Structure for the `qualityattribute` table : 
#

CREATE TABLE `qualityattribute` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=66 DEFAULT CHARSET=latin1;

#
# Structure for the `qualityattributegroup` table : 
#

CREATE TABLE `qualityattributegroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `success` int(2) NOT NULL DEFAULT '5',
  `attributeId` int(11) NOT NULL DEFAULT '0',
  `designId` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

#
# Structure for the `relation` table : 
#

CREATE TABLE `relation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sourceId` int(11) DEFAULT NULL,
  `targetId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=128 DEFAULT CHARSET=latin1;

#
# Structure for the `user` table : 
#

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(50) NOT NULL DEFAULT 'user',
  `pass` varchar(50) NOT NULL DEFAULT 'pass',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=18446744073709551615 DEFAULT CHARSET=latin1;

#
# Data for the `agregation` table  (LIMIT 0,500)
#

INSERT INTO `agregation` (`id`) VALUES 
  (95);
COMMIT;

#
# Data for the `asociation` table  (LIMIT 0,500)
#

INSERT INTO `asociation` (`id`) VALUES 
  (3),
  (9),
  (13),
  (14),
  (16),
  (19),
  (33),
  (37),
  (39),
  (59),
  (60),
  (70),
  (72),
  (74),
  (76),
  (77),
  (93),
  (97),
  (122),
  (125),
  (127);
COMMIT;

#
# Data for the `attribute` table  (LIMIT 0,500)
#

INSERT INTO `attribute` (`id`, `name`, `typ`, `ambient`, `componentId`) VALUES 
  (1,'classOfProductA2',NULL,NULL,22),
  (2,'classOfProductB2',NULL,NULL,22),
  (3,'classOfProductB1',NULL,NULL,23),
  (4,'classOfProductA1',NULL,NULL,23),
  (5,'classOfAbstractProductB',NULL,NULL,30),
  (6,'classOfAbstractProductA',NULL,NULL,30),
  (7,'classOfSmallerB',NULL,NULL,42),
  (8,'classOfSmallerE',NULL,NULL,42),
  (9,'classOfSmallerD',NULL,NULL,42),
  (10,'classOfSmallerB',NULL,NULL,43),
  (11,'classOfSmallerE',NULL,NULL,43),
  (12,'classOfSmallerD',NULL,NULL,43),
  (13,'collectionOfBuilder',NULL,NULL,51),
  (14,'collectionOfBuilder',NULL,NULL,53),
  (15,'classOfHandler',NULL,NULL,64),
  (16,'classOfHandler',NULL,NULL,66),
  (17,'classOfHandler',NULL,NULL,67),
  (18,'classOfA',NULL,NULL,78),
  (19,'classOfE',NULL,NULL,79),
  (20,'classOfD',NULL,NULL,80),
  (21,'classOfC',NULL,NULL,81),
  (22,'classOfB',NULL,NULL,82),
  (23,'classOfA',NULL,NULL,83),
  (24,'classOfE',NULL,NULL,84),
  (25,'classOfD',NULL,NULL,85),
  (26,'classOfC',NULL,NULL,86),
  (27,'classOfB',NULL,NULL,87),
  (28,'collectionOfCommand',NULL,NULL,98),
  (29,'classOfReceiver',NULL,NULL,99),
  (30,'classOfReceiver',NULL,NULL,100),
  (31,'collectionOfCommand',NULL,NULL,102),
  (32,'classOfReceiver',NULL,NULL,103),
  (33,'classOfConcreteColleagueA',NULL,NULL,130),
  (34,'classOfConcreteColleagueB',NULL,NULL,130),
  (35,'classOfMediator',NULL,NULL,131);
COMMIT;

#
# Data for the `boxcomponent` table  (LIMIT 0,500)
#

INSERT INTO `boxcomponent` (`id`) VALUES 
  (1),
  (2),
  (4),
  (5),
  (6),
  (8),
  (10),
  (12),
  (15),
  (22),
  (23),
  (24),
  (25),
  (26),
  (27),
  (28),
  (29),
  (30),
  (31),
  (32),
  (34),
  (36),
  (38),
  (40),
  (42),
  (43),
  (44),
  (45),
  (46),
  (47),
  (51),
  (52),
  (53),
  (54),
  (55),
  (56),
  (58),
  (61),
  (63),
  (64),
  (65),
  (66),
  (67),
  (68),
  (69),
  (71),
  (73),
  (75),
  (78),
  (79),
  (80),
  (81),
  (82),
  (83),
  (84),
  (85),
  (86),
  (87),
  (88),
  (89),
  (91),
  (92),
  (94),
  (98),
  (99),
  (100),
  (101),
  (102),
  (103),
  (104),
  (105),
  (107),
  (108),
  (109),
  (113),
  (114),
  (115),
  (116),
  (117),
  (118),
  (120),
  (121),
  (124),
  (128),
  (129),
  (130),
  (131),
  (132);
COMMIT;

#
# Data for the `class` table  (LIMIT 0,500)
#

INSERT INTO `class` (`id`, `abst`) VALUES 
  (1,0),
  (2,0),
  (4,0),
  (5,1),
  (6,0),
  (8,0),
  (10,0),
  (12,0),
  (15,0),
  (22,0),
  (23,0),
  (24,0),
  (25,0),
  (26,0),
  (27,0),
  (28,0),
  (29,0),
  (30,1),
  (31,0),
  (32,0),
  (34,0),
  (36,0),
  (38,0),
  (40,0),
  (42,0),
  (43,0),
  (44,0),
  (45,0),
  (46,0),
  (47,0),
  (51,0),
  (52,0),
  (53,0),
  (54,0),
  (55,0),
  (56,0),
  (58,0),
  (61,0),
  (63,0),
  (64,0),
  (65,0),
  (66,0),
  (67,0),
  (68,0),
  (69,0),
  (71,0),
  (73,0),
  (75,0),
  (78,0),
  (79,0),
  (80,0),
  (81,0),
  (82,0),
  (83,0),
  (84,0),
  (85,0),
  (86,0),
  (87,0),
  (89,0),
  (91,0),
  (92,0),
  (94,0),
  (98,0),
  (99,0),
  (100,0),
  (102,0),
  (103,0),
  (104,1),
  (105,0),
  (107,0),
  (108,0),
  (109,1),
  (113,0),
  (114,0),
  (115,0),
  (116,1),
  (117,0),
  (118,0),
  (120,0),
  (121,0),
  (124,0),
  (128,0),
  (129,0),
  (130,0),
  (131,0),
  (132,0);
COMMIT;

#
# Data for the `classdiagram` table  (LIMIT 0,500)
#

INSERT INTO `classdiagram` (`id`) VALUES 
  (1),
  (2),
  (3),
  (4),
  (5),
  (6),
  (7),
  (8);
COMMIT;

#
# Data for the `classdiagramcomponent` table  (LIMIT 0,500)
#

INSERT INTO `classdiagramcomponent` (`id`) VALUES 
  (1),
  (2),
  (3),
  (4),
  (5),
  (6),
  (7),
  (8),
  (9),
  (10),
  (11),
  (12),
  (13),
  (14),
  (15),
  (16),
  (17),
  (18),
  (19),
  (20),
  (21),
  (22),
  (23),
  (24),
  (25),
  (26),
  (27),
  (28),
  (29),
  (30),
  (31),
  (32),
  (33),
  (34),
  (35),
  (36),
  (37),
  (38),
  (39),
  (40),
  (41),
  (42),
  (43),
  (44),
  (45),
  (46),
  (47),
  (48),
  (49),
  (50),
  (51),
  (52),
  (53),
  (54),
  (55),
  (56),
  (57),
  (58),
  (59),
  (60),
  (61),
  (62),
  (63),
  (64),
  (65),
  (66),
  (67),
  (68),
  (69),
  (70),
  (71),
  (72),
  (73),
  (74),
  (75),
  (76),
  (77),
  (78),
  (79),
  (80),
  (81),
  (82),
  (83),
  (84),
  (85),
  (86),
  (87),
  (88),
  (89),
  (90),
  (91),
  (92),
  (93),
  (94),
  (95),
  (96),
  (97),
  (98),
  (99),
  (100),
  (101),
  (102),
  (103),
  (104),
  (105),
  (106),
  (107),
  (108),
  (109),
  (110),
  (111),
  (112),
  (113),
  (114),
  (115),
  (116),
  (117),
  (118),
  (119),
  (120),
  (121),
  (122),
  (123),
  (124),
  (125),
  (126),
  (127),
  (128),
  (129),
  (130),
  (131),
  (132);
COMMIT;

#
# Data for the `component` table  (LIMIT 0,500)
#

INSERT INTO `component` (`id`, `compname`, `path`, `diagramId`, `plainDesignId`) VALUES 
  (1,'ConcreteFactoryA',NULL,1,NULL),
  (2,'ProductA1',NULL,1,NULL),
  (3,'fffhhh',NULL,1,NULL),
  (4,'ProductA2',NULL,1,NULL),
  (5,'AbstractFactory',NULL,1,NULL),
  (6,'ConcreteFactoryB',NULL,1,NULL),
  (7,'fd',NULL,1,NULL),
  (8,'AbstractProductB',NULL,1,NULL),
  (9,'fdff',NULL,1,NULL),
  (10,'ProductB1',NULL,1,NULL),
  (11,'fdfd',NULL,1,NULL),
  (12,'ProductB2',NULL,1,NULL),
  (13,'gf',NULL,1,NULL),
  (14,'gfffff',NULL,1,NULL),
  (15,'AbstractProductA',NULL,1,NULL),
  (16,'gfff',NULL,1,NULL),
  (17,'cx',NULL,1,NULL),
  (18,'bd',NULL,1,NULL),
  (19,'hf',NULL,1,NULL),
  (20,'gfgf',NULL,1,NULL),
  (21,'bvbv',NULL,1,NULL),
  (22,'ConcreteFactoryB',NULL,NULL,1),
  (23,'ConcreteFactoryA',NULL,NULL,1),
  (24,'ProductA2',NULL,NULL,1),
  (25,'ProductA1',NULL,NULL,1),
  (26,'ProductB2',NULL,NULL,1),
  (27,'ProductB1',NULL,NULL,1),
  (28,'AbstractProductB',NULL,NULL,2),
  (29,'AbstractProductA',NULL,NULL,2),
  (30,'AbstractFactory',NULL,NULL,2),
  (31,'Biggest',NULL,2,NULL),
  (32,'SmallerD',NULL,2,NULL),
  (33,'hhh',NULL,2,NULL),
  (34,'SmallerA',NULL,2,NULL),
  (35,'sss',NULL,2,NULL),
  (36,'SmallerB',NULL,2,NULL),
  (37,'aaa',NULL,2,NULL),
  (38,'SmallerE',NULL,2,NULL),
  (39,'aaaa',NULL,2,NULL),
  (40,'SmallerC',NULL,2,NULL),
  (41,'aa',NULL,2,NULL),
  (42,'Biggest',NULL,NULL,3),
  (43,'Biggest',NULL,NULL,4),
  (44,'Product',NULL,3,NULL),
  (45,'Director',NULL,3,NULL),
  (46,'Builder',NULL,3,NULL),
  (47,'ConcreteBuilder',NULL,3,NULL),
  (48,'fdfdf',NULL,3,NULL),
  (49,'fdffdff',NULL,3,NULL),
  (50,'fdfd',NULL,3,NULL),
  (51,'Director',NULL,NULL,5),
  (52,'ConcreteBuilder',NULL,NULL,5),
  (53,'Director',NULL,NULL,6),
  (54,'Builder',NULL,NULL,6),
  (55,'Handler',NULL,4,NULL),
  (56,'ConcreteHandlerA',NULL,4,NULL),
  (57,'dadsa',NULL,4,NULL),
  (58,'Client',NULL,4,NULL),
  (59,'dadsda',NULL,4,NULL),
  (60,'dasdsa',NULL,4,NULL),
  (61,'ConcreteHandlerB',NULL,4,NULL),
  (62,'dasdad',NULL,4,NULL),
  (63,'ConcreteHandlerB',NULL,NULL,7),
  (64,'Client',NULL,NULL,7),
  (65,'ConcreteHandlerA',NULL,NULL,7),
  (66,'Client',NULL,NULL,8),
  (67,'Handler',NULL,NULL,8),
  (68,'E',NULL,5,NULL),
  (69,'A',NULL,5,NULL),
  (70,'fd',NULL,5,NULL),
  (71,'D',NULL,5,NULL),
  (72,'fdf',NULL,5,NULL),
  (73,'C',NULL,5,NULL),
  (74,'dd',NULL,5,NULL),
  (75,'B',NULL,5,NULL),
  (76,'ss',NULL,5,NULL),
  (77,'aa',NULL,5,NULL),
  (78,'E',NULL,NULL,9),
  (79,'D',NULL,NULL,9),
  (80,'C',NULL,NULL,9),
  (81,'B',NULL,NULL,9),
  (82,'A',NULL,NULL,9),
  (83,'E',NULL,NULL,10),
  (84,'D',NULL,NULL,10),
  (85,'C',NULL,NULL,10),
  (86,'B',NULL,NULL,10),
  (87,'A',NULL,NULL,10),
  (88,'Command',NULL,6,NULL),
  (89,'ConcreteCommand',NULL,6,NULL),
  (90,'fdf',NULL,6,NULL),
  (91,'Client',NULL,6,NULL),
  (92,'Receiver',NULL,6,NULL),
  (93,'fdfdf',NULL,6,NULL),
  (94,'Invoker',NULL,6,NULL),
  (95,'fvdc',NULL,6,NULL),
  (96,'fdfdfdfdfdf',NULL,6,NULL),
  (97,'fdfd',NULL,6,NULL),
  (98,'Invoker',NULL,NULL,11),
  (99,'ConcreteCommand',NULL,NULL,11),
  (100,'Client',NULL,NULL,11),
  (101,'Command',NULL,NULL,12),
  (102,'Invoker',NULL,NULL,12),
  (103,'Client',NULL,NULL,12),
  (104,'Component',NULL,7,NULL),
  (105,'ConcreteComponent',NULL,7,NULL),
  (106,'ds',NULL,7,NULL),
  (107,'ConcreteDecoratorB',NULL,7,NULL),
  (108,'ConcreteDecoratorA',NULL,7,NULL),
  (109,'Decorator',NULL,7,NULL),
  (110,'sa',NULL,7,NULL),
  (111,'dsds',NULL,7,NULL),
  (112,'aa',NULL,7,NULL),
  (113,'ConcreteDecoratorB',NULL,NULL,13),
  (114,'ConcreteComponent',NULL,NULL,13),
  (115,'ConcreteDecoratorA',NULL,NULL,13),
  (116,'Component',NULL,NULL,14),
  (117,'Colleague',NULL,8,NULL),
  (118,'ConcreteColleagueA',NULL,8,NULL),
  (119,'fsfdsf',NULL,8,NULL),
  (120,'ConcreteMediator',NULL,8,NULL),
  (121,'ConcreteColleagueB',NULL,8,NULL),
  (122,'fdsfdsrer',NULL,8,NULL),
  (123,'fsdfsd',NULL,8,NULL),
  (124,'Mediator',NULL,8,NULL),
  (125,'dadas',NULL,8,NULL),
  (126,'dsd',NULL,8,NULL),
  (127,'fsfds',NULL,8,NULL),
  (128,'ConcreteColleagueB',NULL,NULL,15),
  (129,'ConcreteColleagueA',NULL,NULL,15),
  (130,'ConcreteMediator',NULL,NULL,15),
  (131,'Colleague',NULL,NULL,16),
  (132,'Mediator',NULL,NULL,16);
COMMIT;

#
# Data for the `composition` table  (LIMIT 0,500)
#

INSERT INTO `composition` (`id`) VALUES 
  (49);
COMMIT;

#
# Data for the `dependence` table  (LIMIT 0,500)
#

INSERT INTO `dependence` (`id`) VALUES 
  (35),
  (41),
  (48),
  (96);
COMMIT;

#
# Data for the `design` table  (LIMIT 0,500)
#

INSERT INTO `design` (`id`, `name`) VALUES 
  (1,'Abstract Factory'),
  (2,'Big Object'),
  (3,'Builder'),
  (4,'Chain'),
  (5,'Circular dependence'),
  (6,'Command'),
  (7,'Decorator'),
  (8,'Mediator');
COMMIT;

#
# Data for the `diagram` table  (LIMIT 0,500)
#

INSERT INTO `diagram` (`id`, `diagname`, `designId`) VALUES 
  (1,'estructura',1),
  (2,'structure',2),
  (3,'estructura',3),
  (4,'estructura',4),
  (5,'structure',5),
  (6,'estructura',6),
  (7,'estructura',7),
  (8,'Colleague',8);
COMMIT;

#
# Data for the `implementation` table  (LIMIT 0,500)
#

INSERT INTO `implementation` (`id`) VALUES 
  (90);
COMMIT;

#
# Data for the `inheritance` table  (LIMIT 0,500)
#

INSERT INTO `inheritance` (`id`) VALUES 
  (7),
  (11),
  (17),
  (18),
  (20),
  (21),
  (50),
  (57),
  (62),
  (106),
  (110),
  (111),
  (112),
  (119),
  (123),
  (126);
COMMIT;

#
# Data for the `interface` table  (LIMIT 0,500)
#

INSERT INTO `interface` (`id`) VALUES 
  (88),
  (101);
COMMIT;

#
# Data for the `method` table  (LIMIT 0,500)
#

INSERT INTO `method` (`id`, `name`, `ambient`, `typ`, `abst`, `parameters`, `componentId`) VALUES 
  (1,'createProductB','-','void',0,'',1),
  (2,'createProductA','-','void',0,'',1),
  (3,'CrateProductA','-','void',1,'',5),
  (4,'CreateProductB','-','void',1,'',5),
  (5,'createProductB','-','void',0,'',6),
  (6,'createProductA','-','void',0,'',6),
  (7,'createProductB','-','void',0,'',22),
  (8,'createProductA','-','void',0,'',22),
  (9,'createProductB','-','void',0,'',23),
  (10,'createProductA','-','void',0,'',23),
  (11,'CrateProductA','-','void',1,'',30),
  (12,'CreateProductB','-','void',1,'',30),
  (13,'useOfSmallerC',NULL,NULL,0,'',42),
  (14,'useOfSmallerA',NULL,NULL,0,'',42),
  (15,'useOfSmallerC',NULL,NULL,0,'',43),
  (16,'useOfSmallerA',NULL,NULL,0,'',43),
  (17,'construct','-','void',0,'',45),
  (18,'BuildPart','-','void',0,'',46),
  (19,'BuildPart','-','void',0,'',47),
  (20,'construct','-','void',0,'',53),
  (21,'BuildPart','-','void',0,'',52),
  (22,'useOfProduct',NULL,NULL,0,'',52),
  (23,'BuildPart','-','void',0,'',54),
  (24,'request','-','void',0,'',55),
  (25,'request','-','void',0,'',56),
  (26,'request','-','void',0,'',61),
  (27,'request','-','void',0,'',63),
  (28,'request','-','void',0,'',65),
  (29,'request','-','void',0,'',67),
  (30,'execute','-','void',0,'',88),
  (31,'execute','-','void',0,'',89),
  (32,'action','-','void',0,'',92),
  (33,'execute','-','void',0,'',99),
  (34,'useOfConcreteCommand',NULL,NULL,0,'',100),
  (35,'execute','-','void',0,'',101),
  (36,'useOfConcreteCommand',NULL,NULL,0,'',103),
  (37,'operation','-','void',1,'',104),
  (38,'operation','-','void',0,'',105),
  (39,'operation','-','void',0,'',107),
  (40,'operation','-','void',0,'',108),
  (41,'operation','-','void',1,'',109),
  (42,'operation','-','void',0,'',113),
  (43,'operation','-','void',0,'',114),
  (44,'operation','-','void',0,'',115),
  (45,'operation','-','void',1,'',116);
COMMIT;

#
# Data for the `plaindesign` table  (LIMIT 0,500)
#

INSERT INTO `plaindesign` (`id`, `name`, `boxId`, `absorbedRelations`, `componentsCount`, `internalAbsorbedRelations`, `internalUseAbsorbedRelations`, `useAbsorbedRelations`, `multipleUseAbsorbedByComponent`, `multipleAbsorbedByComponent`) VALUES 
  (1,'down',1,4,6,4,0,0,0,4),
  (2,'up',1,2,3,2,0,0,0,2),
  (3,'down',2,5,1,0,0,2,2,3),
  (4,'up',2,5,1,0,0,2,2,3),
  (5,'down',3,2,2,0,0,1,0,0),
  (6,'up',3,1,2,1,0,0,0,0),
  (7,'down',4,1,3,0,0,0,0,0),
  (8,'up',4,2,2,2,0,0,0,0),
  (9,'down',5,5,5,5,0,0,0,0),
  (10,'up',5,5,5,5,0,0,0,0),
  (11,'down',6,4,3,0,1,1,0,0),
  (12,'up',6,3,3,1,0,1,0,0),
  (13,'down',7,0,3,0,0,0,0,0),
  (14,'up',7,0,1,0,0,0,0,0),
  (15,'down',8,2,3,2,0,0,0,2),
  (16,'up',8,1,2,1,0,0,0,0);
COMMIT;

#
# Data for the `plaindesignbox` table  (LIMIT 0,500)
#

INSERT INTO `plaindesignbox` (`id`, `success`, `designId`) VALUES 
  (1,5,1),
  (2,5,2),
  (3,5,3),
  (4,5,4),
  (5,5,5),
  (6,5,6),
  (7,5,7),
  (8,5,8);
COMMIT;

#
# Data for the `qualityattribute` table  (LIMIT 0,500)
#

INSERT INTO `qualityattribute` (`id`, `name`) VALUES 
  (2,'accountability'),
  (3,'accuracy'),
  (4,'adaptability'),
  (5,'administrability'),
  (6,'affordability'),
  (7,'auditability'),
  (8,'availability'),
  (9,'credibility'),
  (10,'standards compliance'),
  (11,'process capabilities'),
  (12,'compatibility'),
  (13,'composability'),
  (14,'configurability'),
  (15,'correctness'),
  (16,'customizability'),
  (17,'degradability'),
  (18,'demonstrability'),
  (19,'dependability'),
  (20,'deployability'),
  (21,'distributability'),
  (22,'durability'),
  (23,'effectiveness'),
  (24,'efficiency'),
  (25,'evolvability'),
  (26,'extensibility'),
  (27,'fidelity'),
  (28,'flexibility'),
  (29,'installability'),
  (30,'Integrity'),
  (31,'interchangeability'),
  (32,'interoperability'),
  (33,'learnability'),
  (34,'maintainability'),
  (35,'manageability'),
  (36,'mobility'),
  (37,'modularity'),
  (38,'nomadicity'),
  (39,'operability'),
  (40,'portability'),
  (41,'precision'),
  (42,'predictability'),
  (43,'recoverability'),
  (44,'relevance'),
  (45,'reliability'),
  (46,'repeatability'),
  (47,'reproducibility'),
  (48,'responsiveness'),
  (49,'reusability'),
  (50,'robustness'),
  (51,'safety'),
  (52,'scalability'),
  (53,'seamlessness'),
  (54,'serviceability'),
  (55,'securability'),
  (56,'simplicity'),
  (57,'stability'),
  (58,'survivability'),
  (59,'sustainability'),
  (60,'tailorability'),
  (61,'testability'),
  (62,'timeliness'),
  (63,'ubiquity'),
  (64,'understandability'),
  (65,'usability');
COMMIT;

#
# Data for the `qualityattributegroup` table  (LIMIT 0,500)
#

INSERT INTO `qualityattributegroup` (`id`, `success`, `attributeId`, `designId`) VALUES 
  (1,5,4,1),
  (2,5,4,2),
  (3,5,4,3),
  (4,5,4,4),
  (5,5,4,5),
  (6,5,4,6),
  (7,5,4,7),
  (8,5,4,8);
COMMIT;

#
# Data for the `relation` table  (LIMIT 0,500)
#

INSERT INTO `relation` (`id`, `sourceId`, `targetId`) VALUES 
  (3,1,2),
  (7,5,6),
  (9,5,8),
  (11,8,10),
  (13,6,12),
  (14,6,4),
  (16,5,15),
  (17,5,1),
  (18,15,4),
  (19,1,10),
  (20,8,12),
  (21,15,2),
  (33,31,32),
  (35,31,34),
  (37,31,36),
  (39,31,38),
  (41,31,40),
  (48,47,44),
  (49,45,46),
  (50,46,47),
  (57,55,56),
  (59,58,55),
  (60,55,55),
  (62,55,61),
  (70,68,69),
  (72,71,68),
  (74,73,71),
  (76,75,73),
  (77,69,75),
  (90,88,89),
  (93,91,92),
  (95,94,88),
  (96,91,89),
  (97,89,92),
  (106,104,105),
  (110,109,107),
  (111,104,109),
  (112,109,108),
  (119,117,118),
  (122,120,121),
  (123,117,121),
  (125,117,124),
  (126,124,120),
  (127,120,118);
COMMIT;

#
# Data for the `user` table  (LIMIT 0,500)
#

INSERT INTO `user` (`id`, `user`, `pass`) VALUES 
  (1,'admin','123456');
COMMIT;



/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;