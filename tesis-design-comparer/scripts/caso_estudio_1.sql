# SQL Manager 2010 Lite for MySQL 4.5.1.3
# ---------------------------------------
# Host     : localhost
# Port     : 3306
# Database : tesis_db


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES latin1 */;

SET FOREIGN_KEY_CHECKS=0;

DROP DATABASE IF EXISTS `tesis_db`;

CREATE DATABASE `tesis_db`
    CHARACTER SET 'latin1'
    COLLATE 'latin1_swedish_ci';

USE `tesis_db`;

#
# Structure for the `agregation` table : 
#

CREATE TABLE `agregation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=93 DEFAULT CHARSET=latin1;

#
# Structure for the `asociation` table : 
#

CREATE TABLE `asociation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=80 DEFAULT CHARSET=latin1;

#
# Structure for the `attribute` table : 
#

CREATE TABLE `attribute` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) DEFAULT NULL,
  `typ` varchar(20) DEFAULT NULL,
  `ambient` varchar(20) DEFAULT NULL,
  `componentId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

#
# Structure for the `boxcomponent` table : 
#

CREATE TABLE `boxcomponent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=latin1;

#
# Structure for the `class` table : 
#

CREATE TABLE `class` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `abst` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=latin1;

#
# Structure for the `classdiagram` table : 
#

CREATE TABLE `classdiagram` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

#
# Structure for the `classdiagramcomponent` table : 
#

CREATE TABLE `classdiagramcomponent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=latin1;

#
# Structure for the `component` table : 
#

CREATE TABLE `component` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `compname` varchar(50) DEFAULT NULL,
  `path` varchar(100) DEFAULT NULL,
  `diagramId` int(11) DEFAULT NULL,
  `plainDesignId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=latin1;

#
# Structure for the `composition` table : 
#

CREATE TABLE `composition` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=72 DEFAULT CHARSET=latin1;

#
# Structure for the `dependence` table : 
#

CREATE TABLE `dependence` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=60 DEFAULT CHARSET=latin1;

#
# Structure for the `design` table : 
#

CREATE TABLE `design` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

#
# Structure for the `diagram` table : 
#

CREATE TABLE `diagram` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `diagname` varchar(50) DEFAULT NULL,
  `designId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

#
# Structure for the `implementation` table : 
#

CREATE TABLE `implementation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=73 DEFAULT CHARSET=latin1;

#
# Structure for the `inheritance` table : 
#

CREATE TABLE `inheritance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=95 DEFAULT CHARSET=latin1;

#
# Structure for the `interface` table : 
#

CREATE TABLE `interface` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=76 DEFAULT CHARSET=latin1;

#
# Structure for the `method` table : 
#

CREATE TABLE `method` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) DEFAULT NULL,
  `ambient` varchar(20) DEFAULT NULL,
  `typ` varchar(20) DEFAULT NULL,
  `abst` bit(1) NOT NULL DEFAULT b'0',
  `parameters` varchar(100) DEFAULT NULL,
  `componentId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=75 DEFAULT CHARSET=latin1;

#
# Structure for the `package` table : 
#

CREATE TABLE `package` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

#
# Structure for the `plaindesign` table : 
#

CREATE TABLE `plaindesign` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(4) DEFAULT NULL,
  `boxId` int(11) DEFAULT NULL,
  `absorbedRelations` int(11) NOT NULL DEFAULT '0',
  `componentsCount` int(11) NOT NULL DEFAULT '0',
  `internalAbsorbedRelations` int(11) DEFAULT NULL,
  `internalUseAbsorbedRelations` int(11) DEFAULT NULL,
  `useAbsorbedRelations` int(11) DEFAULT NULL,
  `multipleUseAbsorbedByComponent` int(11) DEFAULT NULL,
  `multipleAbsorbedByComponent` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

#
# Structure for the `plaindesignbox` table : 
#

CREATE TABLE `plaindesignbox` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `success` float NOT NULL DEFAULT '5',
  `designId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

#
# Structure for the `qualityattribute` table : 
#

CREATE TABLE `qualityattribute` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=66 DEFAULT CHARSET=latin1;

#
# Structure for the `qualityattributegroup` table : 
#

CREATE TABLE `qualityattributegroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `success` int(2) NOT NULL DEFAULT '5',
  `attributeId` int(11) NOT NULL DEFAULT '0',
  `designId` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

#
# Structure for the `relation` table : 
#

CREATE TABLE `relation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sourceId` int(11) DEFAULT NULL,
  `targetId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=95 DEFAULT CHARSET=latin1;

#
# Structure for the `user` table : 
#

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(50) NOT NULL DEFAULT 'user',
  `pass` varchar(50) NOT NULL DEFAULT 'pass',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=18446744073709551615 DEFAULT CHARSET=latin1;

#
# Data for the `agregation` table  (LIMIT 0,500)
#

INSERT INTO `agregation` (`id`) VALUES 
  (35),
  (92);
COMMIT;

#
# Data for the `asociation` table  (LIMIT 0,500)
#

INSERT INTO `asociation` (`id`) VALUES 
  (3),
  (9),
  (13),
  (14),
  (16),
  (19),
  (69),
  (70),
  (79);
COMMIT;

#
# Data for the `attribute` table  (LIMIT 0,500)
#

INSERT INTO `attribute` (`id`, `name`, `typ`, `ambient`, `componentId`) VALUES 
  (1,'classOfProductA2',NULL,NULL,22),
  (2,'classOfProductB2',NULL,NULL,22),
  (3,'classOfProductB1',NULL,NULL,23),
  (4,'classOfProductA1',NULL,NULL,23),
  (5,'classOfAbstractProductB',NULL,NULL,30),
  (6,'classOfAbstractProductA',NULL,NULL,30),
  (7,'collectionOfComponent',NULL,NULL,38),
  (8,'collectionOfComponent',NULL,NULL,39),
  (9,'productName','String','-',53),
  (10,'productName','String','-',62),
  (11,'classOfConcreteSubject',NULL,NULL,74),
  (12,'classOfSubject',NULL,NULL,75),
  (13,'collectionOfObserver',NULL,NULL,76),
  (14,'classOfRealSubject',NULL,NULL,84),
  (15,'collectionOfStrategy',NULL,NULL,98),
  (16,'collectionOfStrategy',NULL,NULL,99);
COMMIT;

#
# Data for the `boxcomponent` table  (LIMIT 0,500)
#

INSERT INTO `boxcomponent` (`id`) VALUES 
  (1),
  (2),
  (4),
  (5),
  (6),
  (8),
  (10),
  (12),
  (15),
  (22),
  (23),
  (24),
  (25),
  (26),
  (27),
  (28),
  (29),
  (30),
  (31),
  (32),
  (34),
  (37),
  (38),
  (39),
  (40),
  (41),
  (43),
  (44),
  (45),
  (49),
  (50),
  (51),
  (52),
  (53),
  (54),
  (55),
  (57),
  (60),
  (61),
  (62),
  (63),
  (64),
  (65),
  (67),
  (68),
  (73),
  (74),
  (75),
  (76),
  (77),
  (78),
  (80),
  (83),
  (84),
  (85),
  (86),
  (87),
  (89),
  (90),
  (93),
  (95),
  (96),
  (97),
  (98),
  (99),
  (100);
COMMIT;

#
# Data for the `class` table  (LIMIT 0,500)
#

INSERT INTO `class` (`id`, `abst`) VALUES 
  (1,0),
  (2,0),
  (4,0),
  (5,1),
  (6,0),
  (8,0),
  (10,0),
  (12,0),
  (15,0),
  (22,0),
  (23,0),
  (24,0),
  (25,0),
  (26,0),
  (27,0),
  (28,0),
  (29,0),
  (30,1),
  (31,1),
  (32,0),
  (34,0),
  (37,0),
  (38,0),
  (39,1),
  (40,1),
  (41,0),
  (43,0),
  (44,0),
  (45,1),
  (49,0),
  (50,0),
  (51,0),
  (52,1),
  (53,0),
  (54,0),
  (55,1),
  (57,0),
  (60,0),
  (61,0),
  (62,0),
  (63,1),
  (64,0),
  (65,0),
  (67,0),
  (73,0),
  (74,0),
  (76,0),
  (77,0),
  (78,0),
  (80,1),
  (83,0),
  (84,0),
  (85,1),
  (86,1),
  (87,0),
  (89,0),
  (90,0),
  (93,0),
  (95,0),
  (96,0),
  (97,0),
  (98,0),
  (99,0),
  (100,1);
COMMIT;

#
# Data for the `classdiagram` table  (LIMIT 0,500)
#

INSERT INTO `classdiagram` (`id`) VALUES 
  (1),
  (2),
  (3),
  (4),
  (5),
  (6),
  (7);
COMMIT;

#
# Data for the `classdiagramcomponent` table  (LIMIT 0,500)
#

INSERT INTO `classdiagramcomponent` (`id`) VALUES 
  (1),
  (2),
  (3),
  (4),
  (5),
  (6),
  (7),
  (8),
  (9),
  (10),
  (11),
  (12),
  (13),
  (14),
  (15),
  (16),
  (17),
  (18),
  (19),
  (20),
  (21),
  (22),
  (23),
  (24),
  (25),
  (26),
  (27),
  (28),
  (29),
  (30),
  (31),
  (32),
  (33),
  (34),
  (35),
  (36),
  (37),
  (38),
  (39),
  (40),
  (41),
  (42),
  (43),
  (44),
  (45),
  (46),
  (47),
  (48),
  (49),
  (50),
  (51),
  (52),
  (53),
  (54),
  (55),
  (56),
  (57),
  (58),
  (59),
  (60),
  (61),
  (62),
  (63),
  (64),
  (65),
  (66),
  (67),
  (68),
  (69),
  (70),
  (71),
  (72),
  (73),
  (74),
  (75),
  (76),
  (77),
  (78),
  (79),
  (80),
  (81),
  (82),
  (83),
  (84),
  (85),
  (86),
  (87),
  (88),
  (89),
  (90),
  (91),
  (92),
  (93),
  (94),
  (95),
  (96),
  (97),
  (98),
  (99),
  (100);
COMMIT;

#
# Data for the `component` table  (LIMIT 0,500)
#

INSERT INTO `component` (`id`, `compname`, `path`, `diagramId`, `plainDesignId`) VALUES 
  (1,'ConcreteFactoryA',NULL,1,NULL),
  (2,'ProductA1',NULL,1,NULL),
  (3,'fffhhh',NULL,1,NULL),
  (4,'ProductA2',NULL,1,NULL),
  (5,'AbstractFactory',NULL,1,NULL),
  (6,'ConcreteFactoryB',NULL,1,NULL),
  (7,'fd',NULL,1,NULL),
  (8,'AbstractProductB',NULL,1,NULL),
  (9,'fdff',NULL,1,NULL),
  (10,'ProductB1',NULL,1,NULL),
  (11,'fdfd',NULL,1,NULL),
  (12,'ProductB2',NULL,1,NULL),
  (13,'gf',NULL,1,NULL),
  (14,'gfffff',NULL,1,NULL),
  (15,'AbstractProductA',NULL,1,NULL),
  (16,'gfff',NULL,1,NULL),
  (17,'cx',NULL,1,NULL),
  (18,'bd',NULL,1,NULL),
  (19,'hf',NULL,1,NULL),
  (20,'gfgf',NULL,1,NULL),
  (21,'bvbv',NULL,1,NULL),
  (22,'ConcreteFactoryB',NULL,NULL,1),
  (23,'ConcreteFactoryA',NULL,NULL,1),
  (24,'ProductA2',NULL,NULL,1),
  (25,'ProductA1',NULL,NULL,1),
  (26,'ProductB2',NULL,NULL,1),
  (27,'ProductB1',NULL,NULL,1),
  (28,'AbstractProductB',NULL,NULL,2),
  (29,'AbstractProductA',NULL,NULL,2),
  (30,'AbstractFactory',NULL,NULL,2),
  (31,'Component',NULL,2,NULL),
  (32,'Composite',NULL,2,NULL),
  (33,'defgh',NULL,2,NULL),
  (34,'Leaf',NULL,2,NULL),
  (35,'asdfghj',NULL,2,NULL),
  (36,'ds',NULL,2,NULL),
  (37,'Leaf',NULL,NULL,3),
  (38,'Composite',NULL,NULL,3),
  (39,'Component',NULL,NULL,4),
  (40,'Component',NULL,3,NULL),
  (41,'ConcreteComponent',NULL,3,NULL),
  (42,'ds',NULL,3,NULL),
  (43,'ConcreteDecoratorB',NULL,3,NULL),
  (44,'ConcreteDecoratorA',NULL,3,NULL),
  (45,'Decorator',NULL,3,NULL),
  (46,'sa',NULL,3,NULL),
  (47,'dsds',NULL,3,NULL),
  (48,'aa',NULL,3,NULL),
  (49,'ConcreteDecoratorB',NULL,NULL,5),
  (50,'ConcreteComponent',NULL,NULL,5),
  (51,'ConcreteDecoratorA',NULL,NULL,5),
  (52,'Component',NULL,NULL,6),
  (53,'Product',NULL,4,NULL),
  (54,'ConcreteCreator',NULL,4,NULL),
  (55,'Creator',NULL,4,NULL),
  (56,'r3',NULL,4,NULL),
  (57,'ConcreteProduct',NULL,4,NULL),
  (58,'r2',NULL,4,NULL),
  (59,'r1',NULL,4,NULL),
  (60,'ConcreteCreator',NULL,NULL,7),
  (61,'ConcreteProduct',NULL,NULL,7),
  (62,'Product',NULL,NULL,8),
  (63,'Creator',NULL,NULL,8),
  (64,'Subject',NULL,5,NULL),
  (65,'ConcreteSubject',NULL,5,NULL),
  (66,'fd',NULL,5,NULL),
  (67,'ConcreteObserver',NULL,5,NULL),
  (68,'Observer',NULL,5,NULL),
  (69,'csd',NULL,5,NULL),
  (70,'fddf',NULL,5,NULL),
  (71,'fds',NULL,5,NULL),
  (72,'fdfd',NULL,5,NULL),
  (73,'ConcreteSubject',NULL,NULL,9),
  (74,'ConcreteObserver',NULL,NULL,9),
  (75,'Observer',NULL,NULL,10),
  (76,'Subject',NULL,NULL,10),
  (77,'Proxy',NULL,6,NULL),
  (78,'RealSubject',NULL,6,NULL),
  (79,'fdfdf',NULL,6,NULL),
  (80,'Subject',NULL,6,NULL),
  (81,'sdfgh',NULL,6,NULL),
  (82,'asdfgh',NULL,6,NULL),
  (83,'RealSubject',NULL,NULL,11),
  (84,'Proxy',NULL,NULL,11),
  (85,'Subject',NULL,NULL,12),
  (86,'Strategy',NULL,7,NULL),
  (87,'ConcreteStrategyB',NULL,7,NULL),
  (88,'ds',NULL,7,NULL),
  (89,'Context',NULL,7,NULL),
  (90,'ConcreteStrategyA',NULL,7,NULL),
  (91,'d',NULL,7,NULL),
  (92,'dsw',NULL,7,NULL),
  (93,'ConcreteStrategyC',NULL,7,NULL),
  (94,'dss',NULL,7,NULL),
  (95,'ConcreteStrategyC',NULL,NULL,13),
  (96,'ConcreteStrategyB',NULL,NULL,13),
  (97,'ConcreteStrategyA',NULL,NULL,13),
  (98,'Context',NULL,NULL,13),
  (99,'Context',NULL,NULL,14),
  (100,'Strategy',NULL,NULL,14);
COMMIT;

#
# Data for the `composition` table  (LIMIT 0,500)
#

INSERT INTO `composition` (`id`) VALUES 
  (71);
COMMIT;

#
# Data for the `dependence` table  (LIMIT 0,500)
#

INSERT INTO `dependence` (`id`) VALUES 
  (59);
COMMIT;

#
# Data for the `design` table  (LIMIT 0,500)
#

INSERT INTO `design` (`id`, `name`) VALUES 
  (1,'Abstract Factory'),
  (2,'Composite'),
  (3,'Decorator'),
  (4,'Factory Method'),
  (5,'Observer'),
  (6,'Proxy'),
  (7,'Strategy');
COMMIT;

#
# Data for the `diagram` table  (LIMIT 0,500)
#

INSERT INTO `diagram` (`id`, `diagname`, `designId`) VALUES 
  (1,'estructura',1),
  (2,'estructura',2),
  (3,'estructura',3),
  (4,'estructura',4),
  (5,'estructura',5),
  (6,'Subject',6),
  (7,'estructura',7);
COMMIT;

#
# Data for the `implementation` table  (LIMIT 0,500)
#

INSERT INTO `implementation` (`id`) VALUES 
  (72);
COMMIT;

#
# Data for the `inheritance` table  (LIMIT 0,500)
#

INSERT INTO `inheritance` (`id`) VALUES 
  (7),
  (11),
  (17),
  (18),
  (20),
  (21),
  (33),
  (36),
  (42),
  (46),
  (47),
  (48),
  (56),
  (58),
  (66),
  (81),
  (82),
  (88),
  (91),
  (94);
COMMIT;

#
# Data for the `interface` table  (LIMIT 0,500)
#

INSERT INTO `interface` (`id`) VALUES 
  (68),
  (75);
COMMIT;

#
# Data for the `method` table  (LIMIT 0,500)
#

INSERT INTO `method` (`id`, `name`, `ambient`, `typ`, `abst`, `parameters`, `componentId`) VALUES 
  (1,'createProductB','-','void',0,'',1),
  (2,'createProductA','-','void',0,'',1),
  (3,'CrateProductA','-','void',1,'',5),
  (4,'CreateProductB','-','void',1,'',5),
  (5,'createProductB','-','void',0,'',6),
  (6,'createProductA','-','void',0,'',6),
  (7,'createProductB','-','void',0,'',22),
  (8,'createProductA','-','void',0,'',22),
  (9,'createProductB','-','void',0,'',23),
  (10,'createProductA','-','void',0,'',23),
  (11,'CrateProductA','-','void',1,'',30),
  (12,'CreateProductB','-','void',1,'',30),
  (13,'remove','-','void',0,'Component',31),
  (14,'add','-','void',0,'Component',31),
  (15,'operation','-','void',1,'',31),
  (16,'opoeration','-','void',0,'',32),
  (17,'add','-','void',0,'Component',32),
  (18,'remove','-','void',0,'Component',32),
  (19,'operation','-','void',0,'',34),
  (20,'operation','-','void',0,'',37),
  (21,'opoeration','-','void',0,'',38),
  (22,'add','-','void',0,'Component',38),
  (23,'remove','-','void',0,'Component',38),
  (24,'remove','-','void',0,'Component',39),
  (25,'add','-','void',0,'Component',39),
  (26,'operation','-','void',1,'',39),
  (27,'operation','-','void',1,'',40),
  (28,'operation','-','void',0,'',41),
  (29,'operation','-','void',0,'',43),
  (30,'operation','-','void',0,'',44),
  (31,'operation','-','void',1,'',45),
  (32,'operation','-','void',0,'',49),
  (33,'operation','-','void',0,'',50),
  (34,'operation','-','void',0,'',51),
  (35,'operation','-','void',1,'',52),
  (36,'calculateTaxes','-','void',0,'',53),
  (37,'FactoryMethod','-','void',0,'',54),
  (38,'anOperation','-','void',1,'',55),
  (39,'FactoryMethod','-','void',0,'',55),
  (40,'FactoryMethod','-','void',0,'',60),
  (41,'calculateTaxes','-','void',0,'',62),
  (42,'anOperation','-','void',1,'',63),
  (43,'FactoryMethod','-','void',0,'',63),
  (44,'useOfProduct',NULL,NULL,0,'',63),
  (45,'dettach','-','void',0,'',64),
  (46,'notify','-','void',0,'',64),
  (47,'attach','-','void',0,'',64),
  (48,'getState','-','void',0,'',65),
  (49,'setState','-','void',0,'',65),
  (50,'update','-','void',0,'',67),
  (51,'update','-','void',0,'',68),
  (52,'getState','-','void',0,'',73),
  (53,'setState','-','void',0,'',73),
  (54,'update','-','void',0,'',74),
  (55,'update','-','void',0,'',75),
  (56,'dettach','-','void',0,'',76),
  (57,'notify','-','void',0,'',76),
  (58,'attach','-','void',0,'',76),
  (59,'request','-','void',0,'',77),
  (60,'request','-','void',0,'',78),
  (61,'request','-','void',1,'',80),
  (62,'request','-','void',0,'',83),
  (63,'request','-','void',0,'',84),
  (64,'request','-','void',1,'',85),
  (65,'ownAlhorithm','-','void',1,'',86),
  (66,'ownAlgorithm','-','void',0,'',87),
  (67,'run','-','void',0,'',89),
  (68,'ownAlgorithm','-','void',0,'',90),
  (69,'ownAlgorithm','-','void',0,'',93),
  (70,'ownAlgorithm','-','void',0,'',95),
  (71,'ownAlgorithm','-','void',0,'',96),
  (72,'ownAlgorithm','-','void',0,'',97),
  (73,'run','-','void',0,'',99),
  (74,'ownAlhorithm','-','void',1,'',100);
COMMIT;

#
# Data for the `plaindesign` table  (LIMIT 0,500)
#

INSERT INTO `plaindesign` (`id`, `name`, `boxId`, `absorbedRelations`, `componentsCount`, `internalAbsorbedRelations`, `internalUseAbsorbedRelations`, `useAbsorbedRelations`, `multipleUseAbsorbedByComponent`, `multipleAbsorbedByComponent`) VALUES 
  (1,'down',1,4,6,4,0,0,0,4),
  (2,'up',1,2,3,2,0,0,0,2),
  (3,'down',2,1,2,0,0,0,0,0),
  (4,'up',2,1,1,1,0,0,0,0),
  (5,'down',3,0,3,0,0,0,0,0),
  (6,'up',3,0,1,0,0,0,0,0),
  (7,'down',4,0,2,0,0,0,0,0),
  (8,'up',4,1,2,0,1,1,0,0),
  (9,'down',5,1,2,1,0,0,0,0),
  (10,'up',5,2,2,2,0,0,0,0),
  (11,'down',6,1,2,1,0,0,0,0),
  (12,'up',6,0,1,0,0,0,0,0),
  (13,'down',7,1,4,0,0,0,0,0),
  (14,'up',7,1,2,1,0,0,0,0);
COMMIT;

#
# Data for the `plaindesignbox` table  (LIMIT 0,500)
#

INSERT INTO `plaindesignbox` (`id`, `success`, `designId`) VALUES 
  (1,7.25,1),
  (2,8,2),
  (3,6,3),
  (4,9,4),
  (5,3,5),
  (6,4.2,6),
  (7,9.33333,7);
COMMIT;

#
# Data for the `qualityattribute` table  (LIMIT 0,500)
#

INSERT INTO `qualityattribute` (`id`, `name`) VALUES 
  (2,'accountability'),
  (3,'accuracy'),
  (4,'adaptability'),
  (5,'administrability'),
  (6,'affordability'),
  (7,'auditability'),
  (8,'availability'),
  (9,'credibility'),
  (10,'standards compliance'),
  (11,'process capabilities'),
  (12,'compatibility'),
  (13,'composability'),
  (14,'configurability'),
  (15,'correctness'),
  (16,'customizability'),
  (17,'degradability'),
  (18,'demonstrability'),
  (19,'dependability'),
  (20,'deployability'),
  (21,'distributability'),
  (22,'durability'),
  (23,'effectiveness'),
  (24,'efficiency'),
  (25,'evolvability'),
  (26,'extensibility'),
  (27,'fidelity'),
  (28,'flexibility'),
  (29,'installability'),
  (30,'Integrity'),
  (31,'interchangeability'),
  (32,'interoperability'),
  (33,'learnability'),
  (34,'maintainability'),
  (35,'manageability'),
  (36,'mobility'),
  (37,'modularity'),
  (38,'nomadicity'),
  (39,'operability'),
  (40,'portability'),
  (41,'precision'),
  (42,'predictability'),
  (43,'recoverability'),
  (44,'relevance'),
  (45,'reliability'),
  (46,'repeatability'),
  (47,'reproducibility'),
  (48,'responsiveness'),
  (49,'reusability'),
  (50,'robustness'),
  (51,'safety'),
  (52,'scalability'),
  (53,'seamlessness'),
  (54,'serviceability'),
  (55,'securability'),
  (56,'simplicity'),
  (57,'stability'),
  (58,'survivability'),
  (59,'sustainability'),
  (60,'tailorability'),
  (61,'testability'),
  (62,'timeliness'),
  (63,'ubiquity'),
  (64,'understandability'),
  (65,'usability');
COMMIT;

#
# Data for the `qualityattributegroup` table  (LIMIT 0,500)
#

INSERT INTO `qualityattributegroup` (`id`, `success`, `attributeId`, `designId`) VALUES 
  (1,1,34,1),
  (2,10,13,1),
  (3,10,4,1),
  (4,8,50,1),
  (5,8,64,2),
  (6,10,13,2),
  (7,6,25,2),
  (8,10,28,3),
  (9,2,64,3),
  (10,8,4,4),
  (11,10,37,4),
  (12,1,34,5),
  (13,1,24,5),
  (14,7,4,5),
  (15,10,61,6),
  (16,1,64,6),
  (17,8,62,6),
  (18,1,49,6),
  (19,1,19,6),
  (20,10,16,7),
  (21,8,28,7),
  (22,10,4,7);
COMMIT;

#
# Data for the `relation` table  (LIMIT 0,500)
#

INSERT INTO `relation` (`id`, `sourceId`, `targetId`) VALUES 
  (3,1,2),
  (7,5,6),
  (9,5,8),
  (11,8,10),
  (13,6,12),
  (14,6,4),
  (16,5,15),
  (17,5,1),
  (18,15,4),
  (19,1,10),
  (20,8,12),
  (21,15,2),
  (33,31,32),
  (35,32,31),
  (36,31,34),
  (42,40,41),
  (46,45,43),
  (47,40,45),
  (48,45,44),
  (56,55,54),
  (58,53,57),
  (59,55,53),
  (66,64,65),
  (69,67,65),
  (70,68,64),
  (71,64,68),
  (72,68,67),
  (79,77,78),
  (81,80,77),
  (82,80,78),
  (88,86,87),
  (91,86,90),
  (92,89,86),
  (94,86,93);
COMMIT;

#
# Data for the `user` table  (LIMIT 0,500)
#

INSERT INTO `user` (`id`, `user`, `pass`) VALUES 
  (1,'admin','123456');
COMMIT;



/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;