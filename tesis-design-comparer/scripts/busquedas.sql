# SQL Manager 2010 Lite for MySQL 4.5.1.3
# ---------------------------------------
# Host     : localhost
# Port     : 3306
# Database : tesis_db


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES latin1 */;

SET FOREIGN_KEY_CHECKS=0;

DROP DATABASE IF EXISTS `tesis_db`;

CREATE DATABASE `tesis_db`
    CHARACTER SET 'latin1'
    COLLATE 'latin1_swedish_ci';

USE `tesis_db`;

#
# Structure for the `agregation` table : 
#

DROP TABLE IF EXISTS `agregation`;

CREATE TABLE `agregation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

#
# Structure for the `asociation` table : 
#

DROP TABLE IF EXISTS `asociation`;

CREATE TABLE `asociation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=61 DEFAULT CHARSET=latin1;

#
# Structure for the `attribute` table : 
#

DROP TABLE IF EXISTS `attribute`;

CREATE TABLE `attribute` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) DEFAULT NULL,
  `typ` varchar(20) DEFAULT NULL,
  `ambient` varchar(20) DEFAULT NULL,
  `componentId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

#
# Structure for the `boxcomponent` table : 
#

DROP TABLE IF EXISTS `boxcomponent`;

CREATE TABLE `boxcomponent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=68 DEFAULT CHARSET=latin1;

#
# Structure for the `class` table : 
#

DROP TABLE IF EXISTS `class`;

CREATE TABLE `class` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `abst` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=68 DEFAULT CHARSET=latin1;

#
# Structure for the `classdiagram` table : 
#

DROP TABLE IF EXISTS `classdiagram`;

CREATE TABLE `classdiagram` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

#
# Structure for the `classdiagramcomponent` table : 
#

DROP TABLE IF EXISTS `classdiagramcomponent`;

CREATE TABLE `classdiagramcomponent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=68 DEFAULT CHARSET=latin1;

#
# Structure for the `component` table : 
#

DROP TABLE IF EXISTS `component`;

CREATE TABLE `component` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `compname` varchar(50) DEFAULT NULL,
  `path` varchar(100) DEFAULT NULL,
  `diagramId` int(11) DEFAULT NULL,
  `plainDesignId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=68 DEFAULT CHARSET=latin1;

#
# Structure for the `composition` table : 
#

DROP TABLE IF EXISTS `composition`;

CREATE TABLE `composition` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=50 DEFAULT CHARSET=latin1;

#
# Structure for the `dependence` table : 
#

DROP TABLE IF EXISTS `dependence`;

CREATE TABLE `dependence` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;

#
# Structure for the `design` table : 
#

DROP TABLE IF EXISTS `design`;

CREATE TABLE `design` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

#
# Structure for the `diagram` table : 
#

DROP TABLE IF EXISTS `diagram`;

CREATE TABLE `diagram` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `diagname` varchar(50) DEFAULT NULL,
  `designId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

#
# Structure for the `implementation` table : 
#

DROP TABLE IF EXISTS `implementation`;

CREATE TABLE `implementation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

#
# Structure for the `inheritance` table : 
#

DROP TABLE IF EXISTS `inheritance`;

CREATE TABLE `inheritance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=63 DEFAULT CHARSET=latin1;

#
# Structure for the `interface` table : 
#

DROP TABLE IF EXISTS `interface`;

CREATE TABLE `interface` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

#
# Structure for the `method` table : 
#

DROP TABLE IF EXISTS `method`;

CREATE TABLE `method` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) DEFAULT NULL,
  `ambient` varchar(20) DEFAULT NULL,
  `typ` varchar(20) DEFAULT NULL,
  `abst` bit(1) NOT NULL DEFAULT b'0',
  `parameters` varchar(100) DEFAULT NULL,
  `componentId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

#
# Structure for the `package` table : 
#

DROP TABLE IF EXISTS `package`;

CREATE TABLE `package` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

#
# Structure for the `plaindesign` table : 
#

DROP TABLE IF EXISTS `plaindesign`;

CREATE TABLE `plaindesign` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(4) DEFAULT NULL,
  `boxId` int(11) DEFAULT NULL,
  `absorbedRelations` int(11) NOT NULL DEFAULT '0',
  `componentsCount` int(11) NOT NULL DEFAULT '0',
  `internalAbsorbedRelations` int(11) DEFAULT NULL,
  `internalUseAbsorbedRelations` int(11) DEFAULT NULL,
  `useAbsorbedRelations` int(11) DEFAULT NULL,
  `multipleUseAbsorbedByComponent` int(11) DEFAULT NULL,
  `multipleAbsorbedByComponent` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

#
# Structure for the `plaindesignbox` table : 
#

DROP TABLE IF EXISTS `plaindesignbox`;

CREATE TABLE `plaindesignbox` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `success` float NOT NULL DEFAULT '5',
  `designId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

#
# Structure for the `qualityattribute` table : 
#

DROP TABLE IF EXISTS `qualityattribute`;

CREATE TABLE `qualityattribute` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=66 DEFAULT CHARSET=latin1;

#
# Structure for the `qualityattributegroup` table : 
#

DROP TABLE IF EXISTS `qualityattributegroup`;

CREATE TABLE `qualityattributegroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `success` int(2) NOT NULL DEFAULT '5',
  `attributeId` int(11) NOT NULL DEFAULT '0',
  `designId` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

#
# Structure for the `relation` table : 
#

DROP TABLE IF EXISTS `relation`;

CREATE TABLE `relation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sourceId` int(11) DEFAULT NULL,
  `targetId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=63 DEFAULT CHARSET=latin1;

#
# Structure for the `user` table : 
#

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(50) NOT NULL DEFAULT 'user',
  `pass` varchar(50) NOT NULL DEFAULT 'pass',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=18446744073709551615 DEFAULT CHARSET=latin1;

#
# Data for the `asociation` table  (LIMIT 0,500)
#

INSERT INTO `asociation` (`id`) VALUES 
  (3),
  (9),
  (13),
  (14),
  (16),
  (19),
  (33),
  (37),
  (39),
  (59),
  (60);
COMMIT;

#
# Data for the `attribute` table  (LIMIT 0,500)
#

INSERT INTO `attribute` (`id`, `name`, `typ`, `ambient`, `componentId`) VALUES 
  (1,'classOfProductA2',NULL,NULL,22),
  (2,'classOfProductB2',NULL,NULL,22),
  (3,'classOfProductB1',NULL,NULL,23),
  (4,'classOfProductA1',NULL,NULL,23),
  (5,'classOfAbstractProductB',NULL,NULL,30),
  (6,'classOfAbstractProductA',NULL,NULL,30),
  (7,'classOfSmallerB',NULL,NULL,42),
  (8,'classOfSmallerE',NULL,NULL,42),
  (9,'classOfSmallerD',NULL,NULL,42),
  (10,'classOfSmallerB',NULL,NULL,43),
  (11,'classOfSmallerE',NULL,NULL,43),
  (12,'classOfSmallerD',NULL,NULL,43),
  (13,'collectionOfBuilder',NULL,NULL,51),
  (14,'collectionOfBuilder',NULL,NULL,53),
  (15,'classOfHandler',NULL,NULL,64),
  (16,'classOfHandler',NULL,NULL,66),
  (17,'classOfHandler',NULL,NULL,67);
COMMIT;

#
# Data for the `boxcomponent` table  (LIMIT 0,500)
#

INSERT INTO `boxcomponent` (`id`) VALUES 
  (1),
  (2),
  (4),
  (5),
  (6),
  (8),
  (10),
  (12),
  (15),
  (22),
  (23),
  (24),
  (25),
  (26),
  (27),
  (28),
  (29),
  (30),
  (31),
  (32),
  (34),
  (36),
  (38),
  (40),
  (42),
  (43),
  (44),
  (45),
  (46),
  (47),
  (51),
  (52),
  (53),
  (54),
  (55),
  (56),
  (58),
  (61),
  (63),
  (64),
  (65),
  (66),
  (67);
COMMIT;

#
# Data for the `class` table  (LIMIT 0,500)
#

INSERT INTO `class` (`id`, `abst`) VALUES 
  (1,0),
  (2,0),
  (4,0),
  (5,1),
  (6,0),
  (8,0),
  (10,0),
  (12,0),
  (15,0),
  (22,0),
  (23,0),
  (24,0),
  (25,0),
  (26,0),
  (27,0),
  (28,0),
  (29,0),
  (30,1),
  (31,0),
  (32,0),
  (34,0),
  (36,0),
  (38,0),
  (40,0),
  (42,0),
  (43,0),
  (44,0),
  (45,0),
  (46,0),
  (47,0),
  (51,0),
  (52,0),
  (53,0),
  (54,0),
  (55,0),
  (56,0),
  (58,0),
  (61,0),
  (63,0),
  (64,0),
  (65,0),
  (66,0),
  (67,0);
COMMIT;

#
# Data for the `classdiagram` table  (LIMIT 0,500)
#

INSERT INTO `classdiagram` (`id`) VALUES 
  (1),
  (2),
  (3),
  (4);
COMMIT;

#
# Data for the `classdiagramcomponent` table  (LIMIT 0,500)
#

INSERT INTO `classdiagramcomponent` (`id`) VALUES 
  (1),
  (2),
  (3),
  (4),
  (5),
  (6),
  (7),
  (8),
  (9),
  (10),
  (11),
  (12),
  (13),
  (14),
  (15),
  (16),
  (17),
  (18),
  (19),
  (20),
  (21),
  (22),
  (23),
  (24),
  (25),
  (26),
  (27),
  (28),
  (29),
  (30),
  (31),
  (32),
  (33),
  (34),
  (35),
  (36),
  (37),
  (38),
  (39),
  (40),
  (41),
  (42),
  (43),
  (44),
  (45),
  (46),
  (47),
  (48),
  (49),
  (50),
  (51),
  (52),
  (53),
  (54),
  (55),
  (56),
  (57),
  (58),
  (59),
  (60),
  (61),
  (62),
  (63),
  (64),
  (65),
  (66),
  (67);
COMMIT;

#
# Data for the `component` table  (LIMIT 0,500)
#

INSERT INTO `component` (`id`, `compname`, `path`, `diagramId`, `plainDesignId`) VALUES 
  (1,'ConcreteFactoryA',NULL,1,NULL),
  (2,'ProductA1',NULL,1,NULL),
  (3,'fffhhh',NULL,1,NULL),
  (4,'ProductA2',NULL,1,NULL),
  (5,'AbstractFactory',NULL,1,NULL),
  (6,'ConcreteFactoryB',NULL,1,NULL),
  (7,'fd',NULL,1,NULL),
  (8,'AbstractProductB',NULL,1,NULL),
  (9,'fdff',NULL,1,NULL),
  (10,'ProductB1',NULL,1,NULL),
  (11,'fdfd',NULL,1,NULL),
  (12,'ProductB2',NULL,1,NULL),
  (13,'gf',NULL,1,NULL),
  (14,'gfffff',NULL,1,NULL),
  (15,'AbstractProductA',NULL,1,NULL),
  (16,'gfff',NULL,1,NULL),
  (17,'cx',NULL,1,NULL),
  (18,'bd',NULL,1,NULL),
  (19,'hf',NULL,1,NULL),
  (20,'gfgf',NULL,1,NULL),
  (21,'bvbv',NULL,1,NULL),
  (22,'ConcreteFactoryB',NULL,NULL,1),
  (23,'ConcreteFactoryA',NULL,NULL,1),
  (24,'ProductA2',NULL,NULL,1),
  (25,'ProductA1',NULL,NULL,1),
  (26,'ProductB2',NULL,NULL,1),
  (27,'ProductB1',NULL,NULL,1),
  (28,'AbstractProductB',NULL,NULL,2),
  (29,'AbstractProductA',NULL,NULL,2),
  (30,'AbstractFactory',NULL,NULL,2),
  (31,'Biggest',NULL,2,NULL),
  (32,'SmallerD',NULL,2,NULL),
  (33,'hhh',NULL,2,NULL),
  (34,'SmallerA',NULL,2,NULL),
  (35,'sss',NULL,2,NULL),
  (36,'SmallerB',NULL,2,NULL),
  (37,'aaa',NULL,2,NULL),
  (38,'SmallerE',NULL,2,NULL),
  (39,'aaaa',NULL,2,NULL),
  (40,'SmallerC',NULL,2,NULL),
  (41,'aa',NULL,2,NULL),
  (42,'Biggest',NULL,NULL,3),
  (43,'Biggest',NULL,NULL,4),
  (44,'Product',NULL,3,NULL),
  (45,'Director',NULL,3,NULL),
  (46,'Builder',NULL,3,NULL),
  (47,'ConcreteBuilder',NULL,3,NULL),
  (48,'fdfdf',NULL,3,NULL),
  (49,'fdffdff',NULL,3,NULL),
  (50,'fdfd',NULL,3,NULL),
  (51,'Director',NULL,NULL,5),
  (52,'ConcreteBuilder',NULL,NULL,5),
  (53,'Director',NULL,NULL,6),
  (54,'Builder',NULL,NULL,6),
  (55,'Handler',NULL,4,NULL),
  (56,'ConcreteHandlerA',NULL,4,NULL),
  (57,'dadsa',NULL,4,NULL),
  (58,'Client',NULL,4,NULL),
  (59,'dadsda',NULL,4,NULL),
  (60,'dasdsa',NULL,4,NULL),
  (61,'ConcreteHandlerB',NULL,4,NULL),
  (62,'dasdad',NULL,4,NULL),
  (63,'ConcreteHandlerB',NULL,NULL,7),
  (64,'Client',NULL,NULL,7),
  (65,'ConcreteHandlerA',NULL,NULL,7),
  (66,'Client',NULL,NULL,8),
  (67,'Handler',NULL,NULL,8);
COMMIT;

#
# Data for the `composition` table  (LIMIT 0,500)
#

INSERT INTO `composition` (`id`) VALUES 
  (49);
COMMIT;

#
# Data for the `dependence` table  (LIMIT 0,500)
#

INSERT INTO `dependence` (`id`) VALUES 
  (35),
  (41),
  (48);
COMMIT;

#
# Data for the `design` table  (LIMIT 0,500)
#

INSERT INTO `design` (`id`, `name`) VALUES 
  (1,'Abstract Factory'),
  (2,'Big Object'),
  (3,'Builder'),
  (4,'Chain');
COMMIT;

#
# Data for the `diagram` table  (LIMIT 0,500)
#

INSERT INTO `diagram` (`id`, `diagname`, `designId`) VALUES 
  (1,'estructura',1),
  (2,'structure',2),
  (3,'estructura',3),
  (4,'estructura',4);
COMMIT;

#
# Data for the `inheritance` table  (LIMIT 0,500)
#

INSERT INTO `inheritance` (`id`) VALUES 
  (7),
  (11),
  (17),
  (18),
  (20),
  (21),
  (50),
  (57),
  (62);
COMMIT;

#
# Data for the `method` table  (LIMIT 0,500)
#

INSERT INTO `method` (`id`, `name`, `ambient`, `typ`, `abst`, `parameters`, `componentId`) VALUES 
  (1,'createProductB','-','void',0,'',1),
  (2,'createProductA','-','void',0,'',1),
  (3,'CrateProductA','-','void',1,'',5),
  (4,'CreateProductB','-','void',1,'',5),
  (5,'createProductB','-','void',0,'',6),
  (6,'createProductA','-','void',0,'',6),
  (7,'createProductB','-','void',0,'',22),
  (8,'createProductA','-','void',0,'',22),
  (9,'createProductB','-','void',0,'',23),
  (10,'createProductA','-','void',0,'',23),
  (11,'CrateProductA','-','void',1,'',30),
  (12,'CreateProductB','-','void',1,'',30),
  (13,'useOfSmallerC',NULL,NULL,0,'',42),
  (14,'useOfSmallerA',NULL,NULL,0,'',42),
  (15,'useOfSmallerC',NULL,NULL,0,'',43),
  (16,'useOfSmallerA',NULL,NULL,0,'',43),
  (17,'construct','-','void',0,'',45),
  (18,'BuildPart','-','void',0,'',46),
  (19,'BuildPart','-','void',0,'',47),
  (20,'construct','-','void',0,'',53),
  (21,'BuildPart','-','void',0,'',52),
  (22,'useOfProduct',NULL,NULL,0,'',52),
  (23,'BuildPart','-','void',0,'',54),
  (24,'request','-','void',0,'',55),
  (25,'request','-','void',0,'',56),
  (26,'request','-','void',0,'',61),
  (27,'request','-','void',0,'',63),
  (28,'request','-','void',0,'',65),
  (29,'request','-','void',0,'',67);
COMMIT;

#
# Data for the `plaindesign` table  (LIMIT 0,500)
#

INSERT INTO `plaindesign` (`id`, `name`, `boxId`, `absorbedRelations`, `componentsCount`, `internalAbsorbedRelations`, `internalUseAbsorbedRelations`, `useAbsorbedRelations`, `multipleUseAbsorbedByComponent`, `multipleAbsorbedByComponent`) VALUES 
  (1,'down',1,4,6,4,0,0,0,4),
  (2,'up',1,2,3,2,0,0,0,2),
  (3,'down',2,5,1,0,0,2,2,3),
  (4,'up',2,5,1,0,0,2,2,3),
  (5,'down',3,2,2,0,0,1,0,0),
  (6,'up',3,1,2,1,0,0,0,0),
  (7,'down',4,1,3,0,0,0,0,0),
  (8,'up',4,2,2,2,0,0,0,0);
COMMIT;

#
# Data for the `plaindesignbox` table  (LIMIT 0,500)
#

INSERT INTO `plaindesignbox` (`id`, `success`, `designId`) VALUES 
  (1,5,1),
  (2,5,2),
  (3,5,3),
  (4,5,4);
COMMIT;

#
# Data for the `qualityattribute` table  (LIMIT 0,500)
#

INSERT INTO `qualityattribute` (`id`, `name`) VALUES 
  (2,'accountability'),
  (3,'accuracy'),
  (4,'adaptability'),
  (5,'administrability'),
  (6,'affordability'),
  (7,'auditability'),
  (8,'availability'),
  (9,'credibility'),
  (10,'standards compliance'),
  (11,'process capabilities'),
  (12,'compatibility'),
  (13,'composability'),
  (14,'configurability'),
  (15,'correctness'),
  (16,'customizability'),
  (17,'degradability'),
  (18,'demonstrability'),
  (19,'dependability'),
  (20,'deployability'),
  (21,'distributability'),
  (22,'durability'),
  (23,'effectiveness'),
  (24,'efficiency'),
  (25,'evolvability'),
  (26,'extensibility'),
  (27,'fidelity'),
  (28,'flexibility'),
  (29,'installability'),
  (30,'Integrity'),
  (31,'interchangeability'),
  (32,'interoperability'),
  (33,'learnability'),
  (34,'maintainability'),
  (35,'manageability'),
  (36,'mobility'),
  (37,'modularity'),
  (38,'nomadicity'),
  (39,'operability'),
  (40,'portability'),
  (41,'precision'),
  (42,'predictability'),
  (43,'recoverability'),
  (44,'relevance'),
  (45,'reliability'),
  (46,'repeatability'),
  (47,'reproducibility'),
  (48,'responsiveness'),
  (49,'reusability'),
  (50,'robustness'),
  (51,'safety'),
  (52,'scalability'),
  (53,'seamlessness'),
  (54,'serviceability'),
  (55,'securability'),
  (56,'simplicity'),
  (57,'stability'),
  (58,'survivability'),
  (59,'sustainability'),
  (60,'tailorability'),
  (61,'testability'),
  (62,'timeliness'),
  (63,'ubiquity'),
  (64,'understandability'),
  (65,'usability');
COMMIT;

#
# Data for the `qualityattributegroup` table  (LIMIT 0,500)
#

INSERT INTO `qualityattributegroup` (`id`, `success`, `attributeId`, `designId`) VALUES 
  (1,5,4,1),
  (2,5,3,1),
  (3,5,2,1),
  (4,5,5,2),
  (5,5,6,2),
  (6,5,7,2),
  (7,5,8,2),
  (8,5,3,3),
  (9,5,2,3),
  (10,5,5,4),
  (11,5,6,4),
  (12,5,4,4);
COMMIT;

#
# Data for the `relation` table  (LIMIT 0,500)
#

INSERT INTO `relation` (`id`, `sourceId`, `targetId`) VALUES 
  (3,1,2),
  (7,5,6),
  (9,5,8),
  (11,8,10),
  (13,6,12),
  (14,6,4),
  (16,5,15),
  (17,5,1),
  (18,15,4),
  (19,1,10),
  (20,8,12),
  (21,15,2),
  (33,31,32),
  (35,31,34),
  (37,31,36),
  (39,31,38),
  (41,31,40),
  (48,47,44),
  (49,45,46),
  (50,46,47),
  (57,55,56),
  (59,58,55),
  (60,55,55),
  (62,55,61);
COMMIT;

#
# Data for the `user` table  (LIMIT 0,500)
#

INSERT INTO `user` (`id`, `user`, `pass`) VALUES 
  (1,'admin','123456');
COMMIT;



/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;