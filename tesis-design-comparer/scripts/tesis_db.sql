/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE */;
/*!40101 SET SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES */;
/*!40103 SET SQL_NOTES='ON' */;

drop database if exists tesis_db;

CREATE DATABASE `tesis_db` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `tesis_db`;
CREATE TABLE `agregation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
CREATE TABLE `asociation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
CREATE TABLE `attribute` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) DEFAULT NULL,
  `typ` varchar(20) DEFAULT NULL,
  `ambient` varchar(20) DEFAULT NULL,
  `componentId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
CREATE TABLE `boxcomponent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
CREATE TABLE `class` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `abst` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
CREATE TABLE `classdiagram` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
CREATE TABLE `classdiagramcomponent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
CREATE TABLE `component` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `compname` varchar(50) DEFAULT NULL,
  `path` varchar(100) DEFAULT NULL,
  `diagramId` int(11) DEFAULT NULL,
  `plainDesignId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
CREATE TABLE `composition` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
CREATE TABLE `dependence` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
CREATE TABLE `design` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
CREATE TABLE `diagram` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `diagname` varchar(50) DEFAULT NULL,
  `designId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
CREATE TABLE `implementation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
CREATE TABLE `inheritance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
CREATE TABLE `interface` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
CREATE TABLE `method` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) DEFAULT NULL,
  `ambient` varchar(20) DEFAULT NULL,
  `typ` varchar(20) DEFAULT NULL,
  `abst` bit(1) NOT NULL DEFAULT b'0',
  `parameters` varchar(100) DEFAULT NULL,
  `componentId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
CREATE TABLE `package` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
CREATE TABLE `plaindesign` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(4) DEFAULT NULL,
  `boxId` int(11) DEFAULT NULL,
  `absorbedRelations` int(11) NOT NULL DEFAULT '0',
  `componentsCount` int(11) NOT NULL DEFAULT '0',
  `internalAbsorbedRelations` int(11) DEFAULT NULL,
  `internalUseAbsorbedRelations` int(11) DEFAULT NULL,
  `useAbsorbedRelations` int(11) DEFAULT NULL,
  `multipleUseAbsorbedByComponent` int(11) DEFAULT NULL,
  `multipleAbsorbedByComponent` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
CREATE TABLE `plaindesignbox` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `success` float NOT NULL DEFAULT '5',
  `designId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
CREATE TABLE `qualityattribute` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=66 DEFAULT CHARSET=latin1;
CREATE TABLE `qualityattributegroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `success` int(2) NOT NULL DEFAULT '5',
  `attributeId` int(11) NOT NULL DEFAULT '0',
  `designId` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
CREATE TABLE `relation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sourceId` int(11) DEFAULT NULL,
  `targetId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(50) NOT NULL DEFAULT 'user',
  `pass` varchar(50) NOT NULL DEFAULT 'pass',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=18446744073709551615 DEFAULT CHARSET=latin1;

INSERT INTO `user` VALUES (1,'admin','123456');

INSERT INTO `qualityattribute` VALUES (2,'accountability');
INSERT INTO `qualityattribute` VALUES (3,'accuracy');
INSERT INTO `qualityattribute` VALUES (4,'adaptability');
INSERT INTO `qualityattribute` VALUES (5,'administrability');
INSERT INTO `qualityattribute` VALUES (6,'affordability');
INSERT INTO `qualityattribute` VALUES (7,'auditability');
INSERT INTO `qualityattribute` VALUES (8,'availability');
INSERT INTO `qualityattribute` VALUES (9,'credibility');
INSERT INTO `qualityattribute` VALUES (10,'standards compliance');
INSERT INTO `qualityattribute` VALUES (11,'process capabilities');
INSERT INTO `qualityattribute` VALUES (12,'compatibility');
INSERT INTO `qualityattribute` VALUES (13,'composability');
INSERT INTO `qualityattribute` VALUES (14,'configurability');
INSERT INTO `qualityattribute` VALUES (15,'correctness');
INSERT INTO `qualityattribute` VALUES (16,'customizability');
INSERT INTO `qualityattribute` VALUES (17,'degradability');
INSERT INTO `qualityattribute` VALUES (18,'demonstrability');
INSERT INTO `qualityattribute` VALUES (19,'dependability');
INSERT INTO `qualityattribute` VALUES (20,'deployability');
INSERT INTO `qualityattribute` VALUES (21,'distributability');
INSERT INTO `qualityattribute` VALUES (22,'durability');
INSERT INTO `qualityattribute` VALUES (23,'effectiveness');
INSERT INTO `qualityattribute` VALUES (24,'efficiency');
INSERT INTO `qualityattribute` VALUES (25,'evolvability');
INSERT INTO `qualityattribute` VALUES (26,'extensibility');
INSERT INTO `qualityattribute` VALUES (27,'fidelity');
INSERT INTO `qualityattribute` VALUES (28,'flexibility');
INSERT INTO `qualityattribute` VALUES (29,'installability');
INSERT INTO `qualityattribute` VALUES (30,'Integrity');
INSERT INTO `qualityattribute` VALUES (31,'interchangeability');
INSERT INTO `qualityattribute` VALUES (32,'interoperability');
INSERT INTO `qualityattribute` VALUES (33,'learnability');
INSERT INTO `qualityattribute` VALUES (34,'maintainability');
INSERT INTO `qualityattribute` VALUES (35,'manageability');
INSERT INTO `qualityattribute` VALUES (36,'mobility');
INSERT INTO `qualityattribute` VALUES (37,'modularity');
INSERT INTO `qualityattribute` VALUES (38,'nomadicity');
INSERT INTO `qualityattribute` VALUES (39,'operability');
INSERT INTO `qualityattribute` VALUES (40,'portability');
INSERT INTO `qualityattribute` VALUES (41,'precision');
INSERT INTO `qualityattribute` VALUES (42,'predictability');
INSERT INTO `qualityattribute` VALUES (43,'recoverability');
INSERT INTO `qualityattribute` VALUES (44,'relevance');
INSERT INTO `qualityattribute` VALUES (45,'reliability');
INSERT INTO `qualityattribute` VALUES (46,'repeatability');
INSERT INTO `qualityattribute` VALUES (47,'reproducibility');
INSERT INTO `qualityattribute` VALUES (48,'responsiveness');
INSERT INTO `qualityattribute` VALUES (49,'reusability');
INSERT INTO `qualityattribute` VALUES (50,'robustness');
INSERT INTO `qualityattribute` VALUES (51,'safety');
INSERT INTO `qualityattribute` VALUES (52,'scalability');
INSERT INTO `qualityattribute` VALUES (53,'seamlessness');
INSERT INTO `qualityattribute` VALUES (54,'serviceability');
INSERT INTO `qualityattribute` VALUES (55,'securability');
INSERT INTO `qualityattribute` VALUES (56,'simplicity');
INSERT INTO `qualityattribute` VALUES (57,'stability');
INSERT INTO `qualityattribute` VALUES (58,'survivability');
INSERT INTO `qualityattribute` VALUES (59,'sustainability');
INSERT INTO `qualityattribute` VALUES (60,'tailorability');
INSERT INTO `qualityattribute` VALUES (61,'testability');
INSERT INTO `qualityattribute` VALUES (62,'timeliness');
INSERT INTO `qualityattribute` VALUES (63,'ubiquity');
INSERT INTO `qualityattribute` VALUES (64,'understandability');
INSERT INTO `qualityattribute` VALUES (65,'usability');

/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
