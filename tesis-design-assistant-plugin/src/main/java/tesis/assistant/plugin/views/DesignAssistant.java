package tesis.assistant.plugin.views;

import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;

import tesis.assistant.plugin.actions.AssistanceAction;
import tesis.assistant.plugin.actions.ConfigAction;
import tesis.assistant.plugin.actions.DoubleClickAction;
import tesis.assistant.plugin.actions.SaveAction;
import tesis.assistant.plugin.ui.ResultTable;
import tesis.assistant.plugin.util.PluginPreferencies;

public class DesignAssistant extends ViewPart {

	/**
	 * The ID of the view as specified by the extension.
	 */
	public static final String ID = "tesis.assistant.plugin.views.DesignAssistant";

	private ResultTable viewer;
	private AssistanceAction assistanceAction;
	private SaveAction saveAction;
	private DoubleClickAction doubleClickAction;
	private ConfigAction configAction;

	/*
	 * The content provider class is responsible for
	 * providing objects to the view. It can wrap
	 * existing objects in adapters or simply return
	 * objects as-is. These objects may be sensitive
	 * to the current input of the view, or ignore
	 * it and always show the same content 
	 * (like Task List, for example).
	 */
	 
	class ViewContentProvider implements IStructuredContentProvider {
		public void inputChanged(Viewer v, Object oldInput, Object newInput) {
		}
		public void dispose() {
		}
		public Object[] getElements(Object parent) {
			return new String[] { "One", "Two", "Three" };
		}
	}
	class ViewLabelProvider extends LabelProvider implements ITableLabelProvider {
		public String getColumnText(Object obj, int index) {
			return getText(obj);
		}
		public Image getColumnImage(Object obj, int index) {
			return getImage(obj);
		}
		public Image getImage(Object obj) {
			return PlatformUI.getWorkbench().
					getSharedImages().getImage(ISharedImages.IMG_OBJ_ELEMENT);
		}
	}
	class NameSorter extends ViewerSorter {
	}

	/**
	 * The constructor.
	 */
	public DesignAssistant() {
	}

	/**
	 * This is a callback that will allow us
	 * to create the viewer and initialize it.
	 */
	public void createPartControl(Composite parent) {
		PluginPreferencies.getInstance().loadPluginSettings();

		viewer = new ResultTable(parent);
		makeActions();
		hookContextMenu();
		hookDoubleClickAction();
		contributeToActionBars();
	}

	private void hookContextMenu() {
		MenuManager menuMgr = new MenuManager("#PopupMenu");
		menuMgr.setRemoveAllWhenShown(true);
		menuMgr.addMenuListener(new IMenuListener() {
			public void menuAboutToShow(IMenuManager manager) {
				DesignAssistant.this.fillContextMenu(manager);
			}
		});
	}

	private void contributeToActionBars() {
		IActionBars bars = getViewSite().getActionBars();
		fillLocalPullDown(bars.getMenuManager());
		fillLocalToolBar(bars.getToolBarManager());
	}

	private void fillLocalPullDown(IMenuManager manager) {
		manager.add(assistanceAction);
		manager.add(new Separator());
		manager.add(saveAction);
		manager.add(new Separator());
		manager.add(configAction);
	}

	private void fillContextMenu(IMenuManager manager) {
		manager.add(assistanceAction);
		manager.add(saveAction);
		manager.add(configAction);
		// Other plug-ins can contribute there actions here
		manager.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
	}
	
	private void fillLocalToolBar(IToolBarManager manager) {
		manager.add(assistanceAction);
		manager.add(saveAction);
		manager.add(configAction);
	}

	private void makeActions() {
		assistanceAction =  new AssistanceAction(viewer);
		assistanceAction.setText("Recibir asistencia");
		assistanceAction.setToolTipText("Recibir asistencia para el diagrama actual");
		assistanceAction.setImageDescriptor(ImageDescriptor.createFromURL(DesignAssistant.class.getResource("/icons/assistance.png")));

		saveAction = new SaveAction();
		saveAction.setText("Almacenar");
		saveAction.setToolTipText("Almacenar el diagrama actual en la base de conocimiento");
		saveAction.setImageDescriptor(ImageDescriptor.createFromURL(DesignAssistant.class.getResource("/icons/save.png")));

		configAction = new ConfigAction();
		configAction.setText("Configuraci\u00F3n");
		configAction.setToolTipText("Abrir el panel de configuraci\u00F3n");
		configAction.setImageDescriptor(ImageDescriptor.createFromURL(DesignAssistant.class.getResource("/icons/config.png")));

		doubleClickAction = new DoubleClickAction(viewer);
	}

	private void hookDoubleClickAction() {
		viewer.addDoubleClickListener(new IDoubleClickListener() {
			public void doubleClick(DoubleClickEvent event) {
				doubleClickAction.run();
			}
		});
	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	public void setFocus() {
		viewer.getControl().setFocus();
	}

}