package tesis.assistant.plugin.actions;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.ide.IDE;

import tesis.assistant.plugin.model.DesignModel;

public class DoubleClickAction extends Action {

	private static final String TMP_FILE_PREFIX = "design assistant - ";
	private static final String TMP_FILE_DIR = "design assistant";

	private TableViewer viewer;

	/**
	 * The constructor.
	 */
	public DoubleClickAction(TableViewer viewer) {
		this.viewer = viewer;
	}

	/**
	 * The action has been activated. The argument of the method represents the 'real' action sitting in the workbench
	 * UI.
	 * 
	 */
	public void run() {

		try {
			IWorkbenchWindow window = (IWorkbenchWindow) viewer.getData("workbenchWindow");
			IProject activeProject = (IProject) viewer.getData("activeProject");

			if (activeProject.isOpen()) {

				// get design information from selected option
				ISelection selection = viewer.getSelection();
				DesignModel designModel = (DesignModel) ((IStructuredSelection) selection).getFirstElement();
				String fileName = TMP_FILE_PREFIX + designModel.getName();
				String design = designModel.getDesign();

				// get temp file location
				IPath path = new Path("/" + activeProject.getName() + "/" + TMP_FILE_DIR + "/" + fileName + ".cld");
				IFile file = ResourcesPlugin.getWorkspace().getRoot().getFile(path);
				File diagramLocation = file.getRawLocation().makeAbsolute().toFile();

				// create temp directory
				diagramLocation.getParentFile().mkdirs();

				// save temp file
				BufferedWriter bw = new BufferedWriter(new FileWriter(diagramLocation));
				bw.write(design);
				bw.close();

				// refresh active project
				activeProject.refreshLocal(IResource.DEPTH_INFINITE, new NullProgressMonitor());

				// open temp file with the default editor
				IWorkbenchPage page = window.getActivePage();
				IDE.openEditor(page, file, true);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
