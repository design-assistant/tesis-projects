package tesis.assistant.plugin.actions;

import org.eclipse.jface.action.Action;

import tesis.assistant.plugin.ui.ConfigDialog;
import tesis.assistant.plugin.util.PluginPreferencies;

public class ConfigAction extends Action {

	/**
	 * The constructor.
	 */
	public ConfigAction() {
	}

	/**
	 * The action has been activated. The argument of the method represents the 'real' action sitting in the workbench UI.
	 * 
	 */
	public void run() {

		PluginPreferencies preferencies = PluginPreferencies.getInstance();
		ConfigDialog dialog = new ConfigDialog();

		// load dialog values
		dialog.setServerURL(preferencies.getServerURL());

		dialog.setVisible(true);

		if (dialog.isAccepted()) {
			// set new values
			preferencies.setServerURL(dialog.getServerURL());

			// save preferences
			preferencies.savePluginSettings();
		}

	}

}
