package tesis.assistant.plugin.actions;

import java.io.IOException;

import javax.swing.JOptionPane;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.MessageDialog;

import tesis.assistant.plugin.model.msg.AddRequest;
import tesis.assistant.plugin.model.msg.AddResponse;
import tesis.assistant.plugin.ui.SaveDiagramDialog;
import tesis.assistant.plugin.util.HTTPConnector;
import tesis.assistant.plugin.util.HTTPConnector.HTTPConnectorException;
import tesis.assistant.plugin.util.PluginPreferencies;
import tesis.assistant.plugin.util.QualityAttrs;
import tesis.assistant.plugin.util.WindowContent;

import com.fasterxml.jackson.databind.ObjectMapper;

public class SaveAction extends Action {

	private static final ObjectMapper MAPPER = new ObjectMapper();

	/**
	 * The constructor.
	 */
	public SaveAction() {
	}

	/**
	 * The action has been activated. The argument of the method represents the 'real' action sitting in the workbench
	 * UI.
	 * 
	 */
	public void run() {

		String serverURL = PluginPreferencies.getInstance().getServerURL();
		if (serverURL.isEmpty() || "http://".equals(serverURL)) {
			MessageDialog.openWarning(null, "Atenci\u00F3n", "Debe ingresar la URL del servidor de asistencia en 'Configuraci\u00F3n'.");
			return;
		}

		String diagramName = WindowContent.getDiagramName();
		if (diagramName != null) { // Si tiene abierto un diagrama

			SaveDiagramDialog dialog = new SaveDiagramDialog(QualityAttrs.LIST);
			dialog.setNameDiagram(diagramName);
			dialog.setVisible(true);

			if (dialog.isAccepted()) {
				AddRequest addRequest = new AddRequest();
				addRequest.setDescription(dialog.getDescription());
				addRequest.setDesign(WindowContent.getSelectedDiagram());
				addRequest.setName(dialog.getNameDiagram());
				addRequest.setQualityAttributes(dialog.getQualityAttributes());

				AddResponse addResponse = null;
				String errorMessage = null;

				try {
					String jsonData = MAPPER.writeValueAsString(addRequest);
					String service = PluginPreferencies.getInstance().getServerURL() + "/add/amateras";

					String response = HTTPConnector.post(service, 10000, HTTPConnector.CHARSET_UNICODE)
							.setOutput(jsonData, HTTPConnector.CONTENT_TYPE_JSON, HTTPConnector.CHARSET_UNICODE)
							.setProps(HTTPConnector.PROP_COMPRESS_REQUEST_CONTENT).executeString();

					addResponse = MAPPER.readValue(response, AddResponse.class);

				} catch (IOException e) {
					e.printStackTrace();
					errorMessage = e.getClass().getSimpleName() + " - " + e.getMessage();

					if (e instanceof HTTPConnectorException) {
						String response = null;
						try {
							response = ((HTTPConnectorException) e).getResponseBodyString();
							addResponse = MAPPER.readValue(response, AddResponse.class);
						} catch (IOException e1) {
							e1.printStackTrace();
							if (response != null) System.err.println("Server returned:\n" + response);
						}
					}
				}

				if (addResponse != null) {
					if (addResponse.getErrorMessage() == null) {
						JOptionPane.showMessageDialog(null, "Su diagrama se ha almacenado correctamente en\nla base de conocimiento.", "Resultado de operaci\u00F3n",
								JOptionPane.INFORMATION_MESSAGE);
					} else {
						JOptionPane.showMessageDialog(null, "Ocurri\u00F3 un error en el servidor:\n\n    " + addResponse.getErrorMessage(),
								"Resultado de operaci\u00F3n", JOptionPane.ERROR_MESSAGE);
					}
				} else {
					JOptionPane.showMessageDialog(null, "Ha ocurrido un error en la comunicaci\u00F3n con el servidor:\n\n" + errorMessage,
							"Atenci\u00F3n", JOptionPane.ERROR_MESSAGE);
				}
			}

		} else {
			JOptionPane.showMessageDialog(null, "Debe tener seleccionado un diagrama de clases de la herramienta AmaterasUML (.cld).",
					"Atenci\u00F3n", JOptionPane.WARNING_MESSAGE);
		}
	}

}
