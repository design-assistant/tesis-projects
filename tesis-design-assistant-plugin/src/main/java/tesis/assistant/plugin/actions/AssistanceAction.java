package tesis.assistant.plugin.actions;

import java.io.IOException;

import javax.swing.JOptionPane;

import org.eclipse.core.resources.IProject;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

import tesis.assistant.plugin.model.DesignModelProvider;
import tesis.assistant.plugin.model.msg.AssistRequest;
import tesis.assistant.plugin.model.msg.AssistResponse;
import tesis.assistant.plugin.model.msg.MsgValuedDiagram;
import tesis.assistant.plugin.ui.SelectQualityAttrDialog;
import tesis.assistant.plugin.util.HTTPConnector;
import tesis.assistant.plugin.util.HTTPConnector.HTTPConnectorException;
import tesis.assistant.plugin.util.PluginPreferencies;
import tesis.assistant.plugin.util.QualityAttrs;
import tesis.assistant.plugin.util.WindowContent;

import com.fasterxml.jackson.databind.ObjectMapper;

public class AssistanceAction extends Action {

	private static final ObjectMapper MAPPER = new ObjectMapper();

	private TableViewer viewer;

	/**
	 * The constructor.
	 */
	public AssistanceAction(TableViewer viewer) {
		this.viewer = viewer;
	}

	/**
	 * The action has been activated. The argument of the method represents the 'real' action sitting in the workbench
	 * UI.
	 * 
	 */
	public void run() {

		String serverURL = PluginPreferencies.getInstance().getServerURL();
		if (serverURL.isEmpty() || "http://".equals(serverURL)) {
			MessageDialog.openWarning(null, "Atenci\u00F3n", "Debe ingresar la URL del servidor de asistencia en 'Configuraci\u00F3n'.");
			return;
		}

		IWorkbenchWindow workbenchWindow = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		IProject activeProject = WindowContent.getActiveProject();

		// clean previous assistance result
		viewer.setInput(null);
		DesignModelProvider.INSTANCE.getDesignModels().clear();

		if (WindowContent.isOpenClassDiagram()) {

			SelectQualityAttrDialog dialog = new SelectQualityAttrDialog(QualityAttrs.LIST);
			dialog.setVisible(true);

			if (dialog.isAccepted()) {
				AssistRequest assistRequest = new AssistRequest();
				assistRequest.setDesign(WindowContent.getSelectedDiagram());
				assistRequest.setQualityAttributes(dialog.getSelectedQualityAttrs());
				assistRequest.setMatching(dialog.getMatchingType());

				AssistResponse assistResponse = null;
				String errorMessage = null;

				try {
					String jsonData = MAPPER.writeValueAsString(assistRequest);
					String service = PluginPreferencies.getInstance().getServerURL() + "/assist/amateras";

					String response = HTTPConnector.post(service, 10000, HTTPConnector.CHARSET_UNICODE)
							.setOutput(jsonData, HTTPConnector.CONTENT_TYPE_JSON, HTTPConnector.CHARSET_UNICODE)
							.setProps(HTTPConnector.PROP_COMPRESS_REQUEST_CONTENT).executeString();

					assistResponse = MAPPER.readValue(response, AssistResponse.class);

				} catch (IOException e) {
					e.printStackTrace();
					errorMessage = e.getClass().getSimpleName() + " - " + e.getMessage();

					if (e instanceof HTTPConnectorException) {
						String response = null;
						try {
							response = ((HTTPConnectorException) e).getResponseBodyString();
							assistResponse = MAPPER.readValue(response, AssistResponse.class);
						} catch (IOException e1) {
							e1.printStackTrace();
							if (response != null) System.err.println("Server returned:\n" + response);
						}
					}
				}

				if (assistResponse != null) {
					if (assistResponse.getErrorMessage() == null) {
						for (MsgValuedDiagram ar : assistResponse.getAssistResult()) {
							DesignModelProvider.INSTANCE.addDesignModel(ar.getName(), ar.getDesign(), ar.getDescription(),
									ar.getQualityAttributes(), ar.getSuccess());
						}

						viewer.setInput(DesignModelProvider.INSTANCE.getDesignModels());
						viewer.setData("workbenchWindow", workbenchWindow);
						viewer.setData("activeProject", activeProject);
					} else {
						JOptionPane.showMessageDialog(null,
								"Ocurri\u00F3 un error en el servidor:\n\n    " + assistResponse.getErrorMessage(),
								"Resultado de operaci\u00F3n", JOptionPane.ERROR_MESSAGE);
					}
				} else {
					JOptionPane.showMessageDialog(null, "Ha ocurrido un error en la comunicaci\u00F3n con el servidor:\n\n" + errorMessage,
							"Atenci\u00F3n", JOptionPane.ERROR_MESSAGE);
				}
			}

		} else {
			JOptionPane.showMessageDialog(null, "Debe tener seleccionado un diagrama de clases de la herramienta AmaterasUML (.cld).",
					"Atenci\u00F3n", JOptionPane.WARNING_MESSAGE);
		}

	}

}
