package tesis.assistant.plugin.ui;

import java.awt.BorderLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SpinnerNumberModel;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;

public class SaveDiagramDialog extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JTable table;
	private JComboBox<String> comboBoxQualityAttributs;
	private JSpinner spinnerAppreciation;
	private JTextArea textAreaDescription;
	private JTextField textFieldDiagramName;

	private boolean isAccepted;

	private Map<String, Integer> evaluatedQualityAttributes;

	/**
	 * Create the dialog.
	 */
	public SaveDiagramDialog(List<String> qualityAttrs) {

		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

		setTitle("Almacenar diagama en base de conocimiento");
		setModal(true);
		setAlwaysOnTop(true);
		setResizable(false);

		setSize(450, 380);
		setLocationRelativeTo(null);

	    Image img = new ImageIcon(SaveDiagramDialog.class.getResource("/icons/save.png")).getImage();
	    setIconImage(img);

		evaluatedQualityAttributes = new HashMap<String, Integer>();

		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);

		// -----panelDescription
		JPanel panelDescription = new JPanel();
		panelDescription.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelDescription.setBounds(209, 55, 225, 219);
		contentPanel.add(panelDescription);
		panelDescription.setLayout(null);

		JLabel lblDescription = new JLabel("Descripci\u00F3n");
		lblDescription.setBounds(10, 11, 54, 14);
		panelDescription.add(lblDescription);

		JSeparator separator = new JSeparator();
		separator.setBounds(0, 36, 225, 2);
		panelDescription.add(separator);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setBounds(10, 51, 203, 157);
		panelDescription.add(scrollPane);

		textAreaDescription = new JTextArea();
		scrollPane.setViewportView(textAreaDescription);
		textAreaDescription.setLineWrap(true);

		// -----panelQualityAttributs
		JPanel panelQualityAttributs = new JPanel();
		panelQualityAttributs.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelQualityAttributs.setBounds(10, 55, 190, 286);
		contentPanel.add(panelQualityAttributs);
		panelQualityAttributs.setLayout(null);

		JLabel lblQualityAttributes = new JLabel("Atributos de calidad");
		lblQualityAttributes.setBounds(10, 11, 95, 14);
		panelQualityAttributs.add(lblQualityAttributes);

		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(0, 36, 241, 13);
		panelQualityAttributs.add(separator_1);

		table = new JTable(0, 2);
		table.setTableHeader(null);
		table.setShowGrid(false);

		table.getColumnModel().getColumn(0).setWidth(80);
		table.getColumnModel().getColumn(0).setPreferredWidth(80);
		JScrollPane scrollpane = new JScrollPane(table);
		scrollpane.setBounds(10, 49, 170, 159);
		panelQualityAttributs.add(scrollpane);

		comboBoxQualityAttributs = new JComboBox<String>();
		comboBoxQualityAttributs.setBounds(10, 219, 118, 20);
		panelQualityAttributs.add(comboBoxQualityAttributs);

		for (String qualityAttr : qualityAttrs) {
			comboBoxQualityAttributs.addItem(qualityAttr);
		}

		spinnerAppreciation = new JSpinner();
		spinnerAppreciation.setBounds(138, 220, 42, 20);
		panelQualityAttributs.add(spinnerAppreciation);
		spinnerAppreciation.setModel(new SpinnerNumberModel(0, -10, 10, 1));

		JButton btnAddQualityAttribut = new JButton("Agregar");
		btnAddQualityAttribut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				SaveDiagramDialog dialog = SaveDiagramDialog.this;

				if (dialog.containsQualityAttribut(getQualityAttribut())) {
					JOptionPane.showMessageDialog(dialog, "El Atributo de Calidad ya fue agregado.", "Atenci\u00F3n",
							JOptionPane.WARNING_MESSAGE);
					return;
				}
				addQualityAttribut(getQualityAttribut(), getAppreciation());
			}
		});
		btnAddQualityAttribut.setBounds(10, 252, 71, 23);
		panelQualityAttributs.add(btnAddQualityAttribut);

		JButton btnRemoveQualityAttribut = new JButton("Eliminar");
		btnRemoveQualityAttribut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				SaveDiagramDialog dialog = SaveDiagramDialog.this;
				
				dialog.setEvaluatedQualityAttrs();

				if (!dialog.removeSelectedQualityAttributs()) {
					JOptionPane.showMessageDialog(dialog, "Para eliminar debe seleccionar al menos un Atributo de Calidad.",
							"Atenci\u00F3n", JOptionPane.WARNING_MESSAGE);
					return;
				}
				
			}
		});
		btnRemoveQualityAttribut.setBounds(91, 252, 71, 23);
		panelQualityAttributs.add(btnRemoveQualityAttribut);

		// -----panelName
		JPanel panelName = new JPanel();
		panelName.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelName.setBounds(10, 11, 424, 34);
		contentPanel.add(panelName);
		panelName.setLayout(null);

		JLabel lblDiagramName = new JLabel("Nombre del diagrama");
		lblDiagramName.setBounds(10, 11, 101, 14);
		panelName.add(lblDiagramName);

		textFieldDiagramName = new JTextField();
		textFieldDiagramName.setBounds(121, 8, 293, 20);
		panelName.add(textFieldDiagramName);
		textFieldDiagramName.setColumns(10);

		// -----buttonPane
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			buttonPane.setBounds(244, 296, 190, 45);
			contentPanel.add(buttonPane);
			{
				JButton okButton = new JButton("Aceptar");
				okButton.setBounds(10, 11, 71, 23);
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						SaveDiagramDialog dialog = SaveDiagramDialog.this;

						dialog.setEvaluatedQualityAttrs();

						if (dialog.getQualityAttributes().isEmpty()) {
							JOptionPane.showMessageDialog(dialog, "Debe agregar al menos un Atributo de Calidad.", "Atenci\u00F3n",
									JOptionPane.WARNING_MESSAGE);
							return;
						}

						if (dialog.getNameDiagram().isEmpty()) {
							JOptionPane.showMessageDialog(dialog, "Debe agregar un nombre al diagrama para guardarlo.", "Atenci\u00F3n",
									JOptionPane.WARNING_MESSAGE);
							return;
						}

						if (dialog.getDescription().isEmpty()) {
							int option = JOptionPane.showConfirmDialog(dialog,
									"No agreg\u00F3 descripci\u00F3n.\n\u00BFDesea continuar y guardar el diagrama sin descripci\u00F3n?",
									"Atenci\u00F3n", JOptionPane.YES_NO_OPTION);
							if (option == 1)
								return;
						}

						dialog.setAccepted(true);
						dialog.closeDialog();
					}
				});
				buttonPane.setLayout(null);
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancelar");
				cancelButton.setBounds(105, 11, 75, 23);
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						closeDialog();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}

	}

	public void closeDialog() {
		this.dispose();
	}

	public boolean containsQualityAttribut(String qualityAttribut) {
		for (int i = 0; i < table.getModel().getRowCount(); i++) {
			if (qualityAttribut.endsWith((String) table.getModel().getValueAt(i, 0))) {
				return true;
			}
		}
		return false;
	}

	public void addQualityAttribut(String qualityAttribut, int appreciation) {
		Vector<Object> row = new Vector<Object>();
		row.add(qualityAttribut);
		row.add(appreciation);

		((DefaultTableModel) table.getModel()).addRow(row);
	}

	public boolean removeSelectedQualityAttributs() {
		DefaultTableModel model = (DefaultTableModel) this.table.getModel();
		int[] rows = this.table.getSelectedRows();
		if (rows.length == 0){
			return false;
		}
		for (int i = 0; i < rows.length; i++) {
			model.removeRow(rows[i] - i);
		}
		return true;
	}

	public String getQualityAttribut() {
		return comboBoxQualityAttributs.getSelectedItem().toString();
	}

	public int getAppreciation() {
		return (Integer) spinnerAppreciation.getValue();
	}

	// ---------------------------
	public Map<String, Integer> getQualityAttributes() {
		return evaluatedQualityAttributes;
	}

	private void setEvaluatedQualityAttrs() {
		evaluatedQualityAttributes.clear();
		for (int i = 0; i < table.getModel().getRowCount(); i++) {
			evaluatedQualityAttributes.put((String) table.getModel().getValueAt(i, 0), (Integer) table.getModel().getValueAt(i, 1));
		}
	}

	public String getDescription() {
		return textAreaDescription.getText();
	}

	public String getNameDiagram() {
		return this.textFieldDiagramName.getText();
	}

	public void setNameDiagram(String nameDiagram) {
		this.textFieldDiagramName.setText(nameDiagram);
	}

	public boolean isAccepted() {
		return isAccepted;
	}

	private void setAccepted(boolean isAccepted) {
		this.isAccepted = isAccepted;
	}
}
