package tesis.assistant.plugin.ui;

import java.util.TreeMap;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;

import tesis.assistant.plugin.model.DesignModel;

public class ResultTable extends TableViewer {

	public ResultTable(Composite parent) {
		super(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.BORDER);
		createColumns(parent, this);
		final Table table = this.getTable();
		table.setHeaderVisible(true);
		table.setLinesVisible(true);

		this.setContentProvider(new ArrayContentProvider());
	}

	private void createColumns(final Composite parent, final TableViewer viewer) {
		String[] titles = { "Nombre de dise\u00F1o", "Nivel de \u00E9xito", "Descripci\u00F3n", "Atributos" };
		int[] bounds = { 150, 100, 500, 200 };

		TableViewerColumn col = createTableViewerColumn(titles[0], bounds[0], 0);
		col.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				DesignModel designModel = (DesignModel) element;
				return designModel.getName();
			}
		});

		col = createTableViewerColumn(titles[1], bounds[1], 1);
		col.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				DesignModel designModel = (DesignModel) element;
				return Double.toString(designModel.getSuccess());
			}
		});

		col = createTableViewerColumn(titles[2], bounds[2], 2);
		col.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				DesignModel designModel = (DesignModel) element;
				return designModel.getDescription();
			}
		});

		col = createTableViewerColumn(titles[3], bounds[3], 3);
		col.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				DesignModel designModel = (DesignModel) element;
				String text = new TreeMap<String, Integer>(designModel.getQualityAttributes()).toString();
				return text.substring(1, text.length() - 1);
			}
		});
	}

	private TableViewerColumn createTableViewerColumn(String title, int bound, final int colNumber) {
		final TableViewerColumn viewerColumn = new TableViewerColumn(this, SWT.NONE);
		final TableColumn column = viewerColumn.getColumn();
		column.setText(title);
		column.setWidth(bound);
		column.setResizable(true);
		column.setMoveable(true);
		return viewerColumn;
	}

	public void setFocus() {
		this.getControl().setFocus();
	}
}