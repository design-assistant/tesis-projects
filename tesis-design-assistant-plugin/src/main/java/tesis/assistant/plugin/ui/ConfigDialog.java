package tesis.assistant.plugin.ui;

import java.awt.BorderLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

public class ConfigDialog extends JDialog {

	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JTextField textFieldServerURL;

	private boolean accepted = false;

	/**
	 * Create the dialog.
	 */
	public ConfigDialog() {

		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

		setTitle("Configuraci\u00F3n");
		setModal(true);
		setAlwaysOnTop(true);
		setResizable(false);

		setSize(550, 180);
		setLocationRelativeTo(null);

	    Image img = new ImageIcon(ConfigDialog.class.getResource("/icons/config.png")).getImage();
	    setIconImage(img);

		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);

		// -----panelOptions
		JPanel panelOptions = new JPanel();
		panelOptions.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelOptions.setBounds(10, 11, 524, 34);
		contentPanel.add(panelOptions);
		panelOptions.setLayout(null);

		JLabel lblServerURL = new JLabel("URL del servidor");
		lblServerURL.setBounds(10, 11, 101, 14);
		panelOptions.add(lblServerURL);

		textFieldServerURL = new JTextField();
		textFieldServerURL.setBounds(121, 8, 393, 20);
		panelOptions.add(textFieldServerURL);
		textFieldServerURL.setColumns(10);

		// -----buttonPane
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			buttonPane.setBounds(344, 96, 190, 45);
			contentPanel.add(buttonPane);
			{
				JButton okButton = new JButton("Aceptar");
				okButton.setBounds(10, 11, 71, 23);
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						ConfigDialog dialog = ConfigDialog.this;
						dialog.setAccepted(true);
						dialog.closeDialog();
					}
				});
				buttonPane.setLayout(null);
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancelar");
				cancelButton.setBounds(105, 11, 75, 23);
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						closeDialog();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}

	}

	public void closeDialog() {
		this.dispose();
	}

	public String getServerURL() {
		return this.textFieldServerURL.getText();
	}

	public void setServerURL(String serverURL) {
		this.textFieldServerURL.setText(serverURL);
	}

	public boolean isAccepted() {
		return accepted;
	}

	private void setAccepted(boolean accepted) {
		this.accepted = accepted;
	}

}
