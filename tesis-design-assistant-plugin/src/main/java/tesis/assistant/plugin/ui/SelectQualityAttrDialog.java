package tesis.assistant.plugin.ui;

import java.awt.BorderLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;

public class SelectQualityAttrDialog extends JDialog {

	private static final long serialVersionUID = 1L;

	private static final int ALL = 0;
	private static final int ANY = 1;

	private List<String> selectedQualityAttrs;
	private int matchingType;

	private JRadioButton rdbtnAny;
	private JTable table;

	private boolean isAccepted;

	/**
	 * Create the dialog.
	 */
	public SelectQualityAttrDialog(List<String> qualityAttrs) {

		JPanel contentPanel = new JPanel();

		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

		setTitle("Selecci\u00F3n de Atributos de Calidad para asistencia");
		setModal(true);
		setAlwaysOnTop(true);
		setResizable(false);

		setSize(400, 290);
		setLocationRelativeTo(null);

	    Image img = new ImageIcon(SelectQualityAttrDialog.class.getResource("/icons/assistance.png")).getImage();
	    setIconImage(img);

		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);

		selectedQualityAttrs = new ArrayList<String>();
		matchingType = ANY;

		JPanel panelMatchingType = new JPanel();
		panelMatchingType.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelMatchingType.setBounds(211, 11, 173, 104);
		contentPanel.add(panelMatchingType);
		panelMatchingType.setLayout(null);

		JLabel lblMatchingType = new JLabel("Criterio de selecci\u00F3n");
		lblMatchingType.setBounds(10, 11, 98, 14);
		panelMatchingType.add(lblMatchingType);

		// ---------------RadioButtons-----------------
		rdbtnAny = new JRadioButton("Cualquiera");
		rdbtnAny.setSelected(matchingType == ANY);
		rdbtnAny.setBounds(10, 45, 109, 23);
		panelMatchingType.add(rdbtnAny);

		JRadioButton rdbtnAll = new JRadioButton("Todos");
		rdbtnAll.setSelected(matchingType == ALL);
		rdbtnAll.setBounds(10, 71, 109, 23);
		panelMatchingType.add(rdbtnAll);

		ButtonGroup buttonGroup = new ButtonGroup();
		buttonGroup.add(rdbtnAll);
		buttonGroup.add(rdbtnAny);

		// ----------------------------
		JSeparator separator = new JSeparator();
		separator.setBounds(0, 36, 173, 2);
		panelMatchingType.add(separator);

		JPanel panelQualityAttributs = new JPanel();
		panelQualityAttributs.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelQualityAttributs.setBounds(10, 11, 183, 242);
		contentPanel.add(panelQualityAttributs);
		panelQualityAttributs.setLayout(null);

		Object[][] tableData = new Object[qualityAttrs.size()][2];
		for (int i = 0; i < qualityAttrs.size(); i++) {
			tableData[i][0] = Boolean.FALSE;
			tableData[i][1] = qualityAttrs.get(i);
		}

		table = new JTable(new DefaultTableModel(tableData, new String[] { "", "" }) {
			private static final long serialVersionUID = 1L;

			@Override
			public Class<?> getColumnClass(int columnIndex) {
				if (columnIndex == 0) {
					return Boolean.class;
				}
				return super.getColumnClass(columnIndex);
			}

			@Override
			public boolean isCellEditable(int row, int column) {
				return column == 0;
			}
		});

		table.setTableHeader(null);
		table.setShowGrid(false);

		table.getColumnModel().getColumn(0).setWidth(15);
		table.getColumnModel().getColumn(0).setPreferredWidth(15);
		JScrollPane scrollpane = new JScrollPane(table);
		scrollpane.setBounds(10, 49, 163, 182);
		panelQualityAttributs.add(scrollpane);

		JLabel lblQualityAttributes = new JLabel("Atributos de calidad");
		lblQualityAttributes.setBounds(10, 11, 95, 14);
		panelQualityAttributs.add(lblQualityAttributes);

		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(0, 36, 241, 2);
		panelQualityAttributs.add(separator_1);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			buttonPane.setBounds(211, 208, 173, 45);
			contentPanel.add(buttonPane);
			{
				JButton okButton = new JButton("Aceptar");
				okButton.setBounds(10, 11, 71, 23);
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						SelectQualityAttrDialog dialog = SelectQualityAttrDialog.this;
						dialog.setMatchingType();
						dialog.setSelectedQualityAttrs();

						if (dialog.getSelectedQualityAttrs().isEmpty()) {
							JOptionPane.showMessageDialog(dialog, "Debe seleccionar al menos un Atributo de Calidad.", "Atenci\u00F3n",
									JOptionPane.WARNING_MESSAGE);
							return;
						}

						dialog.setAccepted(true);
						dialog.closeDialog();
					}
				});
				buttonPane.setLayout(null);
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancelar");
				cancelButton.setBounds(88, 11, 75, 23);
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						SelectQualityAttrDialog dialog = SelectQualityAttrDialog.this;
						dialog.setAccepted(false);
						dialog.closeDialog();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}

	public List<String> getSelectedQualityAttrs() {
		return selectedQualityAttrs;
	}

	private void setSelectedQualityAttrs() {
		selectedQualityAttrs.clear();
		for (int i = 0; i < table.getModel().getRowCount(); i++) {
			if ((Boolean) table.getModel().getValueAt(i, 0)) {
				selectedQualityAttrs.add((String) table.getModel().getValueAt(i, 1));
			}
		}
	}

	public int getMatchingType() {
		return matchingType;
	}

	private void setMatchingType() {
		this.matchingType = rdbtnAny.isSelected() ? ANY : ALL;
	}

	public boolean isAccepted() {
		return isAccepted;
	}

	private void setAccepted(boolean isAccepted) {
		this.isAccepted = isAccepted;
	}

	public void closeDialog() {
		this.dispose();
	}
}
