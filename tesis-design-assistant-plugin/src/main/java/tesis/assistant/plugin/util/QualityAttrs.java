package tesis.assistant.plugin.util;

import java.util.ArrayList;
import java.util.List;

public class QualityAttrs {

	public static final List<String> LIST = new ArrayList<String>() {

		private static final long serialVersionUID = 1L;

		{
			add("Adaptabilidad");
			add("Escalabilidad");
			add("Flexibilidad");
			add("Mantenibilidad");
			add("Modificabilidad");
			add("Portabilidad");
			add("Testeabilidad");
			add("Usabilidad");
		}
	};

}
