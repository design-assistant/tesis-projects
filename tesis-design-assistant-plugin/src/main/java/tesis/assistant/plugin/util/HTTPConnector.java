package tesis.assistant.plugin.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Map;
import java.util.Map.Entry;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import java.util.zip.Inflater;
import java.util.zip.InflaterInputStream;

import javax.xml.bind.DatatypeConverter;

/**
 * @author Ciri
 * @version 1.1
 */
public class HTTPConnector {

	private static final int IO_BUFFER_SIZE = 8 * 1024; // 8 KiB
	private static final int GZIP_BUFFER_SIZE = 4 * 1024; // 4 KiB
	private static final int MIN_GZIP_SIZE = 256; // bytes

	// most popular content types
	public static final String CONTENT_TYPE_ANY = "*/*";
	public static final String CONTENT_TYPE_BINARY = "application/octet-stream";
	public static final String CONTENT_TYPE_HTML = "text/html";
	public static final String CONTENT_TYPE_JSON = "application/json";
	public static final String CONTENT_TYPE_TEXT = "text/plain";
	public static final String CONTENT_TYPE_URLENC = "application/x-www-form-urlencoded";
	public static final String CONTENT_TYPE_XML = "application/xml";

	// most popular encodings
	public static final String CHARSET_ASCII = "US-ASCII";
	public static final String CHARSET_CHINESE_SIMPLIFIED = "GB2312";
	public static final String CHARSET_CHINESE_TRADITIONAL = "Big5";
	public static final String CHARSET_JAPANESE = "Shift_JIS";
	public static final String CHARSET_LATIN_1 = "ISO-8859-1";
	public static final String CHARSET_LATIN_2 = "ISO-8859-2";
	public static final String CHARSET_UNICODE = "UTF-8";
	public static final String CHARSET_WINDOWS = "windows-1252";
	public static final String CHARSET_WINDOWS_CYRILLIC = "windows-1251";

	// HTTP properties
	public static final int PROP_COMPRESS_REQUEST_CONTENT = 1 << 0;
	public static final int PROP_ENABLE_CACHING = 1 << 1;
	public static final int PROP_DISABLE_KEEP_ALIVE = 1 << 2;

	// request method enum
	private enum RequestMethod {
		GET, POST, PUT, DELETE;
	}

	private HTTPConnector() {
	}

	/*
	 * --------------------- HTTP REQUEST CALLS ---------------------
	 */

	/**
	 * 
	 * @param URL
	 * @param timeout
	 * @param charset
	 * @return
	 * @throws MalformedURLException
	 */
	public static HTTPRequest get(String URL, int timeout, String charset) throws MalformedURLException {
		return new HTTPRequest(RequestMethod.GET, URL, timeout, charset);
	}

	/**
	 * 
	 * @param URL
	 * @param timeout
	 * @param charset
	 * @return
	 * @throws MalformedURLException
	 */
	public static HTTPOutputRequest post(String URL, int timeout, String charset) throws MalformedURLException {
		return new HTTPOutputRequest(RequestMethod.POST, URL, timeout, charset);
	}

	/**
	 * 
	 * @param URL
	 * @param timeout
	 * @param charset
	 * @return
	 * @throws MalformedURLException
	 */
	public static HTTPOutputRequest put(String URL, int timeout, String charset) throws MalformedURLException {
		return new HTTPOutputRequest(RequestMethod.PUT, URL, timeout, charset);
	}

	/**
	 * 
	 * @param URL
	 * @param timeout
	 * @param charset
	 * @return
	 * @throws MalformedURLException
	 */
	public static HTTPRequest delete(String URL, int timeout, String charset) throws MalformedURLException {
		return new HTTPRequest(RequestMethod.DELETE, URL, timeout, charset);
	}

	/*
	 * --------------------- HTTP REQUEST CLASSES ---------------------
	 */

	public static class HTTPRequest {

		protected RequestMethod requestMethod;

		protected URL url;
		protected int timeout;
		protected String charset;

		protected String authorization;
		protected Proxy proxy;
		protected String proxyAuth;
		protected int props;

		protected HttpURLConnection urlConn;
		protected ByteArrayOutputStream response;

		/**
		 * 
		 * @param requestMethod
		 * @param url
		 * @param timeout
		 * @param charset
		 * @throws MalformedURLException
		 */
		protected HTTPRequest(RequestMethod requestMethod, String url, int timeout, String charset) throws MalformedURLException {
			this.requestMethod = requestMethod;
			this.url = new URL(url);
			this.timeout = timeout;
			this.charset = charset;
		}

		/**
		 * 
		 * @param user
		 * @param pass
		 * @return
		 */
		public HTTPRequest setBasicAuth(String user, String pass) {
			this.authorization = "Basic " + DatatypeConverter.printBase64Binary((user + ":" + pass).getBytes());
			return this;
		}

		/**
		 * 
		 * @param proxyHost
		 * @param proxyPort
		 * @return
		 */
		public HTTPRequest setProxy(String proxyHost, int proxyPort) {
			this.proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(proxyHost, proxyPort));
			return this;
		}

		/**
		 * 
		 * @param user
		 * @param pass
		 * @return
		 */
		public HTTPRequest setProxyBasicAuth(String user, String pass) {
			this.proxyAuth = "Basic " + DatatypeConverter.printBase64Binary((user + ":" + pass).getBytes());
			return this;
		}

		/**
		 * 
		 * @param props
		 * @return
		 */
		public HTTPRequest setProps(int props) {
			this.props = props;
			return this;
		}

		/**
		 * 
		 * @throws IOException
		 */
		private void execute() throws IOException {

			if (proxy == null) {
				urlConn = (HttpURLConnection) url.openConnection();
			} else {
				urlConn = (HttpURLConnection) url.openConnection(proxy);
			}

			// set request method
			urlConn.setRequestMethod(requestMethod.name());

			// prepare connection properties
			urlConn.setConnectTimeout(timeout);
			urlConn.setReadTimeout(timeout);

			urlConn.setRequestProperty("User-Agent",
					"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36");
			urlConn.setRequestProperty("Accept", "*/*");
			urlConn.setRequestProperty("Accept-Encoding", "gzip, deflate");

			// set the response charset
			if (charset != null) {
				urlConn.setRequestProperty("Accept-Charset", charset);
			}

			// set authentication info
			if (authorization != null) {
				urlConn.setRequestProperty("Authorization", authorization);
			}
			if (proxyAuth != null) {
				urlConn.setRequestProperty("Proxy-Authorization", proxyAuth);
			}

			// check active flags
			if ((props & PROP_ENABLE_CACHING) == 0) {
				urlConn.setUseCaches(false);
				urlConn.setRequestProperty("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
			}

			if ((props & PROP_DISABLE_KEEP_ALIVE) != 0) {
				urlConn.setRequestProperty("Connection", "close");
			} else {
				urlConn.setRequestProperty("Connection", "keep-alive");
			}

			// send request data
			sendData();

			// receive the response
			receiveResponse();

			if (urlConn.getResponseCode() >= 400) {
				throw new HTTPConnectorException(urlConn, response, charset);
			}
		}

		/**
		 * 
		 * @return
		 * @throws HTTPConnectorException
		 * @throws IOException
		 */
		public byte[] executeByteArray() throws HTTPConnectorException, IOException {
			execute();
			return response.toByteArray();
		}

		/**
		 * 
		 * @return
		 * @throws HTTPConnectorException
		 * @throws IOException
		 */
		public String executeString() throws HTTPConnectorException, IOException {
			execute();
			return (charset != null) ? response.toString(charset) : response.toString();
		}

		/**
		 * 
		 * @throws IOException
		 */
		protected void sendData() throws IOException {
			// do nothing
		}

		/**
		 * 
		 * @throws IOException
		 */
		protected void receiveResponse() throws IOException {
			InputStream is = null;
			try {

				if (urlConn.getResponseCode() >= 400) {
					is = urlConn.getErrorStream();
				} else {
					is = urlConn.getInputStream();
				}

				if ("gzip".equalsIgnoreCase(urlConn.getContentEncoding())) {
					is = new GZIPInputStream(is, GZIP_BUFFER_SIZE);
				} else if ("deflate".equalsIgnoreCase(urlConn.getContentEncoding())) {
					is = new InflaterInputStream(is, new Inflater(true), GZIP_BUFFER_SIZE);
				}

				is = new BufferedInputStream(is, IO_BUFFER_SIZE);

				response = new ByteArrayOutputStream(IO_BUFFER_SIZE);
				copy(is, response, IO_BUFFER_SIZE);

			} finally {
				close(is);
			}
		}

	}

	public static class HTTPOutputRequest extends HTTPRequest {

		protected byte[] data;
		protected String mediaType;
		protected String mediaCharset;

		/**
		 * 
		 * @param requestMethod
		 * @param url
		 * @param timeout
		 * @param charset
		 * @throws MalformedURLException
		 */
		protected HTTPOutputRequest(RequestMethod requestMethod, String url, int timeout, String charset) throws MalformedURLException {
			super(requestMethod, url, timeout, charset);
		}

		/**
		 * 
		 * @param data
		 * @param mediaType
		 * @param mediaCharset
		 * @return
		 */
		public HTTPOutputRequest setOutput(byte[] data, String mediaType, String mediaCharset) {
			this.data = data;
			this.mediaType = mediaType;
			this.mediaCharset = mediaCharset;
			return this;
		}

		/**
		 * 
		 * @param data
		 * @param mediaType
		 * @param mediaCharset
		 * @return
		 * @throws UnsupportedEncodingException
		 */
		public HTTPOutputRequest setOutput(String data, String mediaType, String mediaCharset) throws UnsupportedEncodingException {
			return setOutput((mediaCharset != null) ? data.getBytes(mediaCharset) : data.getBytes(), mediaType, mediaCharset);
		}

		@Override
		protected void sendData() throws IOException {

			if (mediaType != null && mediaCharset != null) {
				urlConn.setRequestProperty("Content-Type", mediaType + "; charset=" + mediaCharset);
			} else if (mediaType != null) {
				urlConn.setRequestProperty("Content-Type", mediaType);
			} else if (mediaCharset != null) {
				urlConn.setRequestProperty("Content-Type", "charset=" + mediaCharset);
			}

			urlConn.setDoOutput(true);

			if (data == null) {
				return; // nothing to send
			}

			// only if data length is higher than the minimum required
			boolean compressData = (props & PROP_COMPRESS_REQUEST_CONTENT) != 0 && data.length > MIN_GZIP_SIZE;

			if (compressData) {
				// set request property
				urlConn.setRequestProperty("Content-Encoding", "gzip");
			}

			// send the request
			OutputStream os = null;
			try {

				os = urlConn.getOutputStream();

				if (compressData) {
					os = new GZIPOutputStream(os, GZIP_BUFFER_SIZE);
				}

				os = new BufferedOutputStream(os, IO_BUFFER_SIZE);

				copy(new ByteArrayInputStream(data), os, IO_BUFFER_SIZE);

			} finally {
				close(os);
			}
		}

	}

	/*
	 * --------------------- UTIL METHODS ---------------------
	 */

	/**
	 * 
	 * @param c
	 */
	private static void close(Closeable c) {
		if (c == null)
			return;
		try {
			c.close();
		} catch (IOException e) {
		}
	}

	/**
	 * 
	 * @param input
	 * @param output
	 * @param bufferSize
	 * @return
	 * @throws IOException
	 */
	private static int copy(InputStream input, OutputStream output, int bufferSize) throws IOException {
		byte[] buffer = new byte[bufferSize];
		int n = 0, length = 0;
		while (-1 != (n = input.read(buffer))) {
			output.write(buffer, 0, n);
			length += n;
		}
		return length;
	}

	/*
	 * --------------------- EXCEPTIONS ---------------------
	 */

	public static class HTTPConnectorException extends IOException {

		private static final long serialVersionUID = 1L;

		protected int responseCode;
		protected String responseMessage;
		protected ByteArrayOutputStream response;
		protected String charset;

		/**
		 * 
		 * @param urlConn
		 */
		public HTTPConnectorException(HttpURLConnection urlConn, ByteArrayOutputStream response, String charset) {
			super(urlConn.getHeaderField(0));

			try {
				this.responseCode = urlConn.getResponseCode();
				this.responseMessage = urlConn.getResponseMessage();
			} catch (IOException e) {
				this.responseCode = -1;
				this.responseMessage = null;
			}

			this.response = response;
			this.charset = charset;
		}

		/**
		 * 
		 * @return
		 */
		public int getResponseCode() {
			return responseCode;
		}

		/**
		 * 
		 * @return
		 */
		public String getResponseMessage() {
			return responseMessage;
		}

		/**
		 * 
		 * @return
		 */
		public byte[] getResponseBodyByteArray() {
			return response.toByteArray();

		}

		/**
		 * 
		 * @return
		 * @throws UnsupportedEncodingException
		 */
		public String getResponseBodyString() throws UnsupportedEncodingException {
			return (charset != null) ? response.toString(charset) : response.toString();
		}

	}

	/*
	 * --------------------- PARAMETERS ENCODING ---------------------
	 */

	/**
	 * 
	 * @param params
	 * @param encoding
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static final String encodeParams(Map<String, String> params, String encoding) throws UnsupportedEncodingException {
		StringBuilder encodedParams = new StringBuilder(2048);

		for (Entry<String, String> param : params.entrySet()) {

			if (encodedParams.length() > 0) {
				encodedParams.append('&');
			}

			encodedParams.append(URLEncoder.encode(param.getKey(), encoding));
			encodedParams.append('=');
			encodedParams.append(URLEncoder.encode(param.getValue(), encoding));
		}

		return encodedParams.toString();
	}

}
