package tesis.assistant.plugin.util;

import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.InstanceScope;
import org.osgi.service.prefs.BackingStoreException;

import tesis.assistant.plugin.activator.Activator;

public class PluginPreferencies {

	private static final class PluginPreferenciesHolder {
		private static final PluginPreferencies INSTANCE = new PluginPreferencies();
	}

	public static PluginPreferencies getInstance() {
		return PluginPreferenciesHolder.INSTANCE;
	}

	private PluginPreferencies() {
	}

	private String serverURL;

	public String getServerURL() {
		return serverURL;
	}

	public void setServerURL(String serverURL) {
		this.serverURL = serverURL;
	}

	public void savePluginSettings() {
		// saves plugin preferences at the workspace level
		IEclipsePreferences prefs = InstanceScope.INSTANCE.getNode(Activator.PLUGIN_ID);

		prefs.put("serverURL", this.serverURL);

		try {
			// prefs are automatically flushed during a plugin's "super.stop()".
			prefs.flush();
		} catch (BackingStoreException e) {
			e.printStackTrace();
		}
	}

	public void loadPluginSettings() {
		IEclipsePreferences prefs = InstanceScope.INSTANCE.getNode(Activator.PLUGIN_ID);
		// you might want to call prefs.sync() if you're worried about others changing your settings

		this.serverURL = prefs.get("serverURL", "http://");
	}

}
