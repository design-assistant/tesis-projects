package tesis.assistant.plugin.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

public class WindowContent {

	public static String getSelectedDiagram() {

		StringBuffer buffer = new StringBuffer();

		IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		if (window != null) {
			IWorkbenchPage page = window.getActivePage();
			if (page != null) {
				try {
					IFile file = (IFile) page.getActiveEditor().getEditorInput().getAdapter(IFile.class);

					InputStream input = file.getContents();

					BufferedReader reader = new BufferedReader(new InputStreamReader(input));
					String line;
					while ((line = reader.readLine()) != null) {
						buffer.append(line);
						buffer.append('\n');
					}
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (CoreException e) {
					e.printStackTrace();
				}
			}
		}
		return buffer.toString();
	}

	public static String getActiveFileName() {
		IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		if (window != null) {
			IWorkbenchPage page = window.getActivePage();
			if (page != null) {
				IEditorPart editorPart = page.getActiveEditor();
				if (editorPart != null) {
					IFile file = (IFile) editorPart.getEditorInput().getAdapter(IFile.class);
					return file.getName();
				}
			}
		}
		return null;
	}

	public static boolean isOpenClassDiagram() {
		String name = getActiveFileName();
		if (name != null) {
			return name.length() > 4 && name.substring(name.length() - 4).equalsIgnoreCase(".cld");
		}
		return false;
	}

	public static String getDiagramName() {
		if (isOpenClassDiagram()) {
			String name = getActiveFileName();
			return name.substring(0, name.lastIndexOf("."));
		}
		return null;
	}

	public static IProject getActiveProject() {
		IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		if (window != null) {
			IWorkbenchPage page = window.getActivePage();
			if (page != null) {
				IEditorPart editorPart = page.getActiveEditor();
				if (editorPart != null) {
					IFileEditorInput editorInput = (IFileEditorInput) editorPart.getEditorInput();
					return editorInput.getFile().getProject();
				}
			}
		}
		return null;
	}

}
