package tesis.assistant.plugin.model;

import java.util.Map;

public class DesignModel {

	private String name;
	private String design;
	private String description;
	private Map<String, Integer> qualityAttributes;
	private double success;

	public DesignModel(String name, String design, String description, Map<String, Integer> qualityAttributes, double success) {
		this.name = name;
		this.design = design;
		this.description = description;
		this.qualityAttributes = qualityAttributes;
		this.success = success;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDesign() {
		return design;
	}

	public void setDesign(String design) {
		this.design = design;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Map<String, Integer> getQualityAttributes() {
		return qualityAttributes;
	}

	public void setQualityAttributes(Map<String, Integer> qualityAttributes) {
		this.qualityAttributes = qualityAttributes;
	}

	public double getSuccess() {
		return success;
	}

	public void setSuccess(double success) {
		this.success = success;
	}

}

