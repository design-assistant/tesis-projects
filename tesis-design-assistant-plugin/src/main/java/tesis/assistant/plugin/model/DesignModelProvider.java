package tesis.assistant.plugin.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public enum DesignModelProvider {
	INSTANCE;

	private List<DesignModel> designModels;

	private DesignModelProvider() {
		designModels = new ArrayList<DesignModel>();
	}

	public List<DesignModel> getDesignModels() {
		return designModels;
	}

	public boolean addDesignModel(String name, String design, String description, Map<String, Integer> qualityAttributes, double success) {
		return designModels.add(new DesignModel(name, design, description, qualityAttributes, success));
	}

	public boolean addDesignModel(DesignModel designModel) {
		return designModels.add(designModel);
	}

}