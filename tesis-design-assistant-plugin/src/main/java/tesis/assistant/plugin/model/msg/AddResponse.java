package tesis.assistant.plugin.model.msg;

public class AddResponse {

	private String errorMessage;
	private MsgDiagram addResult;

	public AddResponse() {
		this(null);
	}

	public AddResponse(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public MsgDiagram getAddResult() {
		return addResult;
	}

	public void setAddResult(MsgDiagram addResult) {
		this.addResult = addResult;
	}

}
