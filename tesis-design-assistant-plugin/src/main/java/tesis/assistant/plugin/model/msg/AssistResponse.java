package tesis.assistant.plugin.model.msg;

import java.util.ArrayList;
import java.util.List;

public class AssistResponse {

	private String errorMessage;
	private List<MsgValuedDiagram> assistResult;

	public AssistResponse() {
		this(null);
	}

	public AssistResponse(String errorMessage) {
		this.errorMessage = errorMessage;
		this.assistResult = new ArrayList<MsgValuedDiagram>();
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public List<MsgValuedDiagram> getAssistResult() {
		return assistResult;
	}

	public void setAssistResult(List<MsgValuedDiagram> assistResult) {
		this.assistResult = assistResult;
	}

	public void addAssistResponseItem(MsgValuedDiagram assistResponseItem) {
		this.assistResult.add(assistResponseItem);
	}

}
